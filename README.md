## Instructions

You will also need to clone `tumi-unity-arfoundation` submodule by cloning this repo recursively:
```
git clone --recursive https://gitlab.com/gary.lee/tumi-ios
```

Both `tumi-ios`	and `tumi-unity-arfoundation` are required to be in the same folder for buid script to work.
