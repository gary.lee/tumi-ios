//
//  NotificationListVC.swift
//  Tumi
//
//  Created by Bhavin Joshi on 22/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class NotificationListVC: UIViewController {
    @IBOutlet var lbltitle: UILabel!
    var arrNotificationlist = NSMutableArray()
    @IBOutlet var tblNotification: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CallAPI()
        self.lbltitle.text = "NOTIFICATION".localized()
        
        self.tblNotification.register(UINib(nibName: "NotificationListCell", bundle: nil), forCellReuseIdentifier: "NotificationListCell")
        // Do any additional setup after loading the view.
    }
    
    
    func CallAPI()  {
        
        let parameters = ["lng" : UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:push_notification_list, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    self.arrNotificationlist.addObjects(from: dict.value(forKey: "data") as! [Any])
                    self.tblNotification.dataSource = self
                    self.tblNotification.delegate = self
                    self.tblNotification.reloadData()
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
}

extension NotificationListVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotificationlist.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCell", for: indexPath) as? NotificationListCell
        let dict = self.arrNotificationlist.object(at: indexPath.row) as! NSDictionary
        cell?.lblcontent.text = dict.value(forKey: "description") as? String
        
        cell?.selectionStyle = .none
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //UITableViewAutomaticDimension
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrNotificationlist.object(at: indexPath.row) as! NSDictionary
        let Notificationdetail = storyBoard.instantiateViewController(withIdentifier: "NotificationDetailVC") as? NotificationDetailVC
        Notificationdetail?.Dict = dict
        self.navigationController?.pushViewController(Notificationdetail!, animated: true)
        
    }
}
