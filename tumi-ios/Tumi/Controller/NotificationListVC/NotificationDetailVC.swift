//
//  NotificationDetailVC.swift
//  Tumi
//
//  Created by Bhavin Joshi on 22/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class NotificationDetailVC: UIViewController {
    @IBOutlet var imgheight: NSLayoutConstraint!
    
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet var imgNotify: UIImageView!
    @IBOutlet var imgHeight: NSLayoutConstraint!
    @IBOutlet var lbltitle: UILabel!
    var Dict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbltitle.text = "NOTIFICATION".localized()
        
        let image = self.Dict.value(forKey: "image_path") as? String
        if image == ""{
            self.imgHeight.constant = 0
            self.imgNotify.isHidden = true
            
        }
        else{
            self.imgHeight.constant = 180
            self.imgNotify.isHidden = false
            var urlstring = self.Dict.value(forKey:"image_path") as? String
            urlstring = urlstring?.replacingOccurrences(of: " ", with: "%20")
            
            DispatchQueue.main.async {
                self.imgNotify.sd_setImage(with: URL(string:urlstring!.trim), placeholderImage: UIImage(named: ""))
            }
        }
        self.lblcontent.text = self.Dict.value(forKey: "description") as? String
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
