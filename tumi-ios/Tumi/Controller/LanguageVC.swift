//
//  LanguageVC.swift
//  Tumi
//
//  Created by Redspark on 03/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class LanguageVC: UIViewController {
    @IBOutlet weak var vwLnaguage:UIView!
    @IBOutlet weak var imgCheck:UIImageView!
    @IBOutlet weak var imgCheck1:UIImageView!
    @IBOutlet weak var imgCheck2:UIImageView!
    var strLang:String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        
        Bundle.setLanguage(lang: "en")
        // Do any additional setup after loading the view.
    }
    func setUpUI(){
        self.vwLnaguage.layer.cornerRadius = 10.0
        self.vwLnaguage.clipsToBounds = true
        self.strLang = "en"
        UserDataHolder.sharedUser.language = self.strLang
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionLanguage(_ sender:UIButton){
        
        if(sender.tag == 1){
            self.imgCheck.image = UIImage(named: "checked")
            self.imgCheck1.image = UIImage(named: "unchecked")
            self.imgCheck2.image = UIImage(named: "unchecked")
            self.strLang = "en"
            
            Bundle.setLanguage(lang: "en")
        }else if(sender.tag == 2){
            self.imgCheck.image = UIImage(named: "unchecked")
            self.imgCheck1.image = UIImage(named: "checked")
            self.imgCheck2.image = UIImage(named: "unchecked")
            self.strLang = "chinese"
            
            Bundle.setLanguage(lang: "zh-Hans")
        }else if(sender.tag == 3){
            self.imgCheck.image = UIImage(named: "unchecked")
            self.imgCheck1.image = UIImage(named: "unchecked")
            self.imgCheck2.image = UIImage(named: "checked")
              self.strLang = "traditional chinese"
            
            Bundle.setLanguage(lang: "zh-Hant")
        }
        UserDataHolder.sharedUser.language = self.strLang
    }
    
    @IBAction func actionNext(_ sender:UIButton){
        let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        self.navigationController?.pushViewController(objMemberShip!, animated: true)
    }
    
}

extension Bundle {
    private static var bundle: Bundle!
    
    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let appLang = UserDefaults.standard.string(forKey: "app_lang") ?? "ru"
            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        
        return bundle;
    }
    
    public static func setLanguage(lang: String) {
        UserDefaults.standard.set(lang, forKey: "app_lang")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }
    
    func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized(), arguments: arguments)
    }
}

