//
//  CategoryCartVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 09/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class CategoryCartVC: UIViewController {
    @IBOutlet weak var tblListCategory:UITableView!
    @IBOutlet weak var lblNorecords:UILabel!
    
    
    var arrayData = NSMutableArray()
    var strcate_id = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
        
        self.callAPI()
    }
    func setupUI(){
        self.lblNorecords.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.tblListCategory.register(UINib(nibName: "CategoryCartCell", bundle: nil), forCellReuseIdentifier: "CategoryCartCell")
        self.tblListCategory.delegate = self
        self.tblListCategory.dataSource = self
    }
    
    func callAPI()  {
        
        let parameters = ["lng":UserDataHolder.sharedUser.language, "cate_id":strcate_id] as [String : Any]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:PRODUCT_LIST, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async{
                    
                    self.arrayData.addObjects(from: dict.value(forKey: "data") as! [Any])
                    
                    self.tblListCategory.delegate = self
                    self.tblListCategory.dataSource = self
                    self.tblListCategory.reloadData()
                    
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func actionShare(sender: UIButton){
        
        let dict = arrayData.object(at: sender.tag) as! NSDictionary
        
        let share = dict.value(forKey: "eshop_link") as? String
        let shareAll = [share!]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func actionCart(sender: UIButton){
        
        let dict = arrayData.object(at: sender.tag) as! NSDictionary
        
        let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as? PrivacyPolicyVC
        objMemberShip?.isCart = true
        objMemberShip?.strUrl = (dict.value(forKey: "eshop_link") as? String)!
        self.present(objMemberShip!, animated: true, completion: nil)
    }
    
    
}
extension CategoryCartVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayData.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCartCell", for: indexPath) as? CategoryCartCell
        cell?.selectionStyle = .none
        
        
        let dict = arrayData.object(at: indexPath.row) as! NSDictionary
        
        cell?.lblProductName.text = dict.value(forKey: "title") as? String
        
        var url = dict.value(forKey: "image") as? String
        url = url!.replacingOccurrences(of:" ", with:"%20")
        
        DispatchQueue.main.async {
            cell?.imgCat.sd_setImage(with: URL(string:url!), placeholderImage: UIImage(named: ""))
        }
        
        cell?.btnCart.tag = indexPath.row
        cell?.btnShare.tag = indexPath.row
        
        cell?.btnCart.addTarget(self, action: #selector(actionCart), for: .touchUpInside)
        cell?.btnShare.addTarget(self, action: #selector(actionShare), for: .touchUpInside)
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350; //UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = arrayData.object(at: indexPath.row) as! NSDictionary
        
        var url = dict.value(forKey: "image") as? String
        url = url!.replacingOccurrences(of:" ", with:"%20")
        
        let objImageViewerVc = storyBoard.instantiateViewController(withIdentifier: "ImageViewer") as?
        ImageViewer
        objImageViewerVc?.imageURL = url!
        self.navigationController?.pushViewController(objImageViewerVc!, animated: true)
        
    }
}
