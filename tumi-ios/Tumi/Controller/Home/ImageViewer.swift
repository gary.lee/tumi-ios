//
//  ImageViewer.swift
//  Tumi
//
//  Created by Redspark on 05/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import ZoomImageView

class ImageViewer: UIViewController    {
    
    var imageURL : String = ""
    var imgView = UIImageView()
    var img = UIImage()
    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.imgViewver.zoomMode = .fill
        
        imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: nil, options: []) { (image, error, imageCacheType, imageUrl) in
            
            // Perform your operations here.
            
            self.imageScrollView.setup()
            self.imageScrollView.imageScrollViewDelegate = self
            self.imageScrollView.imageContentMode = .aspectFit
            self.imageScrollView.initialOffset = .center

            self.img = image!
            self.imageScrollView.display(image: image!)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionSave(_ sender :UIButton){
        let shareAll = [self.img]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func actionBack(_ sender :UIButton){
     self.navigationController?.popViewController(animated: true)
    }
}


extension ImageViewer: ImageScrollViewDelegate {
    
    func imageScrollViewDidChangeOrientation(imageScrollView: ImageScrollView) {
        print("Did change orientation")
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        print("scrollViewDidEndZooming at scale \(scale)")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollViewDidScroll at offset \(scrollView.contentOffset)")
    }
}
