//
//  CategoryVC.swift
//  Tumi
//
//  Created by Redspark on 05/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController {
    
    var catId : Int = 0
    
    @IBOutlet weak var tblListCategory:UITableView!
    @IBOutlet weak var lblNorecords:UILabel!
    
    var arrayData = NSMutableArray()
    
    var strcate_id = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    
        self.callAPI()
        
    }
    
    func callAPI()  {
        
        let parameters = ["lng":UserDataHolder.sharedUser.language, "cate_id":strcate_id] as [String : Any]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:SUB_CAT, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async{
                    
                    self.arrayData.addObjects(from: dict.value(forKey: "data") as! [Any])
                    
                    self.tblListCategory.delegate = self
                    self.tblListCategory.dataSource = self
                    self.tblListCategory.reloadData()
                    
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    
    func setupUI(){
        self.lblNorecords.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.tblListCategory.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        self.tblListCategory.delegate = self
        self.tblListCategory.dataSource = self
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
extension CategoryVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  self.arrayData.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeCell
        cell?.selectionStyle = .none
        
        let dict = arrayData.object(at: indexPath.row) as! NSDictionary
        
        cell?.lblDetail.text = dict.value(forKey: "subcategory_name") as? String
        
  
        
        var urlstring = dict.value(forKey: "image_path") as? String
        urlstring = urlstring!.replacingOccurrences(of: " ", with: "%20")
        
        DispatchQueue.main.async {
            cell?.imgCat.sd_setImage(with: URL(string:urlstring!.trim),placeholderImage: UIImage(named: ""))
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250; //UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let dict = arrayData.object(at: indexPath.row) as! NSDictionary
        
        let objCategoryVc = storyBoard.instantiateViewController(withIdentifier: "CategoryCartVC") as? CategoryCartVC
        objCategoryVc?.strcate_id = (dict.value(forKey: "id") as? Int)!
        self.navigationController?.pushViewController(objCategoryVc!, animated: true)
    }
}
