//
//  ARPageViewController.swift
//  Tumi
//
//  Created by Dhruv Patel on 10/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ARPageViewController: UIViewController,CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        Initialization()
        // Do any additional setup after loading the view.
    }
    
    func Initialization()
    {
       
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "ARVC1") as? ARVC1
        controller1?.title = "MEET_CHRIS_PRATT".localized()
        controller1?.objPage = self
        //controller1?.parentpage = self
        //controller1?.response = self.Response
        //controller1?.Parameter = self.Parameter
        controllerArray.append(controller1!)
        
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "ARVC2") as? ARVC2
        controller2?.title = "CHRIS_PRATT_3D_AVATAR".localized()
        //controller2?.parentpage = self
        controllerArray.append(controller2!)
        
        let controller3 = storyboard.instantiateViewController(withIdentifier: "ARVC3") as? ARVC3
        controller3?.title = "EXPLORE_ALPHA_3_IN_3D".localized()
        controller3?.objPage = self
//        controller3?.Parameter = self.Parameter
       controllerArray.append(controller3!)
        
//        let controller4 = storyboard.instantiateViewController(withIdentifier: "CashOutVC") as? CashOutVC
//        controller4?.title = "Cash out"
//        // controller4?.parentpage = self
//        //controller4?.Parameter = self.Parameter
//        controllerArray.append(controller4!)
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .bottomMenuHairlineColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuHeight(50.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        if Device.DeviceType.IS_IPHON_X{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.frame.size.width, height: self.view.frame.size.height-130), pageMenuOptions: parameters)
        }
        else{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.frame.size.width, height: self.view.frame.size.height-130 ), pageMenuOptions: parameters)
        }
        
        
    
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu?.currentPageIndex = 0
        //self.view.addSubview(pageMenu!.view)
        
        addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        pageMenu!.didMove(toParentViewController: self)
        
        pageMenu?.controllerScrollView.isScrollEnabled = false;
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {

    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        if(index == 1){
            let videoPlayerVC = storyBoard.instantiateViewController(withIdentifier: "VideoPlayerVC") as? VideoPlayerVC
            self.present(videoPlayerVC!, animated: false, completion: {
                DispatchQueue.main.async {
                    videoPlayerVC?.startUnity(nil)
                }
            })
            
        }
    }
    
//    func didMoveToPage(_ controller: UIViewController, index: Int) {
//        if (index == 1) {
//            let videoPlayerVC = controller as? VideoPlayerVC
//
//            DispatchQueue.main.async {
//                videoPlayerVC?.startUnity(nil)
//            }
//        }
//    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
