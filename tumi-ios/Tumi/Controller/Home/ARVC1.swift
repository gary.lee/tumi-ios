//
//  ARVC1.swift
//  Tumi
//
//  Created by Dhruv Patel on 10/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ARVC1: UIViewController {
    
    var objPage : ARPageViewController?
    @IBOutlet var lblContent: UILabel!
    @IBOutlet var imgBottom: NSLayoutConstraint!
    
    
    class func instantiateFromStoryboard() -> ARVC1
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ARVC1") as! ARVC1
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblContent.text = "LOCATE_CHRIS_TRIGGER".localized()
        // Do any additional setup after loading the view.
        
        if(Device.DeviceType.IS_IPHON_X){
            imgBottom.constant = 60
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        
        let objVideoVc = storyBoard.instantiateViewController(withIdentifier: "ArvideoVC")
        objPage?.navigationController?.pushViewController(objVideoVc, animated: true)
    }
    

}
