//
//  ARVC2.swift
//  Tumi
//
//  Created by Dhruv Patel on 10/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import ARKit

class ARVC2: UIViewController {
    @IBOutlet weak var sceneView: ARSCNView!
    var videoPlayerNode: SKVideoNode!
    class func instantiateFromStoryboard() -> ARVC2{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ARVC2") as! ARVC2
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ///addTapGestureToSceneView()
          sceneView.delegate = self
        configureLighting()
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpSceneView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpSceneView() {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        
       // sceneView.session.run(configuration)
        
      
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        sceneView.session.run(configuration, options: options)
       // sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    func configureLighting() {
        sceneView.autoenablesDefaultLighting = true
        sceneView.automaticallyUpdatesLighting = true
    }
    
    

}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}

extension UIColor {
    open class var transparentLightBlue: UIColor {
        return UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 0.50)
    }
}

extension ARVC2: ARSCNViewDelegate {

    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
    
         guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
        
        // 2
        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
//        //2. Get The Physical Width & Height Of Our Reference Image
        DispatchQueue.main.async {
            let width = width
            let height = height
            
            //3. Create An SCNNode To Hold Our Video Player With The Same Size As The Image Target
            let videoHolder = SCNNode()
            let videoHolderGeometry = SCNPlane(width:  width, height:height)
            videoHolder.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
            videoHolder.geometry = videoHolderGeometry
            
            //4. Create Our Video Player
            if let videoURL = Bundle.main.url(forResource: "Sample Video", withExtension: "mp4"){
                
                self.setupVideoOnNode(videoHolder, fromURL: videoURL)
            }
            
            //5. Add It To The Hierarchy
            node.addChildNode(videoHolder)
        }
        
    }
    
    /// Creates A Video Player As An SCNGeometries Diffuse Contents
    ///
    /// - Parameters:
    ///   - node: SCNNode
    ///   - url: URL
    func setupVideoOnNode(_ node: SCNNode, fromURL url: URL){
        
        //1. Create An SKVideoNode
        if(videoPlayerNode != nil){
                  videoPlayerNode = nil
            }
        
        //2. Create An AVPlayer With Our Video URL
        let videoPlayer = AVPlayer(url: url)
        videoPlayer.actionAtItemEnd = .none
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.playerItemDidReachEnd),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: videoPlayer.currentItem)
        //3. Intialize The Video Node With Our Video Player
        videoPlayerNode = SKVideoNode(avPlayer: videoPlayer)
        videoPlayerNode.yScale = -1
        
        //4. Create A SpriteKitScene & Postion It
        let spriteKitScene = SKScene(size: CGSize(width: self.view.frame.size.width-40, height: 200))
        spriteKitScene.scaleMode = .aspectFit
        videoPlayerNode.position = CGPoint(x: spriteKitScene.size.width/2, y: spriteKitScene.size.height/2)
        videoPlayerNode.size = spriteKitScene.size
        spriteKitScene.addChild(videoPlayerNode)
        
        //6. Set The Nodes Geoemtry Diffuse Contenets To Our SpriteKit Scene
        node.geometry?.firstMaterial?.diffuse.contents = spriteKitScene
        
        //5. Play The Video
        videoPlayerNode.play()
        videoPlayer.volume = 0
        
    }

    @objc func playerItemDidReachEnd(notification: NSNotification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: kCMTimeZero)
        }
    }
}
