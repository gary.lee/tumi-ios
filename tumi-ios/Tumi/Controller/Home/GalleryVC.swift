//
//  GalleryVC.swift
//  Tumi
//
//  Created by Redspark on 05/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class GalleryVC: UIViewController {
    
    var catId : Int = 0
    var cModel = CategoryModel()
    var strName = String()

    @IBOutlet weak var clctionGallery:UICollectionView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblNorecords:UILabel!
    
    var imageArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUPUI()
        // Do any additional setup after loading the view.
        
        let screenRect: CGRect = UIScreen.main.bounds
        let screenWidth: CGFloat = screenRect.size.width
        var cellWidth = Float(screenWidth / 3.4)
        
        if(Device.DeviceType.IS_IPHONE_5_OR_LESS){
            cellWidth = Float(screenWidth / 3.3)
        }
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 5, right: 5)
        let size = CGSize(width:Int(cellWidth), height: 130)
        layout.itemSize = size
        self.clctionGallery.collectionViewLayout = layout
    }
    func setUPUI(){
        getImages()
        self.lblTitle.text = strName
        self.lblNorecords.isHidden = true
        self.clctionGallery.register(UINib(nibName: "GalleryCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCell")
        self.clctionGallery.delegate = self
        self.clctionGallery.dataSource = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    func getImages() {
        
        let parameters = ["lng" : UserDataHolder.sharedUser.language ,"cate_id" : catId] as [String : Any]
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GET_IMAGES, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let _data = dict["data"] as? [String] {
                        self.imageArray = _data
                        print(self.imageArray.count)
                        self.clctionGallery.reloadData()
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }

}

extension GalleryVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath as IndexPath) as! GalleryCell
        DispatchQueue.main.async {
            cell.imgGallery.sd_setImage(with: URL(string:self.imageArray[indexPath.row]), placeholderImage: UIImage(named: ""))
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objImageViewerVc = storyBoard.instantiateViewController(withIdentifier: "ImageViewer") as?
        ImageViewer
        objImageViewerVc?.imageURL = imageArray[indexPath.row]
        self.navigationController?.pushViewController(objImageViewerVc!, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: UIScreen.main.bounds.width/3, height: UIScreen.main.bounds.width/6)
        let cWidth = collectionView.bounds.width/3.0
        let cHeight = cWidth
        return CGSize(width: cWidth, height: cHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
