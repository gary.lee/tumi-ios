//
//  ARVC3.swift
//  Tumi
//
//  Created by Dhruv Patel on 10/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ARVC3: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    //  @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnStart : UIButton!
    
    var objPage : ARPageViewController?
    
    var page = Int()
    
    fileprivate var items = [Character1]()
    
    fileprivate var currentPage: Int = 0 {
        didSet {
            let character = self.items[self.currentPage]
            self.infoLabel.text = character.name.uppercased().localized()
        }
    }
    
    class func instantiateFromStoryboard() -> ARVC1 {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ARVC1") as! ARVC1
    }
    
    fileprivate var pageSize: CGSize {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    fileprivate var orientation: UIDeviceOrientation {
        return UIDevice.current.orientation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.setupLayout()
        self.items = self.createItems()
        
        //self.infoLabel.text = "ALPHA_TUMI_BRIEF_PACK".localized()
        let character = self.items[0]
        self.infoLabel.text = character.name.uppercased().localized()
        self.lblcontent.text = "CLICK_START_BUTTON_TO".localized()
        self.btnStart.setTitle("START".localized(), for: UIControlState.normal);
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func setupLayout() {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 30)
    }
    
    fileprivate func createItems() -> [Character1] {
        let characters = [
            Character1(imageName: "bag", name: "ALPHA 3 TUMI BRIEF PACK®", movie: ""),
            Character1(imageName: "Suicase", name: "ALPHA 3 INTERNATIONAL DUAL ACCESS 4 WHEELED CARRY-ON", movie: ""),
        ]
        return characters
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageWidth: CGFloat = self.collectionView.frame.size.width
        page = Int(floor((self.collectionView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        
        let character = self.items[page]
        self.infoLabel.text = character.name.uppercased().localized()
    }
    
    
    // MARK: - Card Collection Delegate & DataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CarouselCollectionViewCell.identifier, for: indexPath) as! CarouselCollectionViewCell
        
        let character = items[(indexPath as NSIndexPath).row]
        cell.image.image = UIImage(named: character.imageName)
        
        cell.image.layer.cornerRadius = 10
        cell.image.layer.masksToBounds = true
        cell.image.layer.borderWidth = 1.0
        cell.image.layer.borderColor = UIColor.gray.cgColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    
    @IBAction func startTapped(_ sender: Any) {
        
        let objIntro = storyBoard.instantiateViewController(withIdentifier: "IntroductionVC") as? IntroductionVC
        objIntro?.Index = page
        objPage?.navigationController?.pushViewController(objIntro!, animated: false)
        
    }
    
    
}
