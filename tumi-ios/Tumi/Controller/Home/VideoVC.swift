//
//  VideoVC.swift
//  Tumi
//
//  Created by Redspark on 05/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import AVKit

class VideoVC: UIViewController {
    
    var catId : Int = 0
    
    @IBOutlet weak var tblListVideo:UITableView!
    @IBOutlet weak var lblNorecords:UILabel!
    
    var videoArray : [CategoryModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        getVideos()
        self.lblNorecords.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.tblListVideo.register(UINib(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        self.tblListVideo.delegate = self
        self.tblListVideo.dataSource = self
    }
    func getVideos() {
        let parameters : [String : Any] = ["cate_id":catId,"lng" : UserDataHolder.sharedUser.language]
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GET_VIDEOS, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let _data = dict["data"] as? [Dictionary<String, Any>] {
                        for i in 0..<_data.count {
                            let cModel = CategoryModel()
                            print(_data[i])
                            cModel.initWith(dictionary: _data[i])
                            print(cModel.videoPath)
                            self.videoArray.append(cModel)
                        }
                        print(self.videoArray.count)
                        self.tblListVideo.reloadData()
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(1, 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }
        
        return nil
    }
    
//    private func thumbnailForVideoAtURL(url: NSURL) -> UIImage? {
//
//        let asset = AVAsset(URL: url as URL)
//        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
//
//        var time = asset.duration
//        time.value = min(time.value, 2)
//
//        do {
//            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
//            return UIImage(CGImage: imageRef)
//        } catch {
//            print("error")
//            return nil
//        }
//    }
}
extension VideoVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return videoArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell", for: indexPath) as? VideoCell
        cell?.selectionStyle = .none
        var cModel = CategoryModel()
        cModel = videoArray[indexPath.row]
        cell?.lblDetail.text = cModel.name
        
        var vUrl = cModel.imagePath.trim
        vUrl = vUrl.replacingOccurrences(of:" ", with:"%20")
        cell?.imgCat.sd_setImage(with: URL(string:vUrl.trim), placeholderImage: UIImage(named: ""))
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250; //UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cModel = CategoryModel()
        cModel = videoArray[indexPath.row]
        
        var vUrl = cModel.videoPath.trim
        vUrl = vUrl.replacingOccurrences(of:" ", with:"%20")
        playVideo(from: vUrl)
    }
    private func playVideo(from file:String) {
        
        
        
        let videoURL = URL(string: file)
//        let file = file.components(separatedBy: ".")
        
//        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
//            debugPrint( "\(file.joined(separator: ".")) not found")
//            return
//        }
//        let player = AVPlayer(url: URL(fileURLWithPath: path))
        
        
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
        
    }
}
