//
//  HomeVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    @IBOutlet weak var tblListProduct:UITableView!
    @IBOutlet weak var lblNorecords:UILabel!
    
    var isCateVC : Bool = false
    
    var categoryArray : [CategoryModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
       self.setupUI()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        UserDataHolder.sharedUser.forOnlieStore = "0"
        let objCategoryVc = storyBoard.instantiateViewController(withIdentifier: "CategoryVC") as? CategoryVC
        objCategoryVc?.strcate_id = Int(UserDataHolder.sharedUser.foronlinestoreid)!
        self.navigationController?.pushViewController(objCategoryVc!, animated: true)
        
    }

    
    func CallAPI()  {
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng" : UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GET_MEMBERSHIP_DETAIL, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    let userdict = dict.value(forKey: "data") as? NSDictionary
                    if let lname  = userdict?.value(forKey: "membership_level") as? String{
                        UserDataHolder.sharedUser.membership_level = lname
                         NotificationCenter.default.post(name: Notification.Name("membershipupdate"), object: nil)
                
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    
    func setupUI(){
        getCategories()
        self.CallAPI()
        self.lblNorecords.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        self.tblListProduct.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        self.tblListProduct.delegate = self
        self.tblListProduct.dataSource = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getCategories() {
        
        let parameters = ["lng" : UserDataHolder.sharedUser.language]
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GET_CATEGORIES, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let _data = dict["data"] as? [Dictionary<String, Any>] {
                        for i in 0..<_data.count {
                            let cModel = CategoryModel()
                            print(_data[i])
                            cModel.initWith(dictionary: _data[i])
                            print(cModel.imagePath)
                            self.categoryArray.append(cModel)
                        }
                        print(self.categoryArray.count)
                        self.tblListProduct.reloadData()
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
            //            if((Constants.USERDEFAULTS .value(forKey: "isFirst")) != nil){
            //                Constants.USERDEFAULTS .removeObject(forKey: "isFirst")
            //                self.showHint()
            //            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    
}
extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       return categoryArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as? HomeCell
        var cModel = CategoryModel()
        cModel = categoryArray[indexPath.row]
        cell?.selectionStyle = .none
        cell?.lblDetail.text = cModel.name
        
//        var urlstring = cModel.imagePath.trim
//        urlstring = urlstring.replacingOccurrences(of: " ", with: "%20")
//
//        DispatchQueue.main.async {
//            cell?.imgCat.sd_setImage(with: URL(string:urlstring.trim), placeholderImage: UIImage(named: ""))
//        }
        
        var vUrl = cModel.imagePath.trim
        vUrl = vUrl.replacingOccurrences(of:" ", with:"%20")
        
        cell?.imgCat.sd_setImage(with: URL(string:vUrl.trim), placeholderImage: UIImage(named: ""))
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250; //UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var cModel = CategoryModel()
        cModel = categoryArray[indexPath.row]
        
        print(cModel.type)
        if(cModel.type == "Video"){
            let objVideoVc = storyBoard.instantiateViewController(withIdentifier: "VideoVC") as? VideoVC
            objVideoVc?.catId = cModel.id
            self.navigationController?.pushViewController(objVideoVc!, animated: true)
        }else if(cModel.type == "Images"){
            let objGalleryVc = storyBoard.instantiateViewController(withIdentifier: "GalleryVC") as? GalleryVC
            objGalleryVc?.catId = cModel.id
            objGalleryVc?.strName = cModel.name
            
            self.navigationController?.pushViewController(objGalleryVc!, animated: true)
        }else if(cModel.type == "catalogue"){
            let objCategoryVc = storyBoard.instantiateViewController(withIdentifier: "CategoryVC") as? CategoryVC
            objCategoryVc?.strcate_id = cModel.id
            self.navigationController?.pushViewController(objCategoryVc!, animated: true)
        }
       
    }
}
