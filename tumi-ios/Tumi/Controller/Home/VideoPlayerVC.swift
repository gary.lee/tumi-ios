//
//  VideoPlayerVC.swift
//  Tumi
//
//  Created by Redspark on 18/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import QuartzCore
import ReplayKit

class VideoPlayerVC: UIViewController , ARSCNViewDelegate ,RPPreviewViewControllerDelegate {

    @IBOutlet weak var unityContainerView: UIView!
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet weak var imgGrid: UIImageView!
    
    @IBOutlet var segment: UISegmentedControl!
    
    let recorder = RPScreenRecorder.shared()
    private var isRecording = false
    
    var animations = [String: CAAnimation]()
    var idle:Bool = true

    let node = SCNNode()
    
    var isPhoto:Bool = true
    var unityViewAdded = false;
    
    func addArTV()  {
        
        let videoURL = Bundle.main.url(forResource: "Sample Video", withExtension: "mp4")
        player = AVPlayer(url: videoURL!)
        
        let tvGeo = SCNPlane(width: 1.6, height: 0.9)
        tvGeo.firstMaterial?.diffuse.contents = player
        tvGeo.firstMaterial?.isDoubleSided = true
        
        let tvNode = SCNNode(geometry: tvGeo)
        tvNode.position = SCNVector3(x: 0, y: 0, z: -3)
        sceneView.scene.rootNode .addChildNode(tvNode)
        
        isPhoto = true
        
        player.play()
    }
    
    var player : AVPlayer!
    
    @objc func handleUnityReady() {
        showUnitySubView();
        UnitySendMessage("SceneControlObj", "changeScene", "SimpleAR");
    }
    
    @objc func pauseUnity() {
        stopUnity(nil);
    }
    
    func showUnitySubView() {
        if let unityView = UnityGetGLView() {
            // insert subview at index 0 ensures unity view is behind current UI view
            
            unityContainerView?.insertSubview(unityView, at: 0)
            
            
            /* unityView.translatesAutoresizingMaskIntoConstraints = false
             let views = ["view": unityView]
             let w = NSLayoutConstraint.constraints(withVisualFormat: "|-0-[view]-0-|", options: [], metrics: nil, views: views)
             let h = NSLayoutConstraint.constraints(withVisualFormat: "V:|-75-[view]-0-|", options: [], metrics: nil, views: views)
             view.addConstraints(w + h)*/
        }
    }
    
    @IBAction func startUnity(_ sender: UIButton?)
    {
        if let unityView = UnityGetGLView()
        {
//            unityView.isHidden = false;
//            showUnitySubView();
        }
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate
        {
            NotificationCenter.default.addObserver(self, selector: #selector(handleUnityReady), name: NSNotification.Name("UnityReady"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(pauseUnity), name: NSNotification.Name("PauseUnity"), object: nil)
            appDelegate.startUnity()
        }
    }
    
    @IBAction func stopUnity(_ sender: UIButton?)
    {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate
        {
            appDelegate.stopUnity()
            
            if let unityView = UnityGetGLView()
            {
                unityView.removeFromSuperview()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Set the view's delegate
        //sceneView.delegate = self®
        
        // Show statistics such as fps and timing information
//        sceneView.showsStatistics = true
        
        // Create a new scene
//        let scene = SCNScene()
        
        self.segment.setTitle("Photo".localized(), forSegmentAt: 0)
        self.segment.setTitle("video".localized(), forSegmentAt: 1)
        self.lblcontent.text = "click_the_tracker_on_your".localized()
        // Set the scene to the view
//        sceneView.scene = scene
        
        // Load the DAE animations
//        loadAnimations()
        
    }
    
    @IBAction func segmentTapped(_ sender: Any) {
        
        if(sender as AnyObject).selectedSegmentIndex == 0 {
            
            isPhoto = true
             btnCamera.setImage(UIImage(named: "camerabtn"), for: .normal)
        }
        else{
            
            isPhoto = false
            btnCamera.setImage(UIImage(named: "videobtn"), for: .normal)
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        destViewController = mainStoryboard.instantiateViewController(withIdentifier: "ARPageViewController")
        self.setViewControlelr(objVC: destViewController)
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        
//        if(isPhoto){
        
//        var image :UIImage?
//        let currentLayer = UnityGetGLView().layer;
//        let currentScale = UIScreen.main.scale
//        UIGraphicsBeginImageContextWithOptions(currentLayer.frame.size, false, currentScale);
//        guard let currentContext = UIGraphicsGetCurrentContext() else {return}
//        currentLayer.render(in: currentContext)
//        image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        guard let img = image else { return }
        
        
        if (isPhoto) {
            UnitySendMessage("ScreenCapObject", "doScreenShot", "");
            
            let image = sceneView.snapshot()
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            
            let alert = UIAlertController(title: "Saved", message: "Image save succefully.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(alert, animated: true, completion: nil)
        } else {
            
            if !isRecording {
                isRecording = true
                startRecording()
                btnCamera.setImage(UIImage(named: "videostopbtn"), for: .normal)
            } else {
                isRecording = false
                stopRecording()
                btnCamera.setImage(UIImage(named: "videobtn"), for: .normal)
            }
        }
    }
    
    func startRecording() {
        
        DispatchQueue.main.async {
            
            guard self.recorder.isAvailable else {
                print("Recording is not available at this time.")
                return
            }
            
            if self.recorder.isRecording {
                self.stopRecording()
            }
            
            self.recorder.startRecording{ [unowned self] (error) in
                
                guard error == nil else {
                    
                    print(error?.localizedDescription as Any)
                    print("There was an error starting the recording.")
                    return
                }
                
                print("Started Recording Successfully")
                self.isRecording = true
                
                DispatchQueue.main.async {
                    self.lblcontent.isHidden = true
                    self.segment.isHidden = true
                    self.btnBack.isHidden = true
                    self.imgGrid.isHidden = true
                }
            }
        }
        
      
        
    }
    
    func stopRecording() {
        self.lblcontent.isHidden = false
        self.segment.isHidden = false
        self.btnBack.isHidden = false
        self.imgGrid.isHidden = false
        
        recorder.stopRecording { [unowned self] (preview, error) in
            print("Stopped recording")
            
            guard preview != nil else {
                print("Preview controller is not available.")
                return
            }
            
            let alert = UIAlertController(title: "Recording Finished", message: "Would you like to edit or delete your recording?", preferredStyle: .alert)
            
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { (action: UIAlertAction) in
                self.recorder.discardRecording(handler: { () -> Void in
                    print("Recording suffessfully deleted.")
                })
            })
            
            let editAction = UIAlertAction(title: "Edit", style: .default, handler: { (action: UIAlertAction) -> Void in
                preview?.previewControllerDelegate = self
                self.present(preview!, animated: true, completion: nil)
            })
            
            alert.addAction(editAction)
            alert.addAction(deleteAction)
            self.present(alert, animated: true, completion: nil)
            
            self.isRecording = false
        }
    }
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
        startUnity(nil)
    }
    
    func loadAnimations () {
        // Load the character in the idle animation
        let idleScene = SCNScene(named: "art.scnassets/idleFixed.dae")!
        
        // This node will be parent of all the animation models
        //let node = SCNNode()
        
        // Add all the child nodes to the parent node
        for child in idleScene.rootNode.childNodes {
            node.addChildNode(child)
        }
        
        // Set up some properties
        node.position = SCNVector3(0, -1, -2)
        node.scale = SCNVector3(0.2, 0.2, 0.2)
        
        // Add the node to the scene
        sceneView.scene.rootNode.addChildNode(node)
        sceneView.allowsCameraControl = true;
        
        // Load all the DAE animations
        loadAnimation(withKey: "dancing", sceneName: "art.scnassets/twist_danceFixed", animationIdentifier: "twist_danceFixed-1")
    }
    
    
    func loadAnimation(withKey: String, sceneName:String, animationIdentifier:String) {
        let sceneURL = Bundle.main.url(forResource: sceneName, withExtension: "dae")
        let sceneSource = SCNSceneSource(url: sceneURL!, options: nil)
        
        if let animationObject = sceneSource?.entryWithIdentifier(animationIdentifier, withClass: CAAnimation.self) {
            // The animation will only play once
            animationObject.repeatCount = 1
            // To create smooth transitions between animations
            animationObject.fadeInDuration = CGFloat(1)
            animationObject.fadeOutDuration = CGFloat(0.5)
            
            // Store the animation for later use
            animations[withKey] = animationObject
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: sceneView)
        
        // Let's test if a 3D Object was touch
        var hitTestOptions = [SCNHitTestOption: Any]()
        hitTestOptions[SCNHitTestOption.boundingBoxOnly] = true
        
        let hitResults: [SCNHitTestResult]  = sceneView.hitTest(location, options: hitTestOptions)
        
        if hitResults.first != nil {
            if(idle) {
                playAnimation(key: "dancing")
            } else {
                stopAnimation(key: "dancing")
            }
            idle = !idle
            return
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //1. Get The Current Touch Point
        guard let currentTouchPoint = touches.first?.location(in: sceneView),
            //2. Get The Next Feature Point Etc
            let hitTest = sceneView.hitTest(currentTouchPoint, types: .existingPlane).first else { return }
        
        //3. Convert To World Coordinates
        let worldTransform = hitTest.worldTransform
        
        //4. Set The New Position
        let newPosition = SCNVector3(worldTransform.columns.3.x, worldTransform.columns.3.y, worldTransform.columns.3.z)
        
        //5. Apply To The Node
        node.simdPosition = float3(newPosition.x, newPosition.y, newPosition.z)
        
    }
    
    func playAnimation(key: String) {
        // Add the animation to start playing it right away
//        sceneView.scene.rootNode.addAnimation(animations[key]!, forKey: key)
    }
    
    func stopAnimation(key: String) {
        // Stop the animation with a smooth transition
//        sceneView.scene.rootNode.removeAnimation(forKey: key, blendOutDuration: CGFloat(0.5))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

//        DispatchQueue.main.async {
//            self.startUnity(nil);
//        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopUnity(nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    // MARK: - ARSCNViewDelegate
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
