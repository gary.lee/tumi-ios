//
//  ArvideoVC.swift
//  Tumi
//
//  Created by Redspark on 22/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import AVKit
import VideoScreenRecorder
import ReplayKit

class ArvideoVC: UIViewController,RPPreviewViewControllerDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var lblContent: UILabel!
    @IBOutlet weak var ctrlSwitch: UISegmentedControl!
    @IBOutlet weak var imgGrid: UIImageView!
    
    
    
    @IBOutlet weak var unityContainerView: UIView!
    var isPhoto = Bool()
    var player : AVPlayer!
    var video_url = String()
    
    let recorder = RPScreenRecorder.shared()
    var isRecord = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        sceneView.delegate = self
        sceneView.session.delegate = self
        isPhoto = true

        self.lblContent.text = "LOCATE_CHRIS_TRIGGER".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.Configuration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
   
    func Configuration()  {
        guard let referenceImages = ARReferenceImage.referenceImages(inGroupNamed: "AR Resources", bundle: nil) else {
            fatalError("Missing expected asset catalog resources.")
        }
        
        var configarstion:ARConfiguration!;
        
        if #available(iOS 12.0, *) {
            configarstion = ARImageTrackingConfiguration()
            (configarstion as! ARImageTrackingConfiguration).trackingImages = referenceImages
            (configarstion as! ARImageTrackingConfiguration).maximumNumberOfTrackedImages = referenceImages.count
        } else {
            configarstion = ARWorldTrackingConfiguration()
            (configarstion as! ARWorldTrackingConfiguration).detectionImages = referenceImages
        }
        
        sceneView.session.run(configarstion, options: [.resetTracking, .removeExistingAnchors])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        sceneView.session.pause()
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ChangeCamera(_ sender: Any) {
        
        if(sender as AnyObject).selectedSegmentIndex == 0 {
            
            isPhoto = true
            btnCamera.setImage(UIImage(named: "camerabtn"), for: .normal)
        }
        else{
            
            isPhoto = false
            btnCamera.setImage(UIImage(named: "videobtn"), for: .normal)
        }
        
    }
    
    func startRecording()
    {
        guard recorder.isAvailable else {
            print("Recording is not available at this time.")
            return
        }
        
        recorder.isMicrophoneEnabled = true
        
        recorder.startRecording{ [unowned self] (error) in
            
            guard error == nil else {
                print("There was an error starting the recording.")
                return
            }

            DispatchQueue.main.async {
                self.ctrlSwitch.isHidden = true;
                self.lblContent.isHidden = true;
                self.imgGrid.isHidden = true;
            }
            
            print("Started Recording Successfully")
        }
    }
    
    func stopRecording()
    {
        recorder.stopRecording { [unowned self] (preview, error) in
            print("Stopped recording")
            
            guard preview != nil else {
                print("Preview controller is not available.")
                return
            }
            
            let alert = UIAlertController(title: "Recording Finished", message: "Would you like to edit or delete your recording?", preferredStyle: .alert)
            
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { (action: UIAlertAction) in
                self.recorder.discardRecording(handler: { () -> Void in
                    print("Recording suffessfully deleted.")
                })
            })
            
            let editAction = UIAlertAction(title: "Edit", style: .default, handler: { (action: UIAlertAction) -> Void in
                preview?.previewControllerDelegate = self
                self.present(preview!, animated: true, completion: nil)
            })
            
            self.ctrlSwitch.isHidden = false;
            self.lblContent.isHidden = false;
            self.imgGrid.isHidden = false;
            
            alert.addAction(editAction)
            alert.addAction(deleteAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        dismiss(animated: true)
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        
        if(isPhoto){
            
            let image = sceneView.snapshot()
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            
            let alert = UIAlertController(title: "Saved", message: "Image save succefully.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"OK", style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(alert, animated: true, completion: nil)
        }
        else{
    
            if(!isRecord){
                isRecord = true
                self.btnCamera .setImage(UIImage(named: "videostopbtn"), for: .normal)
                startRecording()
            }
            else
            {
                self.btnCamera .setImage(UIImage(named: "videobtn"), for: .normal)
                stopRecording()
            }
        }
    }
    
    
}

extension ArvideoVC: ARSessionDelegate{
    
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        
        //1. Enumerate Our Anchors To See If We Have Found Our Target Anchor
        for anchor in anchors{
            
            if let imageAnchor = anchor as? ARImageAnchor{
                
                //2. If The ImageAnchor Is No Longer Tracked Then Handle The Event
                if !imageAnchor.isTracked{
                    
                    // print("Node is out of view")
                    
                    self.sceneView.session.remove(anchor: anchor)
                }
                else{
                    self.sceneView.session.add(anchor: anchor)
                }
            }
            
        }
    }
}

extension ArvideoVC: ARSCNViewDelegate{
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        //1. Check We Have Detected An ARImageAnchor
        guard let validAnchor = anchor as? ARImageAnchor else { return }
        
        //2. Create A Video Player Node For Each Detected Target
        node.addChildNode(createdVideoPlayerNodeFor(validAnchor.referenceImage))
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if node.isHidden {
            print("Node is out of view")
        }
    }
    
    func createdVideoPlayerNodeFor(_ target: ARReferenceImage) -> SCNNode{
        
        //1. Create An SCNNode To Hold Our VideoPlayer
        let videoPlayerNode = SCNNode()
        
        //2. Create An SCNPlane & An AVPlayer
        let videoPlayerGeometry = SCNPlane(width: target.physicalSize.width, height: target.physicalSize.height)
        // var videoPlayer = AVPlayer()
        
        //3. If We Have A Valid Name & A Valid Video URL The Instanciate The AVPlayer
        
//        if let name = target.name,
//
//        }
     
        
         let name = target.name
         print(name!)
        
        
        
         let validURL = Bundle.main.url(forResource: "AR_Trigger_Video_Final", withExtension: "mp4")
         player = AVPlayer(url: validURL!)
         player.volume = 1
         player.play()
        
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: .main) { [weak self] _ in
            self?.player?.seek(to: kCMTimeZero)
            self?.player?.play()
        }
        
//        let parameters = ["image_name" : targetName]
//
//        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:get_ARVideo, completion: { (json) in
//
//            let dict = json as! NSDictionary
//
//            if dict["status"] as? Int == 1
//            {
//                self.video_url = (dict.value(forKey: "video_url") as? String)!
//                self.video_url = self.video_url.replacingOccurrences(of:" ", with:"%20")
//
//                self.player = AVPlayer(url: URL(string: self.video_url)!)
//                self.player.volume = 1
//                self.player.play()
//
//                NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player.currentItem, queue: .main) { [weak self] _ in
//                    self?.player?.seek(to: kCMTimeZero)
//                    self?.player?.play()
//                }
//
//                //4. Assign The AVPlayer & The Geometry To The Video Player
//                videoPlayerGeometry.firstMaterial?.diffuse.contents = self.player
//                videoPlayerNode.geometry = videoPlayerGeometry
//
//                //5. Rotate It
//                videoPlayerNode.eulerAngles.x = -.pi / 2
//
//            }
//            else{
//            }
//
//        }, failure: { (error) in
//
//            print(error)
//        })
        
        //4. Assign The AVPlayer & The Geometry To The Video Player
        videoPlayerGeometry.firstMaterial?.diffuse.contents = self.player
        videoPlayerNode.geometry = videoPlayerGeometry
        
        //5. Rotate It
        videoPlayerNode.eulerAngles.x = -.pi / 2

        
        return videoPlayerNode
        
    }
   
}
