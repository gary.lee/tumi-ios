//
//  faqViewController.swift
//  Tumi
//
//  Created by Redspark on 17/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class faqViewController: UIViewController {

    @IBOutlet var tbl_faq: UITableView!
    
    
    @IBOutlet weak var lblTitle: UILabel!
    var arrayIndex = NSMutableArray()
    var arrayData = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.tbl_faq.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        
        self.lblTitle.text = "FAQ".localized()
        
        tbl_faq.tableFooterView = UIView()
        self.callAPI()
        
        
    }
    
    func callAPI()  {
        
        let parameters = ["lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:FAQ, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async{
                    
                    self.arrayData.addObjects(from: dict.value(forKey: "data") as! [Any])
                    
                    self.tbl_faq.delegate = self
                    self.tbl_faq.dataSource = self
                    self.tbl_faq.reloadData()
                    
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension faqViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrayIndex .contains(section)){
            
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
        
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        cell.selectionStyle = .none
        
        
        let dict = arrayData.object(at: indexPath.section) as! NSDictionary
        
        let strContent = dict.value(forKey: "ans") as! String
        let encodedData = strContent.data(using: String.Encoding.utf8)!
        
        var attributedString: NSAttributedString
        
        do {
            attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            
            cell.textLabel?.attributedText = attributedString
            
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("error")
        }
        cell.textLabel?.numberOfLines = 0
        cell.imageView?.image = nil
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let dict = arrayData.object(at: section) as! NSDictionary
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 5, width:
            tableView.bounds.size.width - 60, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: Constants.FONT, size: 15)
        headerLabel.textColor = UIColor.black
        headerLabel.numberOfLines = 0
        headerLabel.text = dict.value(forKey: "que") as? String
        headerLabel.sizeToFit()
        
        let downImg = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 10, width: 10, height: 10))
        downImg.image = UIImage(named: "down")
        if(arrayIndex .contains(section)){
            downImg.image = UIImage(named: "up")
        }
        downImg.contentMode = .scaleAspectFit
        headerView.addSubview(downImg)
        headerView.addSubview(headerLabel)
        
        let btnExpand = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
        btnExpand.tag = section
        btnExpand.addTarget(self, action:#selector(self.ExpandTapped(_:)), for: .touchUpInside)
        
        
        let separtor = UILabel(frame: CGRect(x: 20, y: 49, width: tableView.bounds.size.width - 20, height: 0.6))
        separtor.backgroundColor = UIColor.lightGray
        
        headerView.addSubview(separtor)
        headerView.addSubview(btnExpand)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 60
    }
    
    @objc func ExpandTapped(_ sender: UIButton) {
        
        if(arrayIndex .contains(sender.tag)){
            arrayIndex.remove(sender.tag)
            self.tbl_faq.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
        }
        else{
            
            if(arrayIndex.count > 0){
                
                let value = self.arrayIndex.object(at: 0) as! Int
                self.arrayIndex.remove(value)
                self.tbl_faq.reloadSections(IndexSet(integer: value), with: .automatic)
                self.arrayIndex.add(sender.tag)
                self.tbl_faq.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
            else{
                
                arrayIndex.add(sender.tag)
                self.tbl_faq.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tbl_faq.reloadData()
        }
        
    }
    
}
