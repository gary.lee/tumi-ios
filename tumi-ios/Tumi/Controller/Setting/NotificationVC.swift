//
//  NotificationVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {

    @IBOutlet var btnswitch: UISwitch!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet var lblTitlr: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTitlr.text = "NOTIFICATION".localized()
        self.lblcontent.text = "NOTIFICATION".localized()
        self.btnConfirm.setTitle("CONFIRM".localized(), for: .normal)
        
        
        self.getNotification()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionResetPassword(_ sender: Any) {
        self.setNotification()
    }
    
    func setNotification()
    {
      var notification = String()
        if self.btnswitch.isOn{
            notification = "1"
        }
        else{
            notification = "0"
        }
        
        let parameter : [String : Any] = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng":UserDataHolder.sharedUser.language,"notifications":notification]
        AFWrapper.postMethod(params: parameter as [String : AnyObject], apikey:set_notification, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1{
                appDelegate.isupdateNotification = true
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    
    
    
    
    
    func getNotification()
    {
        
        let parameter : [String : Any] = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng":UserDataHolder.sharedUser.language]
        AFWrapper.postMethod(params: parameter as [String : AnyObject], apikey:get_notification, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                let notifications = dict.value(forKey: "notifications") as? Int
                if(notifications == 1)
                {
                    self.btnswitch.isOn = true
                }
                else{
                    self.btnswitch.isOn = false
                }
                
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    
    

}
