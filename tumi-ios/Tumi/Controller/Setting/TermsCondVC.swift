//
//  TermsCondVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class TermsCondVC: UIViewController {
    
    @IBOutlet var lblterms: UILabel!
    var arrTitle = ["General","Global Terms","English"]
    var arrDescription = ["dfgdfgdfgf gfdgdfgdf fgdfgfgffgd dfgdfgdfgf gfdgdfgdf fgdfgfgffgd dfgdfgdfgf gfdgdfgdf fgdfgfgffgd dfgdfgdfgf gfdgdfgdf fgdfgfgffgd","ssssss ssss ssssss ssss ssssss ssss ssssss ssss ssssss ssss ssssss ssss ssssss ssss","dfgdfgdfgf gfdgdfgdf fgdfgfgffgd dfgdfgdfgf gfdgdfgdf fgdfgfgffgd dfgdfgdfgf gfdgdfgdf fgdfgfgffgd dfgdfgdfgf gfdgdfgdf fgdfgfgffgd ssssss ssss"]
    var arrdata = [TermsCondModel]()
    
     var arrayIndex = NSMutableArray()
     var arrayData = NSMutableArray()
    
    @IBOutlet var tblTermsConditions: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblterms.text = "TERMSCONDITIONS".localized()
        
         self.tblTermsConditions.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        
        for i in 0..<arrTitle.count {
            let tmodel = TermsCondModel()
            tmodel.title = arrTitle[i]
            tmodel.expanded = false
            tmodel.desc = arrDescription[i]
            arrdata.append(tmodel)
        }
        
        tblTermsConditions.tableFooterView = UIView()
        
        self.callAPI()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func callAPI()  {
        
         let parameters = ["lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:TERMS, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async{
                    
                    self.arrayData.addObjects(from: dict.value(forKey: "data") as! [Any])
                    
                    self.tblTermsConditions.delegate = self
                    self.tblTermsConditions.dataSource = self
                    self.tblTermsConditions.reloadData()
                    
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
}

extension TermsCondVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrayIndex .contains(section)){
            
            return 1
        }
        
        return 0
    }
    
    func height(forText text: String?, font: UIFont?, withinWidth width: CGFloat) -> CGFloat {
        
        let constraint = CGSize(width: width, height: 20000.0)
        var size: CGSize
        var boundingBox: CGSize? = nil
        if let aFont = font {
            boundingBox = text?.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: aFont], context: nil).size
        }
        size = CGSize(width: ceil((boundingBox?.width)!), height: ceil((boundingBox?.height)!))
        return size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
        
        cell.layer.shouldRasterize = true
        cell.layer.rasterizationScale = UIScreen.main.scale
        cell.selectionStyle = .none
        
        
        let dict = arrayData.object(at: indexPath.section) as! NSDictionary
        
        let strContent = dict.value(forKey: "text") as! String
        
        let encodedData = strContent.data(using: String.Encoding.utf8)!
        
        var attributedString: NSAttributedString
        
        do {
            attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
            
            cell.textLabel?.attributedText = attributedString
            
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("error")
        }
        cell.textLabel?.numberOfLines = 0
        cell.imageView?.image = nil
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let dict = arrayData.object(at: section) as! NSDictionary
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 14, width:
            tableView.bounds.size.width - 40, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: Constants.FONT, size: 16)
        headerLabel.textColor = UIColor.black
        headerLabel.numberOfLines = 2
        headerLabel.text = dict.value(forKey: "label") as? String
        headerLabel.sizeToFit()
        
        let downImg = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 20, width: 10, height: 10))
        downImg.image = UIImage(named: "down")
        if(arrayIndex .contains(section)){
            downImg.image = UIImage(named: "up")
        }
        downImg.contentMode = .scaleAspectFit
        headerView.addSubview(downImg)
        headerView.addSubview(headerLabel)
        
        let btnExpand = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
        btnExpand.tag = section
        btnExpand.addTarget(self, action:#selector(self.ExpandTapped(_:)), for: .touchUpInside)
        
        let LabelHeight = self.height(forText: headerLabel.text, font: UIFont(name: Constants.FONT, size: 15), withinWidth: self.view.frame.width - 40)
        
        let separtor = UILabel(frame: CGRect(x: 0, y: LabelHeight + 29, width: tableView.bounds.size.width, height: 0.6))
        separtor.backgroundColor = UIColor.lightGray
        
        headerView.addSubview(separtor)
        headerView.addSubview(btnExpand)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        let dict = arrayData.object(at: section) as! NSDictionary
        
        let LabelHeight = self.height(forText: dict.value(forKey: "label") as? String, font: UIFont(name: Constants.FONT, size: 15), withinWidth: self.view.frame.width - 40)
        
        return LabelHeight + 30
    }

    @objc func ExpandTapped(_ sender: UIButton) {
        
        if(arrayIndex .contains(sender.tag)){
            arrayIndex.remove(sender.tag)
            self.tblTermsConditions.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
        }
        else{
            
            if(arrayIndex.count > 0){
                
                let value = self.arrayIndex.object(at: 0) as! Int
                self.arrayIndex.remove(value)
                self.tblTermsConditions.reloadSections(IndexSet(integer: value), with: .automatic)
                self.arrayIndex.add(sender.tag)
                self.tblTermsConditions.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
            else{
                
                arrayIndex.add(sender.tag)
                self.tblTermsConditions.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tblTermsConditions.reloadData()
        }
        
    }

}
