//
//  ContactUsVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController,WWCalendarTimeSelectorProtocol,UIPickerViewDataSource,UIPickerViewDelegate{
    
    @IBOutlet var lblPhoneNumber: UILabel!
    fileprivate var singleDate: Date = Date()
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    let picker3 = UIPickerView()
    
    let pickerTitle = ["Select","Mr","Mrs","Miss"]
    let pickerGender = ["SelectGender","Male","Female"]
    let pickerRegion = ["Select","India","China","USA"]
    let pickerLanguage = ["SelectLanguage","English","中文（繁體)","中文（简体)"]
    
    var selectedtitle = Int()
    var selectedLanguage = Int()
    var selectedCoutry = Int()
    
    @IBOutlet var lblpagetitle: UILabel!
    
    private let placeholder = "MESSAGE".localized()
    
    @IBOutlet var message: UITextView!{
        didSet {
            message.textColor = UIColor.lightGray
            message.text = placeholder
            message.selectedRange = NSRange(location: 0, length: 0)
        }
    }
    @IBOutlet var txtTitle: UITextField!
    
    @IBOutlet var txtFirstName: UITextField!
    
    @IBOutlet var txtLastName: UITextField!
    
    @IBOutlet var txtEmail: UITextField!
    
    @IBOutlet var txtCountry: UITextField!
    
    @IBOutlet var txtCountryCode: UITextField!
    
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtSubject: UITextField!
    @IBOutlet var txtLang: UITextField!
    
    @IBOutlet var btnsubmit: UIButton!
    //
    @IBOutlet var lblmessage: UILabel!
    
    @IBOutlet var lblemailus: UILabel!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblfirstname: UILabel!
    
    @IBOutlet var lblcontent2: UILabel!
    @IBOutlet var lblsubject: UILabel!
    @IBOutlet var lblphone: UILabel!
    @IBOutlet var lbllanguage: UILabel!
    @IBOutlet var lblcountry: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var lbllastname: UILabel!
    @IBOutlet var lblcontent1: UILabel!
    @IBOutlet weak var codeWidth: NSLayoutConstraint!
    
    //  var countries = NSMutableArray()
     var countryList = CountryList()
    
    let countries = NSLocale.isoCountryCodes.map { (code:String) -> String in
        let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
        return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        // Do any additional setup after loading the view.
        
        countryList.delegate = self
        
        selectedtitle = 0
        selectedLanguage = 0
        selectedCoutry = -1
        
        let titleplaceholder = "Select".localized()
        self.txtTitle.placeholder = titleplaceholder
        self.txtCountry.placeholder = titleplaceholder
        self.txtLang.placeholder = titleplaceholder
        
        
        self.lblpagetitle.text = "CONTACTUS".localized()
        self.lblcontent1.text = "CustomerServiceCenter".localized()
        self.lbltitle.text = "TITLE".localized()
        self.lblfirstname.text = "FIRSTNAME".localized()
        self.lbllastname.text = "LASTNAME".localized()
        self.lblemail.text = "Email".localized()
        self.lblcountry.text = "COUNTRY".localized()
        self.lbllanguage.text = "Language".localized()
        self.lblphone.text = "PHONE".localized()
        self.lblsubject.text = "SUBJECT".localized()
        let placeholders = "SUBJECT".localized()
        self.txtSubject.placeholder = placeholders
        self.lblemailus.text = "Emailus".localized()
        self.lblmessage.text = "MESSAGE".localized()
        self.lblcontent2.text = "Pleasefillinthefollowing".localized()
        self.btnsubmit.setTitle("Submit".localized(), for: .normal)
        let placeholders1 = "Code".localized()
        self.txtCountryCode.placeholder = placeholders1
        
        message.delegate = self
        
        let lang = UserDataHolder.sharedUser.language
        
        if(lang != "en"){
            self.codeWidth.constant = 90
        }
        
    }
    
    func setUpUI(){
        txtTitle.setLeftPaddingPoints(10.0)
        txtFirstName.setLeftPaddingPoints(10.0)
        txtLastName.setLeftPaddingPoints(10.0)
        txtPhoneNumber.setLeftPaddingPoints(10.0)
        txtEmail.setLeftPaddingPoints(10.0)
        txtCountry.setLeftPaddingPoints(10.0)
        txtSubject.setLeftPaddingPoints(10.0)
        txtCountryCode.setLeftPaddingPoints(10.0)
        txtLang.setLeftPaddingPoints(10.0)
        
        txtCountry.inputView = picker1
        txtLang.inputView = picker2
        txtTitle.inputView = picker3
        txtLang.text = pickerLanguage[0].localized()
        txtTitle.text = pickerTitle[0].localized()
        txtCountry.text = "SelectRegion".localized()
        
        picker.tag = 0
        picker1.tag = 1
        picker2.tag = 2
        picker3.tag = 3
        
        picker.dataSource = self
        picker.delegate = self
        picker.reloadAllComponents()
        
        picker1.dataSource = self
        picker1.delegate = self
        picker1.reloadAllComponents()
        
        picker2.dataSource = self
        picker2.delegate = self
        picker2.reloadAllComponents()
        
        picker3.dataSource = self
        picker3.delegate = self
        picker3.reloadAllComponents()
        
    }
    @IBAction func btnCallClick(_ sender: Any) {
        let phoneNumber = self.lblPhoneNumber.text
        let cleanPhoneNumber = phoneNumber!.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "-")
        let urlString:String = "tel://\(cleanPhoneNumber)"
        if let phoneCallURL = URL(string: urlString) {
            if (UIApplication.shared.canOpenURL(phoneCallURL)) {
                UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    func ContactUSAPicall()
    {
        let title = self.txtTitle.text
        let firstname = self.txtFirstName.text
        let lastname = self.txtLastName.text
        let emailid = self.txtEmail.text
        let country = self.txtCountry.text
        let phone = self.txtPhoneNumber.text
        let messgae = self.message.text
        let subject = self.txtSubject.text
        
        
        if title == "Select".localized(){
            self.showAlert(withMessage: "Entertitle".localized())
            return
        }
        if firstname == ""{
            self.showAlert(withMessage: "Enterfirst".localized())
            return
        }
        if lastname == ""{
            self.showAlert(withMessage: "Enterlast".localized())
            return
        }
        if emailid == ""{
            self.showAlert(withMessage: "Enteremail".localized())
            return
        }
        if country == "COUNTRY".localized(){
            self.showAlert(withMessage: "Entercountry".localized())
            return
        }
        if phone == ""{
            self.showAlert(withMessage: "Enterphone".localized())
            return
        }
        if((message.text?.count)! == 0){
            self.showAlert(withMessage: "Entermessage".localized())
            return
        }
        if subject == ""{
            self.showAlert(withMessage: "Entersubject".localized())
            return
        }
        
        let parameter : [String : Any] =
            [
                "lng" : UserDataHolder.sharedUser.language,
                "title" : title!,
                "first_name" : firstname!,
                "last_name" : lastname!,
                "email" : emailid!,
                "country" : country!,
                "phone" : phone!,
                "message" : messgae!,
                "subject" : subject!
        ]
        
        AFWrapper.postMethod(params: parameter as [String : AnyObject], apikey:contactus, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
                self.RemoveAllData()
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
    }
    
    func RemoveAllData(){
        self.txtTitle.text = ""
        self.txtFirstName.text = ""
        self.txtLastName.text = ""
        self.txtEmail.text = ""
        self.txtCountry.text = ""
        self.txtLang.text = ""
        self.txtLang.text = ""
        self.txtCountryCode.text = ""
        self.txtPhoneNumber.text = ""
        self.txtSubject.text = ""
        self.message.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSubmitEnquiry(_ sender: Any) {
        self.ContactUSAPicall()
        //        if (txtTitle.text?.count)! != 0 || (txtFirstName.text?.count)! != 0 || (txtLastName.text?.count)! != 0 || (txtEmail.text?.count)! != 0 || (txtCountry.text?.count)! != 0 || (txtCountryCode.text?.count)! != 0 || (txtPhoneNumber.text?.count)! != 0 {
        //            let alert = UIAlertController(title: APPNAME, message: CONTACTUSTHANKYOU, preferredStyle: UIAlertControllerStyle.alert)
        //
        //            alert.addAction(UIAlertAction(title:CONTINUE, style: .cancel, handler: { (action: UIAlertAction!) in
        //
        //            }))
        //
        //            present(alert, animated: true, completion: nil)
        //        }
        //        else{
        //            let alert = UIAlertController(title: APPNAME, message:CONTACTUSBLANKFIELD, preferredStyle: UIAlertControllerStyle.alert)
        //
        //            alert.addAction(UIAlertAction(title:CONTINUE, style: .cancel, handler: { (action: UIAlertAction!) in
        //
        //            }))
        //
        //            present(alert, animated: true, completion: nil)
        //        }
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
//        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
//            self.txtCountryCode.text = country.dialingCode
//            //            self.btnCode.setTitle(country.dialingCode, for: .normal)
//            
//        }
//        // can customize the countryPicker here e.g font and color
//        countryController.detailColor = UIColor.red
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) ->Int {
        if(pickerView.tag == 0){
            return pickerGender.count
        }else if(pickerView.tag == 1){
            return countries.count
        }else if(pickerView.tag == 2){
            return pickerLanguage.count
        }
        else{
            return pickerTitle.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 0){
            return pickerGender[row]
        }else if(pickerView.tag == 1){
            return countries[row]
        }else if(pickerView.tag == 2){
            return pickerLanguage[row]
        }else{
            return pickerTitle[row].localized()
            
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 0){
            //            self.txtGendr.text =  pickerGender[row]
        }else if(pickerView.tag == 1){
            self.txtCountry.text =  countries[row]
            selectedCoutry = row
        }else if(pickerView.tag == 2){
            self.txtLang.text =  pickerLanguage[row].localized()
            selectedLanguage = row
        }else{
            self.txtTitle.text = pickerTitle[row].localized()
            selectedtitle = row
        }
        
    }
    
    
}

extension ContactUsVC: UITextViewDelegate {
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        // Move cursor to beginning on first tap
        if textView.text == placeholder {
            textView.selectedRange = NSRange(location: 0, length: 0)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text == placeholder && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
            textView.selectedRange = NSRange(location: 0, length: 0)
        }
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor.lightGray
            textView.text = placeholder
        }
    }
    
    func textViewShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension ContactUsVC: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.name as Any)
        print(country.flag as Any)
        print(country.countryCode)
        print(country.phoneExtension)
        self.txtCountryCode.text = country.phoneExtension
        
    }
}
