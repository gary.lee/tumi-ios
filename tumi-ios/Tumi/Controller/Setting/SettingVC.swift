//
//  SettingVC.swift
//  Tumi
//
//  Created by Redspark on 05/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {
    
    @IBOutlet var btnsigout: UIButton!
    var arrTitle = [["Reset_password","Notification_","Language"],["TermsConditions","PrivacyPolicy"],["AboutTUMI","Contactus","FAQ"]]
     var arrTitle1 = [["Reset Password","Notification","Language"],["Terms & Conditions","Privacy Policy"],["About Tumi","Contact us","FAQ"]]

    @IBOutlet weak var tblSettings:UITableView!
    var notificationstatus = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

//        self.setupUI()
//        self.getNotification()
        self.btnsigout.setTitle("SIGNOUT".localized(), for: .normal)
        
        // Do any additional setup after loading the view.
        
        self.getNotification()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if(appDelegate.isupdateNotification){
            
            appDelegate.isupdateNotification = false
             self.getNotification()
        }
       
      
    }
    func setupUI(){
        self.navigationController?.isNavigationBarHidden = true
        self.tblSettings.register(UINib(nibName: "SettingFirstCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        
        self.tblSettings.tableFooterView = UIView()
        self.tblSettings.delegate = self
        self.tblSettings.dataSource = self
    }
    @IBAction func actionSignOut(_ sender:UIButton){
        self.showLogoutAlert()
    }
    func showLogoutAlert(){
        let alert = UIAlertController(title: APPNAME, message: "Are you sure want to Logout?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: LOGOUT, style: .destructive, handler: { (action: UIAlertAction!) in
            Utility.sharedUtility.ShowLanguageVC()
            //Utility.sharedUtility.ShowLoginVC()
        }))
        alert.addAction(UIAlertAction(title:CANCEL, style: .cancel, handler: { (action: UIAlertAction!) in
        }))
        
        present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getNotification()
    {
        
        let parameter : [String : Any] = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng":UserDataHolder.sharedUser.language]
        AFWrapper.postMethod(params: parameter as [String : AnyObject], apikey:get_notification, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                 self.notificationstatus = (dict.value(forKey: "notifications") as? Int)!
                 self.setupUI()
                 self.tblSettings.reloadData()
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })

    }
    
    
}
extension SettingVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTitle[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as? SettingFirstCell
        cell?.lblTitle.text = self.arrTitle[indexPath.section][indexPath.row].localized()
        if(indexPath.section == 0){
            cell?.lblLanguage.isHidden = false
            if(indexPath.row ==  1){
                DispatchQueue.main.async {
                    if self.notificationstatus == 0{
                        cell?.lblLanguage.text = "Off".localized()
                    }
                    else{
                        cell?.lblLanguage.text = "on".localized()
                    }
                }
                
            }else if(indexPath.row ==  2){
               
                if UserDataHolder.sharedUser.language == "en"{
                     cell?.lblLanguage.text = "English"
                }
                else if UserDataHolder.sharedUser.language == "chinese"{
                    
                    cell?.lblLanguage.text = "中文（简体)"
                }
                else if UserDataHolder.sharedUser.language == "traditional chinese"{
                    cell?.lblLanguage.text = "中文（繁體)"
                }
                
            }else{
                cell?.lblLanguage.isHidden = true
                
            }
        }else{
            cell?.lblLanguage.isHidden = true
        }
        
        cell?.selectionStyle = .none
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrTitle1[indexPath.section][indexPath.row] == "Reset Password"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordVC") as? ResetPasswordVC
            self.navigationController?.pushViewController(objMemberShip!, animated: true)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "Notification"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
            self.navigationController?.pushViewController(objMemberShip!, animated: true)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "Language"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "LanguageSelectionVC") as? LanguageSelectionVC
            self.navigationController?.pushViewController(objMemberShip!, animated: true)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "Terms & Conditions"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "TermsCondVC") as? TermsCondVC
            self.navigationController?.pushViewController(objMemberShip!, animated: true)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "Privacy Policy"{
            let objPrivacyPolicyVC = storyBoard.instantiateViewController(withIdentifier: "PrivacyVC") as? PrivacyVC
            self.navigationController?.pushViewController(objPrivacyPolicyVC!, animated: true)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "Contact us"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "ContactUsVC") as? ContactUsVC
            self.navigationController?.pushViewController(objMemberShip!, animated: true)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "About Tumi"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as? PrivacyPolicyVC
            objMemberShip?.isCart = false
            self.present(objMemberShip!, animated: true, completion: nil)
        }
        else if self.arrTitle1[indexPath.section][indexPath.row] == "FAQ"{
            let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "faqViewController") as? faqViewController
            self.navigationController?.pushViewController(objMemberShip!, animated: true)
        }
        
    }
}
