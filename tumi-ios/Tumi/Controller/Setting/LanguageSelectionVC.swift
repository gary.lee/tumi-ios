//
//  LanguageSelectionVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class LanguageSelectionVC: UIViewController {
    
    var arrTitle = ["English","中文（繁體)","中文（简体)"]
    
    var selectedRow = 0
    var selectedlanguage = String()
    @IBOutlet weak var lblTitle: UILabel!
    
    
    
    @IBOutlet var btnconfirm: UIButton!
    @IBOutlet var tblLanguages: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDataHolder.sharedUser.language == "en"{
            selectedRow = 0
            self.selectedlanguage = "en"
        }
        else if UserDataHolder.sharedUser.language == "traditional chinese"{
            selectedRow = 1
            self.selectedlanguage = "traditional chinese"
        }
        else if UserDataHolder.sharedUser.language == "chinese"{
            selectedRow = 2
            self.selectedlanguage = "chinese"
        }
        
        let confirmtitle = "CONFIRM".localized()
        self.btnconfirm.setTitle(confirmtitle, for: .normal)
        
        // Do any additional setup after loading the view.
        
        
        self.lblTitle.text = "LANGUAGE".localized()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionConfirmLanguage(_ sender: Any) {
        self.setLanguage()
    }
    
    func setLanguage()
    {
        
        let parameter : [String : Any] = ["user_id":"\(UserDataHolder.sharedUser.userId!)","language":self.selectedlanguage,"lng":UserDataHolder.sharedUser.language]
        AFWrapper.postMethod(params: parameter as [String : AnyObject], apikey:set_language, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async
                    {
                        if self.selectedlanguage == "en"{
                            Bundle.setLanguage(lang: "en")
                            UserDataHolder.sharedUser.language = "en"
                        }
                        else if self.selectedlanguage == "traditional chinese"{
                            Bundle.setLanguage(lang: "zh-Hant")
                            UserDataHolder.sharedUser.language = "traditional chinese"
                        }
                        else if self.selectedlanguage == "chinese"{
                            Bundle.setLanguage(lang: "zh-Hans")
                            UserDataHolder.sharedUser.language = "chinese"
                        }
                        
                        Utility.sharedUtility.sideMenu()
                
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    
    
}

extension LanguageSelectionVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageSelectionCell", for: indexPath) as? LanguageSelectionCell
        cell?.lblTitle.text = self.arrTitle[indexPath.row]
        if(indexPath.row == selectedRow){
            cell?.lblSelectedColor.backgroundColor = UIColor.red
        }else{
            cell?.lblSelectedColor.backgroundColor = UIColor.clear
        }
        
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50 //UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageSelectionCell", for: indexPath) as? LanguageSelectionCell
        //        cell?.lblSelectedColor.backgroundColor = UIColor.red
        selectedRow = indexPath.row
        if indexPath.row == 0{
            self.selectedlanguage = "en"
        }
        else if indexPath.row == 1{
            self.selectedlanguage = "traditional chinese"
        }
        else if indexPath.row == 2{
            self.selectedlanguage = "chinese"
        }
        tblLanguages.reloadData()
        
    }
}
