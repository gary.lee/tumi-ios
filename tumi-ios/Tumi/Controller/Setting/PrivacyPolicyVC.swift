//
//  PrivacyPolicyVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController ,UIWebViewDelegate {
    
    @IBOutlet var webView: WKWebView!
    @IBOutlet var lblUrlLink: UILabel!
    @IBOutlet var viewWeb: UIView!
    @IBOutlet weak var web: UIWebView!
    
    var activityIndicator: UIActivityIndicatorView?
    
    var strUrl = String()
    var isCart = Bool()
    var isStore:Bool = false
    
    // var strUrl = "https://www.tumi-hk.com/brand-story/"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator?.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator?.center = self.view.center
        activityIndicator?.layer.zPosition = 1
        self.view .addSubview(activityIndicator!)
        
        if(isStore){
            strUrl = "https://www.tumi-hk.com/"
        }
        else if(!isCart){
            strUrl = "https://www.tumi-hk.com/brand-story/"
        }
        
        lblUrlLink.text = strUrl
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)

        web.allowsInlineMediaPlayback = true;
        web.mediaPlaybackRequiresUserAction = false;
        
        web.delegate = self
        let url = URL (string: strUrl)
        let requestObj = URLRequest(url: url!)
        web.loadRequest(requestObj)
        
        web.layer.cornerRadius = 15;
        web.layer.masksToBounds = true;
        web.clipsToBounds = true;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                dismiss(animated: true, completion: nil)
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    @IBAction func actionClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicator?.startAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        activityIndicator?.stopAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        activityIndicator?.stopAnimating()
    }
    
}
