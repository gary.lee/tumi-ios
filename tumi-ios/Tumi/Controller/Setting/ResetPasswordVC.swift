//
//  ResetPasswordVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    @IBOutlet var txtOldPassword: UITextField!
    
    @IBOutlet var txtNewPassword: UITextField!
    
    @IBOutlet var txtConfirmNewPassword: UITextField!
    
    @IBOutlet var lblpagetitle: UILabel!
    
    @IBOutlet var btnconfirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblpagetitle.text = "RESETPASSWORD".localized()
        let oldplaceholder = "OLDPASSWORD".localized()
        self.txtOldPassword.placeholder = oldplaceholder
        let newplaceholder = "NEWPASSWORD".localized()
        self.txtNewPassword.placeholder = newplaceholder
        let confirmplaceholder = "CONFIRMNEWPASSWORD".localized()
        self.txtConfirmNewPassword.placeholder = confirmplaceholder
        self.btnconfirm.setTitle("CONFIRM".localized(), for: .normal)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionResetPassword(_ sender: Any) {
        
        if (txtNewPassword.text?.count)! == 0 && txtOldPassword.text != "Old password" {
            let alert = UIAlertController(title: APPNAME, message: PASSWORDINCONSISTENTMSG, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:CONTINUE, style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(alert, animated: true, completion: nil)
        }
        
        else if(txtNewPassword.text != txtConfirmNewPassword.text){
            
            let alert = UIAlertController(title: APPNAME, message: "Password does not match", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:CONTINUE, style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            present(alert, animated: true, completion: nil)
            
        }
            
        else{
            
            
            let parameters = ["old_password":txtOldPassword.text!, "new_password":txtNewPassword.text!, "user_id":"\(UserDataHolder.sharedUser.userId!)", "lng":UserDataHolder.sharedUser.language]
            
            AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:RESET_PASSWORD, completion: { (json) in
                
                let dict = json as! NSDictionary
                
                let alert = UIAlertController(title: APPNAME, message: dict.value(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:CONTINUE, style: .cancel, handler: { (action: UIAlertAction!) in
                    
                    self.navigationController?.popViewController(animated: true)
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            }, failure: { (error) in
                
                print(error)
            })
            
        }
    }
    
}
