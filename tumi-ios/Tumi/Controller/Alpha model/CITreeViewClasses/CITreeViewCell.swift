//
//  CITreeViewCell.swift
//  CITreeView
//
//  Created by Apple on 24.01.2018.
//  Copyright © 2018 Cenk Işık. All rights reserved.
//

import UIKit

class CITreeViewCell: UITableViewCell {
    
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewSep: UIView!
    @IBOutlet weak var imgDown: UIImageView!
    @IBOutlet weak var img360: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
   // @IBOutlet weak var avatarImageView: UIImageView!
    
    let leadingValueForChildrenCell:CGFloat = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(level:Int)
    {
        self.leadingConstraint.constant = leadingValueForChildrenCell * CGFloat(level + 1)
        if(level > 0){
            self.nameLabel.textAlignment = .left
            self.nameLabel.font = UIFont(name: Constants.FONT, size: 15)
            viewSep.isHidden = true
           // imgDown.isHidden = true
        }
        else{
            self.nameLabel.textAlignment = .center
            self.nameLabel.font = UIFont(name: Constants.FONT, size: 16)
            viewSep.isHidden = false
            img360.isHidden = true
           // imgDown.isHidden = false
        }
        self.nameLabel.numberOfLines = 0

        self.layoutIfNeeded()
    }
    
 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
