//
//  CITreeViewData.swift
//  CITreeView
//
//  Created by Apple on 24.01.2018.
//  Copyright © 2018 Cenk Işık. All rights reserved.
//

import UIKit

class CITreeViewData1 {
    
    let name : String
    var children : [CITreeViewData1]
    
    init(name : String, children: [CITreeViewData1]) {
        self.name = name
        self.children = children
    }
    
    convenience init(name : String) {
        self.init(name: name, children: [CITreeViewData1]())
    }
    
    func addChild(_ child : CITreeViewData1) {
        self.children.append(child)
    }
    
    func removeChild(_ child : CITreeViewData1) {
        self.children = self.children.filter( {$0 !== child})
    }
}

extension CITreeViewData1 {
    
    static func getDefaultCITreeViewData() -> [CITreeViewData1] {
        

        let subChildChild71 = CITreeViewData1(name: "Hanger".localized())
        let subChildChild72 = CITreeViewData1(name: "Zip_Pocket".localized())
        let subChildChild73 = CITreeViewData1(name: "Zip_Divider".localized())
        let subChildChild74 = CITreeViewData1(name: "hanger_bracket".localized())
        let subChildChild75 = CITreeViewData1(name: "3x".localized())
        let subChildChild76 = CITreeViewData1(name: "Straps".localized())

        let child1 = CITreeViewData1(name: "Dual".localized())
        let child2 = CITreeViewData1(name: "Zipper".localized())
        let child3 = CITreeViewData1(name: "USB".localized())
        let child4 = CITreeViewData1(name: "Dual_Zipper".localized())
        let child5 = CITreeViewData1(name: "Lock".localized())
        let child6 = CITreeViewData1(name: "S_Pocket".localized())
        let child7 = CITreeViewData1(name: "Main_compo".localized(),children:[subChildChild71,subChildChild72,subChildChild73,subChildChild74,subChildChild75,subChildChild76])
        let child8 = CITreeViewData1(name: "Sleeve".localized())
        let child9 = CITreeViewData1(name: "U_zip".localized())
        let child10 = CITreeViewData1(name: "Handle".localized())
        let child11 = CITreeViewData1(name: "Wheels".localized())
        let child12 = CITreeViewData1(name: "Telescoping".localized())
        let child13 = CITreeViewData1(name: "TUMI_Trac".localized())
        let parent1 = CITreeViewData1(name: "FUNCTIONAL".localized(),children:[child1,child2,child3,child4,child5,child6,child7,child8,child9,child10,child11,child12,child13])
        
        let child31 = CITreeViewData1(name: "FXT".localized())
        let child32 = CITreeViewData1(name: "Panels".localized())
        let child33 = CITreeViewData1(name: "Rails".localized())
        let child34 = CITreeViewData1(name: "X_Brace".localized())
        let child35 = CITreeViewData1(name: "Handle_Grip".localized())
        let child36 = CITreeViewData1(name: "Omega".localized())
        let parent2 = CITreeViewData1(name: "DURABILITY".localized(), children: [child31, child32, child33, child34, child35, child36])
        
        let child41 = CITreeViewData1(name: "Monogramming".localized())
        let child42 = CITreeViewData1(name: "TUMI_Kit".localized())
        let child43 = CITreeViewData1(name: "Luggage_Tag".localized())
        let parent3 = CITreeViewData1(name: "PERSONALIZATION".localized(), children: [child41, child42,child43])
        
        return [parent1,parent2,parent3]
    }
    
    
}
