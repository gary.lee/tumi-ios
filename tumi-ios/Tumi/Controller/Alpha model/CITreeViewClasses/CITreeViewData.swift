//
//  CITreeViewData.swift
//  CITreeView
//
//  Created by Apple on 24.01.2018.
//  Copyright © 2018 Cenk Işık. All rights reserved.
//

import UIKit

class CITreeViewData {
    
    let name : String
    var children : [CITreeViewData]
    
    init(name : String, children: [CITreeViewData]) {
        self.name = name
        self.children = children
    }
    
    convenience init(name : String) {
        self.init(name: name, children: [CITreeViewData]())
    }
    
    func addChild(_ child : CITreeViewData) {
        self.children.append(child)
    }
    
    func removeChild(_ child : CITreeViewData) {
        self.children = self.children.filter( {$0 !== child})
    }
}

extension CITreeViewData {
    
    static func getDefaultCITreeViewData() -> [CITreeViewData] {
        
        let child1 = CITreeViewData(name: "Main_Compartment".localized())
        let child2 = CITreeViewData(name: "Gusseted_Pocket".localized())
        let child3 = CITreeViewData(name: "Waterproof_Pocket".localized())
        let child4 = CITreeViewData(name: "Fron_Pockets".localized())
        let child5 = CITreeViewData(name: "Add_System".localized())
        let child6 = CITreeViewData(name: "TUMI_Tracer®".localized())
        let parent1 = CITreeViewData(name: "FUNCTIONAL".localized(),children:[child1,child2,child3,child4,child5,child6])
        
        let child21 = CITreeViewData(name: "Padded_Straps".localized())
        let child22 = CITreeViewData(name: "Leather_Top".localized())
        let child23 = CITreeViewData(name: "Cushioned_Back".localized())
        let parent2 = CITreeViewData(name: "COMFORT".localized(), children: [child21, child22,child23])
        
        let child31 = CITreeViewData(name: "FXT".localized())
        let child32 = CITreeViewData(name: "Leather_Rain".localized())
        let parent3 = CITreeViewData(name: "DURABILITY".localized(), children: [child31, child32])
        
        let child41 = CITreeViewData(name: "Monogramming".localized())
        let child42 = CITreeViewData(name: "TUMI_Kit".localized())
        let child43 = CITreeViewData(name: "Luggage_Tag".localized())
        let parent4 = CITreeViewData(name: "PERSONALIZATION".localized(), children: [child41, child42,child43])
        
        return [parent1,parent2,parent3,parent4]
    }
    
    
}
