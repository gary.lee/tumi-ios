//
//  IntroductionVC.swift
//  Tumi
//
//  Created by Redspark on 19/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import SceneKit

class IntroductionVC: UIViewController {
    
    @IBOutlet var viewMain: UIView!
    @IBOutlet var btnGitIt2: UIButton!
    @IBOutlet var btnGitIt1: UIButton!
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet weak var imgBag: UIImageView!
    
    @IBOutlet var tbl_feature: CITreeView!
    
    @IBOutlet var viewDescription: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet weak var unityContainerView: UIView!
    @IBOutlet var scnView: SCNView!
    @IBOutlet weak var btnGot: UIButton!
    
    
    var data : [CITreeViewData] = []
    var data1 : [CITreeViewData1] = []
    
    let treeViewCellIdentifier = "TreeViewCellIdentifier"
    let treeViewCellNibName = "CITreeViewCell"
    
    var Index = Int()
    
    let node = SCNNode()
    
    var sectionArray = NSMutableArray()
    var arrayIndex = NSMutableArray()
    
    let subcatArray = [["Main_Compartment".localized(), "Gusseted_Pocket".localized(), "Waterproof_Pocket".localized(), "Fron_Pockets".localized(), "Add_System".localized(), "TUMI_Tracer®".localized()], ["Padded_Straps".localized(), "Leather_Top".localized(), "Cushioned_Back".localized()], ["FXT".localized(), "Leather_Rain".localized()],["Monogramming".localized(), "TUMI_Kit".localized(), "Luggage_Tag".localized()]]
    
    let subcatArray1 = [["Dual".localized(), "Zipper".localized(), "USB".localized(), "Dual_Zipper".localized(), "Lock".localized(), "S_Pocket".localized(), "Main_compo".localized(), "U_zip".localized(), "Handle".localized(), "Wheels".localized(), "Telescoping".localized(), "TUMI_Tracer®".localized()], ["FXT".localized(), "Panels".localized(),"Rails".localized(), "X_Brace".localized(), "Handle_Grip".localized(),"Omega".localized()],["Monogramming".localized(), "TUMI_Kit".localized(), "Luggage_Tag".localized()]]
    
    let subcatArray3 = [["Hanger".localized(),"hanger_bracket".localized(), "Zip_Pocket".localized(), "Zip_Divider".localized(), "3x".localized(), "Straps".localized()]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(Index == 0){
            data = CITreeViewData.getDefaultCITreeViewData()
        }
        else{
            data1 = CITreeViewData1.getDefaultCITreeViewData()
        }
        
        
        self.lblTitle.numberOfLines = 3
        
        tbl_feature.collapseNoneSelectedRows = false
        tbl_feature.register(UINib(nibName: treeViewCellNibName, bundle: nil), forCellReuseIdentifier: treeViewCellIdentifier)
        tbl_feature.backgroundColor = .clear
        
        viewMain.backgroundColor = UIColor(white: 0, alpha: 0.6)
        
        self.lblcontent.text = "ORIENTATE_YOUR_PHONE_TO_LANDSCAPE".localized()
        self.btnGitIt1.setTitle("GOTIT".localized(), for: .normal)
        self.btnGitIt2.setTitle("GOTIT".localized(), for: .normal)
        self.btnGot.setTitle("GOTIT".localized(), for: .normal)
        
        self.view2.isHidden = true
        self.viewDescription.isHidden = true
        self.imgBag.isHidden = true
        self.scnView.isHidden = true
        
        appDelegate.myOrientation = .landscape
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        
        if(Index == 0){
            sectionArray = ["FUNCTIONAL".localized(),"COMFORT".localized(),"DURABILITY".localized(),"PERSONALIZATION".localized()]
//            self.AddScen(name: "art.scnassets/BackPack_close.DAE")
        } else {
            sectionArray = ["FUNCTIONAL".localized(),"DURABILITY".localized(),"PERSONALIZATION".localized()]
            self.imgBag.image = UIImage(named: "Suicase")
        }
        
       // self.tbl_feature.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        self.tbl_feature.treeViewDelegate = self
        self.tbl_feature.treeViewDataSource = self
        self.tbl_feature.reloadData()
        
        
      //  self.AddScen(name: "art.scnassets/BackPack_close.DAE")
    }
    
//    func AddScen(name:String)   {
//
//        let scene=SCNScene(named: name)
//        // Add all the child nodes to the parent node
//        for child in (scene?.rootNode.childNodes)! {
//            node.addChildNode(child)
//        }
//        node.scale = SCNVector3(0.85, 0.85, 0.85)
//        node.position = SCNVector3(0, 0, -1)
//        scene?.rootNode.addChildNode(node)
//
//        scnView.scene=scene
//
//        scnView.allowsCameraControl = true
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startUnity(nil);
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.stopUnity(nil);
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        self.stopUnity(nil);
        
        appDelegate.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        
        appDelegate.myOrientation = .portrait
        //self.navigationController?.popViewController(animated: false)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        
        destViewController = mainStoryboard.instantiateViewController(withIdentifier: "ARPageViewController")
        self.setViewControlelr(objVC: destViewController)
    }
    
    @IBAction func got1Tapped(_ sender: Any) {
        
        self.view1.isHidden = true
        self.view2.isHidden = false
    }
    
    @IBAction func got2Tapped(_ sender: Any) {
        
        self.view2.isHidden = true
        self.viewMain.isHidden = true
    }
    
    @objc func handleUnityReady() {
        showUnitySubView()
        if (Index == 0) {
            UnitySendMessage("SceneControlObj", "changeScene", "BackpackScene");
        } else {
            UnitySendMessage("SceneControlObj", "changeScene", "SuitcaseScene");
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            UnitySendMessage("ControlObj", "zoomFunctionalities", "0");
        });
    }

    func showUnitySubView() {
        if let unityView = UnityGetGLView() {
            // insert subview at index 0 ensures unity view is behind current UI view
            unityView.frame = CGRect(x: 0, y: 0, width: unityContainerView.frame.width, height: unityContainerView.frame.height)
            unityContainerView?.insertSubview(unityView, at: 0)
            
            /* unityView.translatesAutoresizingMaskIntoConstraints = false
             let views = ["view": unityView]
             let w = NSLayoutConstraint.constraints(withVisualFormat: "|-0-[view]-0-|", options: [], metrics: nil, views:  views)
             let h = NSLayoutConstraint.constraints(withVisualFormat: "V:|-75-[view]-0-|", options: [], metrics: nil, views: views)
             view.addConstraints(w + h)*/
        }
    }
    
    @IBAction func startUnity(_ sender: UIButton?)
    {
        if let unityView = UnityGetGLView()
        {
//            unityView.isHidden = false;
        }
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate
        {
            NotificationCenter.default.addObserver(self, selector: #selector(handleUnityReady), name: NSNotification.Name("UnityReady"), object: nil)
            appDelegate.startUnity()
        }
    }
    
    @IBAction func stopUnity(_ sender: UIButton?)
    {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate
        {
            appDelegate.stopUnity()
            
            if let unityView = UnityGetGLView()
            {
                unityView.removeFromSuperview()
            }
        }
    }
    
    
    @IBAction func DescriptionGotTapped(_ sender: Any) {
        
        self.viewDescription.isHidden = true
        self.tbl_feature.isHidden = false
        
    }
    
}

extension IntroductionVC : CITreeViewDelegate {
    
    func treeViewNode(_ treeViewNode: CITreeViewNode, willExpandAt indexPath: IndexPath) {
        
    }
    
    func treeViewNode(_ treeViewNode: CITreeViewNode, didExpandAt indexPath: IndexPath) {
        
    }
    
    func treeViewNode(_ treeViewNode: CITreeViewNode, willCollapseAt indexPath: IndexPath) {
        
    }
    
    func treeViewNode(_ treeViewNode: CITreeViewNode, didCollapseAt indexPath: IndexPath) {
        
    }
    
    func willExpandTreeViewNode(treeViewNode: CITreeViewNode, atIndexPath: IndexPath) {
        
    }
    
    func didExpandTreeViewNode(treeViewNode: CITreeViewNode, atIndexPath: IndexPath) {
        
    }
    
    func willCollapseTreeViewNode(treeViewNode: CITreeViewNode, atIndexPath: IndexPath) {
        
    }
    
    func didCollapseTreeViewNode(treeViewNode: CITreeViewNode, atIndexPath: IndexPath) {
        
    }

    func handleSuitCaseControl(indexPath: IndexPath) {
        let item = String(indexPath.row + 1);

        /* ---- Suit case ---- */
        if(indexPath.section == 0){
            self.unityContainerView.isHidden = false;

            /* ---- Functionalities ---- */
            if(indexPath.row == 0 ){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item)
                self.lblDescription.text = "functinoal_suitcase_desc1".localized()
                
            }
            else if(indexPath.row == 1){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "functinoal_suitcase_desc2".localized()
            }
            else if(indexPath.row == 2){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "usb")
                self.lblDescription.text = "functinoal_suitcase_desc3".localized()
            }
            else if(indexPath.row == 3){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "functinoal_suitcase_desc4".localized()
            }
            else if(indexPath.row == 4){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "lock")
                self.lblDescription.text = "functinoal_suitcase_desc5".localized()
            }
            else if(indexPath.row == 5){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "functinoal_suitcase_desc6".localized()
            }
            else if(indexPath.row == 6){
                self.viewDescription.isHidden = true
                self.tbl_feature.isHidden = false
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "hanger")
                self.lblDescription.text = "functinoal_suitcase_desc7".localized()
            }
            else if(indexPath.row == 7){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "hanger")
                self.lblDescription.text = "functinoal_suitcase_desc8".localized()
            }
            else if(indexPath.row == 8){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "functinoal_suitcase_desc9".localized()
            }
            else if(indexPath.row == 9){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "grab")
                self.lblDescription.text = "functinoal_suitcase_desc10".localized()
            }
            else if(indexPath.row == 10){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "functinoal_suitcase_desc11".localized()
            }
            else if(indexPath.row == 11){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                //                    self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "functinoal_suitcase_desc12".localized()
            }
        }
        else if(indexPath.section == 1){
            self.unityContainerView.isHidden = false;

            /* ---- Durabilities ---- */
            if(indexPath.row == 0 ){
                self.imgBag.image = UIImage(named: "Suicase")
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.lblDescription.text = "durability_suitcase_desc1".localized()
            }
            else if(indexPath.row == 1){
                self.imgBag.image = UIImage(named: "Suicase")
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.lblDescription.text = "durability_suitcase_desc2".localized()
            }
            else if(indexPath.row == 2){
                self.imgBag.image = UIImage(named: "grab")
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.lblDescription.text = "durability_suitcase_desc3".localized()
            }
            else if(indexPath.row == 3){
                self.imgBag.image = UIImage(named: "zipper")
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.lblDescription.text = "durability_suitcase_desc4".localized()
            }
            else if(indexPath.row == 4){
                self.imgBag.image = UIImage(named: "Suicase")
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.lblDescription.text = "durability_suitcase_desc5".localized()
            }
            else if(indexPath.row == 5){
                self.imgBag.image = UIImage(named: "usb")
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.lblDescription.text = "durability_suitcase_desc6".localized()
            }
            
        }
        else if(indexPath.section == 2){
            self.unityContainerView.isHidden = true;
            self.imgBag.isHidden = false;
            
            /* ---- Personality ---- */
            if(indexPath.row == 0 ){
                // mono-gramming
                self.imgBag.image = UIImage(named: "mono-gramming")
                self.lblDescription.text = "Mono".localized()
            }
            else if(indexPath.row == 1){
                // accent
                self.imgBag.image = UIImage(named: "accent_suitcase")
                self.lblDescription.text = "Accent_kit".localized()
            }
            else if(indexPath.row == 2){
                // tag
                self.imgBag.image = UIImage(named: "tag_suitcase")
                self.lblDescription.text = "Tag".localized()
            }
        }
    }
    
    func handleBackPackControl(indexPath: IndexPath) {
        let item = String(indexPath.row + 1);

        if(indexPath.section == 0){
            self.unityContainerView.isHidden = false;
            if(indexPath.row == 0 ){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                self.imgBag.image = UIImage(named: "bag_open")
                self.lblDescription.text = "Main".localized()
            }
            else if(indexPath.row == 1){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "Gusseted".localized()
            }
            else if(indexPath.row == 2){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                self.imgBag.image = UIImage(named: "bag_open")
                self.lblDescription.text = "Water_proof".localized()
            }
            else if(indexPath.row == 3){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                self.imgBag.image = UIImage(named: "bag_zip")
                self.lblDescription.text = "front_Zip".localized()
            }
            else if(indexPath.row == 4){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "add_bag".localized()
            }
            else if(indexPath.row == 5){
                UnitySendMessage("ControlObj", "zoomFunctionalities", item);
                self.imgBag.image = UIImage(named: "bag_zip")
                self.lblDescription.text = "Tumi_trace".localized()
            }
            
        }
            
        else if(indexPath.section == 1){
            self.unityContainerView.isHidden = false;
            
            if(indexPath.row == 0 ){
                UnitySendMessage("ControlObj", "zoomConfort", item);
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "Padded".localized()
            }
            else if(indexPath.row == 1){
                UnitySendMessage("ControlObj", "zoomConfort", item);
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "Leather".localized()
            }
            else if(indexPath.row == 2){
                UnitySendMessage("ControlObj", "zoomConfort", item);
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "Cushioned".localized()
            }
            
        }
        else if(indexPath.section == 2){
            self.unityContainerView.isHidden = false;
            
            if(indexPath.row == 0 ){
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "FXT_Bali".localized()
            }
            else if(indexPath.row == 1){
                UnitySendMessage("ControlObj", "zoomDurability", item);
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "Leather_Flaps".localized()
            }
        }
        else{
            self.unityContainerView.isHidden = true;
            self.imgBag.isHidden = false;
        /* ---- Personality ---- */
            if(indexPath.row == 0 ){
                // mono-gramming
                self.imgBag.image = UIImage(named: "mono-gramming")
                self.lblDescription.text = "Mono".localized()
            }
            else if(indexPath.row == 1){
                // accent
                self.imgBag.image = UIImage(named: "accent")
                self.lblDescription.text = "Accent_kit".localized()
            }
            else if(indexPath.row == 2){
                // tag
                self.imgBag.image = UIImage(named: "tag")
                self.lblDescription.text = "Tag".localized()
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.viewDescription.isHidden = false
        self.tbl_feature.isHidden = true
        self.imgBag.isHidden = true
        
        if(Index == 0){
            self.lblTitle.text = String(format: "%d.  %@", indexPath.row + 1, self.subcatArray[indexPath.section][indexPath.row]).localized()
        } else {
            self.lblTitle.text = String(format: "%d.  %@", indexPath.row + 1, self.subcatArray1[indexPath.section][indexPath.row]).localized()
        }
        
        
        if(Index == 0){
            handleBackPackControl(indexPath: indexPath);
        } else {
            handleSuitCaseControl(indexPath: indexPath);
        }   
    }
    
    func treeView(_ treeView: CITreeView, heightForRowAt indexPath: IndexPath, with treeViewNode: CITreeViewNode) -> CGFloat {
        return 60
    }
    
    func treeView(_ treeView: CITreeView, didDeselectRowAt treeViewNode: CITreeViewNode, at indexPath: IndexPath) {
        
    }
    
    func treeView(_ treeView: CITreeView, didSelectRowAt treeViewNode: CITreeViewNode, at indexPath: IndexPath) {
        self.imgBag.isHidden = true;

        if(Index == 0){
            // Backpack

            let dataObj = treeViewNode.item as! CITreeViewData
            print(dataObj.name)
            
            if(dataObj.name != "FUNCTIONAL".localized() && dataObj.name != "COMFORT".localized() && dataObj.name != "DURABILITY".localized() && dataObj.name != "PERSONALIZATION".localized() )
            {
                self.viewDescription.isHidden = false
                self.tbl_feature.isHidden = true
                
//                self.scnView.isHidden = true
//                self.imgBag.isHidden = false
            }
            
            self.lblTitle.text = dataObj.name
            
            if(dataObj.name == "Main_Compartment".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "1");
                self.imgBag.image = UIImage(named: "bag_open")
                self.lblDescription.text = "Main".localized()
            }
            else if(dataObj.name == "Gusseted_Pocket".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "2");
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "Gusseted".localized()
                
            }
            else if(dataObj.name == "Waterproof_Pocket".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "3");
                self.imgBag.image = UIImage(named: "bag_open")
                self.lblDescription.text = "Water_proof".localized()
                
            }
            else if(dataObj.name == "Fron_Pockets".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "4");
                self.imgBag.image = UIImage(named: "bag_zip")
                self.lblDescription.text = "front_Zip".localized()
                
            }
            else if(dataObj.name == "Add_System".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "5");
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "add_bag".localized()
                
            }
            else if(dataObj.name == "TUMI_Tracer®".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "6");
                self.imgBag.image = UIImage(named: "bag_zip")
                self.lblDescription.text = "Tumi_trace".localized()
            }
            // COMFORT
            else if(dataObj.name == "Padded_Straps".localized()){
                UnitySendMessage("ControlObj", "zoomConfort", "1");
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "Padded".localized()
            }
                
            else if(dataObj.name == "Leather_Top".localized()){
                UnitySendMessage("ControlObj", "zoomConfort", "2");
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "Leather".localized()
            }
                
            else if(dataObj.name == "Cushioned_Back".localized()){
                UnitySendMessage("ControlObj", "zoomConfort", "3");
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "Cushioned".localized()
            }
            // Durability
            else if(dataObj.name == "FXT".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "1");
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "FXT_Bali".localized()
            }
            else if(dataObj.name == "Leather_Rain".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "2");
                self.imgBag.image = UIImage(named: "bag_font")
                self.lblDescription.text = "Leather_Flaps".localized()
            }
            // Personal
            else if(dataObj.name == "Monogramming".localized()){
                self.imgBag.isHidden = false;
                self.imgBag.image = UIImage(named: "mono-gramming")
                self.lblDescription.text = "Mono".localized()
            }
                
            else if(dataObj.name == "TUMI_Kit".localized()){
                self.imgBag.isHidden = false;
                self.imgBag.image = UIImage(named: "accent")
                self.lblDescription.text = "Accent_kit".localized()
            }
            else if(dataObj.name == "Luggage_Tag".localized()){
                self.imgBag.isHidden = false;
                self.imgBag.image = UIImage(named: "tag")
                self.lblDescription.text = "Tag".localized()
            }
        } else {
            // Suit case
            let dataObj = treeViewNode.item as! CITreeViewData1
            print(dataObj.name)
            
            if(dataObj.name != "FUNCTIONAL".localized() && dataObj.name != "Main_compo".localized() && dataObj.name != "DURABILITY".localized() && dataObj.name != "PERSONALIZATION".localized() )
            {
                self.viewDescription.isHidden = false
                self.tbl_feature.isHidden = true
                
//                self.scnView.isHidden = true
//                self.imgBag.isHidden = false
            }
            
            self.lblTitle.text = dataObj.name
            // FUNCTIONAL
            if(dataObj.name == "Dual".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "1")
                self.imgBag.image = UIImage(named: "duel")
                self.lblDescription.text = "functinoal_suitcase_desc1".localized()
            }
            else if(dataObj.name == "Zipper".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "2")
                self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "functinoal_suitcase_desc2".localized()
                
            }
            else if(dataObj.name == "USB".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "3")
                self.imgBag.image = UIImage(named: "usb")
                self.lblDescription.text = "functinoal_suitcase_desc3".localized()
                
            }
            else if(dataObj.name == "Dual_Zipper".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "4")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "functinoal_suitcase_desc4".localized()
                
            }
            else if(dataObj.name == "Lock".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "5")
                self.imgBag.image = UIImage(named: "lock")
                self.lblDescription.text = "functinoal_suitcase_desc5".localized()
                
                self.imgBag.image = UIImage(named: "usb")
                self.lblDescription.text = "Tag".localized()
            }
            else if(dataObj.name == "S_Pocket".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "6")
                self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "functinoal_suitcase_desc6".localized()
            }
            // MAIN COMP
            else if(dataObj.name == "Hanger".localized()){
                // a. Hanger Bracket
                UnitySendMessage("ControlObj", "zoomFunctionalities", "72")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "main_comp_desc_hanger".localized()
            }
            else if(dataObj.name == "Zip_Pocket".localized()){
                // b. Large Mesh Zip Pocket
                UnitySendMessage("ControlObj", "zoomFunctionalities", "73")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "main_comp_desc_zip_pocket".localized()
            }
            else if(dataObj.name == "Zip_Divider".localized()){
                // c. Zip Divider
                UnitySendMessage("ControlObj", "zoomFunctionalities", "74")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "main_comp_desc_zip_divider".localized()
            }
            else if(dataObj.name == "hanger_bracket".localized()){
                // d. Hanging mesh zipper
                // pocket with removable USB cable
                UnitySendMessage("ControlObj", "zoomFunctionalities", "77")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "main_comp_desc_hanger_bracket".localized()
            }
            else if(dataObj.name == "3x".localized()){
                // e. 3x Zip Pocket
                UnitySendMessage("ControlObj", "zoomFunctionalities", "75")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "main_comp_desc_3x".localized()
            }
            else if(dataObj.name == "Straps".localized()){
                // f. Compression Straps
                UnitySendMessage("ControlObj", "zoomFunctionalities", "76")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "main_comp_desc_staps".localized()
            }
            // FUNCTIONAL - CONTINUED
            else if(dataObj.name == "Sleeve".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "71")
                self.imgBag.image = UIImage(named: "bag_system")
                self.lblDescription.text = "functinoal_suitcase_desc8".localized()
            }
            else if(dataObj.name == "U_zip".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "8")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "functinoal_suitcase_desc9".localized()
            }
                
            else if(dataObj.name == "Handle".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "9")
                self.imgBag.image = UIImage(named: "grab")
                self.lblDescription.text = "functinoal_suitcase_desc10".localized()
            }
            else if(dataObj.name == "Wheels".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "10")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "functinoal_suitcase_desc11".localized()
            }
                
            else if(dataObj.name == "Telescoping".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "11")
                self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "functinoal_suitcase_desc12".localized()
            }
            // Durability
            else if(dataObj.name == "TUMI_Trac".localized()){
                UnitySendMessage("ControlObj", "zoomFunctionalities", "12")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "Tumi_trace".localized()
            }
            else if(dataObj.name == "FXT".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "1")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "durability_suitcase_desc1".localized()
            }
            else if(dataObj.name == "Panels".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "2")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "durability_suitcase_desc2".localized()
            }
            else if(dataObj.name == "Rails".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "3")
                self.imgBag.image = UIImage(named: "grab")
                self.lblDescription.text = "durability_suitcase_desc3".localized()
            }
            else if(dataObj.name == "X_Brace".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "4")
                self.imgBag.image = UIImage(named: "zipper")
                self.lblDescription.text = "durability_suitcase_desc4".localized()
            }
            else if(dataObj.name == "Handle_Grip".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "5")
                self.imgBag.image = UIImage(named: "Suicase")
                self.lblDescription.text = "durability_suitcase_desc5".localized()
            }
            else if(dataObj.name == "Omega".localized()){
                UnitySendMessage("ControlObj", "zoomDurability", "6")
                self.imgBag.image = UIImage(named: "usb")
                self.lblDescription.text = "durability_suitcase_desc6".localized()
            }
            // Personal
            else if(dataObj.name == "Monogramming".localized()){
                self.imgBag.isHidden = false;
                self.imgBag.image = UIImage(named: "mono-gramming")
                self.lblDescription.text = "Mono".localized()
            }
                
            else if(dataObj.name == "TUMI_Kit".localized()){
                self.imgBag.isHidden = false;
                self.imgBag.image = UIImage(named: "accent_suitcase")
                self.lblDescription.text = "Accent_kit".localized()
            }
            else if(dataObj.name == "Luggage_Tag".localized()){
                
                self.imgBag.isHidden = false;
                self.imgBag.image = UIImage(named: "tag_suitcase")
                self.lblDescription.text = "Tag".localized()
            }
   
            
        }
    }
}

extension IntroductionVC : CITreeViewDataSource {
    
    
    func treeViewSelectedNodeChildren(for treeViewNodeItem: Any) -> [Any] {
        
         if(Index == 0){
            if let dataObj = treeViewNodeItem as? CITreeViewData {
                return dataObj.children
            }
            return []
        }
        
        if let dataObj = treeViewNodeItem as? CITreeViewData1 {
            return dataObj.children
        }
        return []
    }
    
    func treeViewDataArray() -> [Any]{
        if(Index == 0){
            return data
        }
        return data1
    }
    
    func treeView(_ treeView: CITreeView, cellForRowAt indexPath: IndexPath, with treeViewNode: CITreeViewNode) -> UITableViewCell {
        let cell = treeView.dequeueReusableCell(withIdentifier: treeViewCellIdentifier) as! CITreeViewCell
        
        if(Index == 0){
            let dataObj = treeViewNode.item as! CITreeViewData
            cell.nameLabel.text = dataObj.name
            cell.setupCell(level: treeViewNode.level)
            
            if(dataObj.name != "FUNCTIONAL".localized() && dataObj.name != "COMFORT".localized() && dataObj.name != "DURABILITY".localized() && dataObj.name != "PERSONALIZATION".localized() ){
                cell.imgDown.isHidden = true
            }
            else{
                 cell.imgDown.isHidden = false
            }
            
            if(dataObj.name == "Main_Compartment".localized() || dataObj.name == "Fron_Pockets".localized() || dataObj.name == "Add_System".localized()){
                cell.img360.isHidden = false
            }
            else{
               cell.img360.isHidden = true
            }
        } else {
            let dataObj = treeViewNode.item as! CITreeViewData1
            cell.nameLabel.text = dataObj.name
            cell.setupCell(level: treeViewNode.level)
            
            if(dataObj.name == "Dual".localized()){
                cell.img360.isHidden = false
            }
            else{
                cell.img360.isHidden = true
            }

            if(dataObj.name != "FUNCTIONAL".localized() && dataObj.name != "Main_compo".localized() && dataObj.name != "DURABILITY".localized() && dataObj.name != "PERSONALIZATION".localized() ){
                cell.imgDown.isHidden = true
            }
            else{
                cell.imgDown.isHidden = false
            }
        }
      
        
        return cell;
    }
    
}


//extension IntroductionVC: UITableViewDelegate,UITableViewDataSource{
//
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return sectionArray.count
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        if(arrayIndex .contains(section)){
//
//            if(Index == 0){
//                return subcatArray[section].count
//            }
//            return subcatArray1[section].count
//        }
//
//        return 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath)
//
//        cell.layer.shouldRasterize = true
//        cell.layer.rasterizationScale = UIScreen.main.scale
//        cell.selectionStyle = .none
//
//        cell.textLabel?.textColor = UIColor.white
//
//        cell.textLabel?.font = UIFont(name: Constants.FONT, size: 15)
//        cell.textLabel?.numberOfLines = 0
//        cell.imageView?.image = nil
//        cell.backgroundColor = UIColor.clear
//
//        if(Index == 0){
//            cell.textLabel?.text = String(format: "%d.  %@", indexPath.row + 1, self.subcatArray[indexPath.section][indexPath.row])
//        }
//        else{
//            cell.textLabel?.text = String(format: "%d.  %@", indexPath.row + 1, self.subcatArray1[indexPath.section][indexPath.row])
//
//            if(indexPath.section == 0){
//
//                if(indexPath.row == 6){
//
//                    let downImg = UIImageView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
//                    downImg.image = UIImage(named: "down_white")
//                    downImg.contentMode = .scaleAspectFit
//                    cell.accessoryView = downImg
//
//                }
//            }
//        }
//
//
//        return cell
//    }
//
//
////    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
////
////         if(Index == 1){
////
////            if(indexPath.section == 0){
////
////                if(indexPath.row == 6 ){
////
////                }
////            }
////        }
////    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        self.viewDescription.isHidden = false
//        self.tbl_feature.isHidden = true
//
//        self.scnView.isHidden = true
//        self.imgBag.isHidden = false
//
//        if(Index == 0){
//            self.lblTitle.text = String(format: "%d.  %@", indexPath.row + 1, self.subcatArray[indexPath.section][indexPath.row]).localized()
//        }
//        else{
//            self.lblTitle.text = String(format: "%d.  %@", indexPath.row + 1, self.subcatArray1[indexPath.section][indexPath.row]).localized()
//        }
//
//
//        if(Index == 0){
//
//            if(indexPath.section == 0){
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "bag_open")
//                    self.lblDescription.text = "Main".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "Gusseted".localized()
//                }
//                else if(indexPath.row == 2){
//
//                    self.imgBag.image = UIImage(named: "bag_open")
//                    self.lblDescription.text = "Water_proof".localized()
//                }
//                else if(indexPath.row == 3){
//
//                    self.imgBag.image = UIImage(named: "bag_zip")
//                    self.lblDescription.text = "front_Zip".localized()
//                }
//                else if(indexPath.row == 4){
//
//                    self.imgBag.image = UIImage(named: "bag_system")
//                    self.lblDescription.text = "add_bag".localized()
//                }
//                else if(indexPath.row == 5){
//
//                    self.imgBag.image = UIImage(named: "bag_zip")
//                    self.lblDescription.text = "Tumi_trace".localized()
//                }
//
//            }
//
//            else if(indexPath.section == 1){
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "bag_system")
//                    self.lblDescription.text = "Padded".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "Leather".localized()
//                }
//                else if(indexPath.row == 2){
//
//                    self.imgBag.image = UIImage(named: "bag_system")
//                    self.lblDescription.text = "Cushioned".localized()
//                }
//
//            }
//            else if(indexPath.section == 2){
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "FXT_Bali".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "Leather_Flaps".localized()
//                }
//            }
//            else{
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "Mono".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "Accent_kit".localized()
//                }
//                else if(indexPath.row == 2){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                    self.lblDescription.text = "Tag".localized()
//                }
//            }
//        }
//        else{
//
//            if(indexPath.section == 0){
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "duel")
//                    self.lblDescription.text = "functinoal_suitcase_desc1".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "zipper")
//                    self.lblDescription.text = "functinoal_suitcase_desc2".localized()
//                }
//                else if(indexPath.row == 2){
//
//                    self.imgBag.image = UIImage(named: "usb")
//                    self.lblDescription.text = "functinoal_suitcase_desc3".localized()
//                }
//                else if(indexPath.row == 3){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "functinoal_suitcase_desc4".localized()
//                }
//                else if(indexPath.row == 4){
//
//                    self.imgBag.image = UIImage(named: "lock")
//                    self.lblDescription.text = "functinoal_suitcase_desc5".localized()
//                }
//                else if(indexPath.row == 5){
//
//                    self.imgBag.image = UIImage(named: "zipper")
//                    self.lblDescription.text = "functinoal_suitcase_desc6".localized()
//                }
//                else if(indexPath.row == 6){
//
//                    self.viewDescription.isHidden = true
//                    self.tbl_feature.isHidden = false
//
//                  //  self.imgBag.image = UIImage(named: "hanger")
//                  //  self.lblDescription.text = "functinoal_suitcase_desc7".localized()
//                }
//                else if(indexPath.row == 7){
//
//                    self.imgBag.image = UIImage(named: "hanger")
//                    self.lblDescription.text = "functinoal_suitcase_desc8".localized()
//                }
//                else if(indexPath.row == 8){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "functinoal_suitcase_desc9".localized()
//                }
//                else if(indexPath.row == 9){
//
//                    self.imgBag.image = UIImage(named: "grab")
//                    self.lblDescription.text = "functinoal_suitcase_desc10".localized()
//                }
//                else if(indexPath.row == 10){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "functinoal_suitcase_desc11".localized()
//                }
//                else if(indexPath.row == 11){
//
//                    self.imgBag.image = UIImage(named: "zipper")
//                    self.lblDescription.text = "functinoal_suitcase_desc12".localized()
//                }
//            }
//            else if(indexPath.section == 1){
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "durability_suitcase_desc1".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "durability_suitcase_desc2".localized()
//                }
//                else if(indexPath.row == 2){
//
//                    self.imgBag.image = UIImage(named: "grab")
//                    self.lblDescription.text = "durability_suitcase_desc3".localized()
//                }
//                else if(indexPath.row == 3){
//
//                    self.imgBag.image = UIImage(named: "zipper")
//                    self.lblDescription.text = "durability_suitcase_desc4".localized()
//                }
//                else if(indexPath.row == 4){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "durability_suitcase_desc5".localized()
//                }
//                else if(indexPath.row == 5){
//
//                    self.imgBag.image = UIImage(named: "usb")
//                    self.lblDescription.text = "durability_suitcase_desc6".localized()
//                }
//
//            }
//            else if(indexPath.section == 2){
//
//                if(indexPath.row == 0 ){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                    self.lblDescription.text = "Mono".localized()
//                }
//                else if(indexPath.row == 1){
//
//                    self.imgBag.image = UIImage(named: "zipper")
//                    self.lblDescription.text = "Accent_kit".localized()
//                }
//                else if(indexPath.row == 2){
//
//                    self.imgBag.image = UIImage(named: "usb")
//                    self.lblDescription.text = "Tag".localized()
//                }
//
//            }
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        let headerView = UIView()
//        headerView.backgroundColor = UIColor.clear
//
//        var xFrame : CGFloat = 70
//
//         if(Index == 0){
//            if(section == 3){
//                if(UserDataHolder.sharedUser.language == "en"){
//                    xFrame = 90
//                }
//            }
//        }
//         else{
//
//            if(section == 2){
//                if(UserDataHolder.sharedUser.language == "en"){
//                    xFrame = 90
//                }
//            }
//
//        }
//
//
//
//        let headerLabel = UILabel(frame: CGRect(x: tableView.bounds.size.width/2 - xFrame, y: 20, width:
//            300, height: 60))
//        headerLabel.font = UIFont(name: Constants.FONT, size: 16)
//        headerLabel.text = sectionArray.object(at: section) as? String
//        headerLabel.textAlignment = .center
//        headerLabel.textColor = UIColor.white
//        headerLabel.sizeToFit()
//
//        let downImg = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 25, width: 10, height: 10))
//        downImg.image = UIImage(named: "down_white")
//        if(arrayIndex .contains(section)){
//            downImg.image = UIImage(named: "up_white")
//        }
//        downImg.contentMode = .scaleAspectFit
//
//
//        headerView.addSubview(downImg)
//        headerView.addSubview(headerLabel)
//
//        let btnExpand = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
//        btnExpand.tag = section
//        btnExpand.addTarget(self, action:#selector(self.ExpandTapped(_:)), for: .touchUpInside)
//
//
//        let separtor = UILabel(frame: CGRect(x: tableView.bounds.size.width/2 - 25, y: 49, width: 20, height: 0.6))
//        separtor.backgroundColor = UIColor.lightGray
//
//        if(arrayIndex .contains(section)){
//            headerView.backgroundColor = UIColor(red: 188/255.0, green: 33/255.0, blue: 49/255.0, alpha: 1.0)
//        }
//
//
//
//        headerView.addSubview(separtor)
//        headerView.addSubview(btnExpand)
//
//        return headerView
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//        return 50
//    }
//
//    @objc func ExpandTapped(_ sender: UIButton) {
//
//        if(arrayIndex .contains(sender.tag)){
//            arrayIndex.remove(sender.tag)
//            self.tbl_feature.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
//        }
//        else{
//
//            self.scnView.isHidden = true
//            self.imgBag.isHidden = false
//
//            if(Index == 0){
//
//                if(sender.tag == 0){
//
//                    self.scnView.isHidden = false
//                    self.imgBag.isHidden = true
//                    // self.AddScen(name: "art.scnassets/BackPack_close.DAE")
//                }
//                else if(sender.tag == 1){
//
//                    self.imgBag.image = UIImage(named: "bag_system")
//
//                }
//
//                else if(sender.tag == 2){
//
//                    self.imgBag.image = UIImage(named: "bag_font")
//                }
//                else{
//
//                    self.imgBag.image = UIImage(named: "bag_personal")
//                }
//            }
//            else{
//                if(sender.tag == 0){
//
//                    self.scnView.isHidden = true
//                    self.imgBag.isHidden = false
//                    // self.AddScen(name: "art.scnassets/BackPack_close.DAE")
//                }
//                else if(sender.tag == 1){
//
//                    self.imgBag.image = UIImage(named: "open_case")
//
//                }
//
//                else if(sender.tag == 2){
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//
//                }
//                else{
//
//                    self.imgBag.image = UIImage(named: "Suicase")
//                }
//            }
//
//
//            if(arrayIndex.count > 0){
//
//                let value = self.arrayIndex.object(at: 0) as! Int
//                self.arrayIndex.remove(value)
//                self.tbl_feature.reloadSections(IndexSet(integer: value), with: .automatic)
//                self.arrayIndex.add(sender.tag)
//                self.tbl_feature.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
//            }
//            else{
//
//                arrayIndex.add(sender.tag)
//                self.tbl_feature.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
//            }
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//            self.tbl_feature.reloadData()
//        }
//
//    }
//
//}
