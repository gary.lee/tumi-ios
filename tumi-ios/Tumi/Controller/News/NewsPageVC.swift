//
//  NewsPageVC.swift
//  Tumi
//
//  Created by Redspark on 05/07/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class NewsPageVC: UIViewController,CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.Initialization()
    }
    
    func Initialization()
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "RecentVC") as? RecentVC
        controller1?.objPage = self
        controller1?.title = "recent".localized()
        controllerArray.append(controller1!)
        
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "PopulerVC") as? PopulerVC
        controller2?.objPage = self
        controller2?.title = "popular".localized()
        controllerArray.append(controller2!)
        
        let controller3 = storyboard.instantiateViewController(withIdentifier: "FeatureVC") as? FeatureVC
        controller3?.objPage = self
        controller3?.title = "feature".localized()
        controllerArray.append(controller3!)
        
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .bottomMenuHairlineColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuHeight(50.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        if Device.DeviceType.IS_IPHON_X{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.bounds.size.width, height: self.view.frame.size.height-130), pageMenuOptions: parameters)
        }
        else{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.bounds.size.width, height: self.view.frame.size.height-130 ), pageMenuOptions: parameters)
        }
        
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu?.currentPageIndex = 0
        self.view.addSubview(pageMenu!.view)
        
    }
    
    
    @IBAction func sidemenuTapped(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }

}
