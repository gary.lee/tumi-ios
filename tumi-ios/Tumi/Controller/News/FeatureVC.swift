//
//  FeatureVC.swift
//  Tumi
//
//  Created by Redspark on 05/07/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class FeatureVC: UIViewController {

    var objPage : NewsPageVC?
    @IBOutlet weak var tbl_feature: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tbl_feature.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        self.tbl_feature.delegate = self
        self.tbl_feature.dataSource = self
        self.tbl_feature.reloadData()
    }
}


extension FeatureVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell
        
        cell?.layer.shouldRasterize = true
        cell?.layer.rasterizationScale = UIScreen.main.scale
        cell?.selectionStyle = .none
        
        cell?.viewBG.layer.borderWidth = 1.0
        cell?.viewBG.layer.borderColor = UIColor.black.cgColor
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 420; //UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objVideoVc = storyBoard.instantiateViewController(withIdentifier: "NewsDetailsVC") as? NewsDetailsVC
        objPage?.navigationController?.pushViewController(objVideoVc!, animated: true)
    }
}
