//
//  MemberShipVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class MemberShipVC: UIViewController {
    @IBOutlet weak var imgCheck:UIImageView!
    @IBOutlet weak var imgCheck1:UIImageView!
    @IBOutlet weak var vwMember:UIView!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet var lblyes: UILabel!
    
    @IBOutlet var lblno: UILabel!
    @IBOutlet var btnnext: UIButton!
    
    
    var strMember:String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
         self.strMember = "Yes"
        
        self.lbltitle.text = "EXCLUSIVES_CLUB".localized()
        self.lblyes.text = "Yes".localized()
        self.lblno.text = "No".localized()
        self.lblcontent.text = "Are_you_an_existing".localized()
        self.btnnext.setTitle("NEXT".localized(), for: .normal)
        
    }
    func setupUI(){
        self.vwMember.layer.cornerRadius = 10.0
        self.vwMember.clipsToBounds = true
    }
    @IBAction func actionLanguage(_ sender:UIButton){
        
        if(sender.tag == 1){
            self.imgCheck.image = UIImage(named: "checked")
            self.imgCheck1.image = UIImage(named: "unchecked")
            self.strMember = "Yes"
        }else if(sender.tag == 2){
            self.imgCheck.image = UIImage(named: "unchecked")
            self.imgCheck1.image = UIImage(named: "checked")
            self.strMember = "No"
        }
    }
    @IBAction func actionNext(_ sender:UIButton){
        if(strMember == "Yes"){
            let objMemberProfileVC = storyBoard.instantiateViewController(withIdentifier: "MobileNumberVC") as? MobileNumberVC
            objMemberProfileVC?.strMember = strMember!
            self.navigationController?.pushViewController(objMemberProfileVC!, animated: true)
        }else{
            let objMobileNoVc = storyBoard.instantiateViewController(withIdentifier: "MobileNumberVC") as? MobileNumberVC
            objMobileNoVc?.strMember = strMember!
            self.navigationController?.pushViewController(objMobileNoVc!, animated: true)
        }
        
    }
    @IBAction func actionBack(_ sender:UIButton){
      self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
