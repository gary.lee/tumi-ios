//
//  MemberProfileVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class MemberProfileVC: UIViewController {
    
    @IBOutlet var btnnext: UIButton!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblSurname:UILabel!
    @IBOutlet weak var lblPhone:UILabel!
    @IBOutlet weak var vwProfile:UIView!
    
    @IBOutlet var lblMobile: UILabel!
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lblsurname: UILabel!
    @IBOutlet var lblTitle: UILabel!
    var strMember = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()


        self.lblMobile.text = "Mobile".localized()
        self.lblname.text = "First_Name".localized()
        self.lblsurname.text = "Surname".localized()
        self.lblTitle.text = "YOUR_PROFILE".localized()
         self.btnnext.setTitle("NEXT".localized(), for: .normal)
        self.CallAPI()
    }
    func setupUI(){
        
        self.vwProfile.layer.cornerRadius = 10.0
        self.vwProfile.clipsToBounds = true
      
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender:UIButton){
        
        let objSetPwdVC = storyBoard.instantiateViewController(withIdentifier: "SetPassWordVC") as? SetPassWordVC
        self.navigationController?.pushViewController(objSetPwdVC!, animated: true)
    }
    
    
    func CallAPI()  {
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng" : UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GET_MEMBER_PROFILE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    let userdict = dict.value(forKey: "data") as? NSDictionary
                    
                    self.lblName.text = userdict?.value(forKey: "first_name") as? String
                    self.lblSurname.text = userdict?.value(forKey: "surname") as? String
                    self.lblPhone.text = userdict?.value(forKey: "mobile") as? String
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
}
