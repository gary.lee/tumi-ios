//
//  VerifyOTP.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import KWVerificationCodeView

class VerifyOTP: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var verificationCodeView: KWVerificationCodeView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var VwVerify:UIView!
    @IBOutlet weak var lblSeconds:UILabel!
    @IBOutlet weak var btnResend:UIButton!
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var lblContent: UILabel!
    @IBOutlet var lbltitle: UILabel!
    
    var isFrgtpwd:Bool? = false
    var timer : Timer = Timer()
    var seconds: Int = 60
    
    var phoneNumber : String = ""
    
    var strMember = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
        
        self.lbltitle.text = "VERIFY_YOUR_MOBILE".localized()
        self.lblDescription.text = "VERIFY_YOUR_MOBILE".localized()
        self.lbltitle.text = "VERIFY_YOUR_MOBILE".localized()
       self.nextButton.setTitle("NEXT".localized(), for: .normal)
        
        self.lblDescription.text = String(format: "%@ %@","Please_enter".localized(), phoneNumber)
    }
    func setupUI(){
        verificationCodeView.delegate = self
        nextButton.isEnabled = false
        btnResend.isEnabled = false

        let secondvalue = "S".localized()
        
        self.VwVerify.clipsToBounds = true
        self.VwVerify.layer.cornerRadius = 10.0
        let FormattedText = NSMutableAttributedString()
        FormattedText.normalWithUnderLine("Resned_code".localized())
        btnResend.setAttributedTitle(FormattedText, for: .normal)
        lblSeconds.text = "\(seconds)\(secondvalue)"
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    @objc func update() {
         let secondvalue = "S".localized()
        seconds -= 1
        lblSeconds.text = "\(seconds)\(secondvalue)"
        self.lblSeconds.isHidden = false
        if seconds == 0{
            lblSeconds.isHidden = true
            btnResend.isEnabled = true
            timer.invalidate()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionResend(_ sender:UIButton){
        resendOTP()
        btnResend.isEnabled = false
        self.lblSeconds.isHidden = false
        seconds = 60
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender:UIButton){
        if verificationCodeView.hasValidCode() {
            verifyOTP()
        }
//        if txtFirst.text != "" && txtSecond.text != "" && txtThird.text != "" && txtFourth.text != "" {
//            verifyOTP()
//        }
    }
    func verifyOTP() {
        let phone = "+\(phoneNumber.removeWhitespace())"
        let parameters = ["phone":phone,
                          "otp":"\(verificationCodeView.getVerificationCode())", "lng":UserDataHolder.sharedUser.language,"device_token":UserDataHolder.sharedUser.deviceToken, "device_type":device_type]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:VERIFY_OTP, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let uid = dict["user_id"] as? Int{
                       
                        if(self.isFrgtpwd)!{
                            let objSetpwdVC = storyBoard.instantiateViewController(withIdentifier: "SetPassWordVC") as? SetPassWordVC
                            objSetpwdVC?.isfrgt = self.isFrgtpwd
                            self.navigationController?.pushViewController(objSetpwdVC!, animated: true)
                        }else{
                            
                             UserDataHolder.sharedUser.userId = uid
                            
                            if(self.strMember == "Yes"){
                                let objMemberrVC = storyBoard.instantiateViewController(withIdentifier: "MemberProfileVC") as? MemberProfileVC
                                self.navigationController?.pushViewController(objMemberrVC!, animated: true)
                            }
                            else{
                                
                                let objRegisterVC = storyBoard.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC
                                self.navigationController?.pushViewController(objRegisterVC!, animated: true)
                            }
                            
                        }
                       
                    }
                    
                }
            }
            else{
                let title = "Re_send".localized()
                let backtitle = "Back".localized()
                
                let alert = UIAlertController(title: APPNAME, message: dict["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: title, style: .destructive, handler: { (action: UIAlertAction!) in
                    self.resendOTP()
                }))
                
                alert.addAction(UIAlertAction(title: backtitle, style: .cancel, handler: { (action: UIAlertAction!) in
                    
                }))
                self.present(alert, animated: true)
                
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    func resendOTP() {
        
        let phone = "+\(phoneNumber.removeWhitespace())"
        
        let parameters = ["phone":phone,
            "lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:RESEND_OTP, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let uid = dict["user_id"] as? Int{
                        
                        UserDataHolder.sharedUser.userId = uid
                    }
                    
                }
            }
            else{
                
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    

}

extension VerifyOTP: KWVerificationCodeViewDelegate {
    func didChangeVerificationCode() {
        nextButton.isEnabled = verificationCodeView.hasValidCode()
    }
}

extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
}
//extension UIViewController: UITextFieldDelegate {
//    private func textFieldShouldBeginEditing(_ textField: UITextField) {
//        textField.text = ""
//    }
//}
