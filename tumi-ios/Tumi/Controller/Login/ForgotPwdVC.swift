//
//  ForgotPwdVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ForgotPwdVC: UIViewController{
    
    @IBOutlet weak var txtCountryCode:UITextField!
    @IBOutlet weak var txtMobile:UITextField!
    @IBOutlet weak var vwForgotpwd:UIView!
    
    @IBOutlet var btnnext: UIButton!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblcontent: UILabel!
    
    var countryList = CountryList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        countryList.delegate = self
        self.lbltitle.text = "FORGOT_PASSWORD".localized()
        self.lblcontent.text = "We_will_send_forgot".localized()
        self.btnnext.setTitle("NEXT".localized(), for: .normal)
        
        let codeplaceholder = "Code".localized()
        self.txtCountryCode.placeholder = codeplaceholder
        
        let mobileplaceholder = "MobileNumber".localized()
        self.txtMobile.placeholder = mobileplaceholder
        
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        self.vwForgotpwd.layer.cornerRadius = 10.0
        self.vwForgotpwd.clipsToBounds = true
        txtCountryCode.setLeftPaddingPoints(10.0)
        txtMobile.setLeftPaddingPoints(10.0)
    }
    @IBAction func actionCountryCode(_ sender: Any) {
        //        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
        //            self.txtCountryCode.text = country.dialingCode
        //        }
        //        // can customize the countryPicker here e.g font and color
        //        countryController.detailColor = UIColor.red
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender:UIButton){
        self.VerifyOtp()
        //        let objVerifyOTP = storyBoard.instantiateViewController(withIdentifier: "VerifyOTP") as? VerifyOTP
        //        self.navigationController?.pushViewController(objVerifyOTP!, animated: true)
    }
    func VerifyOtp() {
        
        let phone = "+\(txtCountryCode.text!)\(txtMobile.text!)"
        
        
        let parameters = ["phone":phone,
                          "lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:RESEND_OTP, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let uid = dict["user_id"] as? Int{
                        
                        UserDataHolder.sharedUser.userId = uid
                        let objVerifyVC = storyBoard.instantiateViewController(withIdentifier: "VerifyOTP") as? VerifyOTP
                        objVerifyVC?.isFrgtpwd = true
                        objVerifyVC?.phoneNumber = "\(self.txtCountryCode.text!)\(self.txtMobile.text!)"
                        self.navigationController?.pushViewController(objVerifyVC!, animated: true)
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

extension ForgotPwdVC: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.name as Any)
        print(country.flag as Any)
        print(country.countryCode)
        print(country.phoneExtension)
        self.txtCountryCode.text = country.phoneExtension
        
    }
}
