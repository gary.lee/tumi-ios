//
//  RegisterVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController,WWCalendarTimeSelectorProtocol,UIPickerViewDataSource,UIPickerViewDelegate {
    
    fileprivate var singleDate: Date = Date()
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    
    let pickerGender = ["SelectGender","Male","Female"]
    let pickerRegion = ["SelectRegion","India","China","USA"]
    let pickerLanguage = ["SelectLanguage","English","中文（繁體)","中文（简体)"]
    
    var BirthdayString = String()
    
    var genderselected = Int()
    var languageselected = Int()
    var regionselected = Int()
    
    @IBOutlet var btnNext: UIButton!
    @IBOutlet weak var txtSurname:UITextField!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var btnDate:UIButton!
    @IBOutlet weak var txtGendr:UITextField!
    @IBOutlet weak var txtRegion:UITextField!
    @IBOutlet weak var txtLanguage:UITextField!
    @IBOutlet weak var vwRegister:UIView!
    
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var lblsurname: UILabel!
    
    @IBOutlet var lblfirstname: UILabel!
    @IBOutlet var lblpreferlanguage: UILabel!
    
    @IBOutlet var lblregion: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var lblbirthday: UILabel!
    
    @IBOutlet var lblgender: UILabel!
    
    var Language = String()
    
    
    
    let countries = NSLocale.isoCountryCodes.map { (code:String) -> String in
        let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
        return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        
        
        genderselected = 0
        languageselected = 0
        regionselected = -1
        
        
        self.lblsurname.text = "Surname".localized()
        self.lblfirstname.text = "First_Name".localized()
        
        self.lbltitle.text = "SET_YOUR_PROFILE".localized()
        self.lblemail.text = "Email".localized()
        self.lblbirthday.text = "Birthday(mm/dd/yyyy)".localized()
        self.lblgender.text = "Gender".localized()
        self.lblregion.text = "ResidingPlaceRegion".localized()
        self.lblpreferlanguage.text = "PreferredLanguage".localized()
        
        self.btnNext.setTitle("NEXT".localized(), for: .normal)
        
        let gendervalue = "SelectGender".localized()
        self.txtGendr.placeholder = gendervalue
        
        let regionvalue = "SelectRegion".localized()
        self.txtRegion.placeholder = regionvalue
        
        // Do any additional setup after loading the view.
        
        if((Constants.USERDEFAULTS.value(forKey: "isSocial")) != nil){
            getProfile()
        }
    }
    
    
    func getProfile() {
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng" : UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GETPROFILE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    
                    if let _data = dict["data"] as? NSDictionary{
                        print(_data)
                        if let uid = _data["id"] as? Int{
                            UserDataHolder.sharedUser.userId = uid
                        }
                        if let fname  = _data["first_name"] as? String{
                            UserDataHolder.sharedUser.firstName = fname
                            self.txtName.text = fname
                        }
                        if let lname  = _data["last_name"] as? String{
                            UserDataHolder.sharedUser.lastName = lname
                        }
                        if let surname  = _data["surname"] as? String{
                            UserDataHolder.sharedUser.surname = surname
                            self.txtSurname.text = surname
                        }
                        if let phone  = _data["phone"] as? String{
                            UserDataHolder.sharedUser.phoneNumber = phone
                        }
                        if let gender  = _data["sex"] as? String{
                            UserDataHolder.sharedUser.gender = gender
                        }
                        if let email  = _data["email"] as? String{
                            UserDataHolder.sharedUser.email = email
                            self.txtEmail.text = email
                        }
                        if let region  = _data["region"] as? String{
                            UserDataHolder.sharedUser.region = region
                        }
                        if let membership_level  = _data["membership_level"] as? String{
                            UserDataHolder.sharedUser.membership_level = membership_level
                        }
                    }
                }
            }
            else{
                
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
                //
            }
            
            //            if((Constants.USERDEFAULTS .value(forKey: "isFirst")) != nil){
            //                Constants.USERDEFAULTS .removeObject(forKey: "isFirst")
            //                self.showHint()
            //            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
    }
    
    func setUpUI(){
        self.vwRegister.clipsToBounds =  true
        self.vwRegister.layer.cornerRadius = 10.0
        txtSurname.setLeftPaddingPoints(10.0)
        txtName.setLeftPaddingPoints(10.0)
        txtEmail.setLeftPaddingPoints(10.0)
        txtGendr.setLeftPaddingPoints(10.0)
        txtRegion.setLeftPaddingPoints(10.0)
        txtLanguage.setLeftPaddingPoints(10.0)
        self.btnDate.setTitle("", for: .normal)
        txtGendr.inputView = picker
        txtRegion.inputView = picker1
        txtLanguage.inputView = picker2
        picker.tag = 0
        picker1.tag = 1
        picker2.tag = 2
        txtGendr.text = pickerGender[0].localized()
        txtRegion.text = "SelectRegion".localized()
        txtLanguage.text = pickerLanguage[0].localized()
        picker.dataSource = self
        picker.delegate = self
        picker.reloadAllComponents()
        picker1.dataSource = self
        picker1.delegate = self
        picker1.reloadAllComponents()
        picker2.dataSource = self
        picker2.delegate = self
        picker2.reloadAllComponents()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionDate(_ sender:UIButton){
        
        RPicker.selectDate(title: "Select Date", didSelectDate: { (selectedDate) in
            // TODO: Your implementation for date
            
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let myString = formatter.string(from:selectedDate) // string purpose I add here
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "MM / dd / yyyy"
            // again convert your date to string
            let myStringafd = formatter.string(from: yourDate!)
            self.BirthdayString = myStringafd
            print(myStringafd)
            
            self.btnDate.setTitle(myStringafd, for: .normal)
        })
        
        // dateControllerOpen()
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender:UIButton){
        
        
        var gender = String()
        
        if genderselected == 1{
            gender = "Male"
        }
        else if genderselected == 2{
            gender = "Female"
        }
        else{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        
        if languageselected == 1{
            Language = "English"
        }
        else if languageselected == 2{
            Language = "traditional chinese"
        }
        else if languageselected == 3{
            Language = "chinese"
        }
        else{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        
        if regionselected == -1{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        
        if txtName.text == ""{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        if txtEmail.text == ""{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        if txtGendr.text == "Select Gender"{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        if txtSurname.text == ""{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        if txtLanguage.text == "Select Language"{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        if txtRegion.text == "Select Region"{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        if self.BirthdayString == ""{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        
        
        if(txtName.text == "" && txtEmail.text == "" && txtGendr.text == "Select Gender" && txtSurname.text == "" && txtLanguage.text == "Select Language" && txtRegion.text == "Select Region" && btnDate.titleLabel?.text == ""){
            self.showAlert(withMessage: BLANKFIELDMSG)
        }else{
            if (txtEmail.text?.isEmail)!{
                postProfileData()
            }else{
                self.showAlert(withMessage: INVALIDEMAIL)
            }
        }
    }
    func postProfileData() {
        
        var preferlanguage = String()
        if self.txtLanguage.text == "中文（繁體)"{
            preferlanguage = "traditional chinese"
        }
        else if self.txtLanguage.text == "中文（简体)"{
            preferlanguage = "chinese"
        }
        else if self.txtLanguage.text == "English"{
            preferlanguage = "en"
        }
        else{
            preferlanguage = ""
        }
        
        var gender = String()
        
        if genderselected == 1{
            gender = "Male"
        }
        else if genderselected == 2{
            gender = "Female"
        }
        else{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        
        if languageselected == 1{
            Language = "English"
        }
        else if languageselected == 2{
            Language = "traditional chinese"
        }
        else if languageselected == 3{
            Language = "chinese"
        }
        else{
            self.showAlert(withMessage: BLANKFIELDMSG)
            return
        }
        
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)", "surname":txtSurname.text!, "first_name":txtName.text!, "email":txtEmail.text!,"birthdate":self.BirthdayString,"gender":gender,"region":txtRegion.text!,"prefered_language":Language, "lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:SET_PROFILE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let _data = dict["data"] as? NSDictionary{
                        print(_data)
                        if let uid = _data["id"] as? Int{
                            UserDataHolder.sharedUser.userId = uid
                        }
                        if let fname  = _data["first_name"] as? String{
                            UserDataHolder.sharedUser.firstName = fname
                        }
                        if let lname  = _data["last_name"] as? String{
                            UserDataHolder.sharedUser.lastName = lname
                        }
                        if let surname  = _data["surname"] as? String{
                            UserDataHolder.sharedUser.surname = surname
                        }
                        if let phone  = _data["phone"] as? String{
                            UserDataHolder.sharedUser.phoneNumber = phone
                        }
                        if let gender  = _data["sex"] as? String{
                            UserDataHolder.sharedUser.gender = gender
                        }
                        if let email  = _data["email"] as? String{
                            UserDataHolder.sharedUser.email = email
                        }
                        if let region  = _data["region"] as? String{
                            UserDataHolder.sharedUser.region = region
                        }
                        
                        if((Constants.USERDEFAULTS.value(forKey: "isSocial")) != nil){
                            
                            Utility.sharedUtility.sideMenu()
                            UserDataHolder.sharedUser.isLoggedIn = true
                            
                        }
                        else{
                            let objSetPwdVC = storyBoard.instantiateViewController(withIdentifier: "SetPassWordVC") as? SetPassWordVC
                            self.navigationController?.pushViewController(objSetPwdVC!, animated: true)
                        }
                        
                        
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
            //            if((Constants.USERDEFAULTS .value(forKey: "isFirst")) != nil){
            //                Constants.USERDEFAULTS .removeObject(forKey: "isFirst")
            //                self.showHint()
            //            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    func dateControllerOpen(){
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        present(selector, animated: true, completion: nil)
        
        
    }
    
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "MM / dd / yyyy"
        let bdate1 = dateFormatter.string(from: date as Date)
        
        self.btnDate.setTitle(bdate1, for: .normal)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) ->Int {
        if(pickerView.tag == 0){
            return pickerGender.count
        }else if(pickerView.tag == 1){
            return countries.count
        }else{
            return pickerLanguage.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 0){
            return pickerGender[row].localized()
        }else if(pickerView.tag == 1){
            return countries[row]
        }else{
            return pickerLanguage[row].localized()
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 0){
            self.txtGendr.text =  pickerGender[row].localized()
            genderselected = row
        }else if(pickerView.tag == 1){
            self.txtRegion.text =  countries[row]
            regionselected = row
        }else{
            languageselected = row
            self.txtLanguage.text = pickerLanguage[row].localized()
        }
        
    }
}
