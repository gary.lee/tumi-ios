//
//  MobileNumberVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class MobileNumberVC: UIViewController{
    
    
    @IBOutlet weak var vwMobile:UIView!
    @IBOutlet weak var txtCountryCode:UITextField!
    @IBOutlet weak var txtMobileNo:UITextField!
    @IBOutlet weak var imgCheck:UIImageView!
    @IBOutlet weak var lblTandC:UILabel!
    var isAgree = Bool()
    var strMember = String()
    var ismember = String()
    
    var is_edm = String()
    var is_ims = String()
    var is_phone = String()
    var is_sms = String()
    var bdate = String()
    
    @IBOutlet var btnNext: UIButton!
    
    @IBOutlet var lbltitle: UILabel!
    
    @IBOutlet var lblcontent: UILabel!
    @IBOutlet var lblagree: UILabel!
    
    @IBOutlet var btnprivacy: UIButton!
    @IBOutlet var btntermscondition: UIButton!
    
    @IBOutlet var lbland: UILabel!
    
    
    var countryList = CountryList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryList.delegate = self
        
        self.lbltitle.text = "YOUR_MOBILE".localized()
        self.lblcontent.text = "We_will_send".localized()
        self.lblagree.text = "I_would_like_to_receiv".localized()
        self.lbltitle.text = "YOUR_MOBILE".localized()
        self.btnNext.setTitle("NEXT_yourMob".localized(), for: .normal)
        
        let codeplaceholder = "Code".localized()
        self.txtCountryCode.placeholder = codeplaceholder
        
        let mobileplaceholder = "MobileNumber".localized()
        self.txtMobileNo.placeholder = mobileplaceholder
        
        self.lbland.text = "and".localized()
        
        let privacytext = "PrivacyPolicy".localized()
        self.btnprivacy.setTitle(privacytext, for: .normal)
        
        
        let terms = "TermsConditions".localized()
        self.btntermscondition.setTitle(terms, for: .normal)
        
        self.btnprivacy.underline()
        self.btntermscondition.underline()
        
        isAgree = true
        
        self.is_edm = "1"
        self.is_ims = "1"
        self.is_phone = "1"
        self.is_sms = "1"
        self.setupUI()
        
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        if isAgree
        {
            self.imgCheck.image = UIImage(named: "checked")
        }
        self.vwMobile.layer.cornerRadius = 10.0
        self.vwMobile.clipsToBounds = true
        txtCountryCode.setLeftPaddingPoints(10.0)
        txtMobileNo.setLeftPaddingPoints(10.0)
        
        let text = "By_creating_an_account".localized()
        self.lblTandC.text = text
        
    }
    
    @IBAction func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (lblTandC.text)!
        let termsRange = (text as NSString).range(of: "Terms & Conditions")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if gesture.didTapAttributedTextInLabel(label: lblTandC, inRange: termsRange)
        {
            let objTermsCondVC = storyBoard.instantiateViewController(withIdentifier: "TermsCondVC") as? TermsCondVC
            self.navigationController?.pushViewController(objTermsCondVC!, animated: true)
            print("Tapped terms")
            
        }
        else if gesture.didTapAttributedTextInLabel(label: lblTandC, inRange: privacyRange)
        {
            let objPrivacyPolicyVC = storyBoard.instantiateViewController(withIdentifier: "PrivacyVC") as? PrivacyVC
            self.navigationController?.pushViewController(objPrivacyPolicyVC!, animated: true)
            print("Tapped privacy")
        }
        else
        {
            print("Tapped none")
            
        }
    }
    
    @IBAction func btnPrivacyPolicyTapped(_ sender:UIButton){
        let objPrivacyPolicyVC = storyBoard.instantiateViewController(withIdentifier: "PrivacyVC") as? PrivacyVC
        self.navigationController?.pushViewController(objPrivacyPolicyVC!, animated: true)
    }
    @IBAction func btnTAndCTapped(_ sender:UIButton){
        let objTermsCondVC = storyBoard.instantiateViewController(withIdentifier: "TermsCondVC") as? TermsCondVC
        self.navigationController?.pushViewController(objTermsCondVC!, animated: true)
    }
    @IBAction func actionAgree(_ sender:UIButton){
        if isAgree == false
        {
            isAgree =  true
            self.imgCheck.image = UIImage(named: "checked")
            
            is_edm = "1"
            is_ims = "1"
            is_phone = "1"
            is_sms = "1"
            
        }
        else
        {
            isAgree =  false
            self.imgCheck.image = UIImage(named: "unchecked")
            
            is_edm = "2"
            is_ims = "2"
            is_phone = "2"
            is_sms = "2"
        }
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
        //        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
        //            self.txtCountryCode.text = country.dialingCode
        //            //            self.btnCode.setTitle(country.dialingCode, for: .normal)
        //
        //        }
        //        // can customize the countryPicker here e.g font and color
        //        countryController.detailColor = UIColor.red
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
        
    }
    
    @IBAction func actionNext(_ sender:UIButton){
        if txtCountryCode.text == "" || txtMobileNo.text == "" {
            showAlert(withMessage: BLANKMOBILENUMBER)
        }
        else{
            
            if((Constants.USERDEFAULTS.value(forKey: "isSocial")) != nil){
                Social_signUp()
            }
            else{
                signUp()
            }
        }
    }
    
    
    func Social_signUp() {
        
        let code = txtCountryCode.text?.replacingOccurrences(of: "+", with: "")
        let phone = "+\(txtCountryCode.text!)\(txtMobileNo.text!)"
        
        if(strMember == "Yes"){
            ismember = "1"
        }
        else{
            ismember = "0"
        }
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)","phone":phone,
                          "lng":UserDataHolder.sharedUser.language,
                          "areacode":code!,
                          "phone_number":txtMobileNo.text!,"is_edm":is_edm,"is_phone":is_phone,"is_sms":is_sms,"is_ims":is_ims , "is_member":ismember, "prefered_language":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:SOCIAL_SIGNUP, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let uid = dict["user_id"] as? Int{
                        
                        UserDataHolder.sharedUser.userId = uid
                        let objVerifyVC = storyBoard.instantiateViewController(withIdentifier: "VerifyOTP") as? VerifyOTP
                        objVerifyVC?.isFrgtpwd = false
                        objVerifyVC?.strMember = self.strMember
                        objVerifyVC?.phoneNumber = "\(self.txtCountryCode.text!) \(self.txtMobileNo.text!)"
                        self.navigationController?.pushViewController(objVerifyVC!, animated: true)
                    }
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    func signUp() {
        
        let code = txtCountryCode.text?.replacingOccurrences(of: "+", with: "")
        let phone = "+\(txtCountryCode.text!)\(txtMobileNo.text!)"
        
        if(strMember == "Yes"){
            ismember = "1"
        }
        else{
            ismember = "0"
        }
        
        let parameters = ["phone":phone,
                          "lng":UserDataHolder.sharedUser.language,
                          "areacode":code!,
                          "phone_number":txtMobileNo.text!,"is_edm":is_edm,"is_phone":is_phone,"is_sms":is_sms,"is_ims":is_ims , "is_member":ismember, "prefered_language":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:REGISTER, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let uid = dict["user_id"] as? Int{
                        
                        UserDataHolder.sharedUser.userId = uid
                        let objVerifyVC = storyBoard.instantiateViewController(withIdentifier: "VerifyOTP") as? VerifyOTP
                        objVerifyVC?.isFrgtpwd = false
                        objVerifyVC?.strMember = self.strMember
                        objVerifyVC?.phoneNumber = "\(self.txtCountryCode.text!) \(self.txtMobileNo.text!)"
                        self.navigationController?.pushViewController(objVerifyVC!, animated: true)
                    }
                    
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
        //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension MobileNumberVC: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.name as Any)
        print(country.flag as Any)
        print(country.countryCode)
        print(country.phoneExtension)
        self.txtCountryCode.text = country.phoneExtension
        
    }
}
