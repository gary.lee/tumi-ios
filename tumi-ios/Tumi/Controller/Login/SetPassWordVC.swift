//
//  SetPassWordVC.swift
//  Tumi
//
//  Created by Redspark on 04/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class SetPassWordVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var vwSetPwd:UIView!
    @IBOutlet weak var imgCheck:UIImageView!
    @IBOutlet weak var imgCheck1:UIImageView!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    @IBOutlet weak var lblError:UILabel!
    @IBOutlet weak var lblHeight:NSLayoutConstraint!
    
    
    @IBOutlet var btnnext: UIButton!
    @IBOutlet var lbltitle: UILabel!
    
    @IBOutlet var lblcharcterlength: UILabel!
    @IBOutlet var lblpassword: UILabel!
    
    @IBOutlet var lblmustcontain: UILabel!
    @IBOutlet var lblconfirmpassword: UILabel!
    
    
    var isfrgt:Bool? = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        
        
       let confirmLng = "Confirm_Password".localized()
       let passwordLng = "Password".localized()
        
        self.lbltitle.text = "SET_PASSWORD".localized()
        
        self.lblconfirmpassword.text = confirmLng
        self.txtConfirmPassword.placeholder = confirmLng
        
        self.lblpassword.text = passwordLng
        self.txtPassword.placeholder = passwordLng
        
        self.lblcharcterlength.text = "6_16character".localized()
        self.lblmustcontain.text = "Must_include_Letter".localized()
        self.btnnext.setTitle("NEXT".localized(), for: .normal)
        
        
        // Do any additional setup after loading the view.
    }
    func setUpUI(){
        self.txtConfirmPassword.delegate = self
        self.txtPassword.delegate = self
        self.lblHeight.constant = 0.0
        self.lblError.text = ""
        self.vwSetPwd.layer.cornerRadius = 10.0
        self.vwSetPwd.clipsToBounds = true
        txtPassword.setLeftPaddingPoints(10.0)
        txtConfirmPassword.setLeftPaddingPoints(10.0)
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender:UIButton){
        if(txtPassword.text == "" && txtConfirmPassword.text == ""){
            self.lblHeight.constant = 15.0
            self.lblError.text = BLANKFIELDMSG
        }else{
            if(txtPassword.text != txtConfirmPassword.text){
                self.lblHeight.constant = 15.0
                self.lblError.text = PASSWORDINCONSISTENTMSG
            }else{
                self.lblHeight.constant = 0.0
                self.lblError.text = ""
                setPassword()
            }
        }
        //        Utility.sharedUtility.sideMenu()
    }
    func setPassword() {
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)",
            "password":txtPassword.text!, "lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:SET_PASSWORD, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    if(self.isfrgt)!{
                        for controller in self.navigationController!.viewControllers as Array {
                            if controller.isKind(of: LoginVC.self) {
                                self.navigationController!.popToViewController(controller, animated: true)
                                break
                            }
                        }
//                        let objLoginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
//                        self.navigationController?.pushViewController(objLoginVC!, animated: true)
                    }else{
                        UserDataHolder.sharedUser.isLoggedIn = true
                        UserDataHolder.sharedUser.password = self.txtPassword.text!
                        Utility.sharedUtility.sideMenu()
                    }
                   
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let str = textFieldText.replacingCharacters(in: range, with: string)

        if((str.count) >= 5 && (str.count) <= 16 ){
            self.imgCheck.image = UIImage(named:"Tick")
        }else{
            self.imgCheck.image = UIImage(named:"UnTick")
        }
        
        if(str.isValidPassword){
            self.imgCheck1.image = UIImage(named:"Tick")
        }else{
            self.imgCheck1.image = UIImage(named:"UnTick")
        }
        
        
        if(str.count < 6){
            self.imgCheck.image = UIImage(named:"UnTick")
            self.imgCheck1.image = UIImage(named:"UnTick")
        }
        
        return true
    }
    
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        let str = textField.text
//
//        if((str?.count)! >= 6 && (str?.count)! <= 16 ){
//            self.imgCheck.image = UIImage(named:"checked")
//        }else{
//            self.imgCheck.image = UIImage(named:"unchecked")
//        }
//        if(str?.isValidPassword)!{
//            self.imgCheck1.image = UIImage(named:"checked")
//        }else{
//            self.imgCheck1.image = UIImage(named:"unchecked")
//        }
//
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
