//
//  LoginVC.swift
//  Tumi
//
//  Created by Redspark on 03/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class LoginVC: UIViewController,UITextFieldDelegate  {
    
    @IBOutlet weak var vwLogin:UIView!
    @IBOutlet weak var txtMobile:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtCountryCode:UITextField!
    @IBOutlet weak var btnForgotpwd:UIButton!
    @IBOutlet weak var btnSignUp:UIButton!
    @IBOutlet weak var lblInvalid:UILabel!
    @IBOutlet weak var lblHeight:NSLayoutConstraint!
    
    
    @IBOutlet var lblpassword: UILabel!
    @IBOutlet var lblMobile: UILabel!
    @IBOutlet var lblorloginwith: UILabel!
    
    @IBOutlet var btnLogin: UIButton!
    
    var countryList = CountryList()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        
        countryList.delegate = self
        self.lblMobile.text = "Mobile".localized()
        self.lblpassword.text = "Password".localized()
        let placeholder = "Password".localized()
        self.txtPassword.placeholder = placeholder
        
        self.lblorloginwith.text = "or_login _with".localized()
        self.btnLogin.setTitle("LOGIN".localized(), for: .normal)
        let codeplaceholder = "Code".localized()
        self.txtCountryCode.placeholder = codeplaceholder
        
        let mobileplaceholder = "MobileNumber".localized()
        self.txtMobile.placeholder = mobileplaceholder
        
        //UserDataHolder.sharedUser.language = "English"
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Constants.USERDEFAULTS.removeObject(forKey: "isSocial")
    }
    
    func setUpUI(){
        self.vwLogin.layer.cornerRadius = 10.0
        self.vwLogin.clipsToBounds = true
        self.lblHeight.constant = 0.0
        self.txtMobile.delegate =  self
        self.txtPassword.delegate =  self
        txtCountryCode.setLeftPaddingPoints(10.0)
        txtMobile.setLeftPaddingPoints(10.0)
        txtPassword.setLeftPaddingPoints(10.0)
        
        var FormattedText = NSMutableAttributedString()
        
        let forgottext = "Forgot_password".localized()
        
        let donthaveaccount = "Do_not_have_an_account".localized()
        let signup = "SignUp".localized()
        
        //      FormattedText
        //        //  .bold("Forgot password?")
        FormattedText.normalWithUnderLine(forgottext)
        
        btnForgotpwd.setAttributedTitle(FormattedText, for: .normal)
        
        FormattedText = NSMutableAttributedString()
        FormattedText
            .normal(donthaveaccount)
            .bold(signup)
        btnSignUp.setAttributedTitle(FormattedText, for: .normal)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionLogin(_ sender:UIButton){
        
        if(txtMobile.text == "" || txtPassword.text == ""){
            self.lblInvalid.text = ""
            self.lblHeight.constant = 0.0
            self.showAlert(withMessage: BLANKFIELDMSG)
            //            self.lblHeight.constant = 25.0
            //            self.lblInvalid.text = BLANKFIELDMSG
        }else {
            self.lblInvalid.text = ""
            self.lblHeight.constant = 0.0
            login()
        }
    }
    
    func login() {
        
        let phone = "+\(txtCountryCode.text!)\(txtMobile.text!)"
        
        let parameters = ["phone":phone, "password":txtPassword.text!, "device_type":device_type, "device_token":UserDataHolder.sharedUser.deviceToken, "lng":UserDataHolder.sharedUser.language,"prefered_language":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:LOGIN, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let _data = dict["data"] as? NSDictionary{
                        print(_data)
                        if let uid = _data["id"] as? Int{
                            UserDataHolder.sharedUser.userId = uid
                        }
                        if let fname  = _data["first_name"] as? String{
                            UserDataHolder.sharedUser.firstName = fname
                        }
                        if let lname  = _data["last_name"] as? String{
                            UserDataHolder.sharedUser.lastName = lname
                        }
                        if let surname  = _data["surname"] as? String{
                            UserDataHolder.sharedUser.surname = surname
                        }
                        if let phone  = _data["phone"] as? String{
                            UserDataHolder.sharedUser.phoneNumber = phone
                        }
                        if let gender  = _data["sex"] as? String{
                            UserDataHolder.sharedUser.gender = gender
                        }
                        if let email  = _data["email"] as? String{
                            UserDataHolder.sharedUser.email = email
                        }
                        if let region  = _data["region"] as? String{
                            UserDataHolder.sharedUser.region = region
                        }
                        if let membership_level  = _data["membership_level"] as? String{
                            UserDataHolder.sharedUser.membership_level = membership_level
                        }
                        Utility.sharedUtility.sideMenu()
                        UserDataHolder.sharedUser.isLoggedIn = true
                    }
                }
            }
            else{
                self.lblHeight.constant = 0.0
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
                //
            }
            
            //            if((Constants.USERDEFAULTS .value(forKey: "isFirst")) != nil){
            //                Constants.USERDEFAULTS .removeObject(forKey: "isFirst")
            //                self.showHint()
            //            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    @IBAction func actionForgotPwd(_ sender:UIButton){
        let objForgotPwdShip = storyBoard.instantiateViewController(withIdentifier: "ForgotPwdVC") as? ForgotPwdVC
        self.navigationController?.pushViewController(objForgotPwdShip!, animated: true)
        //        let objForgotPwdShip = storyBoard.instantiateViewController(withIdentifier: "VerifyOTP") as? VerifyOTP
        //        self.navigationController?.pushViewController(objForgotPwdShip!, animated: true)
    }
    @IBAction func actionSignUp(_ sender:UIButton){
        let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "MemberShipVC") as? MemberShipVC
        self.navigationController?.pushViewController(objMemberShip!, animated: true)
    }
    @IBAction func actionFB(_ sender:UIButton){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    print(result as Any)
                    
                    let fbDetails = result as! NSDictionary
                    
                    let Fname = fbDetails.value(forKey: "first_name")
                    let Lname = fbDetails.value(forKey: "last_name")
                    let email = fbDetails.value(forKey: "email")
                    
                    let parameters = ["social":"1", "last_name":Lname, "first_name":Fname, "email":email, "device_type":device_type, "device_token":UserDataHolder.sharedUser.deviceToken, "lng":UserDataHolder.sharedUser.language]
                    
                    AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:SOCIAL_LOGIN, completion: { (json) in
                        
                        let dict = json as! NSDictionary
                        
                        if dict["status"] as? Int == 1
                        {
                            DispatchQueue.main.async {
                                
                                if dict["mobile_verify"] as? Int == 1
                                {
                                    if let uid = dict["user_id"] as? Int{
                                        UserDataHolder.sharedUser.userId = uid
                                    }
                                    
                                    Constants.USERDEFAULTS.set(true, forKey: "isSocial")
                                    let objMemberShip = storyBoard.instantiateViewController(withIdentifier: "MemberShipVC") as? MemberShipVC
                                    self.navigationController?.pushViewController(objMemberShip!, animated: true)
                                }
                                else{
                                    
                                    if let _data = dict["data"] as? NSDictionary{
                                        print(_data)
                                        if let uid = _data["id"] as? Int{
                                            UserDataHolder.sharedUser.userId = uid
                                        }
                                        if let fname  = _data["first_name"] as? String{
                                            UserDataHolder.sharedUser.firstName = fname
                                        }
                                        if let lname  = _data["last_name"] as? String{
                                            UserDataHolder.sharedUser.lastName = lname
                                        }
                                        if let surname  = _data["surname"] as? String{
                                            UserDataHolder.sharedUser.surname = surname
                                        }
                                        if let phone  = _data["phone"] as? String{
                                            UserDataHolder.sharedUser.phoneNumber = phone
                                        }
                                        if let gender  = _data["sex"] as? String{
                                            UserDataHolder.sharedUser.gender = gender
                                        }
                                        if let email  = _data["email"] as? String{
                                            UserDataHolder.sharedUser.email = email
                                        }
                                        if let region  = _data["region"] as? String{
                                            UserDataHolder.sharedUser.region = region
                                        }
                                        if let membership_level  = _data["membership_level"] as? String{
                                            UserDataHolder.sharedUser.membership_level = membership_level
                                        }
                                        Utility.sharedUtility.sideMenu()
                                        UserDataHolder.sharedUser.isLoggedIn = true
                                    }
                                }
                                
                            }
                        }
                        
                    }, failure: { (error) in
                        
                        print(error)
                    })
                    
                    
                }
            })
        }
    }
    @IBAction func actionWeChat(_ sender:UIButton){
        
        if WXApi.isWXAppInstalled() {
            let req = SendAuthReq()
            req.scope = "snsapi_userinfo" //Important that this is the same
            req.state = "com.tencent.mm" //This can be any random value
            // WXApi.send(req)
            if WXApi.sendAuthReq(req, viewController: self, delegate: self) {
            }
            
        } else {
            
            self.showAlert(withMessage: "Wechat is not install in your device".localized())
        }
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
        //        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
        //            self.txtCountryCode.text = country.dialingCode
        //            //            self.btnCode.setTitle(country.dialingCode, for: .normal)
        //
        //        }
        //        // can customize the countryPicker here e.g font and color
        //        countryController.detailColor = UIColor.red
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.lblInvalid.text = ""
        self.lblHeight.constant = 0.0
    }
    
}
extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        
        let attrs : [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(name: ProximaNova, size: 15)!,
            NSAttributedStringKey.foregroundColor : UIColor.lightGray,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue]
        let boldString = NSMutableAttributedString(string: text, attributes: attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func normal(_ text:String)->NSMutableAttributedString {
        let attrs : [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(name: ProximaNova, size: 17)!,
            NSAttributedStringKey.foregroundColor : UIColor.lightGray
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
    @discardableResult func normalWithUnderLine(_ text:String)->NSMutableAttributedString {
        let attrs : [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(name: ProximaNova, size: 17)!,
            NSAttributedStringKey.foregroundColor : UIColor.lightGray,
            NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue
        ]
        let normal =  NSAttributedString(string: text,  attributes:attrs)
        self.append(normal)
        return self
    }
}


extension LoginVC: WXApiDelegate {
    func onResp(_ resp: BaseResp) {
        
        print(resp)
    }
    
    func onReq(_ req: BaseReq) {
        print(req)
    }
}

extension LoginVC: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.name as Any)
        print(country.flag as Any)
        print(country.countryCode)
        print(country.phoneExtension)
        self.txtCountryCode.text = country.phoneExtension
    }
}
