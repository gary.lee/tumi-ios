//
//  MyAvatarVC.swift
//  Tumi
//
//  Created by Redspark on 17/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class MyAvatarVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func sidemenuTapped(_ sender: Any) {
        
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
}
