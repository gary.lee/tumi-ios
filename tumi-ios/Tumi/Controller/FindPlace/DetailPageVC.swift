
//
//  DetailPageVC.swift
//  Tumi
//
//  Created by Bhavin Joshi on 22/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class DetailPageVC: UIViewController {

      var arrayIndex = NSMutableArray()
    var Dict = NSDictionary()
    @IBOutlet var tblData: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblData.delegate = self
        self.tblData.dataSource = self
        self.tblData.reloadData()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnbackclick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension DetailPageVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrayIndex .contains(section)){
            
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HoursCell") as! HoursCell
        
        cell.selectionStyle = .none
        
        let arrHours = NSMutableArray()
        arrHours.addObjects(from: self.Dict.value(forKey: "open_hour") as! [Any])
        for i in 0..<arrHours.count{
            let dict = arrHours.object(at: i) as! NSDictionary
            let starttime = dict.value(forKey: "start_time") as! String
            let endtime = dict.value(forKey: "end_time") as! String
            if i == 0{
                cell.lblMonday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 1{
                cell.lblTuesday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 2{
                cell.lblwednesday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 3{
                cell.lblthrusday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 4{
                cell.lblfriday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 5{
                cell.lblsaturday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 6{
                cell.lblSun.text = "\(starttime) AM - \(endtime) PM"
            }
            
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 200))
        headerView.backgroundColor = UIColor.white
        let cell = tableView.dequeueReusableCell(withIdentifier: "FindPlaceListCell") as! FindPlaceListCell
        cell.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 200)
        
        //let dict = self.arrList.object(at: section) as! NSDictionary
        
        cell.lbladdress1.text = self.Dict.value(forKey: "address1") as? String
        cell.lbladress2.text = self.Dict.value(forKey: "address2") as? String
        cell.lbladdress3.text = self.Dict.value(forKey: "address3") as? String
        cell.lblcity.text = self.Dict.value(forKey: "country") as? String
        cell.lblzipcode.text = self.Dict.value(forKey: "phone") as? String
        cell.lblplacetitle.text = self.Dict.value(forKey: "title") as? String
        cell.lblViewDetail.underline()
        cell.lblGerdirection.underline()
        
        cell.btnExpand.tag = section
        cell.btnExpand.addTarget(self, action:#selector(self.ExpandTapped(_:)), for: .touchUpInside)
        
        cell.btnRedirect.tag = section
        cell.btnRedirect.addTarget(self, action:#selector(self.RedirectGooglemap(_:)), for: .touchUpInside)
        
        headerView.addSubview(cell)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 233
    }
    
    @objc func ExpandTapped(_ sender: UIButton) {
        
        if(arrayIndex .contains(sender.tag)){
            arrayIndex.remove(sender.tag)
            self.tblData.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
        }
        else{
            
            if(arrayIndex.count > 0){
                
                let value = self.arrayIndex.object(at: 0) as! Int
                self.arrayIndex.remove(value)
                self.tblData.reloadSections(IndexSet(integer: value), with: .automatic)
                self.arrayIndex.add(sender.tag)
                self.tblData.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
            else{
                
                arrayIndex.add(sender.tag)
                self.tblData.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tblData.reloadData()
        }
        
    }
    
    
    
    @objc func RedirectGooglemap(_ sender: UIButton) {
        
       // let dict = self.arrList.object(at: sender.tag) as! NSDictionary
        
        let lattitude = self.Dict.value(forKey: "latitude") as! String
        let longitude = self.Dict.value(forKey: "longitude") as! String
        
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            UIApplication.shared.openURL(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(Double(lattitude)!),\(Double(longitude)!)&directionsmode=driving")! as URL)
            
        } else {
            NSLog("Can't use comgooglemaps://");
        }
    }
    
    
    
}
