//
//  MapView.swift
//  Tumi
//
//  Created by Bhavin Joshi on 18/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import MapKit

let MINIMUM_ZOOM_ARC = 0.05
let ANNOTATION_REGION_PAD_FACTOR = 1.15
let MAX_DEGREES_ARC = 360
func degreesToRadians(x: Double) -> Double {
    return .pi * x / 180.0
}
func radiandsToDegrees(x: Double) -> Double {
    return x * 180.0 / .pi
}

class MapView: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var arrList = NSMutableArray()
    @IBOutlet var txtzipcodecity: UITextField!
    @IBOutlet var mapview: MKMapView!
    var objPage = FindPlace()
    
    @IBOutlet var lblchooselocation: UILabel!
    
    var pinImage = UIImage(named: "pin1")
    var locationManager =  CLLocationManager()
    var currentCoordinate = CLLocationCoordinate2DMake(-26.208966,28.052314)
    var places = PlaceMark().current
    @IBOutlet var btnTumistore: UIButton!
    
    @IBOutlet var btntumioutlet: UIButton!
    @IBOutlet var btnretailer: UIButton!
    var lati : Double?
    var longi : Double?
    
    var Storname = String()
    var storeAddress = String()
    var storetype = String()
    
    var btumistore = Bool()
    var boutlet = Bool()
    var btumiretailer = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtzipcodecity.setLeftPaddingPoints(15)
        
        self.mapview.isUserInteractionEnabled = false
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        btumistore = true
        boutlet = false
        btumiretailer = false
        
        self.mapview.delegate = self
        self.mapview.isZoomEnabled = true
        self.mapview.isUserInteractionEnabled = true
        self.mapview.isScrollEnabled = true
        
        self.btntumioutlet.setTitleColor(UIColor.black, for: UIControlState.normal)
        self.btnTumistore.setTitleColor(UIColor.red, for: UIControlState.normal)
        self.btnretailer.setTitleColor(UIColor.black, for: UIControlState.normal)
        
        self.lblchooselocation.text = "CHOOSEOURLOCATION".localized()
        self.btnTumistore.setTitle("TUMISTORES".localized(), for: .normal)
        self.btnretailer.setTitle("RETAILERS".localized(), for: .normal)
        
        let placeholder = "ZIPCODE/CITY".localized()
        self.txtzipcodecity.placeholder = placeholder
        
        self.storetype = "Tumi Stores"
        self.pinImage = UIImage(named: "pin1")
        // Do any additional setup after loading the view.
        
        self.getCategories(SaerchTXT:"")
        
    }
    
    
    @IBAction func btnMapClick(_ sender: Any) {
    }
    
    @IBAction func btnnextclick(_ sender: Any) {
        self.txtzipcodecity.resignFirstResponder()
        
        if self.txtzipcodecity.text == ""{
            self.showAlert(withMessage: "Please enter city.")
            return
        }
        
        self.getCategories(SaerchTXT: self.txtzipcodecity.text!)
    }
    
    func getCategories(SaerchTXT : String) {
        
        var categotytype = String()
        
        if btumistore{
            if categotytype == ""{
                categotytype = "Tumi Stores"
            }
            else{
                categotytype = "\(categotytype),Tumi Stores"
            }
        }
        if boutlet{
            if categotytype == ""{
                categotytype = "Tumi Outlets"
            }
            else{
                categotytype = "\(categotytype),Tumi Outlets"
            }
        }
        if btumiretailer{
            if categotytype == ""{
                categotytype = "Retailers"
            }
            else{
                categotytype = "\(categotytype),Retailers"
            }
        }
        if btumistore{
            if categotytype == ""{
                categotytype = "Tumi Stores"
            }
        }
        
        let patameter : [String : Any] = [
            "lng" : UserDataHolder.sharedUser.language,
            "search_text" : SaerchTXT,
            "store_type" : categotytype
        ]
        
        AFWrapper.postMethod(params: patameter as [String : AnyObject], apikey:ONLINE_STORE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async
                    {
                        self.arrList = NSMutableArray()
                        self.arrList.addObjects(from: dict.value(forKey: "data") as! [Any])
                        print("Count:",self.arrList.count)
                        self.mapview.removeAnnotations(self.mapview.annotations)
                        self.setannotation()
                }
            }
            else{
                if let message = dict["message"] as? String{
                    
                    self.mapview.removeAnnotations(self.mapview.annotations)
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    func setannotation(){
        
        if self.arrList.count > 0{
            
            for i in 0..<self.arrList.count{
                
                DispatchQueue.global(qos: .background).async {
                    let dict = self.arrList.object(at: i) as! NSDictionary
                    
                    let lattitude = dict.value(forKey: "latitude") as! String
                    let longitude = dict.value(forKey: "longitude") as! String
                    let storeName = dict.value(forKey: "title") as! String
                    let storeAddress = dict.value(forKey: "address2") as! String
                    let loc : CLLocation = CLLocation.init(latitude:Double(lattitude)!, longitude:Double(longitude)!)
                    
                    DispatchQueue.main.async {
                        // self.setupPinForCurrentLocation(location: location,index: i)
                        
                        self.places += PlaceMark().loadAndPrepareData(strName: String(storeName), strSubName: storeAddress, strLat: loc.coordinate.latitude, strLong: loc.coordinate.longitude, index: i) as! [PlaceMarkLocation]
                        self.mapview.addAnnotation(self.places.last!)
                        
                        DispatchQueue.main.async {
                            //self.mapview.removeAnnotation(self.places.last!)
                            // self.mapview.addAnnotation(self.places.last!)
                            // self.mapview .setCenter(loc.coordinate, animated: true)
                            
                            if(self.txtzipcodecity.text?.count == 0){
                                let location = CLLocationCoordinate2DMake(22.278354, 114.182534)
                                let span = MKCoordinateSpanMake(0.1, 0.1)
                                let region =  MKCoordinateRegionMake(location, span)
                                self.mapview.setRegion(region, animated: true)
                            }
                            else{
                                self.centerMap()
                            }
                        }
                    }
                }
            }
            
            // self.centerMap()
            print("annotations Count:",self.mapview.annotations.count)
        }
        
        //self.mapview.setNeedsDisplay()
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let annotation = annotation as? PlaceMarkLocation{
            if let view = mapView.dequeueReusableAnnotationView(withIdentifier: annotation.identifier){
                return view
            }else{
                let view = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.identifier)
                view.image = pinImage
                view.isEnabled = true
                view.canShowCallout = true
                
                //                var placeMark = annotation as? PlaceMark
                //
                //                let btn = UIButton(type: .detailDisclosure)
                //                view.rightCalloutAccessoryView = btn
                //
                return view
                
            }
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            let dict = self.arrList.object(at: control.tag) as! NSDictionary
            let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPageVC") as? DetailPageVC
            DetailPage?.Dict = dict
            objPage.navigationController?.pushViewController(DetailPage!, animated: true)
            print("button tapped")
        }
    }
    
    @IBAction func btntumioutlet(_ sender: Any) {
        
        if boutlet
        {
            self.btntumioutlet.setTitleColor(UIColor.black, for: UIControlState.normal)
            boutlet = false
        }
        else{
            self.btntumioutlet.setTitleColor(UIColor.red, for: UIControlState.normal)
            boutlet = true
        }
        
        if(txtzipcodecity.text?.count == 0){
            
            self.getCategories(SaerchTXT:"")
        }
        else{
            self.getCategories(SaerchTXT: txtzipcodecity.text!)
        }
    }
    @IBAction func btnTumestoreClick(_ sender: Any)
    {
        
        if btumistore{
            self.btnTumistore.setTitleColor(UIColor.black, for: UIControlState.normal)
            btumistore = false
        }
        else{
            self.btnTumistore.setTitleColor(UIColor.red, for: UIControlState.normal)
            btumistore = true
        }
        
        
        if(txtzipcodecity.text?.count == 0){
            
            self.getCategories(SaerchTXT:"")
        }
        else{
            self.getCategories(SaerchTXT: txtzipcodecity.text!)
        }
        
    }
    
    @IBAction func btnretalerclick(_ sender: Any) {
        
        if btumiretailer{
            self.btnretailer.setTitleColor(UIColor.black, for: UIControlState.normal)
            btumiretailer = false
        }
        else{
            self.btnretailer.setTitleColor(UIColor.red, for: UIControlState.normal)
            btumiretailer = true
        }
        
        if(txtzipcodecity.text?.count == 0){
            
            self.getCategories(SaerchTXT:"")
        }
        else{
            self.getCategories(SaerchTXT: txtzipcodecity.text!)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.mapview.removeAnnotations(self.mapview.annotations)
        let location = locations.last! as CLLocation
        if location.coordinate.latitude != 0 && location.coordinate.longitude != 0 {
            locationManager .stopUpdatingLocation()
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.9, longitudeDelta: 0.9))
            self.mapview.setRegion(region, animated: true)
            // self .setupPinForCurrentLocation(location: location, index: 0)
        }
    }
    
    
    func setupPinForCurrentLocation(location: CLLocation, index : Int){
        
        //        DispatchQueue.main.async {
        DispatchQueue.global(qos: .background).async {
            let geo: CLGeocoder = CLGeocoder()
            
            let loc: CLLocation = CLLocation(latitude:location.coordinate.latitude, longitude: location.coordinate.longitude)
            self.currentCoordinate = loc.coordinate
            var addressString : String = ""
            var addressSubString : String = ""
            
            geo.cancelGeocode()
            geo.reverseGeocodeLocation(loc, completionHandler:
                {(placemarks, error) in
                    if (error != nil)
                    {
                        print(error?.localizedDescription)
                        addressString = ""
                        addressSubString = ""
                        return
                    }
                    let pm = placemarks
                    
                    if pm!.count > 0 {
                        let pm = placemarks![0]
                        
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + " "
                        }
                        if pm.locality != nil {
                            addressSubString = addressSubString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressSubString = addressSubString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressSubString = addressSubString + pm.postalCode! + " "
                        }
                        
                        if addressString.count == 0 {
                            addressString = addressSubString
                        }
                    }
                    
                    
                    let dict = self.arrList.object(at: index) as! NSDictionary
                    
                    let storename = dict.value(forKey: "title") as! String
                    let storeAddress = dict.value(forKey: "address2") as! String
                    
                    
                    self.lati = loc.coordinate.latitude
                    self.longi = loc.coordinate.longitude
                    self.places += PlaceMark().loadAndPrepareData(strName: String(storename), strSubName: storeAddress, strLat: loc.coordinate.latitude, strLong: loc.coordinate.longitude, index: index) as! [PlaceMarkLocation]
                    DispatchQueue.main.async {
                        self.mapview.addAnnotation(self.places.last!)
                        
                        self.mapview.setCenter(loc.coordinate, animated: true)
                        self .centerMap()
                    }
            })
            //        }
            
        }
    }
    
    func centerMap() {
        let annotations = mapview?.annotations
        let count: Int? = mapview?.annotations.count
        if count == 0 {
            return
        }
        var points = [MKMapPoint](repeating: MKMapPoint(), count: count!)
        for i in 0..<(count ?? 0) {
            let coordinate: CLLocationCoordinate2D? = annotations?[i].coordinate
            points[i] = MKMapPointForCoordinate(coordinate!)
        }
        let mapRect: MKMapRect = (MKPolygon(points: points, count: count ?? 0)).boundingMapRect
        var region: MKCoordinateRegion = MKCoordinateRegionForMapRect(mapRect)
        region.span.latitudeDelta *= ANNOTATION_REGION_PAD_FACTOR
        region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR
        
        if region.span.latitudeDelta > Double(MAX_DEGREES_ARC) {
            region.span.latitudeDelta = CLLocationDegrees(MAX_DEGREES_ARC)
        }
        if region.span.longitudeDelta > Double(MAX_DEGREES_ARC) {
            region.span.longitudeDelta = CLLocationDegrees(MAX_DEGREES_ARC)
        }
        if region.span.latitudeDelta < MINIMUM_ZOOM_ARC {
            region.span.latitudeDelta = MINIMUM_ZOOM_ARC
        }
        if region.span.longitudeDelta < MINIMUM_ZOOM_ARC {
            region.span.longitudeDelta = MINIMUM_ZOOM_ARC
        }
        if count == 1 {
            region.span.latitudeDelta = MINIMUM_ZOOM_ARC
            region.span.longitudeDelta = MINIMUM_ZOOM_ARC
        }
        mapview?.setRegion(region, animated: true)
    }
}
