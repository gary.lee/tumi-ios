//
//  FindPlaceListCell.swift
//  Tumi
//
//  Created by Bhavin Joshi on 18/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class FindPlaceListCell: UITableViewCell {

    @IBOutlet var btnRedirect: UIButton!
    @IBOutlet var lblgetdirection: UILabel!
    
    @IBOutlet var lblviewdetail: UILabel!
    @IBOutlet var btnExpand: UIButton!
    @IBOutlet var lblGerdirection: UILabel!
    @IBOutlet var lblViewDetail: UILabel!
    @IBOutlet var lblMiles: UILabel!
    @IBOutlet var lblzipcode: UILabel!
    @IBOutlet var lblcity: UILabel!
    @IBOutlet var lbladdress3: UILabel!
    @IBOutlet var lbladress2: UILabel!
    @IBOutlet var lbladdress1: UILabel!
    @IBOutlet var lblplacetitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
