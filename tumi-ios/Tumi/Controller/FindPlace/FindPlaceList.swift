//
//  FindPlaceList.swift
//  Tumi
//
//  Created by Bhavin Joshi on 18/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class FindPlaceList: UIViewController {
    var arrList = NSMutableArray()
    var parentpage = FindPlace()
    @IBOutlet var tblList: UITableView!
    @IBOutlet var txtZipcode: UITextField!
    @IBOutlet var btnTumistore: UIButton!
    @IBOutlet var btntumioutlet: UIButton!
    @IBOutlet var btnretailer: UIButton!
    
    @IBOutlet var lblchooselocation: UILabel!
    
    var btumistore = Bool()
    var boutlet = Bool()
    var btumiretailer = Bool()
    
    
    
    var arrayIndex = NSMutableArray()
    var storetype = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtZipcode.setLeftPaddingPoints(15)
        self.tblList.register(UINib(nibName: "FindPlaceListCell", bundle: nil), forCellReuseIdentifier: "FindPlaceListCell")
        self.tblList.register(UINib(nibName: "HoursCell", bundle: nil), forCellReuseIdentifier: "HoursCell")
        
        self.btntumioutlet.setTitleColor(UIColor.black, for: UIControlState.normal)
        self.btnTumistore.setTitleColor(UIColor.red, for: UIControlState.normal)
        self.btnretailer.setTitleColor(UIColor.black, for: UIControlState.normal)
        
        
        self.lblchooselocation.text = "CHOOSEOURLOCATION".localized()
        self.btnTumistore.setTitle("TUMISTORES".localized(), for: .normal)
        self.btnretailer.setTitle("RETAILERS".localized(), for: .normal)
        
        let placeholder = "ZIPCODE/CITY".localized()
        self.txtZipcode.placeholder = placeholder
        
        
        self.btnTumistore.setTitleColor(UIColor.red, for: UIControlState.normal)
        self.btnretailer.setTitleColor(UIColor.black, for: UIControlState.normal)
        
        
        btumistore = true
        boutlet = false
        btumiretailer = false
        
        self.storetype = "Tumi Store"
        
        self.getCategories(SaerchTXT:"Hong Kong")
        
    }
    @IBAction func btnnextclick(_ sender: Any) {
        
        self.txtZipcode .resignFirstResponder()
        
        if self.txtZipcode.text == ""{
            self.showAlert(withMessage: "Please enter city.")
            return
        }
        
        self.getCategories(SaerchTXT: self.txtZipcode.text!)
    }
    
    @IBAction func btntumioutlet(_ sender: Any) {
        
        if boutlet
        {
            self.btntumioutlet.setTitleColor(UIColor.black, for: UIControlState.normal)
            boutlet = false
        }
        else{
            self.btntumioutlet.setTitleColor(UIColor.red, for: UIControlState.normal)
            boutlet = true
        }
        if(txtZipcode.text?.count == 0){
            
            self.getCategories(SaerchTXT:"Hong Kong")
        }
        else{
            self.getCategories(SaerchTXT: txtZipcode.text!)
        }
    }
    @IBAction func btnTumestoreClick(_ sender: Any)
    {
        
        if btumistore{
            self.btnTumistore.setTitleColor(UIColor.black, for: UIControlState.normal)
            btumistore = false
        }
        else{
            self.btnTumistore.setTitleColor(UIColor.red, for: UIControlState.normal)
            btumistore = true
        }
        
        if(txtZipcode.text?.count == 0){
            
            self.getCategories(SaerchTXT:"Hong Kong")
        }
        else{
            self.getCategories(SaerchTXT: txtZipcode.text!)
        }
        
    }
    
    @IBAction func btnretalerclick(_ sender: Any) {
        
        if btumiretailer{
            self.btnretailer.setTitleColor(UIColor.black, for: UIControlState.normal)
            btumiretailer = false
        }
        else{
            self.btnretailer.setTitleColor(UIColor.red, for: UIControlState.normal)
            btumiretailer = true
        }
        if(txtZipcode.text?.count == 0){
            
            self.getCategories(SaerchTXT:"Hong Kong")
        }
        else{
            self.getCategories(SaerchTXT: txtZipcode.text!)
        }
    }
    
    func getCategories(SaerchTXT : String) {
        
        
        var categotytype = String()
        
        if btumistore{
            if categotytype == ""{
                categotytype = "Tumi Stores"
            }
            else{
                categotytype = "\(categotytype),Tumi Stores"
            }
        }
        if boutlet{
            if categotytype == ""{
                categotytype = "Tumi Outlets"
            }
            else{
                categotytype = "\(categotytype),Tumi Outlets"
            }
        }
        if btumiretailer{
            if categotytype == ""{
                categotytype = "Retailers"
            }
            else{
                categotytype = "\(categotytype),Retailers"
            }
        }
        
        let patameter : [String : Any] = [
            "lng" : UserDataHolder.sharedUser.language,
            "search_text" : SaerchTXT,
            "store_type" : categotytype
        ]
        
        
        AFWrapper.postMethod(params: patameter as [String : AnyObject], apikey:ONLINE_STORE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async
                    {
                        self.tblList.isHidden = false
                        self.arrList = NSMutableArray()
                        self.arrList.addObjects(from: dict.value(forKey: "data") as! [Any])
                        self.tblList.delegate = self
                        self.tblList.dataSource = self
                        self.tblList.reloadData()
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.tblList.isHidden = true
                    self.showAlert(withMessage: message)
                }
            }
            
            
        }, failure: { (error) in
            
            print(error)
        })
        
    }
    
}
extension FindPlaceList : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrayIndex .contains(section)){
            
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HoursCell") as! HoursCell
        
        cell.selectionStyle = .none
        let dict = self.arrList.object(at: indexPath.section) as! NSDictionary
        let arrHours = NSMutableArray()
        arrHours.addObjects(from: dict.value(forKey: "open_hour") as! [Any])
        for i in 0..<arrHours.count{
            let dict = arrHours.object(at: i) as! NSDictionary
            let starttime = dict.value(forKey: "start_time") as! String
            let endtime = dict.value(forKey: "end_time") as! String
            if i == 0{
                cell.lblMonday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 1{
                cell.lblTuesday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 2{
                cell.lblwednesday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 3{
                cell.lblthrusday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 4{
                cell.lblfriday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 5{
                cell.lblsaturday.text = "\(starttime) AM - \(endtime) PM"
            }
            if i == 6{
                cell.lblSun.text = "\(starttime) AM - \(endtime) PM"
            }
            
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 200))
        headerView.backgroundColor = UIColor.white
        let cell = tableView.dequeueReusableCell(withIdentifier: "FindPlaceListCell") as! FindPlaceListCell
        cell.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 200)
        
        let dict = self.arrList.object(at: section) as! NSDictionary
        
        cell.lbladdress1.text = dict.value(forKey: "address1") as? String
        cell.lbladress2.text = dict.value(forKey: "address2") as? String
        cell.lbladdress3.text = dict.value(forKey: "address3") as? String
        cell.lblcity.text = dict.value(forKey: "country") as? String
        cell.lblzipcode.text = dict.value(forKey: "phone") as? String
        cell.lblplacetitle.text = dict.value(forKey: "title") as? String
        cell.lblViewDetail.underline()
        cell.lblGerdirection.underline()
        
        cell.btnExpand.tag = section
        cell.btnExpand.addTarget(self, action:#selector(self.ExpandTapped(_:)), for: .touchUpInside)
        
        cell.btnRedirect.tag = section
        cell.btnRedirect.addTarget(self, action:#selector(self.RedirectGooglemap(_:)), for: .touchUpInside)
        
        headerView.addSubview(cell)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 200
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 233
    }
    
    @objc func ExpandTapped(_ sender: UIButton) {
        
        if(arrayIndex .contains(sender.tag)){
            arrayIndex.remove(sender.tag)
            self.tblList.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
        }
        else{
            
            if(arrayIndex.count > 0){
                
                let value = self.arrayIndex.object(at: 0) as! Int
                self.arrayIndex.remove(value)
                self.tblList.reloadSections(IndexSet(integer: value), with: .automatic)
                self.arrayIndex.add(sender.tag)
                self.tblList.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
            else{
                
                arrayIndex.add(sender.tag)
                self.tblList.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.tblList.reloadData()
        }
        
    }
    
    
    
    @objc func RedirectGooglemap(_ sender: UIButton) {
        
        let dict = self.arrList.object(at: sender.tag) as! NSDictionary
        
        let lattitude = dict.value(forKey: "latitude") as! String
        let longitude = dict.value(forKey: "longitude") as! String
        
        UserDataHolder.sharedUser.lattitude = lattitude
        UserDataHolder.sharedUser.longitude = longitude
        NotificationCenter.default.post(name: Notification.Name("OpenMap"), object: nil)
    }
    
    
    
}

extension UILabel {
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
}
