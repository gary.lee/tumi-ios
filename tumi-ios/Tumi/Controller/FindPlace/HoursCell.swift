//
//  HoursCell.swift
//  Tumi
//
//  Created by Bhavin Joshi on 18/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class HoursCell: UITableViewCell {

    @IBOutlet var lblsaturday: UILabel!
    @IBOutlet var lblfriday: UILabel!
    @IBOutlet var lblthrusday: UILabel!
    @IBOutlet var lblwednesday: UILabel!
    @IBOutlet var lblSun: UILabel!
    @IBOutlet var lblTuesday: UILabel!
    @IBOutlet var lblMonday: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
