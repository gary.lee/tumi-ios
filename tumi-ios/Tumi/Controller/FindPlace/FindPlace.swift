//
//  FindPlace.swift
//  Tumi
//
//  Created by Bhavin Joshi on 18/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class FindPlace: UIViewController,CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        self.Initialization()
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("OpenMap"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.OpenMap(notification:)), name: Notification.Name("OpenMap"), object: nil)
    }

    
    @objc func OpenMap(notification: Notification) {
        
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(Double(UserDataHolder.sharedUser.lattitude)!),\(Double(UserDataHolder.sharedUser.longitude)!)&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }
            else {
                print("Can't use comgooglemaps://")
            }
        }
        
    }
    
    func Initialization()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "MapView") as? MapView
        controller1?.title = "MAPVIEW".localized()
        //controller1?.objPage = self
        //controller1?.parentpage = self
        //controller1?.response = self.Response
        //controller1?.Parameter = self.Parameter
        controllerArray.append(controller1!)
        
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "FindPlaceList") as? FindPlaceList
        controller2?.title = "LISTVIEW".localized()
        //controller2?.parentpage = self
        controllerArray.append(controller2!)
        
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .bottomMenuHairlineColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuHeight(50.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        if Device.DeviceType.IS_IPHON_X{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.bounds.size.width, height: self.view.frame.size.height-130), pageMenuOptions: parameters)
        }
        else{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.bounds.size.width, height: self.view.frame.size.height-130 ), pageMenuOptions: parameters)
        }
        
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu?.currentPageIndex = 0
        self.view.addSubview(pageMenu!.view)

    }
    
}
