//
//  MemberShipViewController.swift
//  Tumi
//
//  Created by Redspark on 19/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class MemberShipViewController: UIViewController,CAPSPageMenuDelegate {
    
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.Initialization()
    }
    
    func Initialization()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "MyAccountVC") as? MyAccountVC
        controller1?.title = "MYACCOUNT".localized()
        controllerArray.append(controller1!)
        
        
        let controller2 = storyboard.instantiateViewController(withIdentifier: "ExclusiveClubVC") as? ExclusiveClubVC
        controller2?.title = "EXCLUSIVESCLUB".localized()
        //controller2?.parentpage = self
        controllerArray.append(controller2!)
        
//        let controller3 = storyboard.instantiateViewController(withIdentifier: "PurchaseHistoryVC") as? PurchaseHistoryVC
//        controller3?.title = "Purchase_history".localized()
//        //controller2?.parentpage = self
//        controllerArray.append(controller3!)
        
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(0),
            .scrollMenuBackgroundColor(UIColor.white),
            .useMenuLikeSegmentedControl(true),
            .bottomMenuHairlineColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor(red: 180.0/255.0, green: 0.0/255.0, blue: 10.0/255.0, alpha: 1.0)),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .menuHeight(50.0),
            .menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        if Device.DeviceType.IS_IPHON_X{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.frame.size.width, height: self.view.frame.size.height-130), pageMenuOptions: parameters)
        }
        else{
            pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 130.0, width: self.view.frame.size.width, height: self.view.frame.size.height-130 ), pageMenuOptions: parameters)
        }
        
        // Optional delegate
        pageMenu!.delegate = self
        pageMenu?.currentPageIndex = 0
        self.view.addSubview(pageMenu!.view)
    }
    
}
