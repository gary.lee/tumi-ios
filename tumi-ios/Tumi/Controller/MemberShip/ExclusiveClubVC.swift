//
//  ExclusiveClubVC.swift
//  Tumi
//
//  Created by Redspark on 19/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import WebKit

class ExclusiveClubVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet var tbl_club: UITableView!
    
    var arrayIndex = NSMutableArray()
    var arrayData = NSMutableArray()
    
    var contentHeights : [CGFloat] = []
    var heightOfWebview=0
    
    var isFromLoadRequest :Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //        self.tbl_club.register(UITableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        self.tbl_club.register(UINib(nibName: "MembershipCell", bundle: nil), forCellReuseIdentifier: "MembershipCell")
        self.callAPI()
    }
    
    
    func callAPI()  {
        
        let parameters = ["lng":UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:MEMBERSHIP_CONTENT, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                
                self.arrayData.addObjects(from: dict.value(forKey: "data") as! [Any])
                
                self.isFromLoadRequest = true
                self.tbl_club.delegate = self
                self.tbl_club.dataSource = self
                self.tbl_club.reloadData()
                
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
}

extension ExclusiveClubVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(arrayIndex .contains(section)){
            
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MembershipCell", for: indexPath) as? MembershipCell
        
        DispatchQueue.main.async {
            cell?.viewController = self
            cell?.layer.shouldRasterize = true
            cell?.layer.rasterizationScale = UIScreen.main.scale
            cell?.selectionStyle = .none
            
            let dict = self.arrayData.object(at: indexPath.section) as! NSDictionary
            let strContent = dict.value(forKey: "text") as! String
            
            if self.isFromLoadRequest {
                cell?.webView.loadHTMLString(strContent, baseURL: nil)
            }else{
                cell?.webView.delegate = cell
            }
            
            cell?.tag = indexPath.section
            cell?.strContent = strContent
            
            cell?.setSelected(true, animated: true)
        }
        
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        
        let dict = arrayData.object(at: section) as! NSDictionary
        
        let headerLabel = UILabel(frame: CGRect(x: 35, y: 18, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: Constants.FONT, size: 18)
        headerLabel.textColor = UIColor.black
        headerLabel.text = dict.value(forKey: "label") as? String
        headerLabel.sizeToFit()
        
        let downImg = UIImageView(frame: CGRect(x: tableView.bounds.size.width - 30, y: 30, width: 10, height: 10))
        downImg.image = UIImage(named: "down")
        if(arrayIndex .contains(section)){
            downImg.image = UIImage(named: "up")
        }
        
        downImg.contentMode = .scaleAspectFit
        headerView.addSubview(downImg)
        headerView.addSubview(headerLabel)
        
        let SepLabel = UILabel(frame: CGRect(x: 18, y: 5, width:
            5, height: 50))
        SepLabel.backgroundColor = UIColor(red: 180/255.0, green: 0/255.0, blue: 10/255.0, alpha: 1.0)
        
        let btnExpand = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
        btnExpand.tag = section
        btnExpand.addTarget(self, action:#selector(self.ExpandTapped(_:)), for: .touchUpInside)
        
        headerView.addSubview(SepLabel)
        headerView.addSubview(btnExpand)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let cell2 = tableView.cellForRow(at: indexPath) as? MembershipCell
        //return UITableViewAutomaticDimension
        print("cell height--> \(cell2?.webView.scrollView.contentSize.height ?? 150)")
        return cell2?.webView.scrollView.contentSize.height ?? 150
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    @objc func ExpandTapped(_ sender: UIButton) {
        isFromLoadRequest = true
        if(arrayIndex .contains(sender.tag)){
            arrayIndex.remove(sender.tag)
            tbl_club.beginUpdates()
            self.tbl_club.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
            tbl_club.endUpdates()
        }
        else{
            
            if(arrayIndex.count > 0){
                
                let value = self.arrayIndex.object(at: 0) as! Int
                self.arrayIndex.remove(value)
                tbl_club.beginUpdates()
                
                self.tbl_club.reloadSections(IndexSet(integer: value), with: .automatic)
                tbl_club.endUpdates()
                
                self.arrayIndex.add(sender.tag)
                tbl_club.beginUpdates()
                
                self.tbl_club.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
                tbl_club.endUpdates()
                
            }
            else{
                
                arrayIndex.add(sender.tag)
                tbl_club.beginUpdates()
                
                self.tbl_club.reloadSections(IndexSet(integer: sender.tag), with: .automatic)
                tbl_club.endUpdates()
                
            }
        }
        
        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
        self.tbl_club.reloadData()
        // }
        
    }
    
}
