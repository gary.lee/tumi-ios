//
//  MyAccountVC.swift
//  Tumi
//
//  Created by Redspark on 19/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
        return UIColor.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    static let defaultOuterColor = UIColor.rgb(210, 210, 210)
    static let defaultInnerColor: UIColor = .rgb(180, 0, 10)
    static let defaultPulseFillColor = UIColor.rgb(86, 30, 63)
}

class MyAccountVC: UIViewController {
    @IBOutlet var lblname: UILabel!
    
    @IBOutlet var lblcode: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var imgBarcode: UIImageView!
    @IBOutlet var imgcard: UIImageView!
    @IBOutlet var lblCode: UILabel!
    var count: CGFloat = 80
    var progressRing: CircularProgressBar!
    var timer: Timer!
    
    @IBOutlet var viewProgress: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.CallAPI()
    }
    
    
    func generateBarcode(from string: String) -> UIImage? {
        
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setDefaults()
            //Margin
            filter.setValue(7.00, forKey: "inputQuietSpace")
            filter.setValue(data, forKey: "inputMessage")
            //Scaling
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                let context:CIContext = CIContext.init(options: nil)
                let cgImage:CGImage = context.createCGImage(output, from: output.extent)!
                let rawImage:UIImage = UIImage.init(cgImage: cgImage)
                
                //Refinement code to allow conversion to NSData or share UIImage. Code here:
                //http://stackoverflow.com/questions/2240395/uiimage-created-from-cgimageref-fails-with-uiimagepngrepresentation
                let cgimage: CGImage = (rawImage.cgImage)!
                let cropZone = CGRect(x: 0, y: 0, width: Int(rawImage.size.width), height: Int(rawImage.size.height) )
                let cWidth: size_t  = size_t(cropZone.size.width)
                let cHeight: size_t  = size_t(cropZone.size.height)
                let bitsPerComponent: size_t = cgimage.bitsPerComponent
                //THE OPERATIONS ORDER COULD BE FLIPPED, ALTHOUGH, IT DOESN'T AFFECT THE RESULT
                let bytesPerRow = (cgimage.bytesPerRow) / (cgimage.width  * cWidth)
                
                let context2: CGContext = CGContext(data: nil, width: cWidth, height: cHeight, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: cgimage.bitmapInfo.rawValue)!
                
                context2.draw(cgimage, in: cropZone)
                
                let result: CGImage  = context2.makeImage()!
                let finalImage = UIImage(cgImage: result)
                
                return finalImage
                
            }
        }
        
        return nil
    }
    
    
    func CallAPI()  {
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng" : UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GET_MEMBERSHIP_DETAIL, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    let userdict = dict.value(forKey: "data") as? NSDictionary
                    
                    let imageurl = userdict?.value(forKey: "card_image") as! String
                    self.imgcard.sd_setImage(with: URL(string:imageurl), placeholderImage: UIImage(named: ""))
                    
                    let name = userdict?.value(forKey: "name") as! String
                    self.lblname.text = name
                    
                    let membercode = userdict?.value(forKey: "member_code") as? String
                    self.imgBarcode.image = self.generateBarcode(from: membercode ?? "")
                    self.lblCode.text = membercode
                    
                    self.lblcode.text = membercode
                    var membership_level = userdict?.value(forKey: "membership_level") as? String
                    
                    let wI = NSMutableString( string: membership_level! )
                    CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
                    
                    membership_level = wI as String
                    
                    let spend_amount = userdict?.value(forKey: "spend_amount") as? CGFloat
                    let total_amount = userdict?.value(forKey: "total_amount") as? CGFloat
                    let date = userdict?.value(forKey: "date") as? String
                    
                    let hkword = "HK_".localized()
                    //let tomaintain = "toMaintain".localized()
                    let BY = "BY".localized()
                    
                    let strAmount = "\(hkword)\(spend_amount!) \("toMaintain".localized())"//String(format: "\(hkword) %.2f\n\(tomaintain) %@",spend_amount! , membership_level!.localized() )
                    let strdate = String(format: "\(BY)\n%@",date ?? "")
                    
                    self.lblType.text = membership_level?.localized()
                    
                    // HK$ xxxx TO MAINTAIN GOLD
                    
                    var xPosition = self.view.center.x - 70
                    var yPosition = self.view.center.y - 180
                    
                    if(Device.DeviceType.IS_IPHONE_6 || Device.DeviceType.IS_IPHON_X){
                        
                        xPosition = self.view.center.x - 50
                        yPosition = self.view.center.y - 150
                    }
                    
                    let position = CGPoint(x: xPosition, y: yPosition)
                    
                    
                    self.progressRing = CircularProgressBar(radius: 100, position: position, innerTrackColor: .defaultInnerColor, outerTrackColor: .defaultOuterColor, lineWidth: 20)
                    self.progressRing.progress = CGFloat((total_amount! - spend_amount!)/total_amount!) * 100
                    self.progressRing.progressLabel.text = strAmount
                    self.progressRing.completedLabel.text = strdate
                    
                    self.viewProgress.layer.insertSublayer(self.progressRing, at: 0)
                    
                    if(membership_level == "Registered".localized()){
                        self.progressRing.progressLabel.text = "REACH_SILVER".localized()
                        self.progressRing.completedLabel.text = ""
                    }
                    else if(membership_level == "Silver".localized()){
                        self.progressRing.progressLabel.text = "\(hkword)\(spend_amount!) \("REACH_GOLD".localized())"
                        self.progressRing.completedLabel.text = ""
                        //"hk".localized() + spend_amount! + "REACH_GOLD".localized()
                        //HK$ xxxx TO REACH GOLD
                    }
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
}
