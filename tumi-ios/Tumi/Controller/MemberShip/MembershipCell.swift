//
//  MembershipCell.swift
//  Tumi
//
//  Created by Redspark on 17/07/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import WebKit

class MembershipCell: UITableViewCell {
    
    var strContent : String = ""
    
    weak var viewController: ExclusiveClubVC? = nil
    @IBOutlet var webView: UIWebView!
    @IBOutlet weak var heightLayoutConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //        viewController?.isFromLoadRequest = false
        //
        //        webView.loadHTMLString(strContent, baseURL: nil)
        DispatchQueue.main.async {
            self.webView.delegate = self
            self.webView.scrollView.isScrollEnabled = false
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        DispatchQueue.main.async {
            self.viewController?.isFromLoadRequest = false
            
            self.webView.loadHTMLString(self.strContent, baseURL: nil)
            self.webView.delegate = self
            self.webView.scrollView.isScrollEnabled = true
        }
        
    }
    
}
extension MembershipCell: UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error.localizedDescription)
        print("Failed")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading{
            return
        }
        DispatchQueue.main.async {
            self.heightLayoutConstraint.constant = self.webView.scrollView.contentSize.height
            self.viewController?.isFromLoadRequest = false
        }
        
        //        viewController?.tbl_club.beginUpdates()
        //        viewController?.tbl_club.endUpdates()
    }
}
