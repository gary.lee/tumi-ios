//
//  TermsCondCell.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class TermsCondCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    
    @IBOutlet var imgArrow: UIImageView!
    
    @IBOutlet var lblDescription: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
