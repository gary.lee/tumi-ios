//
//  CategoryCartCell.swift
//  Tumi
//
//  Created by Dhruv Patel on 09/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class CategoryCartCell: UITableViewCell {
    
    @IBOutlet weak var imgCat:UIImageView!
    @IBOutlet weak var lblProductName:UILabel!
    
    @IBOutlet var btnCart: UIButton!
    @IBOutlet var btnShare: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
