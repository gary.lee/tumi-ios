//
//  SideMenuCell.swift
//  SwayamVar
//
//  Created by Dhruv Patel on 25/02/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
    @IBOutlet weak var imgIcon:UIImageView!
    @IBOutlet weak var lblMenuName:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
