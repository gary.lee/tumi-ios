//
//  LanguageSelectionCell.swift
//  Tumi
//
//  Created by Dhruv Patel on 08/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class LanguageSelectionCell: UITableViewCell {
    
    @IBOutlet var lblSelectedColor: UILabel!
    
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
