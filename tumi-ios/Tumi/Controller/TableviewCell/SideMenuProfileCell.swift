//
//  SideMenuProfileCell.swift
//  SwayamVar
//
//  Created by Dhruv Patel on 25/02/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class SideMenuProfileCell: UITableViewHeaderFooterView {
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    
    @IBOutlet var lblSurname: UILabel!
    
    
    @IBOutlet var btnView: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.width/2
        self.imgProfile.clipsToBounds = true
        // Initialization code
    }
    
}
