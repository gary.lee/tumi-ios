//
//  VideoCell.swift
//  Tumi
//
//  Created by Redspark on 05/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {
    
    @IBOutlet weak var imgCat:UIImageView!
    @IBOutlet weak var lblDetail:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
