//
//  SideMenuView.swift
//  SwayamVar
//
//  Created by Redspark on 12/03/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import SDWebImage

class SideMenuView: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    //    "Gold" = "Gold";
    //    "Silver" = "Silver";
    //    "Registered" = "Registered";
    //    "DISCOVERY" = "DISCOVERY";
    //    "AUGMENTEDREALITY" = "AUGMENTED REALITY";
    //    "MYAVATAR" = "MY AVATAR";
    //    "ONLINESTORE" = "ONLINE STORE";
    //    "NOTIFICATION" = "NOTIFICATION";
    //    "MEMBERSHIP" = "MEMBERSHIP";
    //    "FIND_A_STORE" = "FIND A STORE";
    //    "SETTINGS" = "SETTINGS";
    
    
    
    let arrMenu = ["DISCOVERY","AUGMENTEDREALITY","ONLINESTORE","NOTIFICATION","MEMBERSHIP","FIND_A_STORE","SETTINGS"]
    let arrImage = ["home","AR","Avtar","Cart","Notification","search","Setting"]
    
    //    let arrMenu = ["DISCOVERY","AUGMENTED REALITY","MY AVATAR","ONLINE STORE","NEWS","NOTIFICATION","MEMBERSHIP","FIND A STORE","SETTINGS"]
    //    let arrImage = ["home","AR","Avtar","Cart","news","Notification","T","search","Setting"]
    
    //let arrMenu = ["DISCOVERY","AUGMENTEDREALITY","MYAVATAR","ONLINESTORE","NOTIFICATION","MEMBERSHIP","FIND_A_STORE","SETTINGS"]
    
    //    let arrMenu = ["DISCOVERY","AUGMENTED REALITY","MY AVATAR","ONLINE STORE","NOTIFICATION","MEMBERSHIP","FIND A STORE","SETTINGS"]
    //    let arrImage = ["home","AR","Avtar","Cart","Notification","T","search","Setting"]
    
    // @IBOutlet var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView:UITableView!
    
    var selectedMenuItem : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(self.membershipupdate(notification:)), name: Notification.Name("membershipupdate"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func membershipupdate(notification: Notification) {
        
        self.tableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupUI()
    }
    func setupUI(){
        self.view.backgroundColor = UIColor.black
        
        // Create a blur effect
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        // Fill the view
        blurEffectView.frame = view.bounds
        
        // Ensure the blur conforms to resizing (not used in a fixed menu UI)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Add the view to the view controller stack
        view.addSubview(blurEffectView)
        
        // Ensure the blur view is in the back
        self.view.sendSubview(toBack: blurEffectView)
        
        //view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.tableView.register(UINib(nibName: "SideMenuCell", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        //tableView.backgroundView = UIImageView(image: UIImage(named: "Background"))
        self.tableView.register(UINib(nibName: "SideMenuProfileCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "SideMenuProfileCell")
        self.tableView.register(UINib(nibName: "SidemenuFooterCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "SidemenuFooterCell")
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.selectRow(at: IndexPath(row: selectedMenuItem, section: 0), animated: false, scrollPosition: .middle)
        
        //  self.tblHeight.constant = CGFloat(self.arrMenu.count * 55)+150
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        
    }
    
    func reload() {
        self.tableView.reloadData();
    }
    @IBAction func btnfacebookclick(_ sender: Any) {
        guard let url = URL(string: "https://www.facebook.com/TumiTravel/") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func btninstagramclick(_ sender: Any) {
        guard let url = URL(string: "https://www.instagram.com/tumitravel/") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func btnwechatclick(_ sender: Any) {
        //        guard let url = URL(string: "https://www.instagram.com/tumitravel/") else { return }
        //        UIApplication.shared.open(url)
    }
    @IBAction func btnsineclick(_ sender: Any) {
        guard let url = URL(string: "https://www.weibo.com/tumilifestyle") else { return }
        UIApplication.shared.open(url)
    }
    @IBAction func btnEnglishClick(_ sender: Any) {
        Bundle.setLanguage(lang: "en")
        UserDataHolder.sharedUser.language = "en"
        Utility.sharedUtility.sideMenu()
    }
    @IBAction func btnlanguage1Click(_ sender: Any) {
        Bundle.setLanguage(lang: "zh-Hant")
        UserDataHolder.sharedUser.language = "traditional chinese"
        Utility.sharedUtility.sideMenu()
        
    }
    @IBAction func btnlanguage2Click(_ sender: Any) {
        Bundle.setLanguage(lang: "zh-Hans")
        UserDataHolder.sharedUser.language = "chinese"
        Utility.sharedUtility.sideMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func callOnlineStore()
    {
        let parameter : [String : Any] = [:]
        AFWrapper.postMethod(params: parameter as [String : AnyObject], apikey:get_online_store_id, completion: { (json) in
            let dict = json as! NSDictionary
            if dict["status"] as? Int == 1
            {
                let id = dict["id"] as! Int
                
                UserDataHolder.sharedUser.forOnlieStore = "1"
                UserDataHolder.sharedUser.foronlinestoreid = "\(id)"
                
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                self.sideMenuController?.hideLeftView()
                
            }
            else
            {
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return arrMenu.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as? SideMenuCell
        cell?.selectionStyle = .none
        
        let menuname = arrMenu[indexPath.row]
        cell?.lblMenuName.text = menuname.localized()
        cell?.imgIcon.image = UIImage(named:arrImage[indexPath.row])
        
        return cell!
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedMenuItem = indexPath.row
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        
        switch (indexPath.row) {
        case 0:
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC")
            self.setViewControlelr(objVC: destViewController)
            
            break
        case 1:
            
            if (ARWorldTrackingConfiguration.isSupported) {
                
                destViewController = mainStoryboard.instantiateViewController(withIdentifier: "ARPageViewController")
                self.setViewControlelr(objVC: destViewController)
                
            } else {
                showAlert(withMessage: "AR_Support".localized())
            }
            
            break
        case 2:
            
            // My Avatar
            
            //            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "MyAvatarVC")
            //            self.setViewControlelr(objVC: destViewController)
            
            let destViewController = mainStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as? PrivacyPolicyVC
            destViewController?.isStore = true
            destViewController?.isCart = false
            let navController = UINavigationController(rootViewController: destViewController!)
            navController.isNavigationBarHidden = true
            self.present(navController, animated:true, completion: nil)
            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            
            break
        case 3:
            
            //            let destViewController = mainStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyVC") as? PrivacyPolicyVC
            //            destViewController?.isStore = true
            //            destViewController?.isCart = false
            //            let navController = UINavigationController(rootViewController: destViewController!)
            //            navController.isNavigationBarHidden = true
            //            self.present(navController, animated:true, completion: nil)
            //            sideMenuController?.hideLeftView(animated: true, completionHandler: nil)
            //
            
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationListVC")
            self.setViewControlelr(objVC: destViewController)
            
            break
            
        case 4:
            
            //News
            
            //            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewsPageVC")
            //            self.setViewControlelr(objVC: destViewController)
            
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "MemberShipViewController")
            self.setViewControlelr(objVC: destViewController)
            
            break
        case 5:
            // self.showLogoutAlert()
            
            //            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "NotificationListVC")
            //            self.setViewControlelr(objVC: destViewController)
            
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "FindPlace")
            self.setViewControlelr(objVC: destViewController)
            
            break
        case 6:
            
            //            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "MemberShipViewController")
            //            self.setViewControlelr(objVC: destViewController)
            
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "SettingVC")
            self.setViewControlelr(objVC: destViewController)
            
            break
            
        case 7 :
            
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "FindPlace")
            self.setViewControlelr(objVC: destViewController)
            
            break
            
        case 8 :
            
            destViewController = mainStoryboard.instantiateViewController(withIdentifier: "SettingVC")
            self.setViewControlelr(objVC: destViewController)
            
            break
            
        default:
            
            break
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55//UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 210
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SideMenuProfileCell") as? SideMenuProfileCell
        cell?.imgProfile.layer.cornerRadius = (cell?.imgProfile.frame.width)!/2
        cell?.imgProfile.clipsToBounds = true
        cell?.btnView.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
        cell?.lblUserName.text =  String(format: "%@ %@", UserDataHolder.sharedUser.firstName!,UserDataHolder.sharedUser.lastName!)
        
        let membership_level  = UserDataHolder.sharedUser.membership_level
        cell?.lblSurname.text = membership_level?.localized()
        
        
        //  cell?.lblSurname.text = UserDataHolder.sharedUser.surname!
        // cell?.contentView.backgroundColor = UIColor
        //cell?.lblUserName.text = "Anamika"//objLogin?.data.results.name
        //        var profile  = profilePic!//objLogin?.data.results.profilepic
        //        profile = profile.replacingOccurrences(of: " ", with: "%20")
        cell?.contentView.backgroundColor = UIColor.black
        
        // Create a blur effect
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        
        // Fill the view
        blurEffectView.frame = (cell?.contentView.bounds)!
        
        // Ensure the blur conforms to resizing (not used in a fixed menu UI)
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Add the view to the view controller stack
        cell?.contentView.addSubview(blurEffectView)
        
        // Ensure the blur view is in the back
        cell?.contentView.sendSubview(toBack: blurEffectView)
        DispatchQueue.main.async {
            cell?.imgProfile.sd_setImage(with: URL(string:""), placeholderImage: UIImage(named: "user"))
        }
        return cell?.contentView
    }
    
    @objc func buttonSelected(sender: UIButton){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        destViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProfileVC")
        self.setViewControlelr(objVC: destViewController)
    }
    
    func resetDefaults() {
        // let dictionary = KuserDefault.dictionaryRepresentation()
        //        dictionary.keys.forEach { key in
        //            KuserDefault.removeObject(forKey: key)
        //        }
        
        
        
        let objLoginVc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        let nav = UINavigationController(rootViewController: objLoginVc!)
        if let window = appDelegate.window {
            window.rootViewController = nav
        }
        appDelegate.window?.makeKeyAndVisible()
        
    }
    func showLogoutAlert(){
        let alert = UIAlertController(title: APPNAME, message: LOGOUTMSG, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: LOGOUT, style: .destructive, handler: { (action: UIAlertAction!) in
            
        }))
        
        alert.addAction(UIAlertAction(title:CANCEL, style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        present(alert, animated: true, completion: nil)
    }
    func callLogout(){
        
    }
}

extension UIViewController{
    func setViewControlelr(objVC:UIViewController){
        let sidemenuVC = sideMenuController!
        objVC.view.backgroundColor = .white
        let navigationController = sidemenuVC.rootViewController as! UINavigationController
        navigationController.pushViewController(objVC, animated: true)
        sidemenuVC.hideLeftView(animated: true, completionHandler: nil)
    }
}
