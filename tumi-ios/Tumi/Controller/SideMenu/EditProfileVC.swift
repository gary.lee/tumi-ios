//
//  EditProfileVC.swift
//  Tumi
//
//  Created by Redspark on 15/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,WWCalendarTimeSelectorProtocol,UIPickerViewDataSource,UIPickerViewDelegate {
    
    fileprivate var singleDate: Date = Date()
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    
    let pickerGender = ["Select Gender","Male","Female"]
    let pickerRegion = ["Select","India","China","USA"]
    let pickerLanguage = ["Select Language","English","中文（繁體)","中文（简体)"]
    
    //@IBOutlet var btnPlus: UIButton!
    
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bottomView: UIView!
    // @IBOutlet weak var lblName:UITextField!
    // @IBOutlet weak var lblMember:UITextField!
    @IBOutlet var txtSurname: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    // @IBOutlet var txtAreaCode: UITextField!
    // @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtEmail: UITextField!
    // @IBOutlet var txtBirthday: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtPlace: UITextField!
    @IBOutlet var txtLanguage: UITextField!
    // @IBOutlet var btnDate: UIButton!
    @IBOutlet var btnConnectToFacebook: UIButton!
    @IBOutlet var btnConnectToWechat: UIButton!
    
    @IBOutlet var switchEmail: UISwitch!
    //   @IBOutlet var swtichMessage: UISwitch!
    // @IBOutlet var swtichInstanseMsg: UISwitch!
    @IBOutlet var swtichPhone: UISwitch!
    @IBOutlet var SwitchMessage: UISwitch!
    @IBOutlet var SwitchInsMsg: UISwitch!
    
    
    var is_edm = String()
    var is_ims = String()
    var is_phone = String()
    var is_sms = String()
    var bdate = String()
    var Pref_lng = String()
    
    //localization
    
    @IBOutlet var lblsurname: UILabel!
    @IBOutlet var lblfirstname: UILabel!
    @IBOutlet var lblregion: UILabel!
    //@IBOutlet var lblareacode: UILabel!
    //@IBOutlet var lblmobilenumber: UILabel!
    @IBOutlet var lblemail: UILabel!
    @IBOutlet var lblmarketcomm: UILabel!
    // @IBOutlet var lblbirthdate: UILabel!
    @IBOutlet var lblacceptcall: UILabel!
    @IBOutlet var lblacceptInstantmsg: UILabel!
    @IBOutlet var lblaccepttextmsg: UILabel!
    @IBOutlet var lblacceptemail: UILabel!
    @IBOutlet var lblgender: UILabel!
    @IBOutlet var lblpreferlang: UILabel!
    
    let countries = NSLocale.isoCountryCodes.map { (code:String) -> String in
        let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
        return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
    }
    
    var editable : Bool = false
    
    var countryList = CountryList()
    
    @IBOutlet var btnsave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpUI()
        // Do any additional setup after loading the view.
        
        countryList.delegate = self
        self.lblsurname.text = "Surname".localized()
        self.lblfirstname.text = "First_Name".localized()
        //  self.lblareacode.text = "AreaCode".localized()
        //self.lblmobilenumber.text = "MobileNumber".localized()
        self.lblemail.text = "Email".localized()
        // self.lblbirthdate.text = "Birthday(mm/dd/yyy)".localized()
        self.lblgender.text = "Gender".localized()
        self.lblregion.text = "ResidingPlaceRegion".localized()
        self.lblpreferlang.text = "PreferredLanguage".localized()
        self.lblmarketcomm.text = "MarketingCommunications".localized()
        self.lblacceptemail.text = "Accept_E_mails".localized()
        self.lblaccepttextmsg.text = "AcceptTextMessages".localized()
        self.lblacceptInstantmsg.text = "AcceptInstantMessages".localized()
        self.lblacceptcall.text = "AcceptCells".localized()
        
        self.btnsave.setTitle("SAVE".localized(), for: .normal)
        
        //  self.lblName.text = UserDataHolder.sharedUser.membership_level
        
        
        // let code = "Code".localized()
        // self.txtAreaCode.placeholder = code
        
        self.txtSurname.text = UserDataHolder.sharedUser.lastName
        self.txtFirstName.text = UserDataHolder.sharedUser.firstName
        // self.txtAreaCode.text = UserDataHolder.sharedUser.areacode
        //self.txtPhoneNumber.text = UserDataHolder.sharedUser.phoneNumber
        self.txtEmail.text = UserDataHolder.sharedUser.email
        // self.btnDate.setTitle(UserDataHolder.sharedUser.birthDate, for: .normal)
        self.txtGender.text = UserDataHolder.sharedUser.gender
        self.txtPlace.text = UserDataHolder.sharedUser.region
        //  self.lblMember.text = String(format: "%@ %@", UserDataHolder.sharedUser.firstName!,UserDataHolder.sharedUser.surname!)
        
        let language = UserDataHolder.sharedUser.language
        
        if language == "English"{
            self.txtLanguage.text = "English"
        }
        else if language == "Traditional Chinese"{
            self.txtLanguage.text = "中文（繁體)"
        }
        else if language == "Chinese"{
            self.txtLanguage.text = "中文（简体)"
        }
        else{
            self.txtLanguage.text = "Select Language"
        }
        
        
        //self.txtBirthday.isUserInteractionEnabled = false
        is_edm = UserDataHolder.sharedUser.is_edm as String
        is_ims = UserDataHolder.sharedUser.is_ims
        is_phone = UserDataHolder.sharedUser.is_phone
        is_sms = UserDataHolder.sharedUser.is_sms
        
        if(is_edm != "1"){
            
            self.switchEmail.isOn = false
        }
        
        if(is_ims != "1"){
            
            self.SwitchInsMsg.isOn = false
        }
        
        if(is_phone != "1"){
            
            self.swtichPhone.isOn = false
        }
        
        if(is_sms != "1"){
            self.SwitchMessage.isOn = false
        }
        
        
    }
    
    func setUpUI(){
        bottomViewHeightConstraint.constant = 650.0
        view.layoutIfNeeded()
        bottomView.isHidden = false
        
        txtSurname.setLeftPaddingPoints(10.0)
        txtFirstName.setLeftPaddingPoints(10.0)
        //txtAreaCode.setLeftPaddingPoints(10.0)
        // txtPhoneNumber.setLeftPaddingPoints(10.0)
        txtEmail.setLeftPaddingPoints(10.0)
        //txtBirthday.setLeftPaddingPoints(10.0)
        txtGender.setLeftPaddingPoints(10.0)
        txtPlace.setLeftPaddingPoints(10.0)
        txtLanguage.setLeftPaddingPoints(10.0)
        
        btnConnectToFacebook.underline()
        btnConnectToWechat.underline()
        
        //textFieldEditable(_editable: editable)
        
        txtGender.inputView = picker
        txtPlace.inputView = picker1
        txtLanguage.inputView = picker2
        picker.tag = 0
        picker1.tag = 1
        picker2.tag = 2
        //        txtGendr.text = pickerGender[0]
        //        txtRegion.text = pickerRegion[0]
        txtLanguage.text = pickerLanguage[0]
        picker.dataSource = self
        picker.delegate = self
        picker.reloadAllComponents()
        picker1.dataSource = self
        picker1.delegate = self
        picker1.reloadAllComponents()
        picker2.dataSource = self
        picker2.delegate = self
        picker2.reloadAllComponents()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionPlus(_ sender: Any) {
        if bottomViewHeightConstraint.constant == 0.0{
            bottomViewHeightConstraint.constant = 650.0
            //btnPlus.setTitle("-", for: .normal)
            bottomView.isHidden = false
        }
        else{
            bottomViewHeightConstraint.constant = 0.0
            // btnPlus.setTitle("+", for: .normal)
            bottomView.isHidden = true
        }
        view.layoutIfNeeded()
    }
    
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSave(_ sender:UIButton){
        self.postProfileData()
    }
    
    
    
    func postProfileData()
    {
        
        var gender = self.txtGender.text
        if gender == "Select Gender"{
            gender = ""
        }
        var language = self.txtLanguage.text
        if language == "Select Language"{
            language = ""
        }
        
        
        let parameters : [String : Any] =
            [
                "user_id":"\(UserDataHolder.sharedUser.userId!)",
                "surname":txtSurname.text!,
                "first_name":txtFirstName.text!,
                "email":txtEmail.text!,
                "gender":gender!,
                "region":txtPlace.text!,
                "is_edm":is_edm,
                "is_phone":is_phone,
                "is_sms":is_sms,
                "is_ims":is_ims,
                "prefered_language":Pref_lng,
                "lng":UserDataHolder.sharedUser.language
        ]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:UPDATE_PROFILE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    Constants.appDelegate.isupdateProfile = true
                    self.showAlert(withMessage: "Your_profile_is_updated".localized())
                }
            }
            else{
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    @IBAction func actionOnOffTap(_ sender: UIButton) {
        
    }
    
    
    @IBAction func actionEdit(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //        if editable {
        //            editable = false
        //        }
        //        else{
        //            editable = true
        //        }
        //        textFieldEditable(_editable: editable)
    }
    
    @IBAction func actionDate(_ sender: UIButton) {
        // dateControllerOpen()
        
        RPicker.selectDate(title: "Select Date", didSelectDate: { (selectedDate) in
            // TODO: Your implementation for date
            
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let myString = formatter.string(from:selectedDate) // string purpose I add here
            // convert your string to date
            let yourDate = formatter.date(from: myString)
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "MM / dd / yyyy"
            // again convert your date to string
            let myStringafd = formatter.string(from: yourDate!)
            
            print(myStringafd)
            
            // self.btnDate.setTitle(myStringafd, for: .normal)
        })
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
        //        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
        //            self.txtAreaCode.text = country.dialingCode
        //            //            self.btnCode.setTitle(country.dialingCode, for: .normal)
        //
        //        }
        //        // can customize the countryPicker here e.g font and color
        //        countryController.detailColor = UIColor.red
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    func textFieldEditable(_editable: Bool) {
        txtSurname.isUserInteractionEnabled = _editable
        txtFirstName.isUserInteractionEnabled = _editable
        // txtAreaCode.isUserInteractionEnabled = _editable
        // txtPhoneNumber.isUserInteractionEnabled = _editable
        txtEmail.isUserInteractionEnabled = _editable
        // txtBirthday.isUserInteractionEnabled = _editable
        txtGender.isUserInteractionEnabled = _editable
        txtPlace.isUserInteractionEnabled = _editable
        txtLanguage.isUserInteractionEnabled = _editable
    }
    
    func dateControllerOpen(){
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        present(selector, animated: true, completion: nil)
        
    }
    
    @IBAction func emaiTapped(_ sender: UISwitch) {
        
        if(sender.isOn){
            is_edm = "2"
        }
        else{
            is_edm = "1"
        }
        
        if is_edm == "1"{
            is_edm = "2"
        }
        else{
            is_edm = "1"
        }
        
    }
    
    @IBAction func messageTapped(_ sender: UISwitch) {
        
        if(sender.isOn){
            is_sms = "2"
        }
        else{
            is_sms = "1"
        }
        
        if is_sms == "1"{
            is_sms = "2"
        }
        else{
            is_sms = "1"
        }
        
    }
    
    @IBAction func InstantTapped(_ sender: UISwitch) {
        
        if(sender.isOn){
            is_ims = "2"
        }
        else{
            is_ims = "1"
        }
        
        if is_ims == "1"{
            is_ims = "2"
        }
        else{
            is_ims = "1"
        }
    }
    
    @IBAction func phoneTapped(_ sender: UISwitch) {
        
        if(sender.isOn){
            is_phone = "2"
        }
        else{
            is_phone = "1"
        }
        
        if is_phone == "1"{
            is_phone = "2"
        }
        else{
            is_phone = "1"
        }
        
    }
    
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "MM / dd / yyyy"
        let bdate1 = dateFormatter.string(from: date as Date)
        //txtBirthday.text = bdate1
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) ->Int {
        if(pickerView.tag == 0){
            return pickerGender.count
        }else if(pickerView.tag == 1){
            return countries.count
        }else{
            return pickerLanguage.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 0){
            return pickerGender[row]
        }else if(pickerView.tag == 1){
            return countries[row]
        }else{
            return pickerLanguage[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 0){
            self.txtGender.text =  pickerGender[row]
        }else if(pickerView.tag == 1){
            self.txtPlace.text =  countries[row]
        }else{
            self.txtLanguage.text = pickerLanguage[row]
            
            if(row == 1){
                Pref_lng = "English"
            }
            else if(row == 2){
                Pref_lng = "Traditional Chinese"
            }
            else if(row == 3){
                Pref_lng = "Chinese"
            }
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



extension EditProfileVC: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.name as Any)
        print(country.flag as Any)
        print(country.countryCode)
        print(country.phoneExtension)
        // self.txtAreaCode.text = country.phoneExtension
        
    }
}
