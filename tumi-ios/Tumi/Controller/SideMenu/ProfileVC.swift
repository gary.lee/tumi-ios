//
//  ProfileVC.swift
//  Tumi
//
//  Created by Dhruv Patel on 09/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController,WWCalendarTimeSelectorProtocol,UIPickerViewDataSource,UIPickerViewDelegate {
    
    fileprivate var singleDate: Date = Date()
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    
    let pickerGender = ["Select Gender","Male","Female"]
    let pickerRegion = ["Select","India","China","USA"]
    let pickerLanguage = ["Select Language","English","Hindi","Chinese"]
    
    @IBOutlet var btnPlus: UIButton!
    
    @IBOutlet var bottomViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet weak var lblName:UITextField!
    @IBOutlet weak var lblMember:UITextField!
    @IBOutlet var txtSurname: UITextField!
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtAreaCode: UITextField!
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtBirthday: UITextField!
    @IBOutlet var txtGender: UITextField!
    @IBOutlet var txtPlace: UITextField!
    @IBOutlet var txtLanguage: UITextField!
    @IBOutlet var btnConnectToFacebook: UIButton!
    @IBOutlet var btnConnectToWechat: UIButton!
    
    @IBOutlet var statusEmail: UILabel!
    
    @IBOutlet var lblStatus: UILabel!
    
    @IBOutlet var statusTextMessage: UILabel!
    
    @IBOutlet var lblStatus2: UILabel!
    
    @IBOutlet var statusInstantMessage: UILabel!
    
    @IBOutlet var lblStatus3: UILabel!
    
    @IBOutlet var statusRepeatCalls: UILabel!
    
    @IBOutlet var lblStatus4: UILabel!
    
    var editable : Bool = false
    

    
    //localization
    
    @IBOutlet var lblsurname: UILabel!
    
    @IBOutlet var lblfirstname: UILabel!
    @IBOutlet var lblregion: UILabel!
    
    @IBOutlet var lblareacode: UILabel!
    
    @IBOutlet var lblmobilenumber: UILabel!
    
    @IBOutlet var lblemail: UILabel!
    
    @IBOutlet var btnedit: UIButton!
    @IBOutlet var lblmarketcomm: UILabel!
    @IBOutlet var lblbirthdate: UILabel!
    
    @IBOutlet var lblacceptcall: UILabel!
    @IBOutlet var lblacceptInstantmsg: UILabel!
    @IBOutlet var lblaccepttextmsg: UILabel!
    @IBOutlet var lblacceptemail: UILabel!
    @IBOutlet var lblgender: UILabel!
    @IBOutlet var lblpreferlang: UILabel!
    
     var countryList = CountryList()
    
    let countries = NSLocale.isoCountryCodes.map { (code:String) -> String in
        let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
        return NSLocale(localeIdentifier: "en_US").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryList.delegate = self
        
        self.lblsurname.text = "Surname".localized()
        self.lblfirstname.text = "First_Name".localized()
        self.lblareacode.text = "AreaCode".localized()
        self.lblmobilenumber.text = "MobileNumber".localized()
        self.lblemail.text = "Email".localized()
        self.lblbirthdate.text = "Birthday(mm/dd/yyyy)".localized()
        self.lblgender.text = "Gender".localized()
        self.lblregion.text = "ResidingPlaceRegion".localized()
        self.lblpreferlang.text = "PreferredLanguage".localized()
        self.lblmarketcomm.text = "MarketingCommunications".localized()
        self.lblacceptemail.text = "Accept_E_mails".localized()
        self.lblaccepttextmsg.text = "AcceptTextMessages".localized()
        self.lblacceptInstantmsg.text = "AcceptInstantMessages".localized()
        self.lblacceptcall.text = "AcceptCells".localized()
        
        let edit = "Edit".localized()
        self.btnedit.setTitle("\(edit) ", for: .normal)
        
        let code = "Code".localized()
        self.txtAreaCode.placeholder = code
        // Do any additional setup after loading the view.
        
        setUpUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // setUpUI()
    }
    
    func setUpUI(){
        bottomViewHeightConstraint.constant = 0.0
        view.layoutIfNeeded()
        bottomView.isHidden = true
        
        txtSurname.setLeftPaddingPoints(10.0)
        txtFirstName.setLeftPaddingPoints(10.0)
        txtAreaCode.setLeftPaddingPoints(10.0)
        txtPhoneNumber.setLeftPaddingPoints(10.0)
        txtEmail.setLeftPaddingPoints(10.0)
        txtBirthday.setLeftPaddingPoints(10.0)
        txtGender.setLeftPaddingPoints(10.0)
        txtPlace.setLeftPaddingPoints(10.0)
        txtLanguage.setLeftPaddingPoints(10.0)
        
        btnConnectToFacebook.underline()
        btnConnectToWechat.underline()
        
        textFieldEditable(_editable: editable)
        
        txtGender.inputView = picker
        txtPlace.inputView = picker1
        txtLanguage.inputView = picker2
        picker.tag = 0
        picker1.tag = 1
        picker2.tag = 2
        //        txtGendr.text = pickerGender[0]
        //        txtRegion.text = pickerRegion[0]
        txtLanguage.text = pickerLanguage[0]
        picker.dataSource = self
        picker.delegate = self
        picker.reloadAllComponents()
        picker1.dataSource = self
        picker1.delegate = self
        picker1.reloadAllComponents()
        picker2.dataSource = self
        picker2.delegate = self
        picker2.reloadAllComponents()
        self.getProfile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionPlus(_ sender: Any) {
        if bottomViewHeightConstraint.constant == 0.0{
            bottomViewHeightConstraint.constant = 500.0
            btnPlus.setTitle("-", for: .normal)
            bottomView.isHidden = false
        }
        else{
            bottomViewHeightConstraint.constant = 0.0
            btnPlus.setTitle("+", for: .normal)
            bottomView.isHidden = true
        }
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(Constants.appDelegate.isupdateProfile){
            Constants.appDelegate.isupdateProfile = false
            self.getProfile()
        }
    }
    
    
    func getProfile() {
        
        let parameters = ["user_id":"\(UserDataHolder.sharedUser.userId!)","lng" : UserDataHolder.sharedUser.language]
        
        AFWrapper.postMethod(params: parameters as [String : AnyObject], apikey:GETPROFILE, completion: { (json) in
            
            let dict = json as! NSDictionary
            
            if dict["status"] as? Int == 1
            {
                DispatchQueue.main.async {
                    
                    if let _data = dict["data"] as? NSDictionary{
                        print(_data)
                        if let uid = _data["id"] as? Int{
                            UserDataHolder.sharedUser.userId = uid
                        }
                        if let fname  = _data["first_name"] as? String{
                            UserDataHolder.sharedUser.firstName = fname
                            self.txtFirstName.text = fname
                        }
                        if let lname  = _data["surname"] as? String{
                            UserDataHolder.sharedUser.lastName = lname
                            self.txtSurname.text = lname
                            self.lblName.text = String(format: "%@ %@", UserDataHolder.sharedUser.firstName!,UserDataHolder.sharedUser.lastName!)
                        }
                        if let lname  = _data["membership_level"] as? String{
                            UserDataHolder.sharedUser.membership_level = lname
                            self.lblMember.text = lname.localized();
                            
                        }
                        if let area_code  = _data["area_code"] as? String{
                            UserDataHolder.sharedUser.areacode = area_code
                            self.txtAreaCode.text = area_code
                        }
                        if let phone  = _data["phone"] as? String{
                            UserDataHolder.sharedUser.phoneNumber = phone
                            self.txtPhoneNumber.text = phone
                        }
                        if let email  = _data["email"] as? String{
                            UserDataHolder.sharedUser.email = email
                            self.txtEmail.text = email
                        }
                        if let region  = _data["region"] as? String{
                            UserDataHolder.sharedUser.region = region
                            self.txtPlace.text = region
                        }
                        if let gender  = _data["gender"] as? String{
                            UserDataHolder.sharedUser.gender = gender
                            self.txtGender.text = gender.localized()
                        }
                        if let birthday  = _data["birthday"] as? String{
                            UserDataHolder.sharedUser.birthDate = birthday
                            self.txtBirthday.text = birthday
                        }
                        if let language  = _data["language"] as? String{
                            UserDataHolder.sharedUser.language = language
                            if language == "English"{
                                self.txtLanguage.text = "English"
                            }
                            else if language == "Traditional Chinese"{
                                self.txtLanguage.text = "中文（繁體)"
                            }
                            else if language == "Chinese"{
                                self.txtLanguage.text = "中文（简体)"
                            }
                            else{
                                self.txtLanguage.text = ""
                            }
                        }
                        
                        let is_edm = _data["is_edm"] as? Int
                        let is_ims = _data["is_ims"] as? Int
                        let is_phone = _data["is_phone"] as? Int
                        let is_sms = _data["is_sms"] as? Int
                        
                        UserDataHolder.sharedUser.is_edm = String(format: "%d", is_edm!)
                        UserDataHolder.sharedUser.is_ims = String(format: "%d", is_ims!)
                        UserDataHolder.sharedUser.is_phone = String(format: "%d", is_phone!)
                        UserDataHolder.sharedUser.is_sms = String(format: "%d", is_sms!)
                        
                        if(is_edm != 1){
                            self.lblStatus.text = "Off".localized()
                            self.statusEmail.backgroundColor = UIColor.gray
                        }
                        else{
                            self.lblStatus.text = "on".localized()
                            self.statusEmail.backgroundColor = UIColor(red: 188/255.0, green: 33/255.0, blue: 49/255.0, alpha: 1.0)
                        }
                        
                        if(is_ims != 1){
                            self.lblStatus3.text = "Off".localized()
                            self.statusInstantMessage.backgroundColor = UIColor.gray
                        }
                        else{
                            self.lblStatus3.text = "on".localized()
                            self.statusInstantMessage.backgroundColor = UIColor(red: 188/255.0, green: 33/255.0, blue: 49/255.0, alpha: 1.0)
                        }
                        
                        if(is_phone != 1){
                            self.lblStatus4.text = "Off".localized()
                            self.statusRepeatCalls.backgroundColor = UIColor.gray
                        }
                        else{
                            self.lblStatus4.text = "on".localized()
                            self.statusRepeatCalls.backgroundColor = UIColor(red: 188/255.0, green: 33/255.0, blue: 49/255.0, alpha: 1.0)
                        }
                        
                        if(is_sms != 1){
                            self.lblStatus2.text = "Off".localized()
                            self.statusTextMessage.backgroundColor = UIColor.gray
                        }
                        else{
                            self.lblStatus2.text = "on".localized()
                            self.statusTextMessage.backgroundColor = UIColor(red: 188/255.0, green: 33/255.0, blue: 49/255.0, alpha: 1.0)
                        }
                        
                    }
                }
            }
            else{
                
                if let message = dict["message"] as? String{
                    self.showAlert(withMessage: message)
                }
                //
            }
            
            NotificationCenter.default.post(name: Notification.Name("membershipupdate"), object: nil)
            //            if((Constants.USERDEFAULTS .value(forKey: "isFirst")) != nil){
            //                Constants.USERDEFAULTS .removeObject(forKey: "isFirst")
            //                self.showHint()
            //            }
            
        }, failure: { (error) in
            
            print(error)
        })
        
        //        Utility.sharedUtility.sideMenu()
    }
    @IBAction func actionBack(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionOnOffTap(_ sender: UIButton) {
        if sender.tag == 0 {
            if lblStatus.text == "on".localized(){
                statusEmail.backgroundColor = UIColor.darkGray
                lblStatus.text = "Off".localized()
            }
            else{
                statusEmail.backgroundColor = UIColor.red
                lblStatus.text = "on".localized()
            }
        }
        else if sender.tag == 1 {
            if lblStatus2.text == "on".localized(){
                statusTextMessage.backgroundColor = UIColor.darkGray
                lblStatus2.text = "Off".localized()
            }
            else{
                statusTextMessage.backgroundColor = UIColor.red
                lblStatus2.text = "on".localized()
            }
        }
        else if sender.tag == 2 {
            if lblStatus3.text == "on".localized(){
                statusInstantMessage.backgroundColor = UIColor.darkGray
                lblStatus3.text = "Off".localized()
            }
            else{
                statusInstantMessage.backgroundColor = UIColor.red
                lblStatus3.text = "on".localized()
            }
        }
        else if sender.tag == 3 {
            if lblStatus4.text == "on".localized(){
                statusRepeatCalls.backgroundColor = UIColor.darkGray
                lblStatus4.text = "Off".localized()
            }
            else{
                statusRepeatCalls.backgroundColor = UIColor.red
                lblStatus4.text = "on".localized()
            }
        }
    }
    
    
    @IBAction func actionEdit(_ sender: Any) {
        
        let objEditProfileVC = storyBoard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC
        self.navigationController?.pushViewController(objEditProfileVC!, animated: true)
        //        if editable {
        //            editable = false
        //        }
        //        else{
        //            editable = true
        //        }
        //        textFieldEditable(_editable: editable)
    }
    
    @IBAction func actionDate(_ sender: Any) {
        dateControllerOpen()
    }
    
    @IBAction func actionCountryCode(_ sender: Any) {
//        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country: Country) in
//            self.txtAreaCode.text = country.dialingCode
//            //            self.btnCode.setTitle(country.dialingCode, for: .normal)
//            
//        }
//        // can customize the countryPicker here e.g font and color
//        countryController.detailColor = UIColor.red
        
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    func textFieldEditable(_editable: Bool) {
        txtSurname.isUserInteractionEnabled = _editable
        txtFirstName.isUserInteractionEnabled = _editable
        txtAreaCode.isUserInteractionEnabled = _editable
        txtPhoneNumber.isUserInteractionEnabled = _editable
        txtEmail.isUserInteractionEnabled = _editable
        txtBirthday.isUserInteractionEnabled = _editable
        txtGender.isUserInteractionEnabled = _editable
        txtPlace.isUserInteractionEnabled = _editable
        txtLanguage.isUserInteractionEnabled = _editable
    }
    
    func dateControllerOpen(){
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateInitialViewController() as! WWCalendarTimeSelector
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        present(selector, animated: true, completion: nil)
    }
    
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "MM / dd / yyyy"
        let bdate1 = dateFormatter.string(from: date as Date)
        txtBirthday.text = bdate1
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) ->Int {
        if(pickerView.tag == 0){
            return pickerGender.count
        }else if(pickerView.tag == 1){
            return countries.count
        }else{
            return pickerLanguage.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.tag == 0){
            return pickerGender[row]
        }else if(pickerView.tag == 1){
            return countries[row]
        }else{
            return pickerLanguage[row]
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView.tag == 0){
            self.txtGender.text =  pickerGender[row]
        }else if(pickerView.tag == 1){
            self.txtPlace.text =  countries[row]
        }else{
            self.txtLanguage.text = pickerLanguage[row]
        }
        
    }
    
}

extension ProfileVC: CountryListDelegate {
    
    func selectedCountry(country: Country) {
        print(country.name as Any)
        print(country.flag as Any)
        print(country.countryCode)
        print(country.phoneExtension)
        self.txtAreaCode.text = country.phoneExtension
       
    }
}


extension UIButton {
    func underline()
    {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius = 0
        self.layer.borderWidth = 0.0
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.masksToBounds = true
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

