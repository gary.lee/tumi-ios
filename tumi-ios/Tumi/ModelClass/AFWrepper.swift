//
//  AFWrepper.swift
//  OfferTreat
//
//  Created by Redspark on 15/05/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import Alamofire

class AFWrapper: NSObject{
    
    class func postMethod (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void)
    {
        if(NetworkManager.sharedInstance.reachability.connection == .none){
           Utility.sharedUtility.showMessage(INTERNETMSG)
           Utility.sharedUtility.HideLoader()
            return
        }
        
        print(params)
        print(apikey)
        
        Utility.sharedUtility.ShowLoader()
        let strURL =
            baseURL + apikey
        let url = URL(string: strURL)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(url!, method: .post, parameters: params)
            .responseJSON
            {
                response in
                switch (response.result)
                {
                case .success:
                    let jsonResponse = response.result.value as! NSDictionary
                    print(jsonResponse)
                    completion(jsonResponse)
                  Utility.sharedUtility.HideLoader()
                case .failure(let error):
                    Utility.sharedUtility.showMessage("Something went wrong")
                  Utility.sharedUtility.HideLoader()
                    failure(error)
                    
                    break
                }
        }
    }
    
    class func GetMethod (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void)
    {
        if(NetworkManager.sharedInstance.reachability.connection == .none){
       
           Utility.sharedUtility.showMessage(INTERNETMSG)
            return
        }
        
        Utility.sharedUtility.ShowLoader()
        let strURL = baseURL + apikey
        let url = URL(string: strURL)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.request(url!, method: .get, parameters: params)
            .responseJSON
            {
                response in
                
                switch (response.result)
                {
                case .success:
                    
                    //do json stuff
                    let jsonResponse = response.result.value as! NSDictionary
                    print(jsonResponse)
                    completion(jsonResponse)
                    //Utils().HideLoader()
                    
                case .failure(let error):
                    
                  Utility.sharedUtility.showMessage("Something went wrong")
                    failure(error)
                   Utility.sharedUtility.HideLoader()
                    break
                }
        }
    }
    
    class func postMultiPartdata (params: [String : AnyObject], apikey: String, completion: @escaping (Any) -> Void, failure:@escaping (Error) -> Void){
        
       if(NetworkManager.sharedInstance.reachability.connection == .none){
  
           Utility.sharedUtility.showMessage(INTERNETMSG)
            return
        }
        
        Utility.sharedUtility.ShowLoader()
        let strURL = baseURL + apikey
        let url = URL(string: strURL)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                //    multipartFormData.append(imageData, withName: "user", fileName: "user.jpg", mimeType: "image/jpeg")
                
                for (key, value) in params {
                    
                    if value is Data{
                        let r = arc4random()
                        let filename = "user\(r).jpg"
                        let imgData = params[key] as! Data
                        MultipartFormData.append(imgData, withName: "profile_image_url", fileName: filename, mimeType: "image/jpeg")
                    }
                    else{
                        MultipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                    
                }
        }, to: url!) { (result) in
            
            switch (result)
            {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response)
                    let jsonResponse = response.result.value as! NSDictionary
                    print(jsonResponse)
                    completion(jsonResponse)
                    Utility.sharedUtility.HideLoader()
                    
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }

}
