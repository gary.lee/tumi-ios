//
//  CategoryModel.swift
//  Tumi
//
//  Created by Dhruv Patel on 12/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class CategoryModel: NSObject {
    
    var id : Int = 0
    var name : String = ""
    var imagePath : String = ""
    var videoPath : String = ""
    var type : String = ""

    func initWith(dictionary: Dictionary<String, Any>) {
        if let _id = dictionary["id"] as? Int {
            id = _id
        }
        if let _name = dictionary["name"] as? String {
            name = _name
        }
        if let _imagePath = dictionary["image_path"] as? String {
            imagePath = _imagePath
        }
        if let _videoPath = dictionary["video_path"] as? String {
            videoPath = _videoPath
        }
        if let _type = dictionary["type"] as? String {
            type = _type
        }
    }
}
