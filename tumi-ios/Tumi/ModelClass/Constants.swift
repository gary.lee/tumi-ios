//
//  Constants.swift
//  Bankable Solution
//
//  Created by Appster on 09/05/17.
//  Copyright © 2017 alm. All rights reserved.
//
//com.googleusercontent.apps.313192316404-ghum6vmr956psnjv486841c1ut1pbsvf gmail URL type
import Foundation
import UIKit

class Constants {
    
    public static let FONT = "Surt-Regular"
    public static let Roboto_Light = "Surt-Light"
    public static let FONT_Bold = "Surt-bold"
    public static let FONT_Medium = "Surt-Medium"
    
    public static let GooglePlaceKey = "AIzaSyCQGN2AbXc2f03qHTVgtJZ5gDcDrYBFNF8"
    public static let NetworkUnavailable = "Network unavailable. Please check your internet connectivity"
    
    public static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    public static let USERDEFAULTS = UserDefaults.standard
    
    public static let APPSTORE_ID = 1237620237
    
    public static let device_type = "ios" // iOS = 2

}
