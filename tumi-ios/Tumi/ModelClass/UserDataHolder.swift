//
//  UserDataHolder.swift
//  Tumi
//
//  Created by Dhruv Patel on 10/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit

class UserDataHolder: NSObject {
    
    static let sharedUser = UserDataHolder()
    
    private override init() {
        super.init()
    }
    
    var defaults: UserDefaults!{
        get{
            return UserDefaults.standard
        }
    }
    
    var userId: Int!{
        get{
            return self.defaults.integer(forKey: "USER_ID")
        }
        set{
            self.defaults.set(newValue, forKey: "USER_ID")
            self.defaults.synchronize()
        }
    }
    
    var language: String!{
        get{
            return (self.defaults.string(forKey: "LANGUAGE") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "LANGUAGE")
            self.defaults.synchronize()
        }
    }
    
    var deviceToken: String!{
        get{
            return (self.defaults.string(forKey: "DEVICE_TOKEN") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "DEVICE_TOKEN")
            self.defaults.synchronize()
        }
    }
    
    var firstName: String!{
        get{
            return (self.defaults.string(forKey: "FIRST_NAME") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "FIRST_NAME")
            self.defaults.synchronize()
        }
    }
    
    var lastName: String!{
        get{
            return (self.defaults.string(forKey: "LAST_NAME") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "LAST_NAME")
            self.defaults.synchronize()
        }
    }
    
    var surname: String!{
        get{
            return (self.defaults.string(forKey: "SURNAME") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "SURNAME")
            self.defaults.synchronize()
        }
    }
    
    var phoneNumber: String!{
        get{
            return (self.defaults.string(forKey: "PHONE_NUMBER") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "PHONE_NUMBER")
            self.defaults.synchronize()
        }
    }
    
    var areacode: String!{
        get{
            return (self.defaults.string(forKey: "area_code") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "area_code")
            self.defaults.synchronize()
        }
    }
    
    var gender: String!{
        get{
            return (self.defaults.string(forKey: "GENDER") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "GENDER")
            self.defaults.synchronize()
        }
    }
    
    var email: String!{
        get{
            return (self.defaults.string(forKey: "EMAIL") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "EMAIL")
            self.defaults.synchronize()
        }
    }
    
    var birthDate: String!{
        get{
            return (self.defaults.string(forKey: "BIRTH_DATE") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "BIRTH_DATE")
            self.defaults.synchronize()
        }
    }
    
    var region: String!{
        get{
            return (self.defaults.string(forKey: "REGION") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "REGION")
            self.defaults.synchronize()
        }
    }
    
    var membership_level: String!{
        get{
            return (self.defaults.string(forKey: "membership_level") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "membership_level")
            self.defaults.synchronize()
        }
    }
    
    var password: String!{
        get{
            return (self.defaults.string(forKey: "PASSWORD") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "PASSWORD")
            self.defaults.synchronize()
        }
    }
    
    var isLoggedIn: Bool!{
        get{
            return self.defaults.bool(forKey: "IS_LOGGED_IN")
        }
        set{
            self.defaults.set(newValue, forKey: "IS_LOGGED_IN")
            self.defaults.synchronize()
        }
    }
    
    
    var is_edm: String!{
        get{
            return (self.defaults.string(forKey: "is_edm") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "is_edm")
            self.defaults.synchronize()
        }
    }
    
    
    var is_ims: String!{
        get{
            return (self.defaults.string(forKey: "is_ims") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "is_ims")
            self.defaults.synchronize()
        }
    }
    
    var is_phone: String!{
        get{
            return (self.defaults.string(forKey: "is_phone") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "is_phone")
            self.defaults.synchronize()
        }
    }
    
    var is_sms: String!{
        get{
            return (self.defaults.string(forKey: "is_sms") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "is_sms")
            self.defaults.synchronize()
        }
    }
    
    var forOnlieStore: String!{
        get{
            return (self.defaults.string(forKey: "forOnlieStore") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "forOnlieStore")
            self.defaults.synchronize()
        }
    }
    
    var foronlinestoreid: String!{
        get{
            return (self.defaults.string(forKey: "foronlinestoreid") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "foronlinestoreid")
            self.defaults.synchronize()
        }
    }
    
    var lattitude: String!{
        get{
            return (self.defaults.string(forKey: "lattitude") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "lattitude")
            self.defaults.synchronize()
        }
    }
    
    var longitude: String!{
        get{
            return (self.defaults.string(forKey: "longitude") ?? "")
        }
        set{
            self.defaults.set(newValue, forKey: "longitude")
            self.defaults.synchronize()
        }
    }
    
    
    func clearUserdefaults() {
        defaults.removeObject(forKey: "USER_ID")
        defaults.removeObject(forKey: "LANGUAGE")
        defaults.removeObject(forKey: "FIRST_NAME")
        defaults.removeObject(forKey: "LAST_NAME")
        defaults.removeObject(forKey: "SURNAME")
        defaults.removeObject(forKey: "PHONE_NUMBER")
        defaults.removeObject(forKey: "GENDER")
        defaults.removeObject(forKey: "EMAIL")
        defaults.removeObject(forKey: "BIRTH_DATE")
        defaults.removeObject(forKey: "REGION")
        defaults.removeObject(forKey: "PASSWORD")
        defaults.removeObject(forKey: "IS_LOGGED_IN")
    }
    
}
