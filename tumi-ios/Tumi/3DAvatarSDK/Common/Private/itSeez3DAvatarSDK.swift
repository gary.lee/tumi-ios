/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import UIKit
import Alamofire
import AlamofireObjectMapper
import OAuthSwift

var modelFolder = "models"
let kServerAddress:String = "https://api.avatarsdk.com/"
//let kServerAddressFake:String = "http://192.168.0.11:8000/"

let kUsersURL:String = "\(kServerAddress)players/"
let kUserURLTemplate: (String) -> String = { return "\(kUsersURL)\($0)/" }

let kAvatarsURL:String = "\(kServerAddress)avatars/"
let kAvatarURLTemplate: (String) -> String = { return "\(kAvatarsURL)\($0)/" }

let kAvatarHaircutsURLTemplate: (String) -> String = { return "\(kAvatarsURL)\($0)/haircuts/" }
let kAvatarHaircutURLTemplate: (String, String) -> String = { return "\(kAvatarsURL)\($0)/haircuts/\($1)" }

let kAuthorizeURL:String = "\(kServerAddress)o/authorize/"
let kTokenURL:String = "\(kServerAddress)o/token/"
let kGrantType:String = "client_credentials"

/// `itSeez3DAvatarSDK` is used to initialize SDK and `authorize` user.
public class itSeez3DAvatarSDK: NSObject {

    static var oauthSwift             : OAuth2Swift!
    static let sessionManager         : SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders

        return SessionManager(configuration: configuration)
    }()

    static let downloadSessionManager : SessionManager = { //TODO: use background queue
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 0
        configuration.timeoutIntervalForResource = 0

        return SessionManager(configuration: configuration)
    }()

    //MARK: Initialize

    /// Should be called first to initialize and handle the configuration of itSeez3DAvatarSDK. After this call you can use the SDK for other purposes.
    ///
    /// - parameter consumerKey: Consumer key provided in the developer section in Web API.
    /// - parameter consumerSecret: Consumer secret provided in the developer section in Web API.
    /// - parameter modelFolderName: Path to folder in Documents (standard iOS folder) which will be used to store the models and temporary files.
    public class func with(consumerKey: String, consumerSecret: String, modelFolderName: String = "models") {
        modelFolder = modelFolderName

        oauthSwift = OAuth2Swift.init(consumerKey: consumerKey, consumerSecret: consumerSecret, authorizeUrl: kAuthorizeURL, accessTokenUrl: kTokenURL, responseType: "token")

        if let credential = Storage.oauthCredential {
            oauthSwift.client.credential.oauthToken = credential.oauthToken
            oauthSwift.client.credential.oauthTokenExpiresAt = credential.oauthTokenExpiresAt
        }

        sessionManager.adapter = OAuthSwift2RequestAdapter(oauthSwift)
        sessionManager.retrier = OAuthSwift2RequestAdapter(oauthSwift)

        downloadSessionManager.adapter = OAuthSwift2RequestAdapter(oauthSwift)
        downloadSessionManager.retrier = OAuthSwift2RequestAdapter(oauthSwift)
    }

    //MARK: Authorize

    /// Is used to authorize user. Will be automatically called again in the case of 401 (Unauthorized) http error.
    ///
    /// - parameter completion: Closure to be executed once the request has finished.
    public class func authorize(completion: @escaping CompletionHandler<Bool>) {
        oauthSwift?.authorize(deviceToken: "", grantType: kGrantType,
            success: { credential in
                Storage.oauthCredential = oauthSwift.client.credential
                completion(.success(true))
        },
            failure: { error in
                completion(.failure(error))
        }
        )
    }

    /// Is used to determine the authorization status.
    public class func isAuthorized() -> Bool {
        return (oauthSwift.client.credential.oauthTokenExpiresAt != nil) && !oauthSwift.client.credential.isTokenExpired()
    }

    internal class func downloadFile(link: URL!, to: URL!, downloadingProgress: @escaping (Double, URL) -> Void, completion: @escaping CompletionHandler<URL>) {

        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (to, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        downloadSessionManager.download(link, to: destination).validate()
            .downloadProgress { (progress) in
                downloadingProgress(progress.fractionCompleted, to)
            }
            .response { (response) in //TODO: error after fileURL
                if let fileURL = response.destinationURL {
                    if let error = response.error {
                        do {
                            if FileManager.default.fileExists(atPath: fileURL.path) {
                                try FileManager.default.removeItem(at: fileURL)
                            }
                        } catch {
                            print("failure: downloadFile");
                        }

                        completion(Result.failure(error))
                    } else {
                        completion(Result.success(fileURL))
                    }
                } else {
                    //completion(Result.failure(error))
                    print("failure: destinationURL is empty");
                }
            }
    }

    //MARK: User

    internal class func getUsers(beforeDate date: Date? = nil, completion: @escaping CompletionHandler<[ResponseUserModel]>) {
        if let _date = date {
            let parameters: Parameters = ["created_before": _date]
            sessionManager.request(kUsersURL, parameters: parameters).validate().responseArray { (response: DataResponse<[ResponseUserModel]>) in
                completion(response.result)
            }
        } else {
            sessionManager.request(kUsersURL).validate().responseArray { (response: DataResponse<[ResponseUserModel]>) in
                completion(response.result)
            }
        }
    }

    internal class func getUser(userCode: String, completion: @escaping CompletionHandler<ResponseUserModel>) {
        let url = kUserURLTemplate(userCode)
        sessionManager.request(url).validate().responseObject { (response: DataResponse<ResponseUserModel>) in
            completion(response.result)
        }
    }

    /// Is used to create a new user on server. Should be called after SDK authorization. All avatars will be created for this user.
    ///
    /// - parameter completion: Closure to be executed once the request has finished.
    public class func createNewUser(completion: @escaping CompletionHandler<ResponseUserModel>) {
        let url = kUsersURL
        sessionManager.request(url, method:.post).validate().responseObject { (response: DataResponse<ResponseUserModel>) in
            switch (response.result) {
            case .success(let value):
                Storage.user = value
            default: break
            }
            completion(response.result)
        }
    }

    /// Is used to change the user avatars are requested for.
    ///
    /// - parameter user: Information about user received from server.
    public class func setUser(user: ResponseUserModel) {
        Storage.user = user
    }

    //MARK: Avatar

    internal class func sendImage(image: UIImage, name: String, description: String, uploadingProgress: @escaping (Double) -> Void, completion: @escaping CompletionHandler<ResponseAvatarModel>) {
        if let data = image.jpegData(compressionQuality: 1.0), let nameData = name.data(using: .utf8), let descData = description.data(using: .utf8) {
            downloadSessionManager.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(data, withName: "photo", fileName: "photo.jpg", mimeType: "image/jpeg")
                    multipartFormData.append(nameData, withName: "name")
                    multipartFormData.append(descData, withName: "description")
            },
                to: kAvatarsURL,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.validate()
                            .uploadProgress { progress in // main queue by default
                                uploadingProgress(progress.fractionCompleted)
                            }
                            .responseObject { (response: DataResponse<ResponseAvatarModel>) in
                                completion(response.result)
                            }
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            }
            )
        }
    }

    internal class func requestAvatars(beforeDate date: Date? = nil, completion: @escaping CompletionHandler<[ResponseAvatarModel]>) {
        if let _date = date {
            let parameters: Parameters = [
                "created_before": _date,
            ]
            sessionManager.request(kAvatarsURL, parameters: parameters).validate().responseArray { (response: DataResponse<[ResponseAvatarModel]>) in
                completion(response.result)
            }
        } else {
            sessionManager.request(kAvatarsURL).validate().responseArray { (response: DataResponse<[ResponseAvatarModel]>) in
                completion(response.result)
            }
        }
    }

    internal class func getAvatar(avatarCode: String, completion: @escaping CompletionHandler<ResponseAvatarModel>) {
        let url = kAvatarURLTemplate(avatarCode)
        sessionManager.request(url).validate().responseObject { (response: DataResponse<ResponseAvatarModel>) in
            completion(response.result)
        }
    }

    internal class func deleteAvatar(avatarCode: String, completion: @escaping CompletionHandler<Bool>) {
        let url = kAvatarURLTemplate(avatarCode)
        sessionManager.request(url, method:.delete).validate().response { (response) in
            if let error = response.error {
                completion(.failure(error))
            } else {
                completion(.success(true))
            }
        }
    }

    //MARK: Haircut

    internal class func getHaircuts(forAvatarCode avatarCode: String, completion: @escaping CompletionHandler<[ResponseAvatarHaircutModel]>) {
        let url = kAvatarHaircutsURLTemplate(avatarCode)
        sessionManager.request(url).validate().responseArray { (response: DataResponse<[ResponseAvatarHaircutModel]>) in
            completion(response.result)
        }
    }

    internal class func getAvatarHaircut(avatarCode: String, haircutIdentity: String, completion: @escaping CompletionHandler<ResponseAvatarHaircutModel>) {
        let url = kAvatarHaircutURLTemplate(avatarCode, haircutIdentity)
        sessionManager.request(url).validate().responseObject { (response: DataResponse<ResponseAvatarHaircutModel>) in
            completion(response.result)
        }
    }

}
