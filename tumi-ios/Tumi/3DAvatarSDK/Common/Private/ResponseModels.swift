/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import ObjectMapper

internal class ISO8601_DateTransform: DateFormatterTransform {

    internal init() {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"

        super.init(dateFormatter: formatter)
    }
    
}

/// `ResponseUserModel` is used for mapping server response. For more information see Web API documentation (https://avatar-api.itseez3d.com/).
open class ResponseUserModel: NSObject, Mappable, NSCoding {
    public var url         : URL?
    public var code        : String!
    public var createdTime : Date?
    public var comment     : String?

    public required init?(map: Map){

    }

    public func mapping(map: Map) {
        url <- (map["url"], URLTransform())
        code <- map["code"]
        createdTime <- (map["created_on"], ISO8601_DateTransform())
        comment <- map["comment"]
    }

    public init(url:URL?, code:String!, createdTime:Date?, comment:String?) {
        self.url = url
        self.code = code
        self.createdTime = createdTime
        self.comment = comment
    }

    required convenience public init(coder aDecoder: NSCoder) {
        let url = aDecoder.decodeObject(forKey: "url") as? URL
        let code = aDecoder.decodeObject(forKey: "code") as! String
        let createdTime = aDecoder.decodeObject(forKey: "createdTime") as? Date
        let comment = aDecoder.decodeObject(forKey: "comment") as? String
        self.init(url: url, code: code, createdTime: createdTime, comment: comment)
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: "url")
        aCoder.encode(code, forKey: "code")
        aCoder.encode(createdTime, forKey: "createdTime")
        aCoder.encode(comment, forKey: "comment")
    }
}

/// `ResponseAvatarModel` is used for mapping server response. For more information see Web API documentation (https://avatar-api.itseez3d.com/).
open class ResponseAvatarModel: NSObject, Mappable, NSCoding {
    public var url         : URL?
    public var code        : String!
    public var status      : String?
    public var progress    : Int?
    public var name        : String?
    public var desc        : String?
    public var createdTime : Date?
    public var changedTime : Date?
    public var meshURL     : URL?
    public var textureURL  : URL?
    public var previewURL  : URL?
    public var haircutsURL : URL?

    public required init?(map: Map){

    }

    public func mapping(map: Map) {
        url <- (map["url"], URLTransform())
        code <- map["code"]
        status <- map["status"]
        progress <- map["progress"]
        name <- map["name"]
        desc <- map["description"]
        createdTime <- (map["created_on"], ISO8601_DateTransform())
        changedTime <- (map["ctime"], ISO8601_DateTransform())
        meshURL <- (map["mesh"], URLTransform())
        textureURL <- (map["texture"], URLTransform())
        previewURL <- (map["preview"], URLTransform())
        haircutsURL <- (map["haircuts"], URLTransform())
    }

    public init(url:URL?, code:String!, status:String?, progress:Int?, name:String?, desc:String?, createdTime:Date?, changedTime:Date?, meshURL:URL?, textureURL:URL?, previewURL:URL?, haircutsURL:URL?) {
        self.url = url
        self.code = code
        self.status = status
        self.progress = progress
        self.name = name
        self.desc = desc
        self.createdTime = createdTime
        self.changedTime = changedTime
        self.meshURL = meshURL
        self.textureURL = textureURL
        self.previewURL = previewURL
        self.haircutsURL = haircutsURL
    }

    required convenience public init(coder aDecoder: NSCoder) {
        let url = aDecoder.decodeObject(forKey: "url") as? URL
        let code = aDecoder.decodeObject(forKey: "code") as! String
        let status = aDecoder.decodeObject(forKey: "status") as? String
        let progress = aDecoder.decodeObject(forKey: "progress") as? Int
        let name = aDecoder.decodeObject(forKey: "name") as? String
        let desc = aDecoder.decodeObject(forKey: "desc") as? String
        let createdTime = aDecoder.decodeObject(forKey: "createdTime") as? Date
        let changedTime = aDecoder.decodeObject(forKey: "changedTime") as? Date
        let meshURL = aDecoder.decodeObject(forKey: "meshURL") as? URL
        let textureURL = aDecoder.decodeObject(forKey: "textureURL") as? URL
        let previewURL = aDecoder.decodeObject(forKey: "previewURL") as? URL
        let haircutsURL = aDecoder.decodeObject(forKey: "haircutsURL") as? URL
        self.init(url: url, code: code, status: status, progress: progress, name: name, desc: desc, createdTime: createdTime, changedTime: changedTime, meshURL: meshURL, textureURL: textureURL, previewURL: previewURL, haircutsURL: haircutsURL)
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(url, forKey: "url")
        aCoder.encode(code, forKey: "code")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(progress, forKey: "progress")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(createdTime, forKey: "createdTime")
        aCoder.encode(changedTime, forKey: "changedTime")
        aCoder.encode(meshURL, forKey: "meshURL")
        aCoder.encode(textureURL, forKey: "textureURL")
        aCoder.encode(previewURL, forKey: "previewURL")
        aCoder.encode(haircutsURL, forKey: "haircutsURL")
    }

    internal func computed() -> Bool
    {
        return status == "Completed";
    }

    internal func failed() -> Bool
    {
        return status == "Failed" || status == "Timed Out";
    }
}

/// `ResponseAvatarHaircutModel` is used for mapping server response. For more information see Web API documentation (https://avatar-api.itseez3d.com/).
open class ResponseAvatarHaircutModel: NSObject, Mappable, NSCoding {
    public var identity         : String!
    public var gender           : String?
    public var previewURL       : URL?
    public var url              : URL?
    public var textureURL       : URL?
    public var modelURL         : URL!
    public var pointcloudURL    : URL?
    public var meshURL          : URL?

    public required init?(map: Map){

    }

    public func mapping(map: Map) {
        identity <- map["identity"]
        gender <- map["gender"]
        previewURL <- (map["preview"], URLTransform())
        url <- (map["url"], URLTransform())
        textureURL <- (map["texture"], URLTransform())
        modelURL <- (map["model"], URLTransform())
        pointcloudURL <- (map["pointcloud"], URLTransform())
        meshURL <- (map["mesh"], URLTransform())
    }

    public init(identity:String!, gender:String?, previewURL:URL?, url:URL?, textureURL:URL?, modelURL:URL?, pointcloudURL:URL?, meshURL:URL?) {
        self.identity = identity
        self.gender = gender
        self.previewURL = previewURL
        self.url = url
        self.textureURL = textureURL
        self.modelURL = modelURL
        self.pointcloudURL = pointcloudURL
        self.meshURL = meshURL
    }

    required convenience public init(coder aDecoder: NSCoder) {
        let identity = aDecoder.decodeObject(forKey: "identity") as! String
        let gender = aDecoder.decodeObject(forKey: "gender") as? String
        let previewURL = aDecoder.decodeObject(forKey: "previewURL") as? URL
        let url = aDecoder.decodeObject(forKey: "url") as? URL
        let textureURL = aDecoder.decodeObject(forKey: "textureURL") as? URL
        let modelURL = aDecoder.decodeObject(forKey: "modelURL") as? URL
        let pointcloudURL = aDecoder.decodeObject(forKey: "pointcloudURL") as? URL
        let meshURL = aDecoder.decodeObject(forKey: "meshURL") as? URL
        self.init(identity: identity, gender: gender, previewURL: previewURL, url: url, textureURL: textureURL, modelURL: modelURL, pointcloudURL: pointcloudURL, meshURL: meshURL)
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(identity, forKey: "identity")
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(previewURL, forKey: "previewURL")
        aCoder.encode(url, forKey: "url")
        aCoder.encode(textureURL, forKey: "textureURL")
        aCoder.encode(modelURL, forKey: "modelURL")
        aCoder.encode(pointcloudURL, forKey: "pointcloudURL")
        aCoder.encode(meshURL, forKey: "meshURL")
    }
}
