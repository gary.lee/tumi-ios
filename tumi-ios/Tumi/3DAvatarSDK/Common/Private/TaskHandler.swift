/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import Alamofire


public typealias CompletionHandler<T>     = (Result<T>) -> Void

/// `TaskHandler` is used for subscription on task progress and completion callbacks. For more information see `AvatarManager` and `HaircutManager`.
public class TaskHandler<Model> {

    public typealias ProgressHandler          = (TaskProgress) -> Void
    
    /// `TaskProgress` is used to provide the task progress
    open class TaskProgress {
        public var state       : String
        public var percent     : Int

        internal init(state: String, percent:Int)
        {
            self.state = state
            self.percent = percent
        }
    }

    var inProgress                = false
    var progressHandler           : ProgressHandler?
    var completionHandler         : CompletionHandler<Model>?

    /// Sets a closure to be called periodically during the lifecycle.
    ///
    /// - parameter handler: Closure to be executed periodically.
    ///
    /// - returns: The task handler.
    @discardableResult
    public func progress(handler: @escaping ProgressHandler) -> Self {
        progressHandler = handler
        return self
    }

    /// Adds a handler to be called once the task has finished.
    ///
    /// - parameter handler: Closure to be executed once the model downloading has finished.
    ///
    /// - returns: The task handler.
    @discardableResult
    public func completion(handler: @escaping CompletionHandler<Model>) -> Self {
        completionHandler = handler
        return self
    }

    func updateProgress(state: String, percent: Int) {
        progressHandler?(TaskProgress(state: state, percent: percent))
    }

    func taskComplete(withResult result:Result<Model>) {
        inProgress = false
        completionHandler?(result)
    }
}
