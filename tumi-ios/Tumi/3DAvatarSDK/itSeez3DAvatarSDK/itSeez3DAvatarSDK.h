/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import <UIKit/UIKit.h>

//! Project version number for itSeez3DAvatarSDK.
FOUNDATION_EXPORT double itSeez3DAvatarSDKVersionNumber;

//! Project version string for itSeez3DAvatarSDK.
FOUNDATION_EXPORT const unsigned char itSeez3DAvatarSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <itSeez3DAvatarSDK/PublicHeader.h>

