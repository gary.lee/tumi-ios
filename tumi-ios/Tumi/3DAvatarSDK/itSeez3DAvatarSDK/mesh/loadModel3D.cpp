/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#include <fstream>
#include <iostream>
#include "loadModel3D.hpp"

void parsePlyHeader(std::istream &inputMesh,
                    std::string &textureBasename,
                    bool &existVertices,
                    bool &existVerticesNormals,
                    bool &existVerticesFlags,
                    bool &existVerticesColors,
                    bool &existVerticesWeights,
                    bool &existFaces,
                    bool &existUvMapping,
                    size_t &countElementVertexInMesh,
                    size_t &countElementFacesInMesh,
                    PhysicalUnits *physicalUnits = 0)
{
    bool loadPhysicalUnits = physicalUnits != 0;
    std::string line;
    while(std::getline(inputMesh, line))
    {
        if (loadPhysicalUnits && (strstr(line.c_str(), "Physical units:") != 0))
        {
            *physicalUnits = string2physicalUnits(line.substr(line.find_last_of(' ') + 1));
            loadPhysicalUnits = false;

            continue;
        }

        if (line.find("comment TextureFile ") != std::string::npos)
        {
            textureBasename = line.substr(line.find_last_of(" ") + 1);
            continue;
        }

        if (line.find("element vertex ") != std::string::npos)
        {
            countElementVertexInMesh = atoi(line.substr(line.find_last_of(" ") + 1).c_str());
            continue;
        }

        if (line.find("property float x") != std::string::npos)
        {
            existVertices = true;
            continue;
        }

        if (line.find("property float nx") != std::string::npos)
        {
            existVerticesNormals = true;
            continue;
        }

        if ((line.find("property int flags") != std::string::npos) ||
                (line.find("property float value") != std::string::npos))
        {
            existVerticesFlags = true;
            continue;
        }

        if (line.find("property uchar red") != std::string::npos)
        {
            existVerticesColors = true;
            continue;
        }

        if (line.find("property float w") != std::string::npos)
        {
            existVerticesWeights = true;
            continue;
        }

        if (line.find("element face ") != std::string::npos)
        {
            countElementFacesInMesh = atoi(line.substr(line.find_last_of(" ") + 1).c_str());
            continue;
        }

        if (line.find("property list uchar int vertex_indices") != std::string::npos)
        {
            existFaces = true;
            continue;
        }

        if (line.find("property list uchar float texcoord") != std::string::npos)
        {
            existUvMapping = true;
            continue;
        }

        if (line.find("end_header") != std::string::npos)
        {
            break;
        }
    }

    if (loadPhysicalUnits)
    {
        *physicalUnits = PHYSICAL_UNITS_UNKNOWN;
    }
}

namespace pointcloudMerge
{

void loadModelFromBinPLY(const std::string &filename,
                         std::vector<Point3f> *vertices,
                         std::vector<Point3f> *verticesNormals,
                         std::vector<std::vector<size_t> > *faces,
                         std::vector<std::vector<Point2d> > *uvMapping,
                         std::string *textureFilename,
                         PhysicalUnits *physicalUnits,
                         std::vector<float> *verticesWeights)
{

    std::ifstream inputMesh(filename.c_str(),std::ios::binary);
    if (!inputMesh.is_open())
    {
//        LOG(INFO) << "Error: can't load mesh from " << filename << std::endl;
        return;
    }

    std::string textureBasename;
    loadModelFromBinPLY(inputMesh, vertices, verticesNormals, faces, uvMapping, &textureBasename, physicalUnits, verticesWeights);

//    loadTexture(filename, textureBasename, texture, textureFilename);
}

void loadModelFromBinPLY(std::istream &inputMesh,
                         std::vector<Point3f> *vertices,
                         std::vector<Point3f> *verticesNormals,
                         std::vector<std::vector<size_t> > *faces,
                         std::vector<std::vector<Point2d> > *uvMapping,
                         std::string *_textureBasename,
                         PhysicalUnits *physicalUnits,
                         std::vector<float> *verticesWeights)
{
    bool existVertices = false, loadVertices = vertices != 0;
    bool existVerticesNormals = false, loadVerticesNormals = verticesNormals != 0;
    bool existFaces = false, loadFaces = faces != 0;
    bool existUvMapping = false, loadUvMapping = uvMapping != 0;
    bool existVerticesFlags = false, existVerticesColors = false;
    bool existVerticesWeights = false, loadVerticesWeights = verticesWeights != 0;

    const int sizeofInt = sizeof(int);
    const int sizeofChar = sizeof(char);
    const int sizeofFloat = sizeof(float);
//    CHECK_EQ(4 * sizeofChar, sizeofFloat);

    std::string textureBasename;
    size_t countElementVertexInMesh, countElementFacesInMesh;
    parsePlyHeader(inputMesh, textureBasename, existVertices, existVerticesNormals,
                   existVerticesFlags, existVerticesColors, existVerticesWeights,
                   existFaces, existUvMapping, countElementVertexInMesh, countElementFacesInMesh, physicalUnits);

    if (loadVertices && !existVertices)
    {
//        LOG(WARNING) << "Error: vertices doesn't exist in mesh file.";
        loadVertices = false;
    }
    if (loadVerticesNormals && !existVerticesNormals)
    {
//        LOG(WARNING) << "Error: normals doesn't exist in mesh file.";
        loadVerticesNormals = false;
    }
    if (loadFaces && !existFaces)
    {
//        LOG(WARNING) << "Error: faces doesn't exist in mesh file.";
        loadFaces = false;
    }
    if (loadUvMapping && !existUvMapping)
    {
//        LOG(WARNING) << "Error: uvMapping doesn't exist in mesh file.";
        loadUvMapping = false;
    }
    if (loadVerticesWeights && !existVerticesWeights)
    {
//        LOG(WARNING) << "Error: vertices weights doesn't exist in mesh file.";
        loadVerticesWeights = false;
    }

    std::vector<std::vector<Point2d> > junkUvMapping;
    if (!loadUvMapping && existUvMapping && loadFaces)
    {
        uvMapping = &junkUvMapping;
        loadUvMapping = true;
    }

    std::vector<std::vector<size_t> > junkFaces;
    if (!loadFaces && existFaces && loadUvMapping)
    {
        faces = &junkFaces;
        loadFaces = true;
    }

    if (_textureBasename != 0)
    {
        *_textureBasename = textureBasename;
    }

    if (loadVertices)
    {
        vertices->resize(countElementVertexInMesh);
    }
    if (loadVerticesNormals)
    {
        verticesNormals->resize(countElementVertexInMesh);
    }
    if (loadVerticesWeights)
    {
        verticesWeights->resize(countElementVertexInMesh);
    }

    if (loadFaces)
    {
        faces->resize(countElementFacesInMesh);
    }
    if (loadUvMapping)
    {
        uvMapping->resize(countElementFacesInMesh);
    }

//    CV_Assert(existVertices || !existVerticesNormals);
    if (existVertices || existVerticesNormals)
    {
        const int verticesValuesInLine = (existVertices ? 3 : 0) +
                (existVerticesNormals ? 3 : 0) +
                (existVerticesFlags ? 1 : 0) +
                (existVerticesColors ? 1 : 0) +
                (existVerticesWeights ? 1 : 0);
        for (size_t i = 0; i < countElementVertexInMesh; ++i)
        {
            std::vector<float> values(verticesValuesInLine);
            inputMesh.read(reinterpret_cast<char *>(&values[0]), sizeofFloat * verticesValuesInLine);
            if (loadVertices)
            {
                (*vertices)[i] = Point3f(values[0],
                                         values[1],
                                         values[2]);
            }

            if (loadVerticesNormals)
            {
                (*verticesNormals)[i] = Point3f(values[3],
                                                values[4],
                                                values[5]);
            }

            if (loadVerticesWeights)
            {
                (*verticesWeights)[i] = values[verticesValuesInLine - 1];
            }
        }
    }

//    CV_Assert(existFaces || !existUvMapping);
    if (loadFaces || loadUvMapping)
    {
        for (size_t i = 0; i < countElementFacesInMesh; ++i)
        {
            unsigned char countVertices;
            inputMesh.read(reinterpret_cast<char *>(&countVertices), sizeofChar);
//            LOG_IF_EVERY_N(WARNING, countVertices != 3, 10) << "vertices count in face: " << (int)countVertices << std::endl;

            if (loadFaces)
            {
                (*faces)[i].resize(countVertices);
                std::vector<int> values(countVertices);
                inputMesh.read(reinterpret_cast<char *>(&values[0]), sizeofInt * countVertices);
                for (size_t j = 0; j < countVertices; ++j)
                {
                    (*faces)[i][j] = values[j];
                }
            }

            if (loadUvMapping)
            {
                (*uvMapping)[i].resize(countVertices);
                unsigned char countUvMapping;
                inputMesh.read(reinterpret_cast<char *>(&countUvMapping), sizeofChar);
                std::vector<float> values(countUvMapping);
                inputMesh.read(reinterpret_cast<char *>(&values[0]), sizeofFloat * countUvMapping);
                for (size_t j = 0; j < countVertices; ++j)
                {
                    (*uvMapping)[i][j] = Point2d(values[2 * j], (1 - values[1 + 2 * j]));
                }
            }
        }
    }
}
}
