/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include "saveModel3D.hpp"

inline std::string pathDelimiter()
{
    return "/";
}

std::string getLabel()
{
    return "Created by ItSeez3D";
}

inline std::string getBasename(const std::string &filename)
{
    size_t lastPathDelimiter = filename.find_last_of(pathDelimiter());
    return (lastPathDelimiter == std::string::npos) ? filename : filename.substr(lastPathDelimiter + 1);
}

inline std::string getPhysicalUnitsLine(PhysicalUnits physicalUnits)
{
    return "Physical units: " + physicalUnits2string(physicalUnits);
}

std::string dumpPlyHeader(const std::string &textureFilename,
                          const bool saveVertices,
                          const bool saveVerticesNormals,
                          const bool saveFaces,
                          const bool saveUvMapping,
                          const bool saveFacesFlags,
                          const bool binaryPLY,
                          const size_t numVertices,
                          const size_t numFaces,
                          const std::vector<std::string> *comments,
                          const PhysicalUnits physicalUnits,
                          const bool saveVerticesColors = false,
                          const bool saveVerticesWeights = false)
{
    std::stringstream header;
    header << "ply\n";
    if (binaryPLY)
    {
        header << "format binary_little_endian 1.0\n";
    }
    else
    {
        header << "format ascii 1.0\n";
    }
    header << "comment " << getLabel() << "\n";
    header << "comment " << getPhysicalUnitsLine(physicalUnits) << "\n";

    if(comments != 0)
    {
        for(size_t i = 0; i < comments->size(); i ++)
        {
            header << "comment " << comments->at(i) << "\n";
        }
    }

    if (!textureFilename.empty())
    {
        header << "comment TextureFile " << getBasename(textureFilename) << "\n";
    }

    if (numVertices != 0)
    {
        header << "element vertex " << numVertices << "\n";
    }

    if (saveVertices)
    {
        header << "property float x\n";
        header << "property float y\n";
        header << "property float z\n";
    }

    if (saveVerticesNormals)
    {
        header << "property float nx\n";
        header << "property float ny\n";
        header << "property float nz\n";
    }

    if (saveVerticesColors)
    {
        header << "property uchar red\n";
        header << "property uchar green\n";
        header << "property uchar blue\n";
        header << "property uchar alpha\n";
    }

    if (saveVerticesWeights)
    {
        header << "property float w\n";
    }

    if (numFaces != 0)
    {
        header << "element face " << numFaces << "\n";
    }

    if (saveFaces)
    {
        header << "property list uchar int vertex_indices\n";
    }

    if (saveUvMapping)
    {
        header << "property list uchar float texcoord\n";
    }

    if (saveFacesFlags)
    {
        header << "property int flags\n";
    }

    header << "end_header\n";

    return header.str();
}

void saveStreamToFile(std::stringstream &stream, const std::string &filename, bool needEncryption)
{
    std::ofstream output(filename.c_str(), std::ios::binary);
    if (needEncryption)
    {
        std::vector<unsigned char> content, encrypted;
        for (std::istreambuf_iterator<char> it = std::istreambuf_iterator<char>(stream);
             it != std::istreambuf_iterator<char>();
             ++it)
        {
            content.push_back(static_cast<unsigned char>(*it));
        }
        output.write(reinterpret_cast<char *>(encrypted.data()), encrypted.size());
    }
    else
    {
        output << stream.rdbuf();
    }
    output.close();
}

namespace pointcloudMerge
{

void saveModelToBinPLY(const std::string &filename,
                       const std::vector<Point3f> *vertices,
                       const std::vector<Point3f> *verticesNormals,
                       const std::vector<std::vector<size_t> > *faces,
                       const std::vector<std::vector<Point2d> > *uvMapping,
                       const std::string &textureFilename,
                       const std::vector<int> *facesFlags,
                       const std::vector<std::string> *comments,
                       const PhysicalUnits physicalUnits,
                       const bool needEncryption,
//                       const std::vector<Vec4b> *verticesColors,
                       const std::vector<float> *verticesWeights)
{
    const bool saveVertices = vertices != 0;
    const bool saveVerticesNormals = verticesNormals != 0;
    const bool saveFaces = faces != 0;
    const bool saveUvMapping = uvMapping != 0;
    const bool saveFacesFlags = facesFlags != 0;
//    const bool saveVerticesColors = verticesColors != 0;
    const bool saveVerticesWeights = verticesWeights != 0;

    const int sizeofInt = sizeof(int);
    const int sizeofChar = sizeof(char);
    const int sizeofFloat = sizeof(float);

    std::stringstream stream;
    std::string header = dumpPlyHeader(textureFilename,
                                       saveVertices,
                                       saveVerticesNormals,
                                       saveFaces,
                                       saveUvMapping,
                                       saveFacesFlags,
                                       true,
                                       saveVertices ? vertices->size() : 0,
                                       saveFaces ? faces->size() : 0,
                                       comments,
                                       physicalUnits,
//                                       saveVerticesColors,
                                       saveVerticesWeights);
    stream.write(header.c_str(), sizeofChar * header.length());

//    CV_Assert(saveVertices || !saveVerticesNormals);
    if (saveVertices || saveVerticesNormals)
    {
//        CV_Assert(!saveVerticesNormals || (vertices->size() == verticesNormals->size()));
        for (size_t i = 0; i < vertices->size(); ++i)
        {
            stream.write((char*)&(*vertices)[i].x, sizeofFloat);
            stream.write((char*)&(*vertices)[i].y, sizeofFloat);
            stream.write((char*)&(*vertices)[i].z, sizeofFloat);

            if (saveVerticesNormals)
            {
                stream.write((char*)&(*verticesNormals)[i].x, sizeofFloat);
                stream.write((char*)&(*verticesNormals)[i].y, sizeofFloat);
                stream.write((char*)&(*verticesNormals)[i].z, sizeofFloat);
            }

//            if (saveVerticesColors)
//            {
//                stream.write((char*)&(*verticesColors)[i][0], sizeofChar);
//                stream.write((char*)&(*verticesColors)[i][1], sizeofChar);
//                stream.write((char*)&(*verticesColors)[i][2], sizeofChar);
//                stream.write((char*)&(*verticesColors)[i][3], sizeofChar);
//            }

            if (saveVerticesWeights)
            {
                stream.write((char*)&(*verticesWeights)[i], sizeofFloat);
            }
        }
    }

//    CV_Assert(saveFaces || !saveUvMapping);
    if (saveFaces || saveUvMapping || saveFacesFlags)
    {
        for (size_t i = 0; i < faces->size(); ++i)
        {
            u_char faceSize = (*faces)[i].size();
            stream.write((char*)&faceSize, sizeofChar);
            for (size_t j = 0; j < (*faces)[i].size(); ++j)
            {
                stream.write((char*)&(*faces)[i][j], sizeofInt);
            }
            if (saveUvMapping)
            {
//                CV_Assert((*faces)[i].size() == (*uvMapping)[i].size());
                u_char uvSize = 2 * (*faces)[i].size();
                stream.write((char*)&uvSize, sizeofChar);
                for (size_t j = 0; j < (*faces)[i].size(); ++j)
                {
                    float x = (*uvMapping)[i][j].x;
                    float y = (1 - (*uvMapping)[i][j].y);

                    double eps = 1e-6;
                    bool areCoordinatesValid = (x > -eps && x < 1.0 + eps) && (y > -eps && y < 1.0 + eps);
                    if (!areCoordinatesValid)
                    {
//                        LOG(ERROR) << "texture coordinates are out of range:";
//                        LOG(ERROR) << "x = " << x;
//                        LOG(ERROR) << "y = " << y;
                    }

                    stream.write((char*)&x, sizeofFloat);
                    stream.write((char*)&y, sizeofFloat);
                }
            }

            if (saveFacesFlags)
            {
                int faceFlag = (*facesFlags)[i];
                stream.write((char*)&faceFlag, sizeofInt);
            }
        }
    }

    saveStreamToFile(stream, filename, needEncryption);

//    if (texture != 0)
//        imwrite(textureFilename, *texture);
}
}
