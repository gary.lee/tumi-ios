/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#include "types.hpp"

std::string physicalUnits2string(PhysicalUnits physicalUnits)
{
    if (physicalUnits >= PHYSICAL_UNITS_COUNT)
    {
        return "";
    }

    return PhysicalUnitsName[physicalUnits];
}

PhysicalUnits string2physicalUnits(const std::string &str)
{
    for (int i = 0; i < PHYSICAL_UNITS_COUNT; ++i)
    {
        if (str == PhysicalUnitsName[i])
        {
            return static_cast<PhysicalUnits>(i);
        }
    }

    return PHYSICAL_UNITS_UNKNOWN;
}
