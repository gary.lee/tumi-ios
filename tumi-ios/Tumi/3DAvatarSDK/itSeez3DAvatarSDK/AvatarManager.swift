/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import Alamofire

/// `AvatarManager` is used to handle all operations with avatars.
public class AvatarManager {

    static var avatarModels = [String: AvatarModel]()
    static var loaderDict = [String: AvatarLoader]()

    /// Is used to create avatar on the server.
    ///
    /// - parameter image:       Image which will be used for the avatar generation.
    /// - parameter name:        Name for this avatar.
    /// - parameter description: Description for this avatar.
    ///
    /// - returns: Task handler object which can be used for subscription on progress and completion callbacks.
    public class func createAvatar(fromImage image: UIImage, name: String, description: String = "") -> TaskHandler<AvatarModel> {
        let sender = AvatarSender()
        return sender.sendImage(image: image, name: name, description: description)
    }

    /// Is used to retrieve paginated list of available avatar models for current user sorted by date.
    ///
    /// - parameter date: Avatars in the next retrieved part will start from the specified date. If not specified, the first part of avatars will be retrived.
    /// - parameter completion: Closure to be executed once the request has finished.
    public class func getAvatars(beforeDate date: Date? = nil, completion: @escaping CompletionHandler<[AvatarModel]>) {
        itSeez3DAvatarSDK.requestAvatars(beforeDate: date) { (result) in
            switch (result) {
            case .success(let value):
                let avatarModels = value.map({ (responseModel) -> AvatarModel in
                    self.buildAvatar(fromResponse: responseModel)
                })
                completion(.success(avatarModels))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    /// Is used to retrieve the `AvatarModel` from the server.
    ///
    /// - parameter code: The avatar model ID for which the server information should be downloaded.
    /// - parameter completion: Closure to be executed once the request has finished.
    public class func getAvatar(code: String, completion: @escaping CompletionHandler<AvatarModel>) {
        itSeez3DAvatarSDK.getAvatar(avatarCode: code) { (result) in
            switch (result) {
            case .success(let value):
                completion(.success(self.buildAvatar(fromResponse: value)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    /// Is used to download the avatar files from the server.
    ///
    /// - parameter avatarModel: The avatar model which files should be downloaded.
    ///
    /// - returns: Task handler object which can be used for subscription on progress and completion callbacks.
    public class func downloadAvatar(avatarModel: AvatarModel) -> TaskHandler<AvatarModel> {

        if let loader = loaderDict[avatarModel.responseAvatarModel.code] {
            return loader.tryDownloadModel()
        }

        let loader = AvatarLoader(avatar: avatarModel)
        loaderDict[avatarModel.responseAvatarModel.code] = loader
        return loader.tryDownloadModel()
    }

    /// Is used to delete avatar from the server. If this avatar was downloaded on this device than it will be also deleted from the device.
    ///
    /// - parameter avatarModel: The avatar model that should be deleted.
    /// - parameter completion: Closure to be executed once the request has finished.
    public class func deleteAvatar(avatarModel: AvatarModel, completion: @escaping CompletionHandler<Bool>) {

        let folder: Folder = avatarModel.folder
        do {
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: folder.generateURL().path) {
                try fileManager.removeItem(at: folder.generateURL())
            }
            itSeez3DAvatarSDK.deleteAvatar(avatarCode: avatarModel.responseAvatarModel.code, completion: completion)
        }
        catch let error {
            completion(Result.failure(error))
        }
    }

    /// Is used to build `AvatarModel` from the server response.
    ///
    /// - parameter response: Information about avatar received from server.
    ///
    /// - returns: The avatar model.
    public class func buildAvatar(fromResponse response:ResponseAvatarModel) -> AvatarModel {
        if let avatarModel = avatarModels[response.code] {
            return avatarModel
        }
        let avatarModel = AvatarModel(responseAvatarModel: response)
        avatarModels[response.code] = avatarModel

        return avatarModel
    }
}
