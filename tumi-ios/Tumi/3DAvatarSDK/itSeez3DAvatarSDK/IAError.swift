/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import Foundation

/// `IAError` is the error type returned by itSeez3dAvatarSDK. It encompasses a few different types of errors, each with its own associated reasons.
///
/// - sendingFailed:               Returned when sending process fails.
/// - downloadingModelFailed:      Returned when some step in the model downloading process fails.
/// - downloadingHaircutFailed:    Returned when some step in the haircut downloading process fails.
public enum IAError: Error {

    /// `DownloadingError` is the error type returned in the case of file downloading error.
    ///
    /// - networkError:         The error URL request finished with.
    /// - unzipError:           The error unzipping process failed with.
    public enum DownloadingError: Error {
        case networkError(error: Error)
        case unzipError(error: Error)
    }

    /// The underlying reason of model downloading error.
    ///
    /// - retriveStatusFailed:         The error status retriving process finished with.
    /// - downloadingFailed:           The error files downloading process finished with.
    public enum DownloadingModelFailureReason {
        case retriveStatusFailed(error: Error)
        case downloadingFailed(error: Error)
    }

    /// The underlying reason of haircut downloading error.
    ///
    /// - downloadingBaseHaircutFailed:            The error base haircut files downloading process finished with.
    /// - downloadingModelHaircutFailed:           The error model haircut files downloading process finished with.
    public enum DownloadingHaircutFailureReason {
        case downloadingBaseHaircutFailed(error: Error)
        case downloadingModelHaircutFailed(error: Error)
    }

    case sendingFailed(error: Error)
    case downloadingModelFailed(reason: DownloadingModelFailureReason)
    case downloadingHaircutFailed(reason: DownloadingHaircutFailureReason)
}
