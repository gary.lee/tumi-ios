/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import Alamofire
import MeshKitPrivate

private let kHaircutsFolder:() -> URL = {
    let fileManager = FileManager.default
    let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
    return documentsURL.appendingPathComponent("haircuts")
}
let kHaircutTextureFileNameTemplate: (String) -> URL = { return kHaircutsFolder().appendingPathComponent($0).appendingPathComponent("texture.png") }
let kHaircutMeshZipFileNameTemplate: (String) -> URL = { return kHaircutsFolder().appendingPathComponent($0).appendingPathComponent("mesh.zip") }
let kHaircutMeshFileNameTemplate: (String) -> URL = { return kHaircutsFolder().appendingPathComponent($0).appendingPathComponent("\($0).ply") }

/// `PlyItem` contains base information about the ply item.
open class PlyItem : NSObject {

    /// File URL to ply mesh.
    @objc public var meshFileURL       : URL!
    /// File URL to ply texture.
    @objc public var textureFileURL    : URL!

    public init(meshFileURL: URL, textureFileURL: URL) {
        self.meshFileURL = meshFileURL
        self.textureFileURL = textureFileURL
    }

    /// Is used to determine whether mesh and texture were downloaded or not.
    public func isDownloaded() -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: self.meshFileURL.path) && fileManager.fileExists(atPath: self.textureFileURL.path)
    }
}


internal class BaseHaircutModel {

    internal var identity          : String
    internal var gender            : String?
    internal var textureURL        : URL?
    internal var meshZipURL        : URL?

    internal var meshZipFileURL    : URL
    internal var plyItem           : PlyItem

    init(haircut: ResponseAvatarHaircutModel) {
        self.identity = haircut.identity
        self.gender = haircut.gender
        self.textureURL = haircut.textureURL
        self.meshZipURL = haircut.meshURL
        self.meshZipFileURL = kHaircutMeshZipFileNameTemplate(identity)

        self.plyItem = PlyItem(meshFileURL: kHaircutMeshFileNameTemplate(identity), textureFileURL: kHaircutTextureFileNameTemplate(identity))
    }

    init(identity: String) {
        self.identity = identity
        self.meshZipFileURL = kHaircutMeshZipFileNameTemplate(identity)

        self.plyItem = PlyItem(meshFileURL: kHaircutMeshFileNameTemplate(identity), textureFileURL: kHaircutTextureFileNameTemplate(identity))
    }

    public func isDownloaded() -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: self.plyItem.meshFileURL.path) && fileManager.fileExists(atPath: self.plyItem.textureFileURL.path)
    }

    internal func download() -> DownloadManager.DownloadManagerResponseObject {
        let requestObject = DownloadManager.DownloadManagerRequestObject(identity: identity)

        if let zipURL = meshZipURL {
            requestObject.downloadingList.append((link: zipURL, fileURL: self.meshZipFileURL))
        }
        if let textureURL = self.textureURL {
            requestObject.downloadingList.append((link: textureURL, fileURL: self.plyItem.textureFileURL))
        }

        return DownloadManager.startDownload(requestObject: requestObject)
    }
}

let kModelHaircutPreviewFileNameTemplate: (String, Folder) -> URL = { return $1.generateURL(to: "\($0)_preview.png") }
let kModelHaircutPointcloudZipFileNameTemplate: (String, Folder) -> URL = { return $1.generateURL(to: "\($0)_pointcloud.zip") }
let kModelHaircutPointcloudFileNameTemplate: (String, Folder) -> URL = { return $1.generateURL(to: "cloud_\($0).ply") }
let kModelHaircutMeshFileNameTemplate: (String, Folder) -> URL = { return $1.generateURL(to: "\($0).ply") }

/// `HaircutModel` contains base information about the haircut model.
public class HaircutModel {

    internal var baseHaircut             : BaseHaircutModel
    /// Model haircut identity.
    public var identity                  : String
    /// Information about haicut received from server.
    public var responseHaircutModel      : ResponseAvatarHaircutModel
    internal var pointcloudFileURL       : URL
    fileprivate var pointcloudZipFileURL : URL
    /// File URL to preview image of this haircut. Currently unavailable.
    public var previewFileURL            : URL
    /// Can be used to present the current haircut on the screen.
    public var plyItem                   : PlyItem

    class func generateModelIdentity(haircut: ResponseAvatarHaircutModel) -> String {
        let folder: Folder = haircut.modelURL.lastPathComponent
        return folder + "/" + haircut.identity
    }

    init(haircut: ResponseAvatarHaircutModel) {
        baseHaircut = BaseHaircutModel(haircut: haircut)
        let folder: Folder = haircut.modelURL.lastPathComponent
        identity = HaircutModel.generateModelIdentity(haircut: haircut)
        self.responseHaircutModel = haircut
        self.pointcloudFileURL = kModelHaircutPointcloudFileNameTemplate(baseHaircut.identity, folder)
        self.pointcloudZipFileURL = kModelHaircutPointcloudZipFileNameTemplate(baseHaircut.identity, folder)
        self.previewFileURL = kModelHaircutPreviewFileNameTemplate(baseHaircut.identity, folder)

        self.plyItem = PlyItem(meshFileURL: kModelHaircutMeshFileNameTemplate(baseHaircut.identity, folder), textureFileURL: self.baseHaircut.plyItem.textureFileURL)
    }
    
    /// Is used to determine whether ply item and preview were downloaded or not.
    public func isDownloaded() -> Bool {
        let fileManager = FileManager.default
        return baseHaircut.isDownloaded() && fileManager.fileExists(atPath: self.plyItem.meshFileURL.path)
    }

    internal func download() -> DownloadManager.DownloadManagerResponseObject {
        let requestObject = DownloadManager.DownloadManagerRequestObject(identity: self.identity)

        if let pointcloudZipURL = responseHaircutModel.pointcloudURL {
            requestObject.downloadingList.append((link: pointcloudZipURL, fileURL: self.pointcloudZipFileURL))
        }

//        if let previewURL = haircutModel.previewURL {
//            requestObject.downloadingList.append((link: previewURL, fileURL: self.previewFileURL))
//        }

        return DownloadManager.startDownload(requestObject: requestObject)
    }
}

internal class HaircutLoader: TaskHandler<HaircutModel> {

    var haircutModel              : HaircutModel

    internal init(haircutModel: HaircutModel) {
        self.haircutModel = haircutModel
    }

    @discardableResult
    internal func tryDownloadModel() -> Self {
        if !inProgress {
            inProgress = true
            downloadModel()
        }
        return self
    }

    private func downloadModel() {
        print("downloadHaircut", haircutModel.identity)

        guard !haircutModel.isDownloaded() else {
            self.taskComplete(withResult: Result.success(haircutModel))
            return
        }

        let modelSuccessHandler: () -> () = {
            DispatchQueue.global(qos: .userInitiated).async {
                PointcloudMerger.mergePointcloud(self.haircutModel.pointcloudFileURL, withMesh: self.haircutModel.baseHaircut.plyItem.meshFileURL, to: self.haircutModel.plyItem.meshFileURL)
                DispatchQueue.main.async { [unowned self] in
                    self.taskComplete(withResult: Result.success(self.haircutModel))
                }
            }
        }

        let baseSuccessHandler: () -> () = {
            self.haircutModel.download()
                .downloadingProgress(handler: { [unowned self] (percent) in
                    self.updateProgress(state: "Model Haircut Download", percent: percent)
                })
                .completion { [unowned self] (result) in
                    switch (result) {
                    case .success:
                        print("model haircut loaded")
                        modelSuccessHandler()
                    case .failure(let error):
                        self.taskComplete(withResult: Result.failure(IAError.downloadingHaircutFailed(reason: .downloadingModelHaircutFailed(error: error))))
                    }
            }
        }

        let baseHaircutModel = haircutModel.baseHaircut
        guard !baseHaircutModel.isDownloaded() else {
            print("base haircut already loaded")
            baseSuccessHandler()
            return
        }

        baseHaircutModel.download()
            .downloadingProgress(handler: { [unowned self] (percent) in
                self.updateProgress(state: "Base Haircut Download", percent: percent)
            })
            .completion { [unowned self] (result) in
                switch (result) {
                case .success:
                    print("base haircut loaded")
                    baseSuccessHandler()
                case .failure(let error):
                    self.taskComplete(withResult: Result.failure(IAError.downloadingHaircutFailed(reason: .downloadingBaseHaircutFailed(error: error))))
                }
        }
    }
}

