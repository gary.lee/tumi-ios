/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import "VisualizationController.h"
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#include <numeric>

@interface VisualizationController ()
{
    EAGLView *glview;
    GLubyte* data;
    cv::Mat RT;

    ModelPLY* modelPly;
    cv::Mat modelTexture;
    GLuint modelTextureId;

    BOOL withHair;
    ModelPLY* hairPly;
    cv::Mat hairTexture;
    GLuint hairTextureId;

    cv::Mat projectionMatrix;
}
@end

@implementation VisualizationController

- (id)initWithGLView:(EAGLView*)view frameSize:(CGSize)size;
{
    if ((self = [super init]))
    {
        glview = view;
        
        _modelScale = 1;
        _modelCenter = cv::Point3f(0,0,0);

        glGenTextures(1, &hairTextureId);
        glBindTexture(GL_TEXTURE_2D, hairTextureId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // This is necessary for non-power-of-two textures
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        glGenTextures(1, &modelTextureId);
        glBindTexture(GL_TEXTURE_2D, modelTextureId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // This is necessary for non-power-of-two textures
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        [self buildProjectionMatrix:[VisualizationController getRGBIntrinsicsFloat] :size.width :size.height :projectionMatrix];
        
        NSInteger dataLength = glview.framebufferHeight * glview.framebufferWidth * 4;
        data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));

        hairPly = NULL;
        modelPly = NULL;
    }
    
    return self;
}

- (void)clean
{
    if (modelPly) {
        delete modelPly;
        modelPly = NULL;
    }
    
    if (hairPly) {
        delete hairPly;
        hairPly = NULL;
    }
    
    glDeleteTextures(1, &modelTextureId);
    glDeleteTextures(1, &hairTextureId);
    
    free(data);
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self clean];
}

#pragma mark - Load Models

- (void)loadTexture:(cv::Mat&)frame
{
    GLenum inputColorFormat = GL_RGBA;
    if (frame.channels() == 1)
    {
        inputColorFormat = GL_LUMINANCE;
    }
    else if (frame.channels() == 3)
    {
        inputColorFormat = GL_RGB;
    }
    GLenum textFormat = inputColorFormat;
    
    GLenum textType = GL_UNSIGNED_BYTE;
    if (frame.depth() == CV_16U)
        textType = GL_UNSIGNED_SHORT;
    else if (frame.depth() == CV_8U)
        textType = GL_UNSIGNED_BYTE;
    else if (frame.depth() == CV_16S)
        textType = GL_SHORT;
    else if (frame.depth() == CV_32F)
        textType = GL_FLOAT;
    
    glTexImage2D(GL_TEXTURE_2D, 0, textFormat, frame.cols,
                 frame.rows, 0, inputColorFormat, textType, 0);
    glTexSubImage2D(GL_TEXTURE_2D,     // Type of texture
                    0,0,0,                 // Pyramid level (for mip-mapping) - 0 is the top level
                    //textFormat,            // Internal colour format to convert to
                    frame.cols,          // Image width  i.e. 640 for Kinect in standard mode
                    frame.rows,          // Image height i.e. 480 for Kinect in standard mode
                    //0,                 // Border width in pixels (can either be 1 or 0)
                    inputColorFormat, // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
                    textType,  // Image data type
                    frame.data);        // The actual image data itself

    int glErCode = glGetError();
    if (glErCode != GL_NO_ERROR)
    {
        NSLog(@"%d", glErCode);
    }
}

- (void)setTexture:(cv::Mat&)_texture to:(cv::Mat&)destTexture withTextureId:(GLuint)textureId
{
    const float maxDimension = 4096;
    float resultedScale = 1.0;

    if (_texture.cols > maxDimension || _texture.rows > maxDimension)
    {
        resultedScale = maxDimension/(_texture.cols+1);
        resultedScale = MIN(maxDimension/(_texture.rows+1), resultedScale);
        cv::Size resultedSize(_texture.cols*resultedScale, _texture.rows*resultedScale);
        cv::resize(_texture, destTexture, resultedSize);
    }
    else
        destTexture = _texture;

    [glview setFramebuffer];

    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, textureId);

    if (!destTexture.empty())
    {
        [self loadTexture:destTexture];
    }
}

- (void)setModel:(PlyItem *) _item
{
    modelPly = new ModelPLY;
    modelPly->load(_item);
    _modelCenter = modelPly->getCenter();
    [self adjustScaleFactor];
}

- (void)setModelTexture:(cv::Mat&)_texture;
{
    [self setTexture:_texture to:modelTexture withTextureId:modelTextureId];
}

- (void)setHairModel:(PlyItem *) _hair
{
    if (_hair)
    {
        if (hairPly)
        {
            delete hairPly;
            hairPly = NULL;
        }
        withHair = YES;
        hairPly = new ModelPLY;
        hairPly->load(_hair);
    }
    else
    {
        withHair = NO;
    }
}

- (void)setHairTexture:(cv::Mat&) _texture
{
    [self setTexture:_texture to:hairTexture withTextureId:hairTextureId];
}

#pragma mark - DRAW

- (void)drawHair
{
    glBindTexture(GL_TEXTURE_2D, hairTextureId);

    glVertexPointer(3, GL_FLOAT, 0, hairPly->facesTriangles.data());
    glNormalPointer(GL_FLOAT, 0, hairPly->facesNormals.data());

    glTexCoordPointer(2, GL_FLOAT, 0, hairPly->facesTextures.data());
    glShadeModel(GL_SMOOTH);
    glDrawArrays(GL_TRIANGLES, 0, (int)hairPly->facesTriangles.size()/3);
}

- (void)drawModel
{
    glColor4f(1,1,1,1);
    BOOL isTextureEmpty = modelTexture.empty();
    
    if (!isTextureEmpty)
    {
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
    }
    
    if (!isTextureEmpty)
    {
        glBindTexture(GL_TEXTURE_2D, modelTextureId);
    }
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    
    float scale = 1.0;
    glScalef(scale, scale, scale);
    
    glVertexPointer(3, GL_FLOAT, 0, modelPly->facesTriangles.data());
    glNormalPointer(GL_FLOAT, 0, modelPly->facesNormals.data());

    if (!isTextureEmpty)
    {
        glTexCoordPointer(2, GL_FLOAT, 0, modelPly->facesTextures.data());
    }

    glShadeModel(GL_SMOOTH);

    glDrawArrays(GL_TRIANGLES, 0, (int)modelPly->facesTriangles.size()/3);

    if (withHair)
    {
        [self drawHair];
    }
    
    glScalef(1.0, 1.0, 1.0);
    if (!isTextureEmpty)
    {
        glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
        glDisable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_TEXTURE_2D);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }

    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}

- (void)draw
{
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(reinterpret_cast<const GLfloat*>(&projectionMatrix.data[0]));
    
    glDepthMask(TRUE);
    glEnable(GL_DEPTH_TEST);
    
    glEnable(GL_NORMALIZE);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(reinterpret_cast<const GLfloat*>(&RT.data[0]));
    glScalef(_modelScale, _modelScale, _modelScale);
    
    [self drawModel];
    
    glScalef(1, 1, 1);
    
    glDisable(GL_NORMALIZE);
    glDisable(GL_DEPTH_TEST);
}

- (void)drawFrame
{
    [glview setFramebuffer];

    [self draw];
    
    bool ok = [glview presentFramebuffer];
    int glErCode = glGetError();
    if (!ok || glErCode != GL_NO_ERROR)
    {
        NSLog(@"GL error detected. Error code: %d\n", glErCode);
    }
}

#pragma mark -

- (void)buildProjectionMatrix:(const cv::Mat&)_intrinsics :(int)screen_width :(int)screen_height :(cv::Mat&) _projectionMatrix
{
    float near = 0.01;  // Near clipping distance
    float far  = 100;  // Far clipping distance

    // Camera parameters
    float f_x = _intrinsics.at<float>(0,0); // Focal length in x axis
    float f_y = _intrinsics.at<float>(1,1); // Focal length in y axis (usually the same?)
    float c_x = _intrinsics.at<float>(0,2); // Camera primary point x
    float c_y = _intrinsics.at<float>(1,2); // Camera primary point y

    _projectionMatrix.create(16, 1, CV_32FC1);

    _projectionMatrix.at<float>(0) = 2.0*f_x/screen_width;
    _projectionMatrix.at<float>(1) = 0.0;
    _projectionMatrix.at<float>(2) = 0.0;
    _projectionMatrix.at<float>(3) = 0.0;

    _projectionMatrix.at<float>(4) = 0.0;
    _projectionMatrix.at<float>(5) = 2.0*f_y/screen_height;
    _projectionMatrix.at<float>(6) = 0.0;
    _projectionMatrix.at<float>(7) = 0.0;

    _projectionMatrix.at<float>(8) = -2*c_x/screen_width + 1.0;
    _projectionMatrix.at<float>(9) = 2*c_y/screen_height - 1.0;
    _projectionMatrix.at<float>(10) = -(far+near)/(far-near);
    _projectionMatrix.at<float>(11) = -1.0;

    _projectionMatrix.at<float>(12) = 0.0;
    _projectionMatrix.at<float>(13) = 0.0;
    _projectionMatrix.at<float>(14) = -2.0*far*near/(far - near);
    _projectionMatrix.at<float>(15) = 0.0;

    _projectionMatrix.reshape(1, 4);
}

- (void)setRT:(const cv::Mat&) _RT;
{
    _RT.convertTo(RT, CV_32FC1);
}

- (cv::Mat)getObjectRT
{
    cv::Mat objectRT = cv::Mat::eye(4,4,CV_64FC1);
    objectRT.at<double>(1,1) = objectRT.at<double>(2,2) = 0.f;
    objectRT.at<double>(1,2) = -1.f;
    objectRT.at<double>(2,1) = 1.f;
    double z = 0.8;
    objectRT.at<double>(2,3) = -z;
    objectRT.at<double>(1,3) = -_modelCenter.y*_modelScale;
    return objectRT;
}

+ (cv::Mat)getRGBIntrinsics
{
    cv::Mat rgbIntrinsics = cv::Mat::zeros(3,3, CV_64FC1);
    rgbIntrinsics.at<double>(0,0) = 6.04445390541804e+02;
    rgbIntrinsics.at<double>(0,2) = 3.09655826878964e+02;
    rgbIntrinsics.at<double>(1,1) = 6.0445390541804352e+02;
    rgbIntrinsics.at<double>(1,2) = 2.53143252538251e+02;
    rgbIntrinsics.at<double>(2,2) = 1.;
    return rgbIntrinsics;
}

+ (cv::Mat)getRGBIntrinsicsFloat
{
    cv::Mat floatIntrinsics;
    cv::Mat rgbIntrinsics = [VisualizationController getRGBIntrinsics];
    rgbIntrinsics.convertTo(floatIntrinsics, CV_32FC1);
    return floatIntrinsics;
}

+ (cv::Mat)getRotationAroundX
{
    cv::Mat rotationAroundX = cv::Mat::eye(4, 4, CV_64FC1);
    rotationAroundX.at<double>(1,1) = rotationAroundX.at<double>(2,2) = 0;
    rotationAroundX.at<double>(1,2) = 1.0f;
    rotationAroundX.at<double>(2,1) = -1.0f;
    return rotationAroundX;
}

+ (cv::Mat)getInvertYZMat
{
    cv::Mat invertYZCoordinates = cv::Mat::eye(4,4,CV_64FC1);
    invertYZCoordinates.at<double>(1,1) = invertYZCoordinates.at<double>(2,2) = -1.0f;
    return invertYZCoordinates;
}

- (void)adjustScaleFactor
{
    // call this after setModel
    const float expectedSize = 0.38;
    const float scaleFactor = expectedSize / modelPly->getSize().y;

    [self setModelScale:scaleFactor];
}

@end
