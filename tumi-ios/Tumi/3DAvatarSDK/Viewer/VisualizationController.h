/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import <Foundation/Foundation.h>
#import "EAGLView.h"
#import "PlyReader.h"
#import <opencv2/highgui/highgui.hpp>
#import <opencv2/imgproc/imgproc.hpp>
#import <itSeez3DAvatarSDK/itSeez3DAvatarSDK-Swift.h>

@interface VisualizationController : NSObject

@property (nonatomic, assign) float modelScale;
@property (nonatomic, assign) cv::Point3f modelCenter;

- (id)initWithGLView:(EAGLView*)view frameSize:(CGSize)size;
- (void)drawFrame;

- (void)setModel:(PlyItem*)_item;
- (void)setModelTexture:(cv::Mat&)_texture;
- (void)setHairModel:(PlyItem *)_hair;
- (void)setHairTexture:(cv::Mat&) _texture;

- (void)setRT:(const cv::Mat&)_RT;
- (cv::Mat)getObjectRT;
+ (cv::Mat)getRotationAroundX;
+ (cv::Mat)getInvertYZMat;

@end
