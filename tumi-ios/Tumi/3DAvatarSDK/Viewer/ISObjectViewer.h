/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import <UIKit/UIKit.h>
#import <3DAvatarSDK/3DAvatarSDK-Swift.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *  Class is used to view the model on the screen. It gives possibility to customize the displaying mode and gestures.
 */
NS_CLASS_AVAILABLE_IOS(8_0) @interface ISObjectViewer : UIView <UIGestureRecognizerDelegate>
/**
 *  Set item with the model for visualization.
 *
 *  @param plyItem Item to display
 *
 */
- (void)setModelPlyItem:(nonnull PlyItem*)plyItem;
/**
 *  Set item with the hair for visualization.
 *
 *  @param plyItem Item to display
 *
 */
- (void)setHairPlyItem:(nonnull PlyItem*)plyItem;

@end

NS_ASSUME_NONNULL_END

