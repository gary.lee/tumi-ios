/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import <QuartzCore/QuartzCore.h>

#import "EAGLView.h"

@interface EAGLView (PrivateMethods)
- (void)createFramebuffer;
- (void)deleteFramebuffer;
@end

@implementation EAGLView

@synthesize context, framebufferWidth, framebufferHeight;

// You must implement this method
+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

//The EAGL view is stored in the nib file. When it's unarchived it's sent -initWithCoder:.
- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame andAntialiasing:(BOOL)withAntialiasing
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _withAntialiasing = withAntialiasing;
        [self commonInit];
    }
    
    return self;
}

- (void)commonInit
{
    _api = kEAGLRenderingAPIOpenGLES1;
    CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
    
    eaglLayer.opaque = TRUE;
    eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithBool:YES], kEAGLDrawablePropertyRetainedBacking,
                                    kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat,
                                    nil];
    
    [self initContextWithAPI:self.api];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self deleteFramebuffer];
    if ([EAGLContext currentContext] == context)
        [EAGLContext setCurrentContext:nil];
}

- (void)setContext:(EAGLContext *)newContext
{
    if (context != newContext)
    {
        [self deleteFramebuffer];
        
        context = newContext;
        
        [EAGLContext setCurrentContext:context];
    }
}

- (void)createFramebuffer
{
    if (context && !defaultFramebuffer)
    {
        [EAGLContext setCurrentContext:context];
        
        if (_withAntialiasing)
        {
            glGenFramebuffersOES(1, &defaultFramebuffer);
            glGenRenderbuffersOES(1, &colorRenderbuffer);
            
            glBindFramebufferOES(GL_FRAMEBUFFER_OES, defaultFramebuffer);
            glBindRenderbufferOES(GL_RENDERBUFFER_OES, colorRenderbuffer);
            [context renderbufferStorage:GL_RENDERBUFFER_OES
                            fromDrawable:(CAEAGLLayer*)self.layer];
            glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES,
                                         GL_COLOR_ATTACHMENT0_OES,
                                         GL_RENDERBUFFER_OES,
                                         colorRenderbuffer);
            glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES,
                                            GL_RENDERBUFFER_WIDTH_OES, &framebufferWidth);
            glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES,
                                            GL_RENDERBUFFER_HEIGHT_OES, &framebufferHeight);
            
            //create MSAA buffer
            glGenFramebuffersOES(1, &msaaFramebuffer);
            glGenRenderbuffersOES(1, &msaaRenderBuffer);
            glBindFramebufferOES(GL_FRAMEBUFFER_OES, msaaFramebuffer);
            glBindRenderbufferOES(GL_RENDERBUFFER_OES, msaaRenderBuffer);
            // Samples is the amount of pixels the MSAA buffer uses to make one pixel on the render buffer.
            glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER_OES, 4, GL_RGBA8_OES,
                                                  framebufferWidth, framebufferHeight);
            glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES,
                                         GL_RENDERBUFFER_OES, msaaRenderBuffer);
            glGenRenderbuffersOES(1, &msaaDepthBuffer);
            glBindRenderbufferOES(GL_RENDERBUFFER_OES, msaaDepthBuffer);
            glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER_OES, 4,
                                                  GL_DEPTH_COMPONENT24_OES, framebufferWidth , framebufferHeight);
            glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES,
                                         GL_RENDERBUFFER_OES, msaaDepthBuffer);
            
            if (glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
            {
                NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER_OES));
            }
        }
        else
        {
            //if (_needLocalFramebuffer)
            {
                glGenFramebuffers(1, &defaultFramebuffer);
                glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
            }
            // Create color render buffer and allocate backing store.
            glGenRenderbuffers(1, &colorRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &framebufferWidth);
            glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &framebufferHeight);
            
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
            
            // Create depth render buffer and allocate backing store.
            glGenRenderbuffers(1, &depthRenderbuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);

            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, framebufferWidth, framebufferHeight);

            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);

            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
            }
        }
    }
}

- (void)deleteFramebuffer
{
    if (context)
    {
        [EAGLContext setCurrentContext:context];
        
        if (_withAntialiasing)
        {
            if (defaultFramebuffer) {
                glDeleteFramebuffersOES(1, &defaultFramebuffer);
                defaultFramebuffer = 0;
            }
            
            if (colorRenderbuffer) {
                glDeleteRenderbuffersOES(1, &colorRenderbuffer);
                colorRenderbuffer = 0;
            }
            
            if(msaaFramebuffer)
            {
                glDeleteFramebuffersOES(1, &msaaFramebuffer);
                msaaFramebuffer = 0;
            }
            
            if(msaaRenderBuffer)
            {
                glDeleteRenderbuffersOES(1, &msaaRenderBuffer);
                msaaRenderBuffer = 0;
            }
            
            if(msaaDepthBuffer)
            {
                glDeleteRenderbuffersOES(1, &msaaDepthBuffer);
                msaaDepthBuffer = 0;
            }
        }
        else
        {
            if (defaultFramebuffer) {
                glDeleteFramebuffers(1, &defaultFramebuffer);
                defaultFramebuffer = 0;
            }

            if (colorRenderbuffer) {
                glDeleteRenderbuffers(1, &colorRenderbuffer);
                colorRenderbuffer = 0;
            }
            
            if (depthRenderbuffer) {
                glDeleteRenderbuffers(1, &depthRenderbuffer);
                depthRenderbuffer = 0;
            }
        }
        NSLog(@"Framebuffer deleted");
        
    }
}

- (void)setFramebuffer
{
    if (context) {
        [EAGLContext setCurrentContext:context];
        
        if (!colorRenderbuffer)
            [self createFramebuffer];

        if (_withAntialiasing)
        {
            glBindFramebufferOES(GL_FRAMEBUFFER_OES, msaaFramebuffer); //Bind MSAA
        }
        else
        {
            glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        }
        glViewport(0, 0, framebufferWidth, framebufferHeight);
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    }
}

- (BOOL)presentFramebuffer
{
    BOOL success = FALSE;
    
    if (context) {
        [EAGLContext setCurrentContext:context];
        
        if (_withAntialiasing)
        {
            glBindFramebufferOES(GL_READ_FRAMEBUFFER_APPLE, msaaFramebuffer);
            glBindFramebufferOES(GL_DRAW_FRAMEBUFFER_APPLE, defaultFramebuffer);
            // Call a resolve to combine buffers
            glResolveMultisampleFramebufferAPPLE();
            // Present final image to screen
            glBindRenderbufferOES(GL_RENDERBUFFER_OES, colorRenderbuffer);
            success = [context presentRenderbuffer:GL_RENDERBUFFER_OES];
        }
        else
        {
            glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
            success = [context presentRenderbuffer:GL_RENDERBUFFER];
        }
    }
    
    return success;
}

- (void)drawFramebuffer
{
    if (_withAntialiasing)
    {
        glBindFramebufferOES(GL_READ_FRAMEBUFFER_APPLE, msaaFramebuffer);
        glBindFramebufferOES(GL_DRAW_FRAMEBUFFER_APPLE, defaultFramebuffer);
        glResolveMultisampleFramebufferAPPLE();
        glBindFramebufferOES(GL_READ_FRAMEBUFFER_APPLE, defaultFramebuffer);
    }
}

- (void)initContextWithAPI:(EAGLRenderingAPI)api
{
    _api = api;
    EAGLContext *aContext = [[EAGLContext alloc] initWithAPI:self.api];
    
    if (!aContext)
        NSLog(@"Failed to create ES context");
    else if (![EAGLContext setCurrentContext:aContext])
        NSLog(@"Failed to set ES context current");
    
    [self setContext:aContext];
    [self setFramebuffer];
}


@end
