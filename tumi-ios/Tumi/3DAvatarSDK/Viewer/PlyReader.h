/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import <Foundation/Foundation.h>
#import <itSeez3DAvatarSDK/itSeez3DAvatarSDK-Swift.h>
#import <opencv2/highgui/highgui.hpp>
#include <vector>

#ifndef __3DViewer__plyReader__
#define __3DViewer__plyReader__

typedef size_t VertexIndex;
typedef std::vector<VertexIndex> Face;

class ModelPLY
{
public:
    int load(PlyItem *item);
    inline void calculateNormal(float *coord, float* norm);
 
    cv::Point3f getSize() const;
    cv::Point3f getCenter() const;

private:
    void fillBoundingBoxVertices();
    void fillInternalArraysForTexturedModel();
    void fillInternalArraysForWireframe();
    
public:
    std::vector<float> vertices;
    std::vector<std::vector<size_t> > faces;

    std::vector<float> facesTextures;
    std::vector<float> facesTriangles;
    std::vector<float> facesNormals;
        
    std::vector<float> facesEdgesVertices;
    std::vector<float> facesEdgesTextures;
    std::vector<float> facesEdgesNormals;
    
    char* imageName;
    
private:
    cv::Point3f boundingBoxP1, boundingBoxP2;
    std::vector<std::vector<std::pair<size_t, size_t> > > verticesWithFaces;
};

#endif /* defined(__3DViewer__plyReader__) */
