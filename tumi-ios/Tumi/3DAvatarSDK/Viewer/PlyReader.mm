/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import "PlyReader.h"
#import "loadModel3D.hpp"
#include <fstream>
#include <unordered_map>

inline void ModelPLY::calculateNormal(float *coord, float* norm)
{
    float va[3], vb[3], vr[3], val;
    va[0] = coord[0] - coord[3];
    va[1] = coord[1] - coord[4];
    va[2] = coord[2] - coord[5];
    
    vb[0] = coord[0] - coord[6];
    vb[1] = coord[1] - coord[7];
    vb[2] = coord[2] - coord[8];
    
    vr[0] = va[1] * vb[2] - vb[1] * va[2];
    vr[1] = vb[0] * va[2] - va[0] * vb[2];
    vr[2] = va[0] * vb[1] - vb[0] * va[1];
    
    val = sqrt(vr[0]*vr[0] + vr[1]*vr[1] + vr[2]*vr[2]);
    
    norm[0] = vr[0]/val;
    norm[1] = vr[1]/val;
    norm[2] = vr[2]/val;
}

void ModelPLY::fillBoundingBoxVertices()
{
    if (!vertices.empty())
    {
        boundingBoxP1 = cv::Point3f(vertices[0], vertices[1], vertices[2]);
        boundingBoxP2 = boundingBoxP1;
        
        for (size_t i = 3; i < vertices.size(); i+=3)
        {
            boundingBoxP1.x = std::min(boundingBoxP1.x, vertices[i]);
            boundingBoxP1.y = std::min(boundingBoxP1.y, vertices[i+1]);
            boundingBoxP1.z = std::min(boundingBoxP1.z, vertices[i+2]);
            boundingBoxP2.x = std::max(boundingBoxP2.x, vertices[i]);
            boundingBoxP2.y = std::max(boundingBoxP2.y, vertices[i+1]);
            boundingBoxP2.z = std::max(boundingBoxP2.z, vertices[i+2]);
        }
    }
}

void ModelPLY::fillInternalArraysForTexturedModel()
{
    size_t float3 = sizeof(float)*3;
    
    facesTriangles.resize(faces.size()*9);
    facesNormals.resize(facesTriangles.size());

    for (size_t iterator = 0, normal_index = 0, triangle_index = 0; iterator < faces.size(); iterator++, normal_index += 9, triangle_index += 9)
    {
        Face currFace = faces[iterator];
        
        memcpy(&facesTriangles[triangle_index], &vertices[currFace[0]*3], float3);
        memcpy(&facesTriangles[triangle_index+3], &vertices[currFace[1]*3], float3);
        memcpy(&facesTriangles[triangle_index+6], &vertices[currFace[2]*3], float3);
        
        calculateNormal(&facesTriangles[triangle_index], &facesNormals[normal_index]);
        
        memcpy(&facesNormals[normal_index+3], &facesNormals[normal_index], float3);
        memcpy(&facesNormals[normal_index+6], &facesNormals[normal_index], float3);
    }
}

int ModelPLY::load(PlyItem *item)
{
    pointcloudMerge::loadModelFromBinPLYinFloat([item.meshFileURL.path UTF8String], &vertices, 0, &faces, &facesTextures);

    fillBoundingBoxVertices();
    fillInternalArraysForTexturedModel();

    return 0;
}

cv::Point3f ModelPLY::getSize() const
{
    return boundingBoxP2 - boundingBoxP1;
}

cv::Point3f ModelPLY::getCenter() const
{
    return (boundingBoxP2 + boundingBoxP1)*0.5f;
}
