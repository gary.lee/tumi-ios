/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#import "EAGLView.h"
#import "ISObjectViewer.h"
#import "PlyReader.h"
#import "VisualizationController.h"
#ifdef __cplusplus
#import <opencv2/highgui/highgui.hpp>
#endif

@interface ISObjectViewer (Gesture)

@end

@interface ISObjectViewer ()
{
    PlyItem *modelItem;
    PlyItem *hairItem;
    
    EAGLView *glView;
    cv::Mat objectRT;
    cv::Mat rotateMatrix;
    cv::Mat invertYZCoordinates;
    cv::Mat rotationAroundX;

    //gesture
    bool isObjectSet;
    CGPoint firstPanPoint;
    CGPoint initPanPoint;
    CGPoint velocityPan;
    CGPoint diffPan;
    CGFloat lastScaleFactor;
    CGFloat initialScaleFactor;
    CGPoint translatedPoint;
    float rotationDegree;

    NSTimer *panTimer;

    int gesture;
    NSTimer *drawTimer;
    NSRecursiveLock *drawLock;
    BOOL isDirty;
    //\gesture
}
@property(nonatomic, retain) VisualizationController * visualizationController;

@end

@implementation ISObjectViewer

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    glView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0);
}

- (void)commonInit
{
    drawLock = [NSRecursiveLock new];//gesture
    if (!glView)
    {
        int size = MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        glView = [[EAGLView alloc] initWithFrame:CGRectMake(0, 0, size, size) andAntialiasing:YES];
        glView.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2.0, [UIScreen mainScreen].bounds.size.height/2.0);
        glView.userInteractionEnabled = false;
        [self addSubview:glView];
        
        objectRT = cv::Mat::eye(4,4,CV_32FC1);
        invertYZCoordinates = [VisualizationController getInvertYZMat];
        rotationAroundX = [VisualizationController getRotationAroundX];
        self.visualizationController = [[VisualizationController alloc] initWithGLView:glView frameSize:CGSizeMake(640, 640)];

        [self setupGestures];//gesture
    }
}

- (cv::Mat)getImageFromFileURL:(NSURL*)fileURL
{
    NSString *imgFileNameHair = fileURL.path;
    cv::Mat cvTexImage;
    cvTexImage = cv::imread([imgFileNameHair UTF8String], -1);
    if (cvTexImage.channels() == 4) {
        cv::cvtColor(cvTexImage, cvTexImage, cv::COLOR_RGB2BGR);
    } else {
        cv::cvtColor(cvTexImage, cvTexImage, cv::COLOR_RGB2BGR);
    }

    return cvTexImage;
}

- (void)setModelPlyItem:(nonnull PlyItem*)_plyItem
{
    [self resetTimer];//gesture
    modelItem = _plyItem;
    cv::Mat cvTexImage = [self getImageFromFileURL:modelItem.textureFileURL];
    [self.visualizationController setModelTexture:cvTexImage];
    [self.visualizationController setModel:modelItem];
    [self setupVisualization];
}

- (void)setHairPlyItem:(nonnull PlyItem*)_plyItem
{
    hairItem = _plyItem;
    cv::Mat cvTexImage = [self getImageFromFileURL:hairItem.textureFileURL];
    [self.visualizationController setHairTexture:cvTexImage];
    [self.visualizationController setHairModel:hairItem];
    [self setupVisualization];
}

- (void)setupVisualization
{
    cv::Mat eye = cv::Mat::eye(4,4,CV_32FC1);
    [self.visualizationController setRT:eye];
    [self setObject:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];

    [self.visualizationController drawFrame];

    [self stopDrawTimer];//gesture
    drawTimer = [NSTimer scheduledTimerWithTimeInterval:1/60 target:self selector:@selector(tick) userInfo:nil repeats:YES];//gesture
}

- (void)setObject:(CGPoint)tapPoint
{
    rotateMatrix = cv::Mat::eye(4,4,CV_64FC1);

    objectRT = [self.visualizationController getObjectRT];
    const double offset = 0.07;

    lastScaleFactor = initialScaleFactor = self.visualizationController.modelScale;//gesture
    objectRT.at<double>(1,3) = -(self.visualizationController.modelCenter.y*initialScaleFactor + offset);//gesture

    lastScaleFactor = initialScaleFactor;//gesture
    [self.visualizationController setModelScale:lastScaleFactor];//gesture

    [self calcAndSetRT];

    isDirty = YES;//gesture
    isObjectSet = YES;//gesture
}

- (void)calcAndSetRT
{
    cv::Mat resultRT = objectRT*rotateMatrix*invertYZCoordinates;
    [self.visualizationController setRT:(rotationAroundX*resultRT.t())];
}

//gesture

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (([gestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]])    ||
        ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]    && [otherGestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]]) ||
        ([gestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])      ||
        ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]      && [otherGestureRecognizer isKindOfClass:[UIRotationGestureRecognizer class]]) ||
        ([gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]    && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]])      ||
        ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]      && [otherGestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]]))
    {
        return YES;
    }
    return NO;
}

#pragma mark - UIGestureRecognizerHandle

- (void)handlePanGesture:(UIPanGestureRecognizer*)sender
{
    [self stopTimer];
    if ([sender numberOfTouches] == 1)
    {
        if ([sender state] == UIGestureRecognizerStateBegan)
        {
            CGPoint translatedPointPan = [(UIPanGestureRecognizer*)sender translationInView:self];
            firstPanPoint = translatedPointPan;
            initPanPoint = translatedPointPan;
        }
        else if ([sender state] == UIGestureRecognizerStateChanged && isObjectSet)
        {
            static int a = 0;
            a++;
            if (a%2) return;
            CGPoint translatedPointPan = [(UIPanGestureRecognizer*)sender translationInView:self];
            CGPoint diff = CGPointMake(firstPanPoint.x - translatedPointPan.x, firstPanPoint.y - translatedPointPan.y);
            firstPanPoint = translatedPointPan;
            CGSize maxDiff = self.bounds.size;
            float angle = (sqrtf(diff.x * diff.x + diff.y * diff.y) / sqrtf(maxDiff.width * maxDiff.height)) * M_PI*2;
            
            cv::Vec4f vec(-(diff.y + 0.1), 0, diff.x + 0.1, 0);
            
            [self rotateWithAngle:angle andVector4f:vec];
        }
    }
    if ([sender state] == UIGestureRecognizerStateEnded && isObjectSet)
    {
        firstPanPoint = CGPointMake(0, 0);
        velocityPan = [(UIPanGestureRecognizer*)sender velocityInView:self];
        CGPoint translatedPointPan = [(UIPanGestureRecognizer*)sender translationInView:self];
        diffPan = CGPointMake(initPanPoint.x - translatedPointPan.x, initPanPoint.y - translatedPointPan.y);
        dispatch_async(dispatch_get_main_queue(), ^(void){
            panTimer = [NSTimer scheduledTimerWithTimeInterval:1/60 target:self selector:@selector(rotate) userInfo:nil repeats:YES];
        });
    }
}

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer
{
//    [self stopTimer];
//    if (!isObjectSet)
//    {
//        CGPoint tapPoint = [gestureRecognizer locationInView:gestureRecognizer.view];
//        [self setObject:tapPoint];
//    }
}

- (void)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer
{
    [self stopTimer];
    CGPoint tapPoint = [gestureRecognizer locationInView:gestureRecognizer.view];
    [self setObject:tapPoint];
}

- (void)handleRotateGesture:(UIGestureRecognizer *)sender
{
    if (gesture != 2)
    {
        firstPanPoint = CGPointMake(0, 0);
    }
    gesture = 2;
    if (gesture == 2)
    {
        if ([sender numberOfTouches] == 2)
        {
            [self stopTimer];
            if ([sender state] == UIGestureRecognizerStateBegan)
            {
                rotationDegree = [(UIRotationGestureRecognizer *)sender rotation];
            }
            else if ([sender state] == UIGestureRecognizerStateChanged && isObjectSet)
            {
                float angle = (rotationDegree - [(UIRotationGestureRecognizer *)sender rotation]) * 4;
                rotationDegree = [(UIRotationGestureRecognizer *)sender rotation];
                
                cv::Vec4f vec(0, 1, 0, 0);
                [self processGesture];
                [self rotateWithAngle:angle andVector4f:vec];
            }
        }
    }
    if ([sender state] == UIGestureRecognizerStateEnded && isObjectSet)
    {
        gesture = 0;
        firstPanPoint = CGPointMake(0, 0);
        translatedPoint = CGPointMake(0, 0);
    }
}

- (void)processGesture
{
    if (firstPanPoint.x != 0 && firstPanPoint.y != 0 && translatedPoint.x != 0 && translatedPoint.y != 0)
    {
        CGPoint diff = CGPointMake(firstPanPoint.x - translatedPoint.x, firstPanPoint.y - translatedPoint.y);
        CGSize maxDiff = self.bounds.size;
        
        cv::Mat rotation = cv::Mat::zeros(4,4,CV_64FC1);
        float x = (diff.x/maxDiff.width) / 2;
        float y = (diff.y/maxDiff.height) / 2;
        float z = 0;
        firstPanPoint = translatedPoint;
        
        rotation.at<double>(0,3) = -x;
        rotation.at<double>(1,3) = y;
        rotation.at<double>(2,3) = z;
        
        objectRT = objectRT + rotation;
        
        [self calcAndSetRT];
    }
}

- (void)handleDoublePanGesture:(UIPanGestureRecognizer*)sender
{
    if (gesture == 0)
    {
        gesture = 1;
    }
    
    [self stopTimer];
    if ([sender numberOfTouches] == 2)
    {
        if ([sender state] == UIGestureRecognizerStateBegan)
        {
            firstPanPoint = [(UIPanGestureRecognizer*)sender locationInView:self];
        }
        else if ([sender state] == UIGestureRecognizerStateChanged && isObjectSet)
        {
            if (firstPanPoint.x == 0 && firstPanPoint.y == 0)
            {
                firstPanPoint = [(UIPanGestureRecognizer*)sender locationInView:self];
            }
            translatedPoint = [(UIPanGestureRecognizer*)sender locationInView:self];
            if (gesture == 1)
            {
                [self processGesture];
                isDirty = YES;
            }
        }
    }
    if ([sender state] == UIGestureRecognizerStateEnded)
    {
        gesture = 0;
    }
}

- (void)handlePinchGesture:(UIGestureRecognizer *)sender
{
    [self stopTimer];
    CGFloat factor = [(UIPinchGestureRecognizer *)sender scale];
    
    self.visualizationController.modelScale = lastScaleFactor * factor;
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        lastScaleFactor *= factor;
    }

    if (gesture == 0)
    {
        isDirty = YES;
    }
}


#pragma mark -

- (void)rotateWithAngle:(CGFloat)angle andVector:(float[3])axis
{
    [self rotateWithAngle:angle andVector4f:cv::Vec4f(axis[0],axis[1],axis[2], 0)];
}

- (void)rotateWithAngle:(CGFloat)angle andVector4f:(cv::Vec4f)vec
{
    cv::Mat rotation = [self generateRotationMatrixForVec:vec andAngle:angle andCurrentRotMatrix:rotateMatrix];
    rotateMatrix = rotateMatrix*rotation;
    [self calcAndSetRT];
    isDirty = YES;
}

- (void)rotate
{
    cv::Vec4f vec(-(diffPan.y + 0.1), 0, diffPan.x + 0.1, 0);
    float velocity = sqrtf(velocityPan.x * velocityPan.x + velocityPan.y * velocityPan.y);
    float angle = (velocity / 60000) * M_PI * 2;
    
    CGFloat dec = MAX(0.99 - 10/velocity, 0.92);
    velocityPan = CGPointMake(velocityPan.x*dec, velocityPan.y*dec);
    
    if (ABS(velocityPan.x) + ABS(velocityPan.y) > 50)
    {
        [self rotateWithAngle:angle andVector4f:vec];
    }
    else
    {
        [self stopTimer];
    }
}

- (cv::Mat)generateRotationMatrixForVec:(cv::Vec4f)vec andAngle:(float)angle andCurrentRotMatrix:(cv::Mat)rotateMatrix
{
    vec = vec * (1.0f/cv::norm(vec));
    cv::Mat rotation = cv::Mat::eye(4,4,CV_64FC1);

    float cosAngle = cos(angle);
    float nCosAngle = 1 - cosAngle;
    float sinAngle = sin(angle);

    cv::Mat vecMat;
    cv::Mat(vec).convertTo(vecMat, CV_64FC1);

    cv::Mat resVector = rotateMatrix.inv()*vecMat;

    double u = resVector.at<double>(0);
    double v = resVector.at<double>(1);
    double w = resVector.at<double>(2);

    double uu = u * u;
    double uv = u * v;
    double uw = u * w;
    double vv = v * v;
    double vw = v * w;
    double ww = w * w;
    double uvnCosA = uv*nCosAngle;
    double uwnCosA = uw*nCosAngle;
    double vwnCosA = vw*nCosAngle;
    double usinA = u*sinAngle;
    double vsinA = v*sinAngle;
    double wsinA = w*sinAngle;

    rotation.at<double>(0,0) = (uu+cosAngle*(vv+ww));
    rotation.at<double>(0,1) = (uvnCosA-wsinA);
    rotation.at<double>(1,0) = (uvnCosA+wsinA);
    rotation.at<double>(0,2) = (uwnCosA+vsinA);
    rotation.at<double>(2,0) = (uwnCosA-vsinA);
    rotation.at<double>(1,2) = (vwnCosA-usinA);
    rotation.at<double>(2,1) = (vwnCosA+usinA);
    rotation.at<double>(1,1) = (vv+cosAngle*(uu+ww));
    rotation.at<double>(2,2) = (ww+cosAngle*(uu+vv));

    return rotation;
}

- (void)stopTimer
{
    if ([panTimer isValid])
    {
        [panTimer invalidate];
        panTimer = nil;
    }
}

- (void)setupGestures
{
    self.userInteractionEnabled = YES;
    self.multipleTouchEnabled = YES;
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    [self addGestureRecognizer:panRecognizer];
    
    UIPanGestureRecognizer *doublePanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoublePanGesture:)];
    [doublePanRecognizer setMinimumNumberOfTouches:2];
    [doublePanRecognizer setMaximumNumberOfTouches:2];
    [doublePanRecognizer requireGestureRecognizerToFail:panRecognizer];
    [doublePanRecognizer setDelegate:self];
    [self addGestureRecognizer:doublePanRecognizer];
    
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [pinchRecognizer setDelegate:self];
    [pinchRecognizer requireGestureRecognizerToFail:panRecognizer];
    [self addGestureRecognizer:pinchRecognizer];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setDelegate:self];
    [self addGestureRecognizer:singleTap];
    
    UIRotationGestureRecognizer *rotateGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotateGesture:)];
    [rotateGesture setDelegate:self];
    [rotateGesture requireGestureRecognizerToFail:panRecognizer];
    [self addGestureRecognizer:rotateGesture];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [longPressGesture setDelegate:self];
    [self addGestureRecognizer:longPressGesture];
}

- (void)tick
{
    if (isDirty && [drawLock tryLock])
    {
        [self.visualizationController drawFrame];
        isDirty = NO;
        [drawLock unlock];
    }
}

- (void)stopDrawTimer
{
    if ([drawTimer isValid])
    {
        [drawTimer invalidate];
        drawTimer = nil;
    }
}

- (void)resetTimer
{
    [self stopTimer];
    [self stopDrawTimer];
}
//\gesture
@end
