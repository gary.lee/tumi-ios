﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// ARWorldMapController
struct ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737;
// CloseSuitcase
struct CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5;
// FlyHanger
struct FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A;
// HoloVideoObject
struct HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB;
// HoloVideoObject/OnEndOfStream
struct OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091;
// HoloVideoObject/OnFatalErrorEvent
struct OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67;
// HoloVideoObject/OnFrameInfoEvent
struct OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F;
// HoloVideoObject/OnOpenEvent
struct OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1;
// HoloVideoObject/OnRenderEvent
struct OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6;
// LayDownSuitcase
struct LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3;
// LayDownSuitcaseOpenFullReverse
struct LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2;
// LightEstimation
struct LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319;
// MakeAppearOnPlane
struct MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3;
// OpenSuitcase
struct OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC;
// PullBackpackDownToSuitcase
struct PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268;
// PullHandle
struct PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379;
// RotateBack
struct RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483;
// RotateBottom
struct RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1;
// RotateFront
struct RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03;
// RotateFront45
struct RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13;
// SVFUnityPluginInterop
struct SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_tFB39B350582053B206393ED428938B171A469EE0;
// System.Action`2<UnityEngine.Texture2D,System.String>
struct Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_tC0B0D671468ADEEF578E3759B5CAB8605D10D798;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<HVConductor/HVSequence>
struct List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F;
// System.Collections.Generic.List`1<HoloVideoObject>
struct List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>
struct List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>
struct List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.BinaryReader
struct BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969;
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TouchCameraWithRotation
struct TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AudioListener
struct AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Experimental.XR.XRSessionSubsystem
struct XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Logger
struct Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Mesh[]
struct MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t06431062CF438D12908F0B93305795CB645DCCA8;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Dropdown
struct Dropdown_tF6331401084B1213CAB10587A6EC81461501930F;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.XR.ARFoundation.ARPlane
struct ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E;
// UnityEngine.XR.ARFoundation.ARPlaneManager
struct ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A;
// UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer
struct ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923;
// UnityEngine.XR.ARFoundation.ARSession
struct ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB;
// UnityEngine.XR.ARFoundation.ARSessionOrigin
struct ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF;
// Zoom
struct Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#define U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController_<Load>d__28
struct  U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D  : public RuntimeObject
{
public:
	// System.Int32 ARWorldMapController_<Load>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARWorldMapController_<Load>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARWorldMapController ARWorldMapController_<Load>d__28::<>4__this
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * ___U3CU3E4__this_2;
	// UnityEngine.Experimental.XR.XRSessionSubsystem ARWorldMapController_<Load>d__28::<sessionSubsystem>5__2
	XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * ___U3CsessionSubsystemU3E5__2_3;
	// System.Int32 ARWorldMapController_<Load>d__28::<bytesPerFrame>5__3
	int32_t ___U3CbytesPerFrameU3E5__3_4;
	// System.Int64 ARWorldMapController_<Load>d__28::<bytesRemaining>5__4
	int64_t ___U3CbytesRemainingU3E5__4_5;
	// System.IO.BinaryReader ARWorldMapController_<Load>d__28::<binaryReader>5__5
	BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * ___U3CbinaryReaderU3E5__5_6;
	// System.Collections.Generic.List`1<System.Byte> ARWorldMapController_<Load>d__28::<allBytes>5__6
	List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B * ___U3CallBytesU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CU3E4__this_2)); }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsessionSubsystemU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CsessionSubsystemU3E5__2_3)); }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * get_U3CsessionSubsystemU3E5__2_3() const { return ___U3CsessionSubsystemU3E5__2_3; }
	inline XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F ** get_address_of_U3CsessionSubsystemU3E5__2_3() { return &___U3CsessionSubsystemU3E5__2_3; }
	inline void set_U3CsessionSubsystemU3E5__2_3(XRSessionSubsystem_t75B8ED54B2BF4876D83B93780A7E13D6A9F32B8F * value)
	{
		___U3CsessionSubsystemU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsessionSubsystemU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CbytesPerFrameU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbytesPerFrameU3E5__3_4)); }
	inline int32_t get_U3CbytesPerFrameU3E5__3_4() const { return ___U3CbytesPerFrameU3E5__3_4; }
	inline int32_t* get_address_of_U3CbytesPerFrameU3E5__3_4() { return &___U3CbytesPerFrameU3E5__3_4; }
	inline void set_U3CbytesPerFrameU3E5__3_4(int32_t value)
	{
		___U3CbytesPerFrameU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CbytesRemainingU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbytesRemainingU3E5__4_5)); }
	inline int64_t get_U3CbytesRemainingU3E5__4_5() const { return ___U3CbytesRemainingU3E5__4_5; }
	inline int64_t* get_address_of_U3CbytesRemainingU3E5__4_5() { return &___U3CbytesRemainingU3E5__4_5; }
	inline void set_U3CbytesRemainingU3E5__4_5(int64_t value)
	{
		___U3CbytesRemainingU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CbinaryReaderU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CbinaryReaderU3E5__5_6)); }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * get_U3CbinaryReaderU3E5__5_6() const { return ___U3CbinaryReaderU3E5__5_6; }
	inline BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 ** get_address_of_U3CbinaryReaderU3E5__5_6() { return &___U3CbinaryReaderU3E5__5_6; }
	inline void set_U3CbinaryReaderU3E5__5_6(BinaryReader_t7467E057B24C42E81B1C3E5C60288BB4B1718969 * value)
	{
		___U3CbinaryReaderU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbinaryReaderU3E5__5_6), value);
	}

	inline static int32_t get_offset_of_U3CallBytesU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D, ___U3CallBytesU3E5__6_7)); }
	inline List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B * get_U3CallBytesU3E5__6_7() const { return ___U3CallBytesU3E5__6_7; }
	inline List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B ** get_address_of_U3CallBytesU3E5__6_7() { return &___U3CallBytesU3E5__6_7; }
	inline void set_U3CallBytesU3E5__6_7(List_1_t138C3A87AA33BCF0AE9B7978FD403BF0464EBC8B * value)
	{
		___U3CallBytesU3E5__6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallBytesU3E5__6_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__28_T5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D_H
#ifndef U3CWAITANDDOU3ED__3_TD3943973C2F0F450353630CBA3C912A07AB2D098_H
#define U3CWAITANDDOU3ED__3_TD3943973C2F0F450353630CBA3C912A07AB2D098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseControl_<WaitAndDo>d__3
struct  U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098  : public RuntimeObject
{
public:
	// System.Int32 BaseControl_<WaitAndDo>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BaseControl_<WaitAndDo>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single BaseControl_<WaitAndDo>d__3::time
	float ___time_2;
	// System.Action BaseControl_<WaitAndDo>d__3::action
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098, ___action_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_action_3() const { return ___action_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITANDDOU3ED__3_TD3943973C2F0F450353630CBA3C912A07AB2D098_H
#ifndef U3CBLINKU3ED__4_T2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427_H
#define U3CBLINKU3ED__4_T2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseControl_<blink>d__4
struct  U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427  : public RuntimeObject
{
public:
	// System.Int32 BaseControl_<blink>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BaseControl_<blink>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.GameObject BaseControl_<blink>d__4::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_2;
	// System.Int32 BaseControl_<blink>d__4::<i>5__2
	int32_t ___U3CiU3E5__2_3;
	// UnityEngine.Renderer BaseControl_<blink>d__4::<renderer>5__3
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___U3CrendererU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_obj_2() { return static_cast<int32_t>(offsetof(U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427, ___obj_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_2() const { return ___obj_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_2() { return &___obj_2; }
	inline void set_obj_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_2 = value;
		Il2CppCodeGenWriteBarrier((&___obj_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CrendererU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427, ___U3CrendererU3E5__3_4)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_U3CrendererU3E5__3_4() const { return ___U3CrendererU3E5__3_4; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_U3CrendererU3E5__3_4() { return &___U3CrendererU3E5__3_4; }
	inline void set_U3CrendererU3E5__3_4(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___U3CrendererU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrendererU3E5__3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBLINKU3ED__4_T2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427_H
#ifndef U3CFILLUNITYBUFFERSU3ED__48_T43B9E283B7914A96EDF86C9C3B4E39161E387017_H
#define U3CFILLUNITYBUFFERSU3ED__48_T43B9E283B7914A96EDF86C9C3B4E39161E387017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_<FillUnityBuffers>d__48
struct  U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017  : public RuntimeObject
{
public:
	// System.Int32 HoloVideoObject_<FillUnityBuffers>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HoloVideoObject_<FillUnityBuffers>d__48::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// HoloVideoObject HoloVideoObject_<FillUnityBuffers>d__48::<>4__this
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017, ___U3CU3E4__this_2)); }
	inline HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFILLUNITYBUFFERSU3ED__48_T43B9E283B7914A96EDF86C9C3B4E39161E387017_H
#ifndef U3CWAITU3ED__72_T0A5F586743F91C0D371ACDE454E07ED03D80400E_H
#define U3CWAITU3ED__72_T0A5F586743F91C0D371ACDE454E07ED03D80400E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkit_<Wait>d__72
struct  U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<Wait>d__72::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<Wait>d__72::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single NativeToolkit_<Wait>d__72::delay
	float ___delay_2;
	// System.Single NativeToolkit_<Wait>d__72::<pauseTarget>5__2
	float ___U3CpauseTargetU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CpauseTargetU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E, ___U3CpauseTargetU3E5__2_3)); }
	inline float get_U3CpauseTargetU3E5__2_3() const { return ___U3CpauseTargetU3E5__2_3; }
	inline float* get_address_of_U3CpauseTargetU3E5__2_3() { return &___U3CpauseTargetU3E5__2_3; }
	inline void set_U3CpauseTargetU3E5__2_3(float value)
	{
		___U3CpauseTargetU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITU3ED__72_T0A5F586743F91C0D371ACDE454E07ED03D80400E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#define VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.UI.VertexHelperExtension
struct  VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#define REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef AUDIODEVICEINFO_T105929EDE9CEF6D58A0CBF914F72C1C1B608D036_H
#define AUDIODEVICEINFO_T105929EDE9CEF6D58A0CBF914F72C1C1B608D036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioDeviceInfo
struct  AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036 
{
public:
	// System.String AudioDeviceInfo::Name
	String_t* ___Name_0;
	// System.String AudioDeviceInfo::Id
	String_t* ___Id_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Id_1() { return static_cast<int32_t>(offsetof(AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036, ___Id_1)); }
	inline String_t* get_Id_1() const { return ___Id_1; }
	inline String_t** get_address_of_Id_1() { return &___Id_1; }
	inline void set_Id_1(String_t* value)
	{
		___Id_1 = value;
		Il2CppCodeGenWriteBarrier((&___Id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of AudioDeviceInfo
struct AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036_marshaled_pinvoke
{
	Il2CppChar ___Name_0[1024];
	Il2CppChar ___Id_1[1024];
};
// Native definition for COM marshalling of AudioDeviceInfo
struct AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036_marshaled_com
{
	Il2CppChar ___Name_0[1024];
	Il2CppChar ___Id_1[1024];
};
#endif // AUDIODEVICEINFO_T105929EDE9CEF6D58A0CBF914F72C1C1B608D036_H
#ifndef HCAPSETTINGSINTEROP_TD574A2802911FCB0A5F59921EE9264B708ECDF4F_H
#define HCAPSETTINGSINTEROP_TD574A2802911FCB0A5F59921EE9264B708ECDF4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HCapSettingsInterop
struct  HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F 
{
public:
	// System.UInt32 HCapSettingsInterop::defaultMaxVertexCount
	uint32_t ___defaultMaxVertexCount_0;
	// System.UInt32 HCapSettingsInterop::defaultMaxIndexCount
	uint32_t ___defaultMaxIndexCount_1;

public:
	inline static int32_t get_offset_of_defaultMaxVertexCount_0() { return static_cast<int32_t>(offsetof(HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F, ___defaultMaxVertexCount_0)); }
	inline uint32_t get_defaultMaxVertexCount_0() const { return ___defaultMaxVertexCount_0; }
	inline uint32_t* get_address_of_defaultMaxVertexCount_0() { return &___defaultMaxVertexCount_0; }
	inline void set_defaultMaxVertexCount_0(uint32_t value)
	{
		___defaultMaxVertexCount_0 = value;
	}

	inline static int32_t get_offset_of_defaultMaxIndexCount_1() { return static_cast<int32_t>(offsetof(HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F, ___defaultMaxIndexCount_1)); }
	inline uint32_t get_defaultMaxIndexCount_1() const { return ___defaultMaxIndexCount_1; }
	inline uint32_t* get_address_of_defaultMaxIndexCount_1() { return &___defaultMaxIndexCount_1; }
	inline void set_defaultMaxIndexCount_1(uint32_t value)
	{
		___defaultMaxIndexCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HCAPSETTINGSINTEROP_TD574A2802911FCB0A5F59921EE9264B708ECDF4F_H
#ifndef HVSEQUENCE_TA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_H
#define HVSEQUENCE_TA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HVConductor_HVSequence
struct  HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1 
{
public:
	// System.Collections.Generic.List`1<HoloVideoObject> HVConductor_HVSequence::VideosToLoad
	List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * ___VideosToLoad_0;

public:
	inline static int32_t get_offset_of_VideosToLoad_0() { return static_cast<int32_t>(offsetof(HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1, ___VideosToLoad_0)); }
	inline List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * get_VideosToLoad_0() const { return ___VideosToLoad_0; }
	inline List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C ** get_address_of_VideosToLoad_0() { return &___VideosToLoad_0; }
	inline void set_VideosToLoad_0(List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * value)
	{
		___VideosToLoad_0 = value;
		Il2CppCodeGenWriteBarrier((&___VideosToLoad_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HVConductor/HVSequence
struct HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_marshaled_pinvoke
{
	List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * ___VideosToLoad_0;
};
// Native definition for COM marshalling of HVConductor/HVSequence
struct HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_marshaled_com
{
	List_1_t5193736D94FB3C884A7F72E642A433324BBB8C5C * ___VideosToLoad_0;
};
#endif // HVSEQUENCE_TA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1_H
#ifndef MATRIX4X4PLUGININTEROP_T66E8E2EC5E470D261DFBA691CBC8244F001820B2_H
#define MATRIX4X4PLUGININTEROP_T66E8E2EC5E470D261DFBA691CBC8244F001820B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Matrix4x4PluginInterop
struct  Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2 
{
public:
	// System.Single Matrix4x4PluginInterop::m00
	float ___m00_0;
	// System.Single Matrix4x4PluginInterop::m01
	float ___m01_1;
	// System.Single Matrix4x4PluginInterop::m02
	float ___m02_2;
	// System.Single Matrix4x4PluginInterop::m03
	float ___m03_3;
	// System.Single Matrix4x4PluginInterop::m10
	float ___m10_4;
	// System.Single Matrix4x4PluginInterop::m11
	float ___m11_5;
	// System.Single Matrix4x4PluginInterop::m12
	float ___m12_6;
	// System.Single Matrix4x4PluginInterop::m13
	float ___m13_7;
	// System.Single Matrix4x4PluginInterop::m20
	float ___m20_8;
	// System.Single Matrix4x4PluginInterop::m21
	float ___m21_9;
	// System.Single Matrix4x4PluginInterop::m22
	float ___m22_10;
	// System.Single Matrix4x4PluginInterop::m23
	float ___m23_11;
	// System.Single Matrix4x4PluginInterop::m30
	float ___m30_12;
	// System.Single Matrix4x4PluginInterop::m31
	float ___m31_13;
	// System.Single Matrix4x4PluginInterop::m32
	float ___m32_14;
	// System.Single Matrix4x4PluginInterop::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m01_1() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m01_1)); }
	inline float get_m01_1() const { return ___m01_1; }
	inline float* get_address_of_m01_1() { return &___m01_1; }
	inline void set_m01_1(float value)
	{
		___m01_1 = value;
	}

	inline static int32_t get_offset_of_m02_2() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m02_2)); }
	inline float get_m02_2() const { return ___m02_2; }
	inline float* get_address_of_m02_2() { return &___m02_2; }
	inline void set_m02_2(float value)
	{
		___m02_2 = value;
	}

	inline static int32_t get_offset_of_m03_3() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m03_3)); }
	inline float get_m03_3() const { return ___m03_3; }
	inline float* get_address_of_m03_3() { return &___m03_3; }
	inline void set_m03_3(float value)
	{
		___m03_3 = value;
	}

	inline static int32_t get_offset_of_m10_4() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m10_4)); }
	inline float get_m10_4() const { return ___m10_4; }
	inline float* get_address_of_m10_4() { return &___m10_4; }
	inline void set_m10_4(float value)
	{
		___m10_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m12_6() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m12_6)); }
	inline float get_m12_6() const { return ___m12_6; }
	inline float* get_address_of_m12_6() { return &___m12_6; }
	inline void set_m12_6(float value)
	{
		___m12_6 = value;
	}

	inline static int32_t get_offset_of_m13_7() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m13_7)); }
	inline float get_m13_7() const { return ___m13_7; }
	inline float* get_address_of_m13_7() { return &___m13_7; }
	inline void set_m13_7(float value)
	{
		___m13_7 = value;
	}

	inline static int32_t get_offset_of_m20_8() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m20_8)); }
	inline float get_m20_8() const { return ___m20_8; }
	inline float* get_address_of_m20_8() { return &___m20_8; }
	inline void set_m20_8(float value)
	{
		___m20_8 = value;
	}

	inline static int32_t get_offset_of_m21_9() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m21_9)); }
	inline float get_m21_9() const { return ___m21_9; }
	inline float* get_address_of_m21_9() { return &___m21_9; }
	inline void set_m21_9(float value)
	{
		___m21_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m23_11() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m23_11)); }
	inline float get_m23_11() const { return ___m23_11; }
	inline float* get_address_of_m23_11() { return &___m23_11; }
	inline void set_m23_11(float value)
	{
		___m23_11 = value;
	}

	inline static int32_t get_offset_of_m30_12() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m30_12)); }
	inline float get_m30_12() const { return ___m30_12; }
	inline float* get_address_of_m30_12() { return &___m30_12; }
	inline void set_m30_12(float value)
	{
		___m30_12 = value;
	}

	inline static int32_t get_offset_of_m31_13() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m31_13)); }
	inline float get_m31_13() const { return ___m31_13; }
	inline float* get_address_of_m31_13() { return &___m31_13; }
	inline void set_m31_13(float value)
	{
		___m31_13 = value;
	}

	inline static int32_t get_offset_of_m32_14() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m32_14)); }
	inline float get_m32_14() const { return ___m32_14; }
	inline float* get_address_of_m32_14() { return &___m32_14; }
	inline void set_m32_14(float value)
	{
		___m32_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4PLUGININTEROP_T66E8E2EC5E470D261DFBA691CBC8244F001820B2_H
#ifndef SVFFILEINFO_TD86F29AD52536786393A9657E2DB669F60A32366_H
#define SVFFILEINFO_TD86F29AD52536786393A9657E2DB669F60A32366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFFileInfo
struct  SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366 
{
public:
	// System.Boolean SVFFileInfo::hasAudio
	bool ___hasAudio_0;
	// System.UInt64 SVFFileInfo::duration100ns
	uint64_t ___duration100ns_1;
	// System.UInt32 SVFFileInfo::frameCount
	uint32_t ___frameCount_2;
	// System.UInt32 SVFFileInfo::maxVertexCount
	uint32_t ___maxVertexCount_3;
	// System.UInt32 SVFFileInfo::maxIndexCount
	uint32_t ___maxIndexCount_4;
	// System.Single SVFFileInfo::bitrateMbps
	float ___bitrateMbps_5;
	// System.Single SVFFileInfo::fileSize
	float ___fileSize_6;
	// System.Double SVFFileInfo::minX
	double ___minX_7;
	// System.Double SVFFileInfo::minY
	double ___minY_8;
	// System.Double SVFFileInfo::minZ
	double ___minZ_9;
	// System.Double SVFFileInfo::maxX
	double ___maxX_10;
	// System.Double SVFFileInfo::maxY
	double ___maxY_11;
	// System.Double SVFFileInfo::maxZ
	double ___maxZ_12;
	// System.UInt32 SVFFileInfo::fileWidth
	uint32_t ___fileWidth_13;
	// System.UInt32 SVFFileInfo::fileHeight
	uint32_t ___fileHeight_14;
	// System.Boolean SVFFileInfo::hasNormals
	bool ___hasNormals_15;

public:
	inline static int32_t get_offset_of_hasAudio_0() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___hasAudio_0)); }
	inline bool get_hasAudio_0() const { return ___hasAudio_0; }
	inline bool* get_address_of_hasAudio_0() { return &___hasAudio_0; }
	inline void set_hasAudio_0(bool value)
	{
		___hasAudio_0 = value;
	}

	inline static int32_t get_offset_of_duration100ns_1() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___duration100ns_1)); }
	inline uint64_t get_duration100ns_1() const { return ___duration100ns_1; }
	inline uint64_t* get_address_of_duration100ns_1() { return &___duration100ns_1; }
	inline void set_duration100ns_1(uint64_t value)
	{
		___duration100ns_1 = value;
	}

	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___frameCount_2)); }
	inline uint32_t get_frameCount_2() const { return ___frameCount_2; }
	inline uint32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(uint32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_maxVertexCount_3() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxVertexCount_3)); }
	inline uint32_t get_maxVertexCount_3() const { return ___maxVertexCount_3; }
	inline uint32_t* get_address_of_maxVertexCount_3() { return &___maxVertexCount_3; }
	inline void set_maxVertexCount_3(uint32_t value)
	{
		___maxVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_maxIndexCount_4() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxIndexCount_4)); }
	inline uint32_t get_maxIndexCount_4() const { return ___maxIndexCount_4; }
	inline uint32_t* get_address_of_maxIndexCount_4() { return &___maxIndexCount_4; }
	inline void set_maxIndexCount_4(uint32_t value)
	{
		___maxIndexCount_4 = value;
	}

	inline static int32_t get_offset_of_bitrateMbps_5() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___bitrateMbps_5)); }
	inline float get_bitrateMbps_5() const { return ___bitrateMbps_5; }
	inline float* get_address_of_bitrateMbps_5() { return &___bitrateMbps_5; }
	inline void set_bitrateMbps_5(float value)
	{
		___bitrateMbps_5 = value;
	}

	inline static int32_t get_offset_of_fileSize_6() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___fileSize_6)); }
	inline float get_fileSize_6() const { return ___fileSize_6; }
	inline float* get_address_of_fileSize_6() { return &___fileSize_6; }
	inline void set_fileSize_6(float value)
	{
		___fileSize_6 = value;
	}

	inline static int32_t get_offset_of_minX_7() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___minX_7)); }
	inline double get_minX_7() const { return ___minX_7; }
	inline double* get_address_of_minX_7() { return &___minX_7; }
	inline void set_minX_7(double value)
	{
		___minX_7 = value;
	}

	inline static int32_t get_offset_of_minY_8() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___minY_8)); }
	inline double get_minY_8() const { return ___minY_8; }
	inline double* get_address_of_minY_8() { return &___minY_8; }
	inline void set_minY_8(double value)
	{
		___minY_8 = value;
	}

	inline static int32_t get_offset_of_minZ_9() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___minZ_9)); }
	inline double get_minZ_9() const { return ___minZ_9; }
	inline double* get_address_of_minZ_9() { return &___minZ_9; }
	inline void set_minZ_9(double value)
	{
		___minZ_9 = value;
	}

	inline static int32_t get_offset_of_maxX_10() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxX_10)); }
	inline double get_maxX_10() const { return ___maxX_10; }
	inline double* get_address_of_maxX_10() { return &___maxX_10; }
	inline void set_maxX_10(double value)
	{
		___maxX_10 = value;
	}

	inline static int32_t get_offset_of_maxY_11() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxY_11)); }
	inline double get_maxY_11() const { return ___maxY_11; }
	inline double* get_address_of_maxY_11() { return &___maxY_11; }
	inline void set_maxY_11(double value)
	{
		___maxY_11 = value;
	}

	inline static int32_t get_offset_of_maxZ_12() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___maxZ_12)); }
	inline double get_maxZ_12() const { return ___maxZ_12; }
	inline double* get_address_of_maxZ_12() { return &___maxZ_12; }
	inline void set_maxZ_12(double value)
	{
		___maxZ_12 = value;
	}

	inline static int32_t get_offset_of_fileWidth_13() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___fileWidth_13)); }
	inline uint32_t get_fileWidth_13() const { return ___fileWidth_13; }
	inline uint32_t* get_address_of_fileWidth_13() { return &___fileWidth_13; }
	inline void set_fileWidth_13(uint32_t value)
	{
		___fileWidth_13 = value;
	}

	inline static int32_t get_offset_of_fileHeight_14() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___fileHeight_14)); }
	inline uint32_t get_fileHeight_14() const { return ___fileHeight_14; }
	inline uint32_t* get_address_of_fileHeight_14() { return &___fileHeight_14; }
	inline void set_fileHeight_14(uint32_t value)
	{
		___fileHeight_14 = value;
	}

	inline static int32_t get_offset_of_hasNormals_15() { return static_cast<int32_t>(offsetof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366, ___hasNormals_15)); }
	inline bool get_hasNormals_15() const { return ___hasNormals_15; }
	inline bool* get_address_of_hasNormals_15() { return &___hasNormals_15; }
	inline void set_hasNormals_15(bool value)
	{
		___hasNormals_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFFILEINFO_TD86F29AD52536786393A9657E2DB669F60A32366_H
#ifndef SVFFRAMEINFO_T53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E_H
#define SVFFRAMEINFO_T53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFFrameInfo
struct  SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E 
{
public:
	// System.UInt64 SVFFrameInfo::frameTimestamp
	uint64_t ___frameTimestamp_0;
	// System.Double SVFFrameInfo::minX
	double ___minX_1;
	// System.Double SVFFrameInfo::minY
	double ___minY_2;
	// System.Double SVFFrameInfo::minZ
	double ___minZ_3;
	// System.Double SVFFrameInfo::maxX
	double ___maxX_4;
	// System.Double SVFFrameInfo::maxY
	double ___maxY_5;
	// System.Double SVFFrameInfo::maxZ
	double ___maxZ_6;
	// System.UInt32 SVFFrameInfo::frameId
	uint32_t ___frameId_7;
	// System.UInt32 SVFFrameInfo::vertexCount
	uint32_t ___vertexCount_8;
	// System.UInt32 SVFFrameInfo::indexCount
	uint32_t ___indexCount_9;
	// System.Int32 SVFFrameInfo::textureWidth
	int32_t ___textureWidth_10;
	// System.Int32 SVFFrameInfo::textureHeight
	int32_t ___textureHeight_11;
	// System.Boolean SVFFrameInfo::isEOS
	bool ___isEOS_12;
	// System.Boolean SVFFrameInfo::isRepeatedFrame
	bool ___isRepeatedFrame_13;
	// System.Boolean SVFFrameInfo::isKeyFrame
	bool ___isKeyFrame_14;

public:
	inline static int32_t get_offset_of_frameTimestamp_0() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___frameTimestamp_0)); }
	inline uint64_t get_frameTimestamp_0() const { return ___frameTimestamp_0; }
	inline uint64_t* get_address_of_frameTimestamp_0() { return &___frameTimestamp_0; }
	inline void set_frameTimestamp_0(uint64_t value)
	{
		___frameTimestamp_0 = value;
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___minX_1)); }
	inline double get_minX_1() const { return ___minX_1; }
	inline double* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(double value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___minY_2)); }
	inline double get_minY_2() const { return ___minY_2; }
	inline double* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(double value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_minZ_3() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___minZ_3)); }
	inline double get_minZ_3() const { return ___minZ_3; }
	inline double* get_address_of_minZ_3() { return &___minZ_3; }
	inline void set_minZ_3(double value)
	{
		___minZ_3 = value;
	}

	inline static int32_t get_offset_of_maxX_4() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___maxX_4)); }
	inline double get_maxX_4() const { return ___maxX_4; }
	inline double* get_address_of_maxX_4() { return &___maxX_4; }
	inline void set_maxX_4(double value)
	{
		___maxX_4 = value;
	}

	inline static int32_t get_offset_of_maxY_5() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___maxY_5)); }
	inline double get_maxY_5() const { return ___maxY_5; }
	inline double* get_address_of_maxY_5() { return &___maxY_5; }
	inline void set_maxY_5(double value)
	{
		___maxY_5 = value;
	}

	inline static int32_t get_offset_of_maxZ_6() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___maxZ_6)); }
	inline double get_maxZ_6() const { return ___maxZ_6; }
	inline double* get_address_of_maxZ_6() { return &___maxZ_6; }
	inline void set_maxZ_6(double value)
	{
		___maxZ_6 = value;
	}

	inline static int32_t get_offset_of_frameId_7() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___frameId_7)); }
	inline uint32_t get_frameId_7() const { return ___frameId_7; }
	inline uint32_t* get_address_of_frameId_7() { return &___frameId_7; }
	inline void set_frameId_7(uint32_t value)
	{
		___frameId_7 = value;
	}

	inline static int32_t get_offset_of_vertexCount_8() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___vertexCount_8)); }
	inline uint32_t get_vertexCount_8() const { return ___vertexCount_8; }
	inline uint32_t* get_address_of_vertexCount_8() { return &___vertexCount_8; }
	inline void set_vertexCount_8(uint32_t value)
	{
		___vertexCount_8 = value;
	}

	inline static int32_t get_offset_of_indexCount_9() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___indexCount_9)); }
	inline uint32_t get_indexCount_9() const { return ___indexCount_9; }
	inline uint32_t* get_address_of_indexCount_9() { return &___indexCount_9; }
	inline void set_indexCount_9(uint32_t value)
	{
		___indexCount_9 = value;
	}

	inline static int32_t get_offset_of_textureWidth_10() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___textureWidth_10)); }
	inline int32_t get_textureWidth_10() const { return ___textureWidth_10; }
	inline int32_t* get_address_of_textureWidth_10() { return &___textureWidth_10; }
	inline void set_textureWidth_10(int32_t value)
	{
		___textureWidth_10 = value;
	}

	inline static int32_t get_offset_of_textureHeight_11() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___textureHeight_11)); }
	inline int32_t get_textureHeight_11() const { return ___textureHeight_11; }
	inline int32_t* get_address_of_textureHeight_11() { return &___textureHeight_11; }
	inline void set_textureHeight_11(int32_t value)
	{
		___textureHeight_11 = value;
	}

	inline static int32_t get_offset_of_isEOS_12() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___isEOS_12)); }
	inline bool get_isEOS_12() const { return ___isEOS_12; }
	inline bool* get_address_of_isEOS_12() { return &___isEOS_12; }
	inline void set_isEOS_12(bool value)
	{
		___isEOS_12 = value;
	}

	inline static int32_t get_offset_of_isRepeatedFrame_13() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___isRepeatedFrame_13)); }
	inline bool get_isRepeatedFrame_13() const { return ___isRepeatedFrame_13; }
	inline bool* get_address_of_isRepeatedFrame_13() { return &___isRepeatedFrame_13; }
	inline void set_isRepeatedFrame_13(bool value)
	{
		___isRepeatedFrame_13 = value;
	}

	inline static int32_t get_offset_of_isKeyFrame_14() { return static_cast<int32_t>(offsetof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E, ___isKeyFrame_14)); }
	inline bool get_isKeyFrame_14() const { return ___isKeyFrame_14; }
	inline bool* get_address_of_isKeyFrame_14() { return &___isKeyFrame_14; }
	inline void set_isKeyFrame_14(bool value)
	{
		___isKeyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFFRAMEINFO_T53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E_H
#ifndef SVFOPENINFO_TACB064007773FBC261C9EDBA22DA78C6D1C367F9_H
#define SVFOPENINFO_TACB064007773FBC261C9EDBA22DA78C6D1C367F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFOpenInfo
struct  SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9 
{
public:
	// System.Boolean SVFOpenInfo::AudioDisabled
	bool ___AudioDisabled_0;
	// System.Boolean SVFOpenInfo::UseHWTexture
	bool ___UseHWTexture_1;
	// System.Boolean SVFOpenInfo::UseHWDecode
	bool ___UseHWDecode_2;
	// System.Boolean SVFOpenInfo::UseKeyedMutex
	bool ___UseKeyedMutex_3;
	// System.Boolean SVFOpenInfo::RenderViaClock
	bool ___RenderViaClock_4;
	// System.Boolean SVFOpenInfo::OutputNormals
	bool ___OutputNormals_5;
	// System.Boolean SVFOpenInfo::StartDownloadOnOpen
	bool ___StartDownloadOnOpen_6;
	// System.Boolean SVFOpenInfo::AutoLooping
	bool ___AutoLooping_7;
	// System.Boolean SVFOpenInfo::lockHWTextures
	bool ___lockHWTextures_8;
	// System.Boolean SVFOpenInfo::forceSoftwareClock
	bool ___forceSoftwareClock_9;
	// System.Single SVFOpenInfo::PlaybackRate
	float ___PlaybackRate_10;
	// System.Single SVFOpenInfo::HRTFMinGain
	float ___HRTFMinGain_11;
	// System.Single SVFOpenInfo::HRTFMaxGain
	float ___HRTFMaxGain_12;
	// System.Single SVFOpenInfo::HRTFGainDistance
	float ___HRTFGainDistance_13;
	// System.Single SVFOpenInfo::HRTFCutoffDistance
	float ___HRTFCutoffDistance_14;
	// System.String SVFOpenInfo::AudioDeviceId
	String_t* ___AudioDeviceId_15;

public:
	inline static int32_t get_offset_of_AudioDisabled_0() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___AudioDisabled_0)); }
	inline bool get_AudioDisabled_0() const { return ___AudioDisabled_0; }
	inline bool* get_address_of_AudioDisabled_0() { return &___AudioDisabled_0; }
	inline void set_AudioDisabled_0(bool value)
	{
		___AudioDisabled_0 = value;
	}

	inline static int32_t get_offset_of_UseHWTexture_1() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___UseHWTexture_1)); }
	inline bool get_UseHWTexture_1() const { return ___UseHWTexture_1; }
	inline bool* get_address_of_UseHWTexture_1() { return &___UseHWTexture_1; }
	inline void set_UseHWTexture_1(bool value)
	{
		___UseHWTexture_1 = value;
	}

	inline static int32_t get_offset_of_UseHWDecode_2() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___UseHWDecode_2)); }
	inline bool get_UseHWDecode_2() const { return ___UseHWDecode_2; }
	inline bool* get_address_of_UseHWDecode_2() { return &___UseHWDecode_2; }
	inline void set_UseHWDecode_2(bool value)
	{
		___UseHWDecode_2 = value;
	}

	inline static int32_t get_offset_of_UseKeyedMutex_3() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___UseKeyedMutex_3)); }
	inline bool get_UseKeyedMutex_3() const { return ___UseKeyedMutex_3; }
	inline bool* get_address_of_UseKeyedMutex_3() { return &___UseKeyedMutex_3; }
	inline void set_UseKeyedMutex_3(bool value)
	{
		___UseKeyedMutex_3 = value;
	}

	inline static int32_t get_offset_of_RenderViaClock_4() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___RenderViaClock_4)); }
	inline bool get_RenderViaClock_4() const { return ___RenderViaClock_4; }
	inline bool* get_address_of_RenderViaClock_4() { return &___RenderViaClock_4; }
	inline void set_RenderViaClock_4(bool value)
	{
		___RenderViaClock_4 = value;
	}

	inline static int32_t get_offset_of_OutputNormals_5() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___OutputNormals_5)); }
	inline bool get_OutputNormals_5() const { return ___OutputNormals_5; }
	inline bool* get_address_of_OutputNormals_5() { return &___OutputNormals_5; }
	inline void set_OutputNormals_5(bool value)
	{
		___OutputNormals_5 = value;
	}

	inline static int32_t get_offset_of_StartDownloadOnOpen_6() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___StartDownloadOnOpen_6)); }
	inline bool get_StartDownloadOnOpen_6() const { return ___StartDownloadOnOpen_6; }
	inline bool* get_address_of_StartDownloadOnOpen_6() { return &___StartDownloadOnOpen_6; }
	inline void set_StartDownloadOnOpen_6(bool value)
	{
		___StartDownloadOnOpen_6 = value;
	}

	inline static int32_t get_offset_of_AutoLooping_7() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___AutoLooping_7)); }
	inline bool get_AutoLooping_7() const { return ___AutoLooping_7; }
	inline bool* get_address_of_AutoLooping_7() { return &___AutoLooping_7; }
	inline void set_AutoLooping_7(bool value)
	{
		___AutoLooping_7 = value;
	}

	inline static int32_t get_offset_of_lockHWTextures_8() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___lockHWTextures_8)); }
	inline bool get_lockHWTextures_8() const { return ___lockHWTextures_8; }
	inline bool* get_address_of_lockHWTextures_8() { return &___lockHWTextures_8; }
	inline void set_lockHWTextures_8(bool value)
	{
		___lockHWTextures_8 = value;
	}

	inline static int32_t get_offset_of_forceSoftwareClock_9() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___forceSoftwareClock_9)); }
	inline bool get_forceSoftwareClock_9() const { return ___forceSoftwareClock_9; }
	inline bool* get_address_of_forceSoftwareClock_9() { return &___forceSoftwareClock_9; }
	inline void set_forceSoftwareClock_9(bool value)
	{
		___forceSoftwareClock_9 = value;
	}

	inline static int32_t get_offset_of_PlaybackRate_10() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___PlaybackRate_10)); }
	inline float get_PlaybackRate_10() const { return ___PlaybackRate_10; }
	inline float* get_address_of_PlaybackRate_10() { return &___PlaybackRate_10; }
	inline void set_PlaybackRate_10(float value)
	{
		___PlaybackRate_10 = value;
	}

	inline static int32_t get_offset_of_HRTFMinGain_11() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFMinGain_11)); }
	inline float get_HRTFMinGain_11() const { return ___HRTFMinGain_11; }
	inline float* get_address_of_HRTFMinGain_11() { return &___HRTFMinGain_11; }
	inline void set_HRTFMinGain_11(float value)
	{
		___HRTFMinGain_11 = value;
	}

	inline static int32_t get_offset_of_HRTFMaxGain_12() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFMaxGain_12)); }
	inline float get_HRTFMaxGain_12() const { return ___HRTFMaxGain_12; }
	inline float* get_address_of_HRTFMaxGain_12() { return &___HRTFMaxGain_12; }
	inline void set_HRTFMaxGain_12(float value)
	{
		___HRTFMaxGain_12 = value;
	}

	inline static int32_t get_offset_of_HRTFGainDistance_13() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFGainDistance_13)); }
	inline float get_HRTFGainDistance_13() const { return ___HRTFGainDistance_13; }
	inline float* get_address_of_HRTFGainDistance_13() { return &___HRTFGainDistance_13; }
	inline void set_HRTFGainDistance_13(float value)
	{
		___HRTFGainDistance_13 = value;
	}

	inline static int32_t get_offset_of_HRTFCutoffDistance_14() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___HRTFCutoffDistance_14)); }
	inline float get_HRTFCutoffDistance_14() const { return ___HRTFCutoffDistance_14; }
	inline float* get_address_of_HRTFCutoffDistance_14() { return &___HRTFCutoffDistance_14; }
	inline void set_HRTFCutoffDistance_14(float value)
	{
		___HRTFCutoffDistance_14 = value;
	}

	inline static int32_t get_offset_of_AudioDeviceId_15() { return static_cast<int32_t>(offsetof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9, ___AudioDeviceId_15)); }
	inline String_t* get_AudioDeviceId_15() const { return ___AudioDeviceId_15; }
	inline String_t** get_address_of_AudioDeviceId_15() { return &___AudioDeviceId_15; }
	inline void set_AudioDeviceId_15(String_t* value)
	{
		___AudioDeviceId_15 = value;
		Il2CppCodeGenWriteBarrier((&___AudioDeviceId_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SVFOpenInfo
struct SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9_marshaled_pinvoke
{
	int8_t ___AudioDisabled_0;
	int8_t ___UseHWTexture_1;
	int8_t ___UseHWDecode_2;
	int8_t ___UseKeyedMutex_3;
	int8_t ___RenderViaClock_4;
	int8_t ___OutputNormals_5;
	int8_t ___StartDownloadOnOpen_6;
	int8_t ___AutoLooping_7;
	int8_t ___lockHWTextures_8;
	int8_t ___forceSoftwareClock_9;
	float ___PlaybackRate_10;
	float ___HRTFMinGain_11;
	float ___HRTFMaxGain_12;
	float ___HRTFGainDistance_13;
	float ___HRTFCutoffDistance_14;
	Il2CppChar ___AudioDeviceId_15[1024];
};
// Native definition for COM marshalling of SVFOpenInfo
struct SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9_marshaled_com
{
	int8_t ___AudioDisabled_0;
	int8_t ___UseHWTexture_1;
	int8_t ___UseHWDecode_2;
	int8_t ___UseKeyedMutex_3;
	int8_t ___RenderViaClock_4;
	int8_t ___OutputNormals_5;
	int8_t ___StartDownloadOnOpen_6;
	int8_t ___AutoLooping_7;
	int8_t ___lockHWTextures_8;
	int8_t ___forceSoftwareClock_9;
	float ___PlaybackRate_10;
	float ___HRTFMinGain_11;
	float ___HRTFMaxGain_12;
	float ___HRTFGainDistance_13;
	float ___HRTFCutoffDistance_14;
	Il2CppChar ___AudioDeviceId_15[1024];
};
#endif // SVFOPENINFO_TACB064007773FBC261C9EDBA22DA78C6D1C367F9_H
#ifndef SVFSTATUSINTEROP_TABB537FA071DFAC368BC5DB4720FA1B92B181696_H
#define SVFSTATUSINTEROP_TABB537FA071DFAC368BC5DB4720FA1B92B181696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFStatusInterop
struct  SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696 
{
public:
	// System.Boolean SVFStatusInterop::isLiveSVFSource
	bool ___isLiveSVFSource_0;
	// System.UInt32 SVFStatusInterop::lastReadFrame
	uint32_t ___lastReadFrame_1;
	// System.UInt32 SVFStatusInterop::unsuccessfulReadFrameCount
	uint32_t ___unsuccessfulReadFrameCount_2;
	// System.UInt32 SVFStatusInterop::droppedFrameCount
	uint32_t ___droppedFrameCount_3;
	// System.UInt32 SVFStatusInterop::errorHresult
	uint32_t ___errorHresult_4;
	// System.Int32 SVFStatusInterop::lastKnownState
	int32_t ___lastKnownState_5;

public:
	inline static int32_t get_offset_of_isLiveSVFSource_0() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___isLiveSVFSource_0)); }
	inline bool get_isLiveSVFSource_0() const { return ___isLiveSVFSource_0; }
	inline bool* get_address_of_isLiveSVFSource_0() { return &___isLiveSVFSource_0; }
	inline void set_isLiveSVFSource_0(bool value)
	{
		___isLiveSVFSource_0 = value;
	}

	inline static int32_t get_offset_of_lastReadFrame_1() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___lastReadFrame_1)); }
	inline uint32_t get_lastReadFrame_1() const { return ___lastReadFrame_1; }
	inline uint32_t* get_address_of_lastReadFrame_1() { return &___lastReadFrame_1; }
	inline void set_lastReadFrame_1(uint32_t value)
	{
		___lastReadFrame_1 = value;
	}

	inline static int32_t get_offset_of_unsuccessfulReadFrameCount_2() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___unsuccessfulReadFrameCount_2)); }
	inline uint32_t get_unsuccessfulReadFrameCount_2() const { return ___unsuccessfulReadFrameCount_2; }
	inline uint32_t* get_address_of_unsuccessfulReadFrameCount_2() { return &___unsuccessfulReadFrameCount_2; }
	inline void set_unsuccessfulReadFrameCount_2(uint32_t value)
	{
		___unsuccessfulReadFrameCount_2 = value;
	}

	inline static int32_t get_offset_of_droppedFrameCount_3() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___droppedFrameCount_3)); }
	inline uint32_t get_droppedFrameCount_3() const { return ___droppedFrameCount_3; }
	inline uint32_t* get_address_of_droppedFrameCount_3() { return &___droppedFrameCount_3; }
	inline void set_droppedFrameCount_3(uint32_t value)
	{
		___droppedFrameCount_3 = value;
	}

	inline static int32_t get_offset_of_errorHresult_4() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___errorHresult_4)); }
	inline uint32_t get_errorHresult_4() const { return ___errorHresult_4; }
	inline uint32_t* get_address_of_errorHresult_4() { return &___errorHresult_4; }
	inline void set_errorHresult_4(uint32_t value)
	{
		___errorHresult_4 = value;
	}

	inline static int32_t get_offset_of_lastKnownState_5() { return static_cast<int32_t>(offsetof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696, ___lastKnownState_5)); }
	inline int32_t get_lastKnownState_5() const { return ___lastKnownState_5; }
	inline int32_t* get_address_of_lastKnownState_5() { return &___lastKnownState_5; }
	inline void set_lastKnownState_5(int32_t value)
	{
		___lastKnownState_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFSTATUSINTEROP_TABB537FA071DFAC368BC5DB4720FA1B92B181696_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#define NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#define ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequest
struct  ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::m_RequestId
	int32_t ___m_RequestId_0;

public:
	inline static int32_t get_offset_of_m_RequestId_0() { return static_cast<int32_t>(offsetof(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167, ___m_RequestId_0)); }
	inline int32_t get_m_RequestId_0() const { return ___m_RequestId_0; }
	inline int32_t* get_address_of_m_RequestId_0() { return &___m_RequestId_0; }
	inline void set_m_RequestId_0(int32_t value)
	{
		___m_RequestId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifndef VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#define VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3Interop
struct  Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA 
{
public:
	// System.Single Vector3Interop::x
	float ___x_0;
	// System.Single Vector3Interop::y
	float ___y_1;
	// System.Single Vector3Interop::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#define U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController_<Save>d__27
struct  U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B  : public RuntimeObject
{
public:
	// System.Int32 ARWorldMapController_<Save>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARWorldMapController_<Save>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARWorldMapController ARWorldMapController_<Save>d__27::<>4__this
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * ___U3CU3E4__this_2;
	// UnityEngine.XR.ARKit.ARWorldMapRequest ARWorldMapController_<Save>d__27::<request>5__2
	ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  ___U3CrequestU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CU3E4__this_2)); }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B, ___U3CrequestU3E5__2_3)); }
	inline ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  get_U3CrequestU3E5__2_3() const { return ___U3CrequestU3E5__2_3; }
	inline ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 * get_address_of_U3CrequestU3E5__2_3() { return &___U3CrequestU3E5__2_3; }
	inline void set_U3CrequestU3E5__2_3(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167  value)
	{
		___U3CrequestU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3ED__27_TAAAF45083EB844EA39B7CB4CF64187F93C5A676B_H
#ifndef LOGLEVEL_T614E5F716939DAB7D93BA30FEC1372E436EC62DE_H
#define LOGLEVEL_T614E5F716939DAB7D93BA30FEC1372E436EC62DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LogLevel
struct  LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE 
{
public:
	// System.Int32 LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T614E5F716939DAB7D93BA30FEC1372E436EC62DE_H
#ifndef U3CGRABSCREENSHOTU3ED__48_T60270DDAAE5D04C3B1C635C16F158B14BEAA509B_H
#define U3CGRABSCREENSHOTU3ED__48_T60270DDAAE5D04C3B1C635C16F158B14BEAA509B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkit_<GrabScreenshot>d__48
struct  U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<GrabScreenshot>d__48::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<GrabScreenshot>d__48::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Rect NativeToolkit_<GrabScreenshot>d__48::screenArea
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenArea_2;
	// System.String NativeToolkit_<GrabScreenshot>d__48::fileType
	String_t* ___fileType_3;
	// System.String NativeToolkit_<GrabScreenshot>d__48::fileName
	String_t* ___fileName_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_screenArea_2() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B, ___screenArea_2)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_screenArea_2() const { return ___screenArea_2; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_screenArea_2() { return &___screenArea_2; }
	inline void set_screenArea_2(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___screenArea_2 = value;
	}

	inline static int32_t get_offset_of_fileType_3() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B, ___fileType_3)); }
	inline String_t* get_fileType_3() const { return ___fileType_3; }
	inline String_t** get_address_of_fileType_3() { return &___fileType_3; }
	inline void set_fileType_3(String_t* value)
	{
		___fileType_3 = value;
		Il2CppCodeGenWriteBarrier((&___fileType_3), value);
	}

	inline static int32_t get_offset_of_fileName_4() { return static_cast<int32_t>(offsetof(U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B, ___fileName_4)); }
	inline String_t* get_fileName_4() const { return ___fileName_4; }
	inline String_t** get_address_of_fileName_4() { return &___fileName_4; }
	inline void set_fileName_4(String_t* value)
	{
		___fileName_4 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGRABSCREENSHOTU3ED__48_T60270DDAAE5D04C3B1C635C16F158B14BEAA509B_H
#ifndef IMAGETYPE_T18E3FD0F43F4CE77C8D94F4627D48157BFBCD130_H
#define IMAGETYPE_T18E3FD0F43F4CE77C8D94F4627D48157BFBCD130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkit_ImageType
struct  ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130 
{
public:
	// System.Int32 NativeToolkit_ImageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETYPE_T18E3FD0F43F4CE77C8D94F4627D48157BFBCD130_H
#ifndef SAVESTATUS_T375ABFF625C7E35D370185AF24E13EE1D6B66BEC_H
#define SAVESTATUS_T375ABFF625C7E35D370185AF24E13EE1D6B66BEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkit_SaveStatus
struct  SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC 
{
public:
	// System.Int32 NativeToolkit_SaveStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVESTATUS_T375ABFF625C7E35D370185AF24E13EE1D6B66BEC_H
#ifndef SVFPLAYBACKSTATE_T0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F_H
#define SVFPLAYBACKSTATE_T0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFPlaybackState
struct  SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F 
{
public:
	// System.Int32 SVFPlaybackState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFPLAYBACKSTATE_T0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F_H
#ifndef SVFREADERSTATEINTEROP_T7766360500DB4DD4843D984E227AB74F026ACEA7_H
#define SVFREADERSTATEINTEROP_T7766360500DB4DD4843D984E227AB74F026ACEA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFReaderStateInterop
struct  SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7 
{
public:
	// System.Int32 SVFReaderStateInterop::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFREADERSTATEINTEROP_T7766360500DB4DD4843D984E227AB74F026ACEA7_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#define NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77 
{
public:
	// T System.Nullable`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77, ___value_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_0() const { return ___value_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#define RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#define VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRChannel
struct  VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496 
{
public:
	// System.Int32 VRChannel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#ifndef U3CSAVEU3ED__50_TE38DB3144F095258C3548DD2EDFD79D5D1F269E7_H
#define U3CSAVEU3ED__50_TE38DB3144F095258C3548DD2EDFD79D5D1F269E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkit_<Save>d__50
struct  U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7  : public RuntimeObject
{
public:
	// System.Int32 NativeToolkit_<Save>d__50::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object NativeToolkit_<Save>d__50::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String NativeToolkit_<Save>d__50::path
	String_t* ___path_2;
	// System.Byte[] NativeToolkit_<Save>d__50::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_3;
	// NativeToolkit_ImageType NativeToolkit_<Save>d__50::imageType
	int32_t ___imageType_4;
	// System.Int32 NativeToolkit_<Save>d__50::<count>5__2
	int32_t ___U3CcountU3E5__2_5;
	// NativeToolkit_SaveStatus NativeToolkit_<Save>d__50::<saved>5__3
	int32_t ___U3CsavedU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_bytes_3() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___bytes_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_3() const { return ___bytes_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_3() { return &___bytes_3; }
	inline void set_bytes_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_3 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_3), value);
	}

	inline static int32_t get_offset_of_imageType_4() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___imageType_4)); }
	inline int32_t get_imageType_4() const { return ___imageType_4; }
	inline int32_t* get_address_of_imageType_4() { return &___imageType_4; }
	inline void set_imageType_4(int32_t value)
	{
		___imageType_4 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___U3CcountU3E5__2_5)); }
	inline int32_t get_U3CcountU3E5__2_5() const { return ___U3CcountU3E5__2_5; }
	inline int32_t* get_address_of_U3CcountU3E5__2_5() { return &___U3CcountU3E5__2_5; }
	inline void set_U3CcountU3E5__2_5(int32_t value)
	{
		___U3CcountU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CsavedU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7, ___U3CsavedU3E5__3_6)); }
	inline int32_t get_U3CsavedU3E5__3_6() const { return ___U3CsavedU3E5__3_6; }
	inline int32_t* get_address_of_U3CsavedU3E5__3_6() { return &___U3CsavedU3E5__3_6; }
	inline void set_U3CsavedU3E5__3_6(int32_t value)
	{
		___U3CsavedU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3ED__50_TE38DB3144F095258C3548DD2EDFD79D5D1F269E7_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef ONENDOFSTREAM_T6ABE2224F14F21D78185646112D85DE309AA9091_H
#define ONENDOFSTREAM_T6ABE2224F14F21D78185646112D85DE309AA9091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnEndOfStream
struct  OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENDOFSTREAM_T6ABE2224F14F21D78185646112D85DE309AA9091_H
#ifndef ONFATALERROREVENT_TC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67_H
#define ONFATALERROREVENT_TC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnFatalErrorEvent
struct  OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFATALERROREVENT_TC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67_H
#ifndef ONFRAMEINFOEVENT_T254A414B4783E99BE1CDD836552B5BEE9BB14C3F_H
#define ONFRAMEINFOEVENT_T254A414B4783E99BE1CDD836552B5BEE9BB14C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnFrameInfoEvent
struct  OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONFRAMEINFOEVENT_T254A414B4783E99BE1CDD836552B5BEE9BB14C3F_H
#ifndef ONOPENEVENT_T9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1_H
#define ONOPENEVENT_T9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnOpenEvent
struct  OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONOPENEVENT_T9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1_H
#ifndef ONRENDEREVENT_TEBC1B611E970CED1D3F6AE62BCC833366AAC60F6_H
#define ONRENDEREVENT_TEBC1B611E970CED1D3F6AE62BCC833366AAC60F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject_OnRenderEvent
struct  OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRENDEREVENT_TEBC1B611E970CED1D3F6AE62BCC833366AAC60F6_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#define GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#define RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifndef RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#define RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifndef RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#define RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#define ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARFeatheredPlaneMeshVisualizer
struct  ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ARFeatheredPlaneMeshVisualizer::m_FeatheringWidth
	float ___m_FeatheringWidth_4;
	// UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer ARFeatheredPlaneMeshVisualizer::m_PlaneMeshVisualizer
	ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * ___m_PlaneMeshVisualizer_7;
	// UnityEngine.Material ARFeatheredPlaneMeshVisualizer::m_FeatheredPlaneMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_FeatheredPlaneMaterial_8;

public:
	inline static int32_t get_offset_of_m_FeatheringWidth_4() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_FeatheringWidth_4)); }
	inline float get_m_FeatheringWidth_4() const { return ___m_FeatheringWidth_4; }
	inline float* get_address_of_m_FeatheringWidth_4() { return &___m_FeatheringWidth_4; }
	inline void set_m_FeatheringWidth_4(float value)
	{
		___m_FeatheringWidth_4 = value;
	}

	inline static int32_t get_offset_of_m_PlaneMeshVisualizer_7() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_PlaneMeshVisualizer_7)); }
	inline ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * get_m_PlaneMeshVisualizer_7() const { return ___m_PlaneMeshVisualizer_7; }
	inline ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 ** get_address_of_m_PlaneMeshVisualizer_7() { return &___m_PlaneMeshVisualizer_7; }
	inline void set_m_PlaneMeshVisualizer_7(ARPlaneMeshVisualizer_t1CAC012DACAA7D8D30B94247B8762D71D2624923 * value)
	{
		___m_PlaneMeshVisualizer_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneMeshVisualizer_7), value);
	}

	inline static int32_t get_offset_of_m_FeatheredPlaneMaterial_8() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67, ___m_FeatheredPlaneMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_FeatheredPlaneMaterial_8() const { return ___m_FeatheredPlaneMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_FeatheredPlaneMaterial_8() { return &___m_FeatheredPlaneMaterial_8; }
	inline void set_m_FeatheredPlaneMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_FeatheredPlaneMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_FeatheredPlaneMaterial_8), value);
	}
};

struct ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ARFeatheredPlaneMeshVisualizer::s_FeatheringUVs
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___s_FeatheringUVs_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ARFeatheredPlaneMeshVisualizer::s_Vertices
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___s_Vertices_6;

public:
	inline static int32_t get_offset_of_s_FeatheringUVs_5() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields, ___s_FeatheringUVs_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_s_FeatheringUVs_5() const { return ___s_FeatheringUVs_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_s_FeatheringUVs_5() { return &___s_FeatheringUVs_5; }
	inline void set_s_FeatheringUVs_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___s_FeatheringUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_FeatheringUVs_5), value);
	}

	inline static int32_t get_offset_of_s_Vertices_6() { return static_cast<int32_t>(offsetof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields, ___s_Vertices_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_s_Vertices_6() const { return ___s_Vertices_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_s_Vertices_6() { return &___s_Vertices_6; }
	inline void set_s_Vertices_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___s_Vertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Vertices_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFEATHEREDPLANEMESHVISUALIZER_TE778AE37070D3D5969CFD0FEEE8826992D092A67_H
#ifndef ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#define ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARWorldMapController
struct  ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARSession ARWorldMapController::m_ARSession
	ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * ___m_ARSession_4;
	// UnityEngine.UI.Text ARWorldMapController::m_ErrorText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ErrorText_5;
	// UnityEngine.UI.Text ARWorldMapController::m_LogText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_LogText_6;
	// UnityEngine.UI.Text ARWorldMapController::m_MappingStatusText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_MappingStatusText_7;
	// UnityEngine.UI.Button ARWorldMapController::m_SaveButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_SaveButton_8;
	// UnityEngine.UI.Button ARWorldMapController::m_LoadButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___m_LoadButton_9;
	// System.Collections.Generic.List`1<System.String> ARWorldMapController::m_LogMessages
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_LogMessages_10;

public:
	inline static int32_t get_offset_of_m_ARSession_4() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_ARSession_4)); }
	inline ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * get_m_ARSession_4() const { return ___m_ARSession_4; }
	inline ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB ** get_address_of_m_ARSession_4() { return &___m_ARSession_4; }
	inline void set_m_ARSession_4(ARSession_tFD6F1BD76D4C003B8141D9B6255B904D8C5036AB * value)
	{
		___m_ARSession_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARSession_4), value);
	}

	inline static int32_t get_offset_of_m_ErrorText_5() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_ErrorText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ErrorText_5() const { return ___m_ErrorText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ErrorText_5() { return &___m_ErrorText_5; }
	inline void set_m_ErrorText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ErrorText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ErrorText_5), value);
	}

	inline static int32_t get_offset_of_m_LogText_6() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LogText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_LogText_6() const { return ___m_LogText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_LogText_6() { return &___m_LogText_6; }
	inline void set_m_LogText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_LogText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogText_6), value);
	}

	inline static int32_t get_offset_of_m_MappingStatusText_7() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_MappingStatusText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_MappingStatusText_7() const { return ___m_MappingStatusText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_MappingStatusText_7() { return &___m_MappingStatusText_7; }
	inline void set_m_MappingStatusText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_MappingStatusText_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MappingStatusText_7), value);
	}

	inline static int32_t get_offset_of_m_SaveButton_8() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_SaveButton_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_SaveButton_8() const { return ___m_SaveButton_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_SaveButton_8() { return &___m_SaveButton_8; }
	inline void set_m_SaveButton_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_SaveButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SaveButton_8), value);
	}

	inline static int32_t get_offset_of_m_LoadButton_9() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LoadButton_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_m_LoadButton_9() const { return ___m_LoadButton_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_m_LoadButton_9() { return &___m_LoadButton_9; }
	inline void set_m_LoadButton_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___m_LoadButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_LoadButton_9), value);
	}

	inline static int32_t get_offset_of_m_LogMessages_10() { return static_cast<int32_t>(offsetof(ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737, ___m_LogMessages_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_LogMessages_10() const { return ___m_LogMessages_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_LogMessages_10() { return &___m_LogMessages_10; }
	inline void set_m_LogMessages_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_LogMessages_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogMessages_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPCONTROLLER_TE07FBF1307D659E7AB45F66CFBA14CAC07C98737_H
#ifndef BASECONTROL_TA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171_H
#define BASECONTROL_TA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseControl
struct  BaseControl_tA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject BaseControl::freeViewIcon
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___freeViewIcon_4;

public:
	inline static int32_t get_offset_of_freeViewIcon_4() { return static_cast<int32_t>(offsetof(BaseControl_tA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171, ___freeViewIcon_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_freeViewIcon_4() const { return ___freeViewIcon_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_freeViewIcon_4() { return &___freeViewIcon_4; }
	inline void set_freeViewIcon_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___freeViewIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___freeViewIcon_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECONTROL_TA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171_H
#ifndef CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#define CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraConfigController
struct  CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.String> CameraConfigController::m_ConfigurationNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___m_ConfigurationNames_4;
	// UnityEngine.UI.Dropdown CameraConfigController::m_Dropdown
	Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * ___m_Dropdown_5;

public:
	inline static int32_t get_offset_of_m_ConfigurationNames_4() { return static_cast<int32_t>(offsetof(CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B, ___m_ConfigurationNames_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_m_ConfigurationNames_4() const { return ___m_ConfigurationNames_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_m_ConfigurationNames_4() { return &___m_ConfigurationNames_4; }
	inline void set_m_ConfigurationNames_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___m_ConfigurationNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ConfigurationNames_4), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_5() { return static_cast<int32_t>(offsetof(CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B, ___m_Dropdown_5)); }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * get_m_Dropdown_5() const { return ___m_Dropdown_5; }
	inline Dropdown_tF6331401084B1213CAB10587A6EC81461501930F ** get_address_of_m_Dropdown_5() { return &___m_Dropdown_5; }
	inline void set_m_Dropdown_5(Dropdown_tF6331401084B1213CAB10587A6EC81461501930F * value)
	{
		___m_Dropdown_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGCONTROLLER_T7D9D87FE02FA864F391F3BB2B9E9C7923F85279B_H
#ifndef CLOSESUITCASE_TED1BE400BA10B9A96CED8ED24A9D106FB682C5A5_H
#define CLOSESUITCASE_TED1BE400BA10B9A96CED8ED24A9D106FB682C5A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CloseSuitcase
struct  CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CloseSuitcase::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject CloseSuitcase::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject CloseSuitcase::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// UnityEngine.GameObject CloseSuitcase::frontCover
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___frontCover_7;
	// System.Boolean CloseSuitcase::isTriggered
	bool ___isTriggered_8;
	// System.Boolean CloseSuitcase::isTriggering
	bool ___isTriggering_9;
	// UnityEngine.Vector3 CloseSuitcase::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 CloseSuitcase::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 CloseSuitcase::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 CloseSuitcase::frontCoverInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontCoverInitV3_13;
	// UnityEngine.Vector3 CloseSuitcase::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 CloseSuitcase::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 CloseSuitcase::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 CloseSuitcase::frontCoverTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontCoverTargetV3_17;
	// UnityEngine.Vector3 CloseSuitcase::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 CloseSuitcase::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 CloseSuitcase::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// UnityEngine.Vector3 CloseSuitcase::frontCoverRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontCoverRotationtV3_21;
	// System.Int32 CloseSuitcase::frontNR
	int32_t ___frontNR_22;
	// System.Int32 CloseSuitcase::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 CloseSuitcase::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 CloseSuitcase::frontCoverNR
	int32_t ___frontCoverNR_25;
	// System.Int32 CloseSuitcase::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 CloseSuitcase::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 CloseSuitcase::mainPocketRCount
	int32_t ___mainPocketRCount_28;
	// System.Int32 CloseSuitcase::frontCoverRCount
	int32_t ___frontCoverRCount_29;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_frontCover_7() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontCover_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_frontCover_7() const { return ___frontCover_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_frontCover_7() { return &___frontCover_7; }
	inline void set_frontCover_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___frontCover_7 = value;
		Il2CppCodeGenWriteBarrier((&___frontCover_7), value);
	}

	inline static int32_t get_offset_of_isTriggered_8() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___isTriggered_8)); }
	inline bool get_isTriggered_8() const { return ___isTriggered_8; }
	inline bool* get_address_of_isTriggered_8() { return &___isTriggered_8; }
	inline void set_isTriggered_8(bool value)
	{
		___isTriggered_8 = value;
	}

	inline static int32_t get_offset_of_isTriggering_9() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___isTriggering_9)); }
	inline bool get_isTriggering_9() const { return ___isTriggering_9; }
	inline bool* get_address_of_isTriggering_9() { return &___isTriggering_9; }
	inline void set_isTriggering_9(bool value)
	{
		___isTriggering_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_frontCoverInitV3_13() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontCoverInitV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontCoverInitV3_13() const { return ___frontCoverInitV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontCoverInitV3_13() { return &___frontCoverInitV3_13; }
	inline void set_frontCoverInitV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontCoverInitV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_frontCoverTargetV3_17() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontCoverTargetV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontCoverTargetV3_17() const { return ___frontCoverTargetV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontCoverTargetV3_17() { return &___frontCoverTargetV3_17; }
	inline void set_frontCoverTargetV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontCoverTargetV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_frontCoverRotationtV3_21() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontCoverRotationtV3_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontCoverRotationtV3_21() const { return ___frontCoverRotationtV3_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontCoverRotationtV3_21() { return &___frontCoverRotationtV3_21; }
	inline void set_frontCoverRotationtV3_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontCoverRotationtV3_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_frontCoverNR_25() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontCoverNR_25)); }
	inline int32_t get_frontCoverNR_25() const { return ___frontCoverNR_25; }
	inline int32_t* get_address_of_frontCoverNR_25() { return &___frontCoverNR_25; }
	inline void set_frontCoverNR_25(int32_t value)
	{
		___frontCoverNR_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}

	inline static int32_t get_offset_of_frontCoverRCount_29() { return static_cast<int32_t>(offsetof(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5, ___frontCoverRCount_29)); }
	inline int32_t get_frontCoverRCount_29() const { return ___frontCoverRCount_29; }
	inline int32_t* get_address_of_frontCoverRCount_29() { return &___frontCoverRCount_29; }
	inline void set_frontCoverRCount_29(int32_t value)
	{
		___frontCoverRCount_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESUITCASE_TED1BE400BA10B9A96CED8ED24A9D106FB682C5A5_H
#ifndef DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#define DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisableVerticalPlanes
struct  DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text DisableVerticalPlanes::m_LogText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_LogText_4;

public:
	inline static int32_t get_offset_of_m_LogText_4() { return static_cast<int32_t>(offsetof(DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19, ___m_LogText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_LogText_4() const { return ___m_LogText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_LogText_4() { return &___m_LogText_4; }
	inline void set_m_LogText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_LogText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_LogText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEVERTICALPLANES_T40082019580280FBDED0DF62CC5DA4E1A8E32D19_H
#ifndef FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#define FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FadePlaneOnBoundaryChange
struct  FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator FadePlaneOnBoundaryChange::m_Animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_Animator_7;
	// UnityEngine.XR.ARFoundation.ARPlane FadePlaneOnBoundaryChange::m_Plane
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___m_Plane_8;
	// System.Single FadePlaneOnBoundaryChange::m_ShowTime
	float ___m_ShowTime_9;
	// System.Boolean FadePlaneOnBoundaryChange::m_UpdatingPlane
	bool ___m_UpdatingPlane_10;

public:
	inline static int32_t get_offset_of_m_Animator_7() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_Animator_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_Animator_7() const { return ___m_Animator_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_Animator_7() { return &___m_Animator_7; }
	inline void set_m_Animator_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_Animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Animator_7), value);
	}

	inline static int32_t get_offset_of_m_Plane_8() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_Plane_8)); }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * get_m_Plane_8() const { return ___m_Plane_8; }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E ** get_address_of_m_Plane_8() { return &___m_Plane_8; }
	inline void set_m_Plane_8(ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * value)
	{
		___m_Plane_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Plane_8), value);
	}

	inline static int32_t get_offset_of_m_ShowTime_9() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_ShowTime_9)); }
	inline float get_m_ShowTime_9() const { return ___m_ShowTime_9; }
	inline float* get_address_of_m_ShowTime_9() { return &___m_ShowTime_9; }
	inline void set_m_ShowTime_9(float value)
	{
		___m_ShowTime_9 = value;
	}

	inline static int32_t get_offset_of_m_UpdatingPlane_10() { return static_cast<int32_t>(offsetof(FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A, ___m_UpdatingPlane_10)); }
	inline bool get_m_UpdatingPlane_10() const { return ___m_UpdatingPlane_10; }
	inline bool* get_address_of_m_UpdatingPlane_10() { return &___m_UpdatingPlane_10; }
	inline void set_m_UpdatingPlane_10(bool value)
	{
		___m_UpdatingPlane_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEPLANEONBOUNDARYCHANGE_T88629D7DC409498EABE1D1CCE054157CBE47CD7A_H
#ifndef FLYHANGER_TEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A_H
#define FLYHANGER_TEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlyHanger
struct  FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject FlyHanger::handle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___handle_4;
	// System.Boolean FlyHanger::isTriggered
	bool ___isTriggered_5;
	// System.Boolean FlyHanger::isTriggering
	bool ___isTriggering_6;
	// UnityEngine.Vector3 FlyHanger::handleInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleInitV3_7;
	// UnityEngine.Vector3 FlyHanger::handleTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTargetV3_8;
	// UnityEngine.Vector3 FlyHanger::handleTranslateV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTranslateV3_9;
	// System.Int32 FlyHanger::handleNR
	int32_t ___handleNR_10;
	// System.Int32 FlyHanger::handleRCount
	int32_t ___handleRCount_11;

public:
	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___handle_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_handle_4() const { return ___handle_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of_isTriggered_5() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___isTriggered_5)); }
	inline bool get_isTriggered_5() const { return ___isTriggered_5; }
	inline bool* get_address_of_isTriggered_5() { return &___isTriggered_5; }
	inline void set_isTriggered_5(bool value)
	{
		___isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_isTriggering_6() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___isTriggering_6)); }
	inline bool get_isTriggering_6() const { return ___isTriggering_6; }
	inline bool* get_address_of_isTriggering_6() { return &___isTriggering_6; }
	inline void set_isTriggering_6(bool value)
	{
		___isTriggering_6 = value;
	}

	inline static int32_t get_offset_of_handleInitV3_7() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___handleInitV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleInitV3_7() const { return ___handleInitV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleInitV3_7() { return &___handleInitV3_7; }
	inline void set_handleInitV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleInitV3_7 = value;
	}

	inline static int32_t get_offset_of_handleTargetV3_8() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___handleTargetV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTargetV3_8() const { return ___handleTargetV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTargetV3_8() { return &___handleTargetV3_8; }
	inline void set_handleTargetV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTargetV3_8 = value;
	}

	inline static int32_t get_offset_of_handleTranslateV3_9() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___handleTranslateV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTranslateV3_9() const { return ___handleTranslateV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTranslateV3_9() { return &___handleTranslateV3_9; }
	inline void set_handleTranslateV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTranslateV3_9 = value;
	}

	inline static int32_t get_offset_of_handleNR_10() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___handleNR_10)); }
	inline int32_t get_handleNR_10() const { return ___handleNR_10; }
	inline int32_t* get_address_of_handleNR_10() { return &___handleNR_10; }
	inline void set_handleNR_10(int32_t value)
	{
		___handleNR_10 = value;
	}

	inline static int32_t get_offset_of_handleRCount_11() { return static_cast<int32_t>(offsetof(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A, ___handleRCount_11)); }
	inline int32_t get_handleRCount_11() const { return ___handleRCount_11; }
	inline int32_t* get_address_of_handleRCount_11() { return &___handleRCount_11; }
	inline void set_handleRCount_11(int32_t value)
	{
		___handleRCount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLYHANGER_TEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A_H
#ifndef FLYHANGERREVERSE_T11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA_H
#define FLYHANGERREVERSE_T11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlyHangerReverse
struct  FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject FlyHangerReverse::handle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___handle_4;
	// System.Boolean FlyHangerReverse::isTriggered
	bool ___isTriggered_5;
	// System.Boolean FlyHangerReverse::isTriggering
	bool ___isTriggering_6;
	// UnityEngine.Vector3 FlyHangerReverse::handleInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleInitV3_7;
	// UnityEngine.Vector3 FlyHangerReverse::handleTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTargetV3_8;
	// UnityEngine.Vector3 FlyHangerReverse::handleTranslateV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTranslateV3_9;
	// System.Int32 FlyHangerReverse::handleNR
	int32_t ___handleNR_10;
	// System.Int32 FlyHangerReverse::handleRCount
	int32_t ___handleRCount_11;

public:
	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___handle_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_handle_4() const { return ___handle_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of_isTriggered_5() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___isTriggered_5)); }
	inline bool get_isTriggered_5() const { return ___isTriggered_5; }
	inline bool* get_address_of_isTriggered_5() { return &___isTriggered_5; }
	inline void set_isTriggered_5(bool value)
	{
		___isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_isTriggering_6() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___isTriggering_6)); }
	inline bool get_isTriggering_6() const { return ___isTriggering_6; }
	inline bool* get_address_of_isTriggering_6() { return &___isTriggering_6; }
	inline void set_isTriggering_6(bool value)
	{
		___isTriggering_6 = value;
	}

	inline static int32_t get_offset_of_handleInitV3_7() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___handleInitV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleInitV3_7() const { return ___handleInitV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleInitV3_7() { return &___handleInitV3_7; }
	inline void set_handleInitV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleInitV3_7 = value;
	}

	inline static int32_t get_offset_of_handleTargetV3_8() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___handleTargetV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTargetV3_8() const { return ___handleTargetV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTargetV3_8() { return &___handleTargetV3_8; }
	inline void set_handleTargetV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTargetV3_8 = value;
	}

	inline static int32_t get_offset_of_handleTranslateV3_9() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___handleTranslateV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTranslateV3_9() const { return ___handleTranslateV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTranslateV3_9() { return &___handleTranslateV3_9; }
	inline void set_handleTranslateV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTranslateV3_9 = value;
	}

	inline static int32_t get_offset_of_handleNR_10() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___handleNR_10)); }
	inline int32_t get_handleNR_10() const { return ___handleNR_10; }
	inline int32_t* get_address_of_handleNR_10() { return &___handleNR_10; }
	inline void set_handleNR_10(int32_t value)
	{
		___handleNR_10 = value;
	}

	inline static int32_t get_offset_of_handleRCount_11() { return static_cast<int32_t>(offsetof(FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA, ___handleRCount_11)); }
	inline int32_t get_handleRCount_11() const { return ___handleRCount_11; }
	inline int32_t* get_address_of_handleRCount_11() { return &___handleRCount_11; }
	inline void set_handleRCount_11(int32_t value)
	{
		___handleRCount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLYHANGERREVERSE_T11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA_H
#ifndef HVCONDUCTOR_T966F2F4E311B562E0F5CE66AEEC767CD63E9D59E_H
#define HVCONDUCTOR_T966F2F4E311B562E0F5CE66AEEC767CD63E9D59E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HVConductor
struct  HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean HVConductor::ShouldLoopSequences
	bool ___ShouldLoopSequences_4;
	// System.Collections.Generic.List`1<HVConductor_HVSequence> HVConductor::sequenceList
	List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F * ___sequenceList_5;
	// System.Int32 HVConductor::currSequence
	int32_t ___currSequence_6;

public:
	inline static int32_t get_offset_of_ShouldLoopSequences_4() { return static_cast<int32_t>(offsetof(HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E, ___ShouldLoopSequences_4)); }
	inline bool get_ShouldLoopSequences_4() const { return ___ShouldLoopSequences_4; }
	inline bool* get_address_of_ShouldLoopSequences_4() { return &___ShouldLoopSequences_4; }
	inline void set_ShouldLoopSequences_4(bool value)
	{
		___ShouldLoopSequences_4 = value;
	}

	inline static int32_t get_offset_of_sequenceList_5() { return static_cast<int32_t>(offsetof(HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E, ___sequenceList_5)); }
	inline List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F * get_sequenceList_5() const { return ___sequenceList_5; }
	inline List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F ** get_address_of_sequenceList_5() { return &___sequenceList_5; }
	inline void set_sequenceList_5(List_1_t72FCE2C4A1716A632723FDF54D82B4B2F8C3750F * value)
	{
		___sequenceList_5 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceList_5), value);
	}

	inline static int32_t get_offset_of_currSequence_6() { return static_cast<int32_t>(offsetof(HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E, ___currSequence_6)); }
	inline int32_t get_currSequence_6() const { return ___currSequence_6; }
	inline int32_t* get_address_of_currSequence_6() { return &___currSequence_6; }
	inline void set_currSequence_6(int32_t value)
	{
		___currSequence_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HVCONDUCTOR_T966F2F4E311B562E0F5CE66AEEC767CD63E9D59E_H
#ifndef HOLOVIDEOOBJECT_T9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB_H
#define HOLOVIDEOOBJECT_T9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloVideoObject
struct  HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String HoloVideoObject::Url
	String_t* ___Url_4;
	// System.Boolean HoloVideoObject::ShouldAutoPlay
	bool ___ShouldAutoPlay_5;
	// System.Single HoloVideoObject::_audioVolume
	float ____audioVolume_6;
	// UnityEngine.Vector3 HoloVideoObject::audioSourceOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___audioSourceOffset_7;
	// System.Boolean HoloVideoObject::flipHandedness
	bool ___flipHandedness_8;
	// System.Single HoloVideoObject::_clockScale
	float ____clockScale_9;
	// System.Boolean HoloVideoObject::computeNormals
	bool ___computeNormals_10;
	// SVFOpenInfo HoloVideoObject::Settings
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  ___Settings_11;
	// System.UInt32 HoloVideoObject::DefaultMaxVertexCount
	uint32_t ___DefaultMaxVertexCount_12;
	// System.UInt32 HoloVideoObject::DefaultMaxIndexCount
	uint32_t ___DefaultMaxIndexCount_13;
	// UnityEngine.Bounds HoloVideoObject::localSpaceBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___localSpaceBounds_14;
	// UnityEngine.Mesh[] HoloVideoObject::meshes
	MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89* ___meshes_15;
	// UnityEngine.MeshFilter HoloVideoObject::meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___meshFilter_16;
	// UnityEngine.MeshRenderer HoloVideoObject::meshRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___meshRenderer_17;
	// UnityEngine.BoxCollider HoloVideoObject::HVCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___HVCollider_18;
	// UnityEngine.Material HoloVideoObject::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_19;
	// UnityEngine.AudioListener HoloVideoObject::listener
	AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * ___listener_20;
	// SVFUnityPluginInterop HoloVideoObject::pluginInterop
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 * ___pluginInterop_21;
	// System.Boolean HoloVideoObject::isInitialized
	bool ___isInitialized_22;
	// SVFOpenInfo HoloVideoObject::openInfo
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  ___openInfo_23;
	// SVFFileInfo HoloVideoObject::fileInfo
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366  ___fileInfo_24;
	// SVFFrameInfo HoloVideoObject::lastFrameInfo
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E  ___lastFrameInfo_25;
	// HoloVideoObject_OnOpenEvent HoloVideoObject::OnOpenNotify
	OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 * ___OnOpenNotify_26;
	// HoloVideoObject_OnFrameInfoEvent HoloVideoObject::OnUpdateFrameInfoNotify
	OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F * ___OnUpdateFrameInfoNotify_27;
	// HoloVideoObject_OnRenderEvent HoloVideoObject::OnRenderCallback
	OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 * ___OnRenderCallback_28;
	// HoloVideoObject_OnFatalErrorEvent HoloVideoObject::OnFatalError
	OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 * ___OnFatalError_29;
	// HoloVideoObject_OnEndOfStream HoloVideoObject::OnEndOfStreamNotify
	OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 * ___OnEndOfStreamNotify_30;
	// UnityEngine.Coroutine HoloVideoObject::UnityBufferCoroutine
	Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * ___UnityBufferCoroutine_31;
	// UnityEngine.Logger HoloVideoObject::logger
	Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * ___logger_32;
	// System.Boolean HoloVideoObject::wasPlaying
	bool ___wasPlaying_34;
	// System.Boolean HoloVideoObject::ShouldPauseAfterPlay
	bool ___ShouldPauseAfterPlay_35;
	// System.UInt32 HoloVideoObject::PauseFrameID
	uint32_t ___PauseFrameID_36;
	// System.Boolean HoloVideoObject::isInstanceDisposed
	bool ___isInstanceDisposed_37;

public:
	inline static int32_t get_offset_of_Url_4() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___Url_4)); }
	inline String_t* get_Url_4() const { return ___Url_4; }
	inline String_t** get_address_of_Url_4() { return &___Url_4; }
	inline void set_Url_4(String_t* value)
	{
		___Url_4 = value;
		Il2CppCodeGenWriteBarrier((&___Url_4), value);
	}

	inline static int32_t get_offset_of_ShouldAutoPlay_5() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___ShouldAutoPlay_5)); }
	inline bool get_ShouldAutoPlay_5() const { return ___ShouldAutoPlay_5; }
	inline bool* get_address_of_ShouldAutoPlay_5() { return &___ShouldAutoPlay_5; }
	inline void set_ShouldAutoPlay_5(bool value)
	{
		___ShouldAutoPlay_5 = value;
	}

	inline static int32_t get_offset_of__audioVolume_6() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ____audioVolume_6)); }
	inline float get__audioVolume_6() const { return ____audioVolume_6; }
	inline float* get_address_of__audioVolume_6() { return &____audioVolume_6; }
	inline void set__audioVolume_6(float value)
	{
		____audioVolume_6 = value;
	}

	inline static int32_t get_offset_of_audioSourceOffset_7() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___audioSourceOffset_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_audioSourceOffset_7() const { return ___audioSourceOffset_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_audioSourceOffset_7() { return &___audioSourceOffset_7; }
	inline void set_audioSourceOffset_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___audioSourceOffset_7 = value;
	}

	inline static int32_t get_offset_of_flipHandedness_8() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___flipHandedness_8)); }
	inline bool get_flipHandedness_8() const { return ___flipHandedness_8; }
	inline bool* get_address_of_flipHandedness_8() { return &___flipHandedness_8; }
	inline void set_flipHandedness_8(bool value)
	{
		___flipHandedness_8 = value;
	}

	inline static int32_t get_offset_of__clockScale_9() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ____clockScale_9)); }
	inline float get__clockScale_9() const { return ____clockScale_9; }
	inline float* get_address_of__clockScale_9() { return &____clockScale_9; }
	inline void set__clockScale_9(float value)
	{
		____clockScale_9 = value;
	}

	inline static int32_t get_offset_of_computeNormals_10() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___computeNormals_10)); }
	inline bool get_computeNormals_10() const { return ___computeNormals_10; }
	inline bool* get_address_of_computeNormals_10() { return &___computeNormals_10; }
	inline void set_computeNormals_10(bool value)
	{
		___computeNormals_10 = value;
	}

	inline static int32_t get_offset_of_Settings_11() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___Settings_11)); }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  get_Settings_11() const { return ___Settings_11; }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9 * get_address_of_Settings_11() { return &___Settings_11; }
	inline void set_Settings_11(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  value)
	{
		___Settings_11 = value;
	}

	inline static int32_t get_offset_of_DefaultMaxVertexCount_12() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___DefaultMaxVertexCount_12)); }
	inline uint32_t get_DefaultMaxVertexCount_12() const { return ___DefaultMaxVertexCount_12; }
	inline uint32_t* get_address_of_DefaultMaxVertexCount_12() { return &___DefaultMaxVertexCount_12; }
	inline void set_DefaultMaxVertexCount_12(uint32_t value)
	{
		___DefaultMaxVertexCount_12 = value;
	}

	inline static int32_t get_offset_of_DefaultMaxIndexCount_13() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___DefaultMaxIndexCount_13)); }
	inline uint32_t get_DefaultMaxIndexCount_13() const { return ___DefaultMaxIndexCount_13; }
	inline uint32_t* get_address_of_DefaultMaxIndexCount_13() { return &___DefaultMaxIndexCount_13; }
	inline void set_DefaultMaxIndexCount_13(uint32_t value)
	{
		___DefaultMaxIndexCount_13 = value;
	}

	inline static int32_t get_offset_of_localSpaceBounds_14() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___localSpaceBounds_14)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_localSpaceBounds_14() const { return ___localSpaceBounds_14; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_localSpaceBounds_14() { return &___localSpaceBounds_14; }
	inline void set_localSpaceBounds_14(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___localSpaceBounds_14 = value;
	}

	inline static int32_t get_offset_of_meshes_15() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___meshes_15)); }
	inline MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89* get_meshes_15() const { return ___meshes_15; }
	inline MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89** get_address_of_meshes_15() { return &___meshes_15; }
	inline void set_meshes_15(MeshU5BU5D_tDD9C723AA6F0225B35A93D871CDC2CEFF7F8CB89* value)
	{
		___meshes_15 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_15), value);
	}

	inline static int32_t get_offset_of_meshFilter_16() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___meshFilter_16)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_meshFilter_16() const { return ___meshFilter_16; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_meshFilter_16() { return &___meshFilter_16; }
	inline void set_meshFilter_16(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___meshFilter_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_16), value);
	}

	inline static int32_t get_offset_of_meshRenderer_17() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___meshRenderer_17)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_meshRenderer_17() const { return ___meshRenderer_17; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_meshRenderer_17() { return &___meshRenderer_17; }
	inline void set_meshRenderer_17(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___meshRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_17), value);
	}

	inline static int32_t get_offset_of_HVCollider_18() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___HVCollider_18)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_HVCollider_18() const { return ___HVCollider_18; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_HVCollider_18() { return &___HVCollider_18; }
	inline void set_HVCollider_18(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___HVCollider_18 = value;
		Il2CppCodeGenWriteBarrier((&___HVCollider_18), value);
	}

	inline static int32_t get_offset_of_material_19() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___material_19)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_19() const { return ___material_19; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_19() { return &___material_19; }
	inline void set_material_19(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_19 = value;
		Il2CppCodeGenWriteBarrier((&___material_19), value);
	}

	inline static int32_t get_offset_of_listener_20() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___listener_20)); }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * get_listener_20() const { return ___listener_20; }
	inline AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 ** get_address_of_listener_20() { return &___listener_20; }
	inline void set_listener_20(AudioListener_tE3E1467B84A4AFD509947B44A7C8ACFB67FF2099 * value)
	{
		___listener_20 = value;
		Il2CppCodeGenWriteBarrier((&___listener_20), value);
	}

	inline static int32_t get_offset_of_pluginInterop_21() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___pluginInterop_21)); }
	inline SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 * get_pluginInterop_21() const { return ___pluginInterop_21; }
	inline SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 ** get_address_of_pluginInterop_21() { return &___pluginInterop_21; }
	inline void set_pluginInterop_21(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8 * value)
	{
		___pluginInterop_21 = value;
		Il2CppCodeGenWriteBarrier((&___pluginInterop_21), value);
	}

	inline static int32_t get_offset_of_isInitialized_22() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___isInitialized_22)); }
	inline bool get_isInitialized_22() const { return ___isInitialized_22; }
	inline bool* get_address_of_isInitialized_22() { return &___isInitialized_22; }
	inline void set_isInitialized_22(bool value)
	{
		___isInitialized_22 = value;
	}

	inline static int32_t get_offset_of_openInfo_23() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___openInfo_23)); }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  get_openInfo_23() const { return ___openInfo_23; }
	inline SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9 * get_address_of_openInfo_23() { return &___openInfo_23; }
	inline void set_openInfo_23(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9  value)
	{
		___openInfo_23 = value;
	}

	inline static int32_t get_offset_of_fileInfo_24() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___fileInfo_24)); }
	inline SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366  get_fileInfo_24() const { return ___fileInfo_24; }
	inline SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366 * get_address_of_fileInfo_24() { return &___fileInfo_24; }
	inline void set_fileInfo_24(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366  value)
	{
		___fileInfo_24 = value;
	}

	inline static int32_t get_offset_of_lastFrameInfo_25() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___lastFrameInfo_25)); }
	inline SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E  get_lastFrameInfo_25() const { return ___lastFrameInfo_25; }
	inline SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E * get_address_of_lastFrameInfo_25() { return &___lastFrameInfo_25; }
	inline void set_lastFrameInfo_25(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E  value)
	{
		___lastFrameInfo_25 = value;
	}

	inline static int32_t get_offset_of_OnOpenNotify_26() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnOpenNotify_26)); }
	inline OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 * get_OnOpenNotify_26() const { return ___OnOpenNotify_26; }
	inline OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 ** get_address_of_OnOpenNotify_26() { return &___OnOpenNotify_26; }
	inline void set_OnOpenNotify_26(OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1 * value)
	{
		___OnOpenNotify_26 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpenNotify_26), value);
	}

	inline static int32_t get_offset_of_OnUpdateFrameInfoNotify_27() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnUpdateFrameInfoNotify_27)); }
	inline OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F * get_OnUpdateFrameInfoNotify_27() const { return ___OnUpdateFrameInfoNotify_27; }
	inline OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F ** get_address_of_OnUpdateFrameInfoNotify_27() { return &___OnUpdateFrameInfoNotify_27; }
	inline void set_OnUpdateFrameInfoNotify_27(OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F * value)
	{
		___OnUpdateFrameInfoNotify_27 = value;
		Il2CppCodeGenWriteBarrier((&___OnUpdateFrameInfoNotify_27), value);
	}

	inline static int32_t get_offset_of_OnRenderCallback_28() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnRenderCallback_28)); }
	inline OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 * get_OnRenderCallback_28() const { return ___OnRenderCallback_28; }
	inline OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 ** get_address_of_OnRenderCallback_28() { return &___OnRenderCallback_28; }
	inline void set_OnRenderCallback_28(OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6 * value)
	{
		___OnRenderCallback_28 = value;
		Il2CppCodeGenWriteBarrier((&___OnRenderCallback_28), value);
	}

	inline static int32_t get_offset_of_OnFatalError_29() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnFatalError_29)); }
	inline OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 * get_OnFatalError_29() const { return ___OnFatalError_29; }
	inline OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 ** get_address_of_OnFatalError_29() { return &___OnFatalError_29; }
	inline void set_OnFatalError_29(OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67 * value)
	{
		___OnFatalError_29 = value;
		Il2CppCodeGenWriteBarrier((&___OnFatalError_29), value);
	}

	inline static int32_t get_offset_of_OnEndOfStreamNotify_30() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___OnEndOfStreamNotify_30)); }
	inline OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 * get_OnEndOfStreamNotify_30() const { return ___OnEndOfStreamNotify_30; }
	inline OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 ** get_address_of_OnEndOfStreamNotify_30() { return &___OnEndOfStreamNotify_30; }
	inline void set_OnEndOfStreamNotify_30(OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091 * value)
	{
		___OnEndOfStreamNotify_30 = value;
		Il2CppCodeGenWriteBarrier((&___OnEndOfStreamNotify_30), value);
	}

	inline static int32_t get_offset_of_UnityBufferCoroutine_31() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___UnityBufferCoroutine_31)); }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * get_UnityBufferCoroutine_31() const { return ___UnityBufferCoroutine_31; }
	inline Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC ** get_address_of_UnityBufferCoroutine_31() { return &___UnityBufferCoroutine_31; }
	inline void set_UnityBufferCoroutine_31(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * value)
	{
		___UnityBufferCoroutine_31 = value;
		Il2CppCodeGenWriteBarrier((&___UnityBufferCoroutine_31), value);
	}

	inline static int32_t get_offset_of_logger_32() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___logger_32)); }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * get_logger_32() const { return ___logger_32; }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F ** get_address_of_logger_32() { return &___logger_32; }
	inline void set_logger_32(Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * value)
	{
		___logger_32 = value;
		Il2CppCodeGenWriteBarrier((&___logger_32), value);
	}

	inline static int32_t get_offset_of_wasPlaying_34() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___wasPlaying_34)); }
	inline bool get_wasPlaying_34() const { return ___wasPlaying_34; }
	inline bool* get_address_of_wasPlaying_34() { return &___wasPlaying_34; }
	inline void set_wasPlaying_34(bool value)
	{
		___wasPlaying_34 = value;
	}

	inline static int32_t get_offset_of_ShouldPauseAfterPlay_35() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___ShouldPauseAfterPlay_35)); }
	inline bool get_ShouldPauseAfterPlay_35() const { return ___ShouldPauseAfterPlay_35; }
	inline bool* get_address_of_ShouldPauseAfterPlay_35() { return &___ShouldPauseAfterPlay_35; }
	inline void set_ShouldPauseAfterPlay_35(bool value)
	{
		___ShouldPauseAfterPlay_35 = value;
	}

	inline static int32_t get_offset_of_PauseFrameID_36() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___PauseFrameID_36)); }
	inline uint32_t get_PauseFrameID_36() const { return ___PauseFrameID_36; }
	inline uint32_t* get_address_of_PauseFrameID_36() { return &___PauseFrameID_36; }
	inline void set_PauseFrameID_36(uint32_t value)
	{
		___PauseFrameID_36 = value;
	}

	inline static int32_t get_offset_of_isInstanceDisposed_37() { return static_cast<int32_t>(offsetof(HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB, ___isInstanceDisposed_37)); }
	inline bool get_isInstanceDisposed_37() const { return ___isInstanceDisposed_37; }
	inline bool* get_address_of_isInstanceDisposed_37() { return &___isInstanceDisposed_37; }
	inline void set_isInstanceDisposed_37(bool value)
	{
		___isInstanceDisposed_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOVIDEOOBJECT_T9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB_H
#ifndef LAYDOWNSUITCASE_T8BC596DA5382C87683030B463F10084C26325FC3_H
#define LAYDOWNSUITCASE_T8BC596DA5382C87683030B463F10084C26325FC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayDownSuitcase
struct  LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LayDownSuitcase::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject LayDownSuitcase::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject LayDownSuitcase::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean LayDownSuitcase::isTriggered
	bool ___isTriggered_7;
	// System.Boolean LayDownSuitcase::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 LayDownSuitcase::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_9;
	// UnityEngine.Vector3 LayDownSuitcase::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 LayDownSuitcase::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 LayDownSuitcase::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 LayDownSuitcase::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_13;
	// UnityEngine.Vector3 LayDownSuitcase::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 LayDownSuitcase::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 LayDownSuitcase::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 LayDownSuitcase::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_17;
	// UnityEngine.Vector3 LayDownSuitcase::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 LayDownSuitcase::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 LayDownSuitcase::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// System.Int32 LayDownSuitcase::thisNR
	int32_t ___thisNR_21;
	// System.Int32 LayDownSuitcase::frontNR
	int32_t ___frontNR_22;
	// System.Int32 LayDownSuitcase::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 LayDownSuitcase::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 LayDownSuitcase::thisRCount
	int32_t ___thisRCount_25;
	// System.Int32 LayDownSuitcase::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 LayDownSuitcase::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 LayDownSuitcase::mainPocketRCount
	int32_t ___mainPocketRCount_28;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_9() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___thisInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_9() const { return ___thisInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_9() { return &___thisInitV3_9; }
	inline void set_thisInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_13() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___thisTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_13() const { return ___thisTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_13() { return &___thisTargetV3_13; }
	inline void set_thisTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_17() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___thisRotationV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_17() const { return ___thisRotationV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_17() { return &___thisRotationV3_17; }
	inline void set_thisRotationV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_thisNR_21() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___thisNR_21)); }
	inline int32_t get_thisNR_21() const { return ___thisNR_21; }
	inline int32_t* get_address_of_thisNR_21() { return &___thisNR_21; }
	inline void set_thisNR_21(int32_t value)
	{
		___thisNR_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_thisRCount_25() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___thisRCount_25)); }
	inline int32_t get_thisRCount_25() const { return ___thisRCount_25; }
	inline int32_t* get_address_of_thisRCount_25() { return &___thisRCount_25; }
	inline void set_thisRCount_25(int32_t value)
	{
		___thisRCount_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYDOWNSUITCASE_T8BC596DA5382C87683030B463F10084C26325FC3_H
#ifndef LAYDOWNSUITCASEHALFTOFULL_TA28DED4A10A6A24B34806D101CFE80DA584AF482_H
#define LAYDOWNSUITCASEHALFTOFULL_TA28DED4A10A6A24B34806D101CFE80DA584AF482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayDownSuitcaseHalfToFull
struct  LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LayDownSuitcaseHalfToFull::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject LayDownSuitcaseHalfToFull::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject LayDownSuitcaseHalfToFull::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean LayDownSuitcaseHalfToFull::isTriggered
	bool ___isTriggered_7;
	// System.Boolean LayDownSuitcaseHalfToFull::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_9;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_13;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_17;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFull::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// System.Int32 LayDownSuitcaseHalfToFull::thisNR
	int32_t ___thisNR_21;
	// System.Int32 LayDownSuitcaseHalfToFull::frontNR
	int32_t ___frontNR_22;
	// System.Int32 LayDownSuitcaseHalfToFull::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 LayDownSuitcaseHalfToFull::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 LayDownSuitcaseHalfToFull::thisRCount
	int32_t ___thisRCount_25;
	// System.Int32 LayDownSuitcaseHalfToFull::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 LayDownSuitcaseHalfToFull::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 LayDownSuitcaseHalfToFull::mainPocketRCount
	int32_t ___mainPocketRCount_28;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_9() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___thisInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_9() const { return ___thisInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_9() { return &___thisInitV3_9; }
	inline void set_thisInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_13() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___thisTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_13() const { return ___thisTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_13() { return &___thisTargetV3_13; }
	inline void set_thisTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_17() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___thisRotationV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_17() const { return ___thisRotationV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_17() { return &___thisRotationV3_17; }
	inline void set_thisRotationV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_thisNR_21() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___thisNR_21)); }
	inline int32_t get_thisNR_21() const { return ___thisNR_21; }
	inline int32_t* get_address_of_thisNR_21() { return &___thisNR_21; }
	inline void set_thisNR_21(int32_t value)
	{
		___thisNR_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_thisRCount_25() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___thisRCount_25)); }
	inline int32_t get_thisRCount_25() const { return ___thisRCount_25; }
	inline int32_t* get_address_of_thisRCount_25() { return &___thisRCount_25; }
	inline void set_thisRCount_25(int32_t value)
	{
		___thisRCount_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYDOWNSUITCASEHALFTOFULL_TA28DED4A10A6A24B34806D101CFE80DA584AF482_H
#ifndef LAYDOWNSUITCASEHALFTOFULLREVERSE_T3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09_H
#define LAYDOWNSUITCASEHALFTOFULLREVERSE_T3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayDownSuitcaseHalfToFullReverse
struct  LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LayDownSuitcaseHalfToFullReverse::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject LayDownSuitcaseHalfToFullReverse::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject LayDownSuitcaseHalfToFullReverse::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean LayDownSuitcaseHalfToFullReverse::isTriggered
	bool ___isTriggered_7;
	// System.Boolean LayDownSuitcaseHalfToFullReverse::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_9;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_13;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_17;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 LayDownSuitcaseHalfToFullReverse::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::thisNR
	int32_t ___thisNR_21;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::frontNR
	int32_t ___frontNR_22;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::thisRCount
	int32_t ___thisRCount_25;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 LayDownSuitcaseHalfToFullReverse::mainPocketRCount
	int32_t ___mainPocketRCount_28;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_9() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___thisInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_9() const { return ___thisInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_9() { return &___thisInitV3_9; }
	inline void set_thisInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_13() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___thisTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_13() const { return ___thisTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_13() { return &___thisTargetV3_13; }
	inline void set_thisTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_17() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___thisRotationV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_17() const { return ___thisRotationV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_17() { return &___thisRotationV3_17; }
	inline void set_thisRotationV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_thisNR_21() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___thisNR_21)); }
	inline int32_t get_thisNR_21() const { return ___thisNR_21; }
	inline int32_t* get_address_of_thisNR_21() { return &___thisNR_21; }
	inline void set_thisNR_21(int32_t value)
	{
		___thisNR_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_thisRCount_25() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___thisRCount_25)); }
	inline int32_t get_thisRCount_25() const { return ___thisRCount_25; }
	inline int32_t* get_address_of_thisRCount_25() { return &___thisRCount_25; }
	inline void set_thisRCount_25(int32_t value)
	{
		___thisRCount_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYDOWNSUITCASEHALFTOFULLREVERSE_T3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09_H
#ifndef LAYDOWNSUITCASEOPENFULL_T03B90B6F984897684EB0C39DEEAEAE83C061DA2D_H
#define LAYDOWNSUITCASEOPENFULL_T03B90B6F984897684EB0C39DEEAEAE83C061DA2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayDownSuitcaseOpenFull
struct  LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LayDownSuitcaseOpenFull::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject LayDownSuitcaseOpenFull::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject LayDownSuitcaseOpenFull::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean LayDownSuitcaseOpenFull::isTriggered
	bool ___isTriggered_7;
	// System.Boolean LayDownSuitcaseOpenFull::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_9;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_13;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_17;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFull::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// System.Int32 LayDownSuitcaseOpenFull::thisNR
	int32_t ___thisNR_21;
	// System.Int32 LayDownSuitcaseOpenFull::frontNR
	int32_t ___frontNR_22;
	// System.Int32 LayDownSuitcaseOpenFull::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 LayDownSuitcaseOpenFull::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 LayDownSuitcaseOpenFull::thisRCount
	int32_t ___thisRCount_25;
	// System.Int32 LayDownSuitcaseOpenFull::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 LayDownSuitcaseOpenFull::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 LayDownSuitcaseOpenFull::mainPocketRCount
	int32_t ___mainPocketRCount_28;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_9() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___thisInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_9() const { return ___thisInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_9() { return &___thisInitV3_9; }
	inline void set_thisInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_13() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___thisTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_13() const { return ___thisTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_13() { return &___thisTargetV3_13; }
	inline void set_thisTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_17() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___thisRotationV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_17() const { return ___thisRotationV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_17() { return &___thisRotationV3_17; }
	inline void set_thisRotationV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_thisNR_21() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___thisNR_21)); }
	inline int32_t get_thisNR_21() const { return ___thisNR_21; }
	inline int32_t* get_address_of_thisNR_21() { return &___thisNR_21; }
	inline void set_thisNR_21(int32_t value)
	{
		___thisNR_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_thisRCount_25() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___thisRCount_25)); }
	inline int32_t get_thisRCount_25() const { return ___thisRCount_25; }
	inline int32_t* get_address_of_thisRCount_25() { return &___thisRCount_25; }
	inline void set_thisRCount_25(int32_t value)
	{
		___thisRCount_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYDOWNSUITCASEOPENFULL_T03B90B6F984897684EB0C39DEEAEAE83C061DA2D_H
#ifndef LAYDOWNSUITCASEOPENFULLREVERSE_TAE474AB5DB6F2AC120F8D0863816B549DD4D30C2_H
#define LAYDOWNSUITCASEOPENFULLREVERSE_TAE474AB5DB6F2AC120F8D0863816B549DD4D30C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayDownSuitcaseOpenFullReverse
struct  LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LayDownSuitcaseOpenFullReverse::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject LayDownSuitcaseOpenFullReverse::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject LayDownSuitcaseOpenFullReverse::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean LayDownSuitcaseOpenFullReverse::isTriggered
	bool ___isTriggered_7;
	// System.Boolean LayDownSuitcaseOpenFullReverse::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_9;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_13;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_17;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 LayDownSuitcaseOpenFullReverse::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// System.Int32 LayDownSuitcaseOpenFullReverse::thisNR
	int32_t ___thisNR_21;
	// System.Int32 LayDownSuitcaseOpenFullReverse::frontNR
	int32_t ___frontNR_22;
	// System.Int32 LayDownSuitcaseOpenFullReverse::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 LayDownSuitcaseOpenFullReverse::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 LayDownSuitcaseOpenFullReverse::thisRCount
	int32_t ___thisRCount_25;
	// System.Int32 LayDownSuitcaseOpenFullReverse::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 LayDownSuitcaseOpenFullReverse::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 LayDownSuitcaseOpenFullReverse::mainPocketRCount
	int32_t ___mainPocketRCount_28;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_9() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___thisInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_9() const { return ___thisInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_9() { return &___thisInitV3_9; }
	inline void set_thisInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_13() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___thisTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_13() const { return ___thisTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_13() { return &___thisTargetV3_13; }
	inline void set_thisTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_17() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___thisRotationV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_17() const { return ___thisRotationV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_17() { return &___thisRotationV3_17; }
	inline void set_thisRotationV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_thisNR_21() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___thisNR_21)); }
	inline int32_t get_thisNR_21() const { return ___thisNR_21; }
	inline int32_t* get_address_of_thisNR_21() { return &___thisNR_21; }
	inline void set_thisNR_21(int32_t value)
	{
		___thisNR_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_thisRCount_25() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___thisRCount_25)); }
	inline int32_t get_thisRCount_25() const { return ___thisRCount_25; }
	inline int32_t* get_address_of_thisRCount_25() { return &___thisRCount_25; }
	inline void set_thisRCount_25(int32_t value)
	{
		___thisRCount_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYDOWNSUITCASEOPENFULLREVERSE_TAE474AB5DB6F2AC120F8D0863816B549DD4D30C2_H
#ifndef LAYDOWNSUITCASEREVERSE_T86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72_H
#define LAYDOWNSUITCASEREVERSE_T86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayDownSuitcaseReverse
struct  LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject LayDownSuitcaseReverse::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject LayDownSuitcaseReverse::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject LayDownSuitcaseReverse::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean LayDownSuitcaseReverse::isTriggered
	bool ___isTriggered_7;
	// System.Boolean LayDownSuitcaseReverse::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_9;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_10;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_11;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_12;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_13;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_14;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_15;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_16;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_17;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_18;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_19;
	// UnityEngine.Vector3 LayDownSuitcaseReverse::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_20;
	// System.Int32 LayDownSuitcaseReverse::thisNR
	int32_t ___thisNR_21;
	// System.Int32 LayDownSuitcaseReverse::frontNR
	int32_t ___frontNR_22;
	// System.Int32 LayDownSuitcaseReverse::bottomNR
	int32_t ___bottomNR_23;
	// System.Int32 LayDownSuitcaseReverse::mainPocketNR
	int32_t ___mainPocketNR_24;
	// System.Int32 LayDownSuitcaseReverse::thisRCount
	int32_t ___thisRCount_25;
	// System.Int32 LayDownSuitcaseReverse::frontRCount
	int32_t ___frontRCount_26;
	// System.Int32 LayDownSuitcaseReverse::bottomRCount
	int32_t ___bottomRCount_27;
	// System.Int32 LayDownSuitcaseReverse::mainPocketRCount
	int32_t ___mainPocketRCount_28;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_9() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___thisInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_9() const { return ___thisInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_9() { return &___thisInitV3_9; }
	inline void set_thisInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_9 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_10() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___frontInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_10() const { return ___frontInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_10() { return &___frontInitV3_10; }
	inline void set_frontInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_10 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_11() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___bottomInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_11() const { return ___bottomInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_11() { return &___bottomInitV3_11; }
	inline void set_bottomInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_11 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_12() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___mainPocketInitV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_12() const { return ___mainPocketInitV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_12() { return &___mainPocketInitV3_12; }
	inline void set_mainPocketInitV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_12 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_13() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___thisTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_13() const { return ___thisTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_13() { return &___thisTargetV3_13; }
	inline void set_thisTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_14() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___frontTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_14() const { return ___frontTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_14() { return &___frontTargetV3_14; }
	inline void set_frontTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_15() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___bottomTargetV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_15() const { return ___bottomTargetV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_15() { return &___bottomTargetV3_15; }
	inline void set_bottomTargetV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_15 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_16() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___mainPocketTargetV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_16() const { return ___mainPocketTargetV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_16() { return &___mainPocketTargetV3_16; }
	inline void set_mainPocketTargetV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_16 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_17() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___thisRotationV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_17() const { return ___thisRotationV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_17() { return &___thisRotationV3_17; }
	inline void set_thisRotationV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_17 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_18() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___frontRotationV3_18)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_18() const { return ___frontRotationV3_18; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_18() { return &___frontRotationV3_18; }
	inline void set_frontRotationV3_18(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_18 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_19() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___bottomRotationV3_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_19() const { return ___bottomRotationV3_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_19() { return &___bottomRotationV3_19; }
	inline void set_bottomRotationV3_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_20() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___mainPocketRotationtV3_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_20() const { return ___mainPocketRotationtV3_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_20() { return &___mainPocketRotationtV3_20; }
	inline void set_mainPocketRotationtV3_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_20 = value;
	}

	inline static int32_t get_offset_of_thisNR_21() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___thisNR_21)); }
	inline int32_t get_thisNR_21() const { return ___thisNR_21; }
	inline int32_t* get_address_of_thisNR_21() { return &___thisNR_21; }
	inline void set_thisNR_21(int32_t value)
	{
		___thisNR_21 = value;
	}

	inline static int32_t get_offset_of_frontNR_22() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___frontNR_22)); }
	inline int32_t get_frontNR_22() const { return ___frontNR_22; }
	inline int32_t* get_address_of_frontNR_22() { return &___frontNR_22; }
	inline void set_frontNR_22(int32_t value)
	{
		___frontNR_22 = value;
	}

	inline static int32_t get_offset_of_bottomNR_23() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___bottomNR_23)); }
	inline int32_t get_bottomNR_23() const { return ___bottomNR_23; }
	inline int32_t* get_address_of_bottomNR_23() { return &___bottomNR_23; }
	inline void set_bottomNR_23(int32_t value)
	{
		___bottomNR_23 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_24() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___mainPocketNR_24)); }
	inline int32_t get_mainPocketNR_24() const { return ___mainPocketNR_24; }
	inline int32_t* get_address_of_mainPocketNR_24() { return &___mainPocketNR_24; }
	inline void set_mainPocketNR_24(int32_t value)
	{
		___mainPocketNR_24 = value;
	}

	inline static int32_t get_offset_of_thisRCount_25() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___thisRCount_25)); }
	inline int32_t get_thisRCount_25() const { return ___thisRCount_25; }
	inline int32_t* get_address_of_thisRCount_25() { return &___thisRCount_25; }
	inline void set_thisRCount_25(int32_t value)
	{
		___thisRCount_25 = value;
	}

	inline static int32_t get_offset_of_frontRCount_26() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___frontRCount_26)); }
	inline int32_t get_frontRCount_26() const { return ___frontRCount_26; }
	inline int32_t* get_address_of_frontRCount_26() { return &___frontRCount_26; }
	inline void set_frontRCount_26(int32_t value)
	{
		___frontRCount_26 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_27() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___bottomRCount_27)); }
	inline int32_t get_bottomRCount_27() const { return ___bottomRCount_27; }
	inline int32_t* get_address_of_bottomRCount_27() { return &___bottomRCount_27; }
	inline void set_bottomRCount_27(int32_t value)
	{
		___bottomRCount_27 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_28() { return static_cast<int32_t>(offsetof(LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72, ___mainPocketRCount_28)); }
	inline int32_t get_mainPocketRCount_28() const { return ___mainPocketRCount_28; }
	inline int32_t* get_address_of_mainPocketRCount_28() { return &___mainPocketRCount_28; }
	inline void set_mainPocketRCount_28(int32_t value)
	{
		___mainPocketRCount_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYDOWNSUITCASEREVERSE_T86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72_H
#ifndef LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#define LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightEstimation
struct  LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Nullable`1<System.Single> LightEstimation::<brightness>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CbrightnessU3Ek__BackingField_4;
	// System.Nullable`1<System.Single> LightEstimation::<colorTemperature>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CcolorTemperatureU3Ek__BackingField_5;
	// System.Nullable`1<UnityEngine.Color> LightEstimation::<colorCorrection>k__BackingField
	Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  ___U3CcolorCorrectionU3Ek__BackingField_6;
	// UnityEngine.Light LightEstimation::m_Light
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___m_Light_7;

public:
	inline static int32_t get_offset_of_U3CbrightnessU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CbrightnessU3Ek__BackingField_4)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CbrightnessU3Ek__BackingField_4() const { return ___U3CbrightnessU3Ek__BackingField_4; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CbrightnessU3Ek__BackingField_4() { return &___U3CbrightnessU3Ek__BackingField_4; }
	inline void set_U3CbrightnessU3Ek__BackingField_4(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CbrightnessU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CcolorTemperatureU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CcolorTemperatureU3Ek__BackingField_5)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CcolorTemperatureU3Ek__BackingField_5() const { return ___U3CcolorTemperatureU3Ek__BackingField_5; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CcolorTemperatureU3Ek__BackingField_5() { return &___U3CcolorTemperatureU3Ek__BackingField_5; }
	inline void set_U3CcolorTemperatureU3Ek__BackingField_5(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CcolorTemperatureU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CcolorCorrectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___U3CcolorCorrectionU3Ek__BackingField_6)); }
	inline Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  get_U3CcolorCorrectionU3Ek__BackingField_6() const { return ___U3CcolorCorrectionU3Ek__BackingField_6; }
	inline Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77 * get_address_of_U3CcolorCorrectionU3Ek__BackingField_6() { return &___U3CcolorCorrectionU3Ek__BackingField_6; }
	inline void set_U3CcolorCorrectionU3Ek__BackingField_6(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  value)
	{
		___U3CcolorCorrectionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_m_Light_7() { return static_cast<int32_t>(offsetof(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319, ___m_Light_7)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_m_Light_7() const { return ___m_Light_7; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_m_Light_7() { return &___m_Light_7; }
	inline void set_m_Light_7(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___m_Light_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Light_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATION_TAC157B297F822653381F8F35A6DE3600920A4319_H
#ifndef LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#define LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightEstimationUI
struct  LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text LightEstimationUI::m_BrightnessText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_BrightnessText_4;
	// UnityEngine.UI.Text LightEstimationUI::m_ColorTemperatureText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ColorTemperatureText_5;
	// UnityEngine.UI.Text LightEstimationUI::m_ColorCorrectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ColorCorrectionText_6;
	// LightEstimation LightEstimationUI::m_LightEstimation
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * ___m_LightEstimation_8;

public:
	inline static int32_t get_offset_of_m_BrightnessText_4() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_BrightnessText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_BrightnessText_4() const { return ___m_BrightnessText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_BrightnessText_4() { return &___m_BrightnessText_4; }
	inline void set_m_BrightnessText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_BrightnessText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BrightnessText_4), value);
	}

	inline static int32_t get_offset_of_m_ColorTemperatureText_5() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_ColorTemperatureText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ColorTemperatureText_5() const { return ___m_ColorTemperatureText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ColorTemperatureText_5() { return &___m_ColorTemperatureText_5; }
	inline void set_m_ColorTemperatureText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ColorTemperatureText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTemperatureText_5), value);
	}

	inline static int32_t get_offset_of_m_ColorCorrectionText_6() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_ColorCorrectionText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ColorCorrectionText_6() const { return ___m_ColorCorrectionText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ColorCorrectionText_6() { return &___m_ColorCorrectionText_6; }
	inline void set_m_ColorCorrectionText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ColorCorrectionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorCorrectionText_6), value);
	}

	inline static int32_t get_offset_of_m_LightEstimation_8() { return static_cast<int32_t>(offsetof(LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4, ___m_LightEstimation_8)); }
	inline LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * get_m_LightEstimation_8() const { return ___m_LightEstimation_8; }
	inline LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 ** get_address_of_m_LightEstimation_8() { return &___m_LightEstimation_8; }
	inline void set_m_LightEstimation_8(LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319 * value)
	{
		___m_LightEstimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LightEstimation_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTESTIMATIONUI_TABE99F2D6E580AB4AD14174CE323C527882D6DA4_H
#ifndef MAKEAPPEARONPLANE_TEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_H
#define MAKEAPPEARONPLANE_TEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MakeAppearOnPlane
struct  MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform MakeAppearOnPlane::m_Content
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Content_4;
	// UnityEngine.Quaternion MakeAppearOnPlane::m_Rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_Rotation_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin MakeAppearOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_Content_4() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3, ___m_Content_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Content_4() const { return ___m_Content_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Content_4() { return &___m_Content_4; }
	inline void set_m_Content_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Content_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_4), value);
	}

	inline static int32_t get_offset_of_m_Rotation_5() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3, ___m_Rotation_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_Rotation_5() const { return ___m_Rotation_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_Rotation_5() { return &___m_Rotation_5; }
	inline void set_m_Rotation_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_Rotation_5 = value;
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> MakeAppearOnPlane::s_Hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___s_Hits_6;

public:
	inline static int32_t get_offset_of_s_Hits_6() { return static_cast<int32_t>(offsetof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields, ___s_Hits_6)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_s_Hits_6() const { return ___s_Hits_6; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_s_Hits_6() { return &___s_Hits_6; }
	inline void set_s_Hits_6(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___s_Hits_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAKEAPPEARONPLANE_TEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_H
#ifndef NATIVETOOLKIT_TE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_H
#define NATIVETOOLKIT_TE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkit
struct  NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields
{
public:
	// System.Action`1<UnityEngine.Texture2D> NativeToolkit::OnScreenshotTaken
	Action_1_tFB39B350582053B206393ED428938B171A469EE0 * ___OnScreenshotTaken_4;
	// System.Action`1<System.String> NativeToolkit::OnScreenshotSaved
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnScreenshotSaved_5;
	// System.Action`1<System.String> NativeToolkit::OnImageSaved
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnImageSaved_6;
	// System.Action`2<UnityEngine.Texture2D,System.String> NativeToolkit::OnImagePicked
	Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA * ___OnImagePicked_7;
	// System.Action`1<System.Boolean> NativeToolkit::OnDialogComplete
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___OnDialogComplete_8;
	// System.Action`1<System.String> NativeToolkit::OnRateComplete
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___OnRateComplete_9;
	// System.Action`2<UnityEngine.Texture2D,System.String> NativeToolkit::OnCameraShotComplete
	Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA * ___OnCameraShotComplete_10;
	// System.Action`3<System.String,System.String,System.String> NativeToolkit::OnContactPicked
	Action_3_tC0B0D671468ADEEF578E3759B5CAB8605D10D798 * ___OnContactPicked_11;
	// NativeToolkit NativeToolkit::instance
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * ___instance_12;
	// UnityEngine.GameObject NativeToolkit::go
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_13;

public:
	inline static int32_t get_offset_of_OnScreenshotTaken_4() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnScreenshotTaken_4)); }
	inline Action_1_tFB39B350582053B206393ED428938B171A469EE0 * get_OnScreenshotTaken_4() const { return ___OnScreenshotTaken_4; }
	inline Action_1_tFB39B350582053B206393ED428938B171A469EE0 ** get_address_of_OnScreenshotTaken_4() { return &___OnScreenshotTaken_4; }
	inline void set_OnScreenshotTaken_4(Action_1_tFB39B350582053B206393ED428938B171A469EE0 * value)
	{
		___OnScreenshotTaken_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnScreenshotTaken_4), value);
	}

	inline static int32_t get_offset_of_OnScreenshotSaved_5() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnScreenshotSaved_5)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnScreenshotSaved_5() const { return ___OnScreenshotSaved_5; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnScreenshotSaved_5() { return &___OnScreenshotSaved_5; }
	inline void set_OnScreenshotSaved_5(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnScreenshotSaved_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnScreenshotSaved_5), value);
	}

	inline static int32_t get_offset_of_OnImageSaved_6() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnImageSaved_6)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnImageSaved_6() const { return ___OnImageSaved_6; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnImageSaved_6() { return &___OnImageSaved_6; }
	inline void set_OnImageSaved_6(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnImageSaved_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnImageSaved_6), value);
	}

	inline static int32_t get_offset_of_OnImagePicked_7() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnImagePicked_7)); }
	inline Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA * get_OnImagePicked_7() const { return ___OnImagePicked_7; }
	inline Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA ** get_address_of_OnImagePicked_7() { return &___OnImagePicked_7; }
	inline void set_OnImagePicked_7(Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA * value)
	{
		___OnImagePicked_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnImagePicked_7), value);
	}

	inline static int32_t get_offset_of_OnDialogComplete_8() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnDialogComplete_8)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_OnDialogComplete_8() const { return ___OnDialogComplete_8; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_OnDialogComplete_8() { return &___OnDialogComplete_8; }
	inline void set_OnDialogComplete_8(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___OnDialogComplete_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnDialogComplete_8), value);
	}

	inline static int32_t get_offset_of_OnRateComplete_9() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnRateComplete_9)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_OnRateComplete_9() const { return ___OnRateComplete_9; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_OnRateComplete_9() { return &___OnRateComplete_9; }
	inline void set_OnRateComplete_9(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___OnRateComplete_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnRateComplete_9), value);
	}

	inline static int32_t get_offset_of_OnCameraShotComplete_10() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnCameraShotComplete_10)); }
	inline Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA * get_OnCameraShotComplete_10() const { return ___OnCameraShotComplete_10; }
	inline Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA ** get_address_of_OnCameraShotComplete_10() { return &___OnCameraShotComplete_10; }
	inline void set_OnCameraShotComplete_10(Action_2_t38D9CB4CED3E752D7C0FBF173D149F5B249540BA * value)
	{
		___OnCameraShotComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnCameraShotComplete_10), value);
	}

	inline static int32_t get_offset_of_OnContactPicked_11() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___OnContactPicked_11)); }
	inline Action_3_tC0B0D671468ADEEF578E3759B5CAB8605D10D798 * get_OnContactPicked_11() const { return ___OnContactPicked_11; }
	inline Action_3_tC0B0D671468ADEEF578E3759B5CAB8605D10D798 ** get_address_of_OnContactPicked_11() { return &___OnContactPicked_11; }
	inline void set_OnContactPicked_11(Action_3_tC0B0D671468ADEEF578E3759B5CAB8605D10D798 * value)
	{
		___OnContactPicked_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnContactPicked_11), value);
	}

	inline static int32_t get_offset_of_instance_12() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___instance_12)); }
	inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * get_instance_12() const { return ___instance_12; }
	inline NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F ** get_address_of_instance_12() { return &___instance_12; }
	inline void set_instance_12(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F * value)
	{
		___instance_12 = value;
		Il2CppCodeGenWriteBarrier((&___instance_12), value);
	}

	inline static int32_t get_offset_of_go_13() { return static_cast<int32_t>(offsetof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields, ___go_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_13() const { return ___go_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_13() { return &___go_13; }
	inline void set_go_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_13 = value;
		Il2CppCodeGenWriteBarrier((&___go_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVETOOLKIT_TE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_H
#ifndef NATIVETOOLKITEXAMPLE_T4708B6A63253E5252BDCDF33B96A233837E02F90_H
#define NATIVETOOLKITEXAMPLE_T4708B6A63253E5252BDCDF33B96A233837E02F90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NativeToolkitExample
struct  NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text NativeToolkitExample::console
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___console_4;
	// UnityEngine.Texture2D NativeToolkitExample::texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture_5;
	// System.String NativeToolkitExample::imagePath
	String_t* ___imagePath_6;

public:
	inline static int32_t get_offset_of_console_4() { return static_cast<int32_t>(offsetof(NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90, ___console_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_console_4() const { return ___console_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_console_4() { return &___console_4; }
	inline void set_console_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___console_4 = value;
		Il2CppCodeGenWriteBarrier((&___console_4), value);
	}

	inline static int32_t get_offset_of_texture_5() { return static_cast<int32_t>(offsetof(NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90, ___texture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_texture_5() const { return ___texture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_texture_5() { return &___texture_5; }
	inline void set_texture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___texture_5 = value;
		Il2CppCodeGenWriteBarrier((&___texture_5), value);
	}

	inline static int32_t get_offset_of_imagePath_6() { return static_cast<int32_t>(offsetof(NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90, ___imagePath_6)); }
	inline String_t* get_imagePath_6() const { return ___imagePath_6; }
	inline String_t** get_address_of_imagePath_6() { return &___imagePath_6; }
	inline void set_imagePath_6(String_t* value)
	{
		___imagePath_6 = value;
		Il2CppCodeGenWriteBarrier((&___imagePath_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVETOOLKITEXAMPLE_T4708B6A63253E5252BDCDF33B96A233837E02F90_H
#ifndef OPENSUITCASE_TBA942EA8EE32F4F769CE5226982507C2D0CE09CC_H
#define OPENSUITCASE_TBA942EA8EE32F4F769CE5226982507C2D0CE09CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenSuitcase
struct  OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject OpenSuitcase::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject OpenSuitcase::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject OpenSuitcase::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean OpenSuitcase::isTriggered
	bool ___isTriggered_7;
	// System.Boolean OpenSuitcase::isTriggering
	bool ___isTriggering_8;
	// UnityEngine.Vector3 OpenSuitcase::frontInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontInitV3_9;
	// UnityEngine.Vector3 OpenSuitcase::bottomInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomInitV3_10;
	// UnityEngine.Vector3 OpenSuitcase::mainPocketInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketInitV3_11;
	// UnityEngine.Vector3 OpenSuitcase::frontTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontTargetV3_12;
	// UnityEngine.Vector3 OpenSuitcase::bottomTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomTargetV3_13;
	// UnityEngine.Vector3 OpenSuitcase::mainPocketTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketTargetV3_14;
	// UnityEngine.Vector3 OpenSuitcase::frontRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___frontRotationV3_15;
	// UnityEngine.Vector3 OpenSuitcase::bottomRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRotationV3_16;
	// UnityEngine.Vector3 OpenSuitcase::mainPocketRotationtV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mainPocketRotationtV3_17;
	// System.Int32 OpenSuitcase::frontNR
	int32_t ___frontNR_18;
	// System.Int32 OpenSuitcase::bottomNR
	int32_t ___bottomNR_19;
	// System.Int32 OpenSuitcase::mainPocketNR
	int32_t ___mainPocketNR_20;
	// System.Int32 OpenSuitcase::frontRCount
	int32_t ___frontRCount_21;
	// System.Int32 OpenSuitcase::bottomRCount
	int32_t ___bottomRCount_22;
	// System.Int32 OpenSuitcase::mainPocketRCount
	int32_t ___mainPocketRCount_23;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}

	inline static int32_t get_offset_of_isTriggering_8() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___isTriggering_8)); }
	inline bool get_isTriggering_8() const { return ___isTriggering_8; }
	inline bool* get_address_of_isTriggering_8() { return &___isTriggering_8; }
	inline void set_isTriggering_8(bool value)
	{
		___isTriggering_8 = value;
	}

	inline static int32_t get_offset_of_frontInitV3_9() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___frontInitV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontInitV3_9() const { return ___frontInitV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontInitV3_9() { return &___frontInitV3_9; }
	inline void set_frontInitV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontInitV3_9 = value;
	}

	inline static int32_t get_offset_of_bottomInitV3_10() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___bottomInitV3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomInitV3_10() const { return ___bottomInitV3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomInitV3_10() { return &___bottomInitV3_10; }
	inline void set_bottomInitV3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomInitV3_10 = value;
	}

	inline static int32_t get_offset_of_mainPocketInitV3_11() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___mainPocketInitV3_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketInitV3_11() const { return ___mainPocketInitV3_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketInitV3_11() { return &___mainPocketInitV3_11; }
	inline void set_mainPocketInitV3_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketInitV3_11 = value;
	}

	inline static int32_t get_offset_of_frontTargetV3_12() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___frontTargetV3_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontTargetV3_12() const { return ___frontTargetV3_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontTargetV3_12() { return &___frontTargetV3_12; }
	inline void set_frontTargetV3_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontTargetV3_12 = value;
	}

	inline static int32_t get_offset_of_bottomTargetV3_13() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___bottomTargetV3_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomTargetV3_13() const { return ___bottomTargetV3_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomTargetV3_13() { return &___bottomTargetV3_13; }
	inline void set_bottomTargetV3_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomTargetV3_13 = value;
	}

	inline static int32_t get_offset_of_mainPocketTargetV3_14() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___mainPocketTargetV3_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketTargetV3_14() const { return ___mainPocketTargetV3_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketTargetV3_14() { return &___mainPocketTargetV3_14; }
	inline void set_mainPocketTargetV3_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketTargetV3_14 = value;
	}

	inline static int32_t get_offset_of_frontRotationV3_15() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___frontRotationV3_15)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_frontRotationV3_15() const { return ___frontRotationV3_15; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_frontRotationV3_15() { return &___frontRotationV3_15; }
	inline void set_frontRotationV3_15(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___frontRotationV3_15 = value;
	}

	inline static int32_t get_offset_of_bottomRotationV3_16() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___bottomRotationV3_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRotationV3_16() const { return ___bottomRotationV3_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRotationV3_16() { return &___bottomRotationV3_16; }
	inline void set_bottomRotationV3_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRotationV3_16 = value;
	}

	inline static int32_t get_offset_of_mainPocketRotationtV3_17() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___mainPocketRotationtV3_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mainPocketRotationtV3_17() const { return ___mainPocketRotationtV3_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mainPocketRotationtV3_17() { return &___mainPocketRotationtV3_17; }
	inline void set_mainPocketRotationtV3_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mainPocketRotationtV3_17 = value;
	}

	inline static int32_t get_offset_of_frontNR_18() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___frontNR_18)); }
	inline int32_t get_frontNR_18() const { return ___frontNR_18; }
	inline int32_t* get_address_of_frontNR_18() { return &___frontNR_18; }
	inline void set_frontNR_18(int32_t value)
	{
		___frontNR_18 = value;
	}

	inline static int32_t get_offset_of_bottomNR_19() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___bottomNR_19)); }
	inline int32_t get_bottomNR_19() const { return ___bottomNR_19; }
	inline int32_t* get_address_of_bottomNR_19() { return &___bottomNR_19; }
	inline void set_bottomNR_19(int32_t value)
	{
		___bottomNR_19 = value;
	}

	inline static int32_t get_offset_of_mainPocketNR_20() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___mainPocketNR_20)); }
	inline int32_t get_mainPocketNR_20() const { return ___mainPocketNR_20; }
	inline int32_t* get_address_of_mainPocketNR_20() { return &___mainPocketNR_20; }
	inline void set_mainPocketNR_20(int32_t value)
	{
		___mainPocketNR_20 = value;
	}

	inline static int32_t get_offset_of_frontRCount_21() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___frontRCount_21)); }
	inline int32_t get_frontRCount_21() const { return ___frontRCount_21; }
	inline int32_t* get_address_of_frontRCount_21() { return &___frontRCount_21; }
	inline void set_frontRCount_21(int32_t value)
	{
		___frontRCount_21 = value;
	}

	inline static int32_t get_offset_of_bottomRCount_22() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___bottomRCount_22)); }
	inline int32_t get_bottomRCount_22() const { return ___bottomRCount_22; }
	inline int32_t* get_address_of_bottomRCount_22() { return &___bottomRCount_22; }
	inline void set_bottomRCount_22(int32_t value)
	{
		___bottomRCount_22 = value;
	}

	inline static int32_t get_offset_of_mainPocketRCount_23() { return static_cast<int32_t>(offsetof(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC, ___mainPocketRCount_23)); }
	inline int32_t get_mainPocketRCount_23() const { return ___mainPocketRCount_23; }
	inline int32_t* get_address_of_mainPocketRCount_23() { return &___mainPocketRCount_23; }
	inline void set_mainPocketRCount_23(int32_t value)
	{
		___mainPocketRCount_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENSUITCASE_TBA942EA8EE32F4F769CE5226982507C2D0CE09CC_H
#ifndef PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#define PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceMultipleObjectsOnPlane
struct  PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaceMultipleObjectsOnPlane::m_PlacedPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlacedPrefab_4;
	// UnityEngine.GameObject PlaceMultipleObjectsOnPlane::<spawnedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CspawnedObjectU3Ek__BackingField_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin PlaceMultipleObjectsOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_PlacedPrefab_4() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___m_PlacedPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlacedPrefab_4() const { return ___m_PlacedPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlacedPrefab_4() { return &___m_PlacedPrefab_4; }
	inline void set_m_PlacedPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlacedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacedPrefab_4), value);
	}

	inline static int32_t get_offset_of_U3CspawnedObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___U3CspawnedObjectU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CspawnedObjectU3Ek__BackingField_5() const { return ___U3CspawnedObjectU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CspawnedObjectU3Ek__BackingField_5() { return &___U3CspawnedObjectU3Ek__BackingField_5; }
	inline void set_U3CspawnedObjectU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CspawnedObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnedObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields
{
public:
	// System.Action PlaceMultipleObjectsOnPlane::onPlacedObject
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___onPlacedObject_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlaceMultipleObjectsOnPlane::s_Hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___s_Hits_8;

public:
	inline static int32_t get_offset_of_onPlacedObject_6() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields, ___onPlacedObject_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_onPlacedObject_6() const { return ___onPlacedObject_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_onPlacedObject_6() { return &___onPlacedObject_6; }
	inline void set_onPlacedObject_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___onPlacedObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPlacedObject_6), value);
	}

	inline static int32_t get_offset_of_s_Hits_8() { return static_cast<int32_t>(offsetof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields, ___s_Hits_8)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_s_Hits_8() const { return ___s_Hits_8; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_s_Hits_8() { return &___s_Hits_8; }
	inline void set_s_Hits_8(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___s_Hits_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMULTIPLEOBJECTSONPLANE_T776325CC089E260D234CC3B495856446CEB362CC_H
#ifndef PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#define PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceOnPlane
struct  PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PlaceOnPlane::m_PlacedPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlacedPrefab_4;
	// UnityEngine.GameObject PlaceOnPlane::<spawnedObject>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CspawnedObjectU3Ek__BackingField_5;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin PlaceOnPlane::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_7;

public:
	inline static int32_t get_offset_of_m_PlacedPrefab_4() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___m_PlacedPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlacedPrefab_4() const { return ___m_PlacedPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlacedPrefab_4() { return &___m_PlacedPrefab_4; }
	inline void set_m_PlacedPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlacedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlacedPrefab_4), value);
	}

	inline static int32_t get_offset_of_U3CspawnedObjectU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___U3CspawnedObjectU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CspawnedObjectU3Ek__BackingField_5() const { return ___U3CspawnedObjectU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CspawnedObjectU3Ek__BackingField_5() { return &___U3CspawnedObjectU3Ek__BackingField_5; }
	inline void set_U3CspawnedObjectU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CspawnedObjectU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspawnedObjectU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_7() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2, ___m_SessionOrigin_7)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_7() const { return ___m_SessionOrigin_7; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_7() { return &___m_SessionOrigin_7; }
	inline void set_m_SessionOrigin_7(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_7), value);
	}
};

struct PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlaceOnPlane::s_Hits
	List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * ___s_Hits_6;

public:
	inline static int32_t get_offset_of_s_Hits_6() { return static_cast<int32_t>(offsetof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields, ___s_Hits_6)); }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * get_s_Hits_6() const { return ___s_Hits_6; }
	inline List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 ** get_address_of_s_Hits_6() { return &___s_Hits_6; }
	inline void set_s_Hits_6(List_1_tE22AC27B04238DDEA6B873A77D0222DA9B480F52 * value)
	{
		___s_Hits_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Hits_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEONPLANE_TC5323EAA9F7603ADAB59DB462174996553CAF0C2_H
#ifndef PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#define PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaneDetectionController
struct  PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text PlaneDetectionController::m_TogglePlaneDetectionText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_TogglePlaneDetectionText_4;
	// UnityEngine.XR.ARFoundation.ARPlaneManager PlaneDetectionController::m_ARPlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_ARPlaneManager_5;

public:
	inline static int32_t get_offset_of_m_TogglePlaneDetectionText_4() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD, ___m_TogglePlaneDetectionText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_TogglePlaneDetectionText_4() const { return ___m_TogglePlaneDetectionText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_TogglePlaneDetectionText_4() { return &___m_TogglePlaneDetectionText_4; }
	inline void set_m_TogglePlaneDetectionText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_TogglePlaneDetectionText_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TogglePlaneDetectionText_4), value);
	}

	inline static int32_t get_offset_of_m_ARPlaneManager_5() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD, ___m_ARPlaneManager_5)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_ARPlaneManager_5() const { return ___m_ARPlaneManager_5; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_ARPlaneManager_5() { return &___m_ARPlaneManager_5; }
	inline void set_m_ARPlaneManager_5(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_ARPlaneManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ARPlaneManager_5), value);
	}
};

struct PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> PlaneDetectionController::s_Planes
	List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * ___s_Planes_6;

public:
	inline static int32_t get_offset_of_s_Planes_6() { return static_cast<int32_t>(offsetof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields, ___s_Planes_6)); }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * get_s_Planes_6() const { return ___s_Planes_6; }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B ** get_address_of_s_Planes_6() { return &___s_Planes_6; }
	inline void set_s_Planes_6(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * value)
	{
		___s_Planes_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEDETECTIONCONTROLLER_TF91A38E8CF0739882A647588DB89B4E0B91482DD_H
#ifndef PULLBACKPACKDOWNTOSUITCASE_TC4B6C719A7E08FBA5DF9B5E80D10C904524AD268_H
#define PULLBACKPACKDOWNTOSUITCASE_TC4B6C719A7E08FBA5DF9B5E80D10C904524AD268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PullBackpackDownToSuitcase
struct  PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PullBackpackDownToSuitcase::backpack
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpack_4;
	// System.Boolean PullBackpackDownToSuitcase::isTriggered
	bool ___isTriggered_5;
	// System.Boolean PullBackpackDownToSuitcase::isTriggering
	bool ___isTriggering_6;
	// UnityEngine.Vector3 PullBackpackDownToSuitcase::backpackInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backpackInitV3_7;
	// UnityEngine.Vector3 PullBackpackDownToSuitcase::backpackTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backpackTargetV3_8;
	// UnityEngine.Vector3 PullBackpackDownToSuitcase::backpackTranslateV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backpackTranslateV3_9;
	// System.Int32 PullBackpackDownToSuitcase::backpackNR
	int32_t ___backpackNR_10;
	// System.Int32 PullBackpackDownToSuitcase::backpackRCount
	int32_t ___backpackRCount_11;

public:
	inline static int32_t get_offset_of_backpack_4() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___backpack_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpack_4() const { return ___backpack_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpack_4() { return &___backpack_4; }
	inline void set_backpack_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpack_4 = value;
		Il2CppCodeGenWriteBarrier((&___backpack_4), value);
	}

	inline static int32_t get_offset_of_isTriggered_5() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___isTriggered_5)); }
	inline bool get_isTriggered_5() const { return ___isTriggered_5; }
	inline bool* get_address_of_isTriggered_5() { return &___isTriggered_5; }
	inline void set_isTriggered_5(bool value)
	{
		___isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_isTriggering_6() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___isTriggering_6)); }
	inline bool get_isTriggering_6() const { return ___isTriggering_6; }
	inline bool* get_address_of_isTriggering_6() { return &___isTriggering_6; }
	inline void set_isTriggering_6(bool value)
	{
		___isTriggering_6 = value;
	}

	inline static int32_t get_offset_of_backpackInitV3_7() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___backpackInitV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backpackInitV3_7() const { return ___backpackInitV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backpackInitV3_7() { return &___backpackInitV3_7; }
	inline void set_backpackInitV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backpackInitV3_7 = value;
	}

	inline static int32_t get_offset_of_backpackTargetV3_8() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___backpackTargetV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backpackTargetV3_8() const { return ___backpackTargetV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backpackTargetV3_8() { return &___backpackTargetV3_8; }
	inline void set_backpackTargetV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backpackTargetV3_8 = value;
	}

	inline static int32_t get_offset_of_backpackTranslateV3_9() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___backpackTranslateV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backpackTranslateV3_9() const { return ___backpackTranslateV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backpackTranslateV3_9() { return &___backpackTranslateV3_9; }
	inline void set_backpackTranslateV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backpackTranslateV3_9 = value;
	}

	inline static int32_t get_offset_of_backpackNR_10() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___backpackNR_10)); }
	inline int32_t get_backpackNR_10() const { return ___backpackNR_10; }
	inline int32_t* get_address_of_backpackNR_10() { return &___backpackNR_10; }
	inline void set_backpackNR_10(int32_t value)
	{
		___backpackNR_10 = value;
	}

	inline static int32_t get_offset_of_backpackRCount_11() { return static_cast<int32_t>(offsetof(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268, ___backpackRCount_11)); }
	inline int32_t get_backpackRCount_11() const { return ___backpackRCount_11; }
	inline int32_t* get_address_of_backpackRCount_11() { return &___backpackRCount_11; }
	inline void set_backpackRCount_11(int32_t value)
	{
		___backpackRCount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULLBACKPACKDOWNTOSUITCASE_TC4B6C719A7E08FBA5DF9B5E80D10C904524AD268_H
#ifndef PULLBAKPACKDOWNTOSUITCASEREVERSE_T0EB8970BACE3576FC6A5FF857B14A835FB4E0E43_H
#define PULLBAKPACKDOWNTOSUITCASEREVERSE_T0EB8970BACE3576FC6A5FF857B14A835FB4E0E43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PullBakpackDownToSuitcaseReverse
struct  PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PullBakpackDownToSuitcaseReverse::backpack
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpack_4;
	// System.Boolean PullBakpackDownToSuitcaseReverse::isTriggered
	bool ___isTriggered_5;
	// System.Boolean PullBakpackDownToSuitcaseReverse::isTriggering
	bool ___isTriggering_6;
	// UnityEngine.Vector3 PullBakpackDownToSuitcaseReverse::backpackInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backpackInitV3_7;
	// UnityEngine.Vector3 PullBakpackDownToSuitcaseReverse::backpackTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backpackTargetV3_8;
	// UnityEngine.Vector3 PullBakpackDownToSuitcaseReverse::backpackTranslateV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backpackTranslateV3_9;
	// System.Int32 PullBakpackDownToSuitcaseReverse::backpackNR
	int32_t ___backpackNR_10;
	// System.Int32 PullBakpackDownToSuitcaseReverse::backpackRCount
	int32_t ___backpackRCount_11;

public:
	inline static int32_t get_offset_of_backpack_4() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___backpack_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpack_4() const { return ___backpack_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpack_4() { return &___backpack_4; }
	inline void set_backpack_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpack_4 = value;
		Il2CppCodeGenWriteBarrier((&___backpack_4), value);
	}

	inline static int32_t get_offset_of_isTriggered_5() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___isTriggered_5)); }
	inline bool get_isTriggered_5() const { return ___isTriggered_5; }
	inline bool* get_address_of_isTriggered_5() { return &___isTriggered_5; }
	inline void set_isTriggered_5(bool value)
	{
		___isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_isTriggering_6() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___isTriggering_6)); }
	inline bool get_isTriggering_6() const { return ___isTriggering_6; }
	inline bool* get_address_of_isTriggering_6() { return &___isTriggering_6; }
	inline void set_isTriggering_6(bool value)
	{
		___isTriggering_6 = value;
	}

	inline static int32_t get_offset_of_backpackInitV3_7() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___backpackInitV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backpackInitV3_7() const { return ___backpackInitV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backpackInitV3_7() { return &___backpackInitV3_7; }
	inline void set_backpackInitV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backpackInitV3_7 = value;
	}

	inline static int32_t get_offset_of_backpackTargetV3_8() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___backpackTargetV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backpackTargetV3_8() const { return ___backpackTargetV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backpackTargetV3_8() { return &___backpackTargetV3_8; }
	inline void set_backpackTargetV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backpackTargetV3_8 = value;
	}

	inline static int32_t get_offset_of_backpackTranslateV3_9() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___backpackTranslateV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backpackTranslateV3_9() const { return ___backpackTranslateV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backpackTranslateV3_9() { return &___backpackTranslateV3_9; }
	inline void set_backpackTranslateV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backpackTranslateV3_9 = value;
	}

	inline static int32_t get_offset_of_backpackNR_10() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___backpackNR_10)); }
	inline int32_t get_backpackNR_10() const { return ___backpackNR_10; }
	inline int32_t* get_address_of_backpackNR_10() { return &___backpackNR_10; }
	inline void set_backpackNR_10(int32_t value)
	{
		___backpackNR_10 = value;
	}

	inline static int32_t get_offset_of_backpackRCount_11() { return static_cast<int32_t>(offsetof(PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43, ___backpackRCount_11)); }
	inline int32_t get_backpackRCount_11() const { return ___backpackRCount_11; }
	inline int32_t* get_address_of_backpackRCount_11() { return &___backpackRCount_11; }
	inline void set_backpackRCount_11(int32_t value)
	{
		___backpackRCount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULLBAKPACKDOWNTOSUITCASEREVERSE_T0EB8970BACE3576FC6A5FF857B14A835FB4E0E43_H
#ifndef PULLHANDLE_TB8A75F932095398EE5B38BBDCDB47713B7965379_H
#define PULLHANDLE_TB8A75F932095398EE5B38BBDCDB47713B7965379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PullHandle
struct  PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PullHandle::handle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___handle_4;
	// System.Boolean PullHandle::isTriggered
	bool ___isTriggered_5;
	// System.Boolean PullHandle::isTriggering
	bool ___isTriggering_6;
	// UnityEngine.Vector3 PullHandle::handleInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleInitV3_7;
	// UnityEngine.Vector3 PullHandle::handleTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTargetV3_8;
	// UnityEngine.Vector3 PullHandle::handleTranslateV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTranslateV3_9;
	// System.Int32 PullHandle::handleNR
	int32_t ___handleNR_10;
	// System.Int32 PullHandle::handleRCount
	int32_t ___handleRCount_11;
	// System.Single PullHandle::waited
	float ___waited_12;

public:
	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___handle_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_handle_4() const { return ___handle_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of_isTriggered_5() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___isTriggered_5)); }
	inline bool get_isTriggered_5() const { return ___isTriggered_5; }
	inline bool* get_address_of_isTriggered_5() { return &___isTriggered_5; }
	inline void set_isTriggered_5(bool value)
	{
		___isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_isTriggering_6() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___isTriggering_6)); }
	inline bool get_isTriggering_6() const { return ___isTriggering_6; }
	inline bool* get_address_of_isTriggering_6() { return &___isTriggering_6; }
	inline void set_isTriggering_6(bool value)
	{
		___isTriggering_6 = value;
	}

	inline static int32_t get_offset_of_handleInitV3_7() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___handleInitV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleInitV3_7() const { return ___handleInitV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleInitV3_7() { return &___handleInitV3_7; }
	inline void set_handleInitV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleInitV3_7 = value;
	}

	inline static int32_t get_offset_of_handleTargetV3_8() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___handleTargetV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTargetV3_8() const { return ___handleTargetV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTargetV3_8() { return &___handleTargetV3_8; }
	inline void set_handleTargetV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTargetV3_8 = value;
	}

	inline static int32_t get_offset_of_handleTranslateV3_9() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___handleTranslateV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTranslateV3_9() const { return ___handleTranslateV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTranslateV3_9() { return &___handleTranslateV3_9; }
	inline void set_handleTranslateV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTranslateV3_9 = value;
	}

	inline static int32_t get_offset_of_handleNR_10() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___handleNR_10)); }
	inline int32_t get_handleNR_10() const { return ___handleNR_10; }
	inline int32_t* get_address_of_handleNR_10() { return &___handleNR_10; }
	inline void set_handleNR_10(int32_t value)
	{
		___handleNR_10 = value;
	}

	inline static int32_t get_offset_of_handleRCount_11() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___handleRCount_11)); }
	inline int32_t get_handleRCount_11() const { return ___handleRCount_11; }
	inline int32_t* get_address_of_handleRCount_11() { return &___handleRCount_11; }
	inline void set_handleRCount_11(int32_t value)
	{
		___handleRCount_11 = value;
	}

	inline static int32_t get_offset_of_waited_12() { return static_cast<int32_t>(offsetof(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379, ___waited_12)); }
	inline float get_waited_12() const { return ___waited_12; }
	inline float* get_address_of_waited_12() { return &___waited_12; }
	inline void set_waited_12(float value)
	{
		___waited_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULLHANDLE_TB8A75F932095398EE5B38BBDCDB47713B7965379_H
#ifndef PULLHANDLEREVERSE_TEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4_H
#define PULLHANDLEREVERSE_TEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PullHandleReverse
struct  PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject PullHandleReverse::handle
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___handle_4;
	// System.Boolean PullHandleReverse::isTriggered
	bool ___isTriggered_5;
	// System.Boolean PullHandleReverse::isTriggering
	bool ___isTriggering_6;
	// UnityEngine.Vector3 PullHandleReverse::handleInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleInitV3_7;
	// UnityEngine.Vector3 PullHandleReverse::handleTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTargetV3_8;
	// UnityEngine.Vector3 PullHandleReverse::handleTranslateV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___handleTranslateV3_9;
	// System.Int32 PullHandleReverse::handleNR
	int32_t ___handleNR_10;
	// System.Int32 PullHandleReverse::handleRCount
	int32_t ___handleRCount_11;

public:
	inline static int32_t get_offset_of_handle_4() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___handle_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_handle_4() const { return ___handle_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_handle_4() { return &___handle_4; }
	inline void set_handle_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___handle_4), value);
	}

	inline static int32_t get_offset_of_isTriggered_5() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___isTriggered_5)); }
	inline bool get_isTriggered_5() const { return ___isTriggered_5; }
	inline bool* get_address_of_isTriggered_5() { return &___isTriggered_5; }
	inline void set_isTriggered_5(bool value)
	{
		___isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_isTriggering_6() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___isTriggering_6)); }
	inline bool get_isTriggering_6() const { return ___isTriggering_6; }
	inline bool* get_address_of_isTriggering_6() { return &___isTriggering_6; }
	inline void set_isTriggering_6(bool value)
	{
		___isTriggering_6 = value;
	}

	inline static int32_t get_offset_of_handleInitV3_7() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___handleInitV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleInitV3_7() const { return ___handleInitV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleInitV3_7() { return &___handleInitV3_7; }
	inline void set_handleInitV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleInitV3_7 = value;
	}

	inline static int32_t get_offset_of_handleTargetV3_8() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___handleTargetV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTargetV3_8() const { return ___handleTargetV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTargetV3_8() { return &___handleTargetV3_8; }
	inline void set_handleTargetV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTargetV3_8 = value;
	}

	inline static int32_t get_offset_of_handleTranslateV3_9() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___handleTranslateV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_handleTranslateV3_9() const { return ___handleTranslateV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_handleTranslateV3_9() { return &___handleTranslateV3_9; }
	inline void set_handleTranslateV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___handleTranslateV3_9 = value;
	}

	inline static int32_t get_offset_of_handleNR_10() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___handleNR_10)); }
	inline int32_t get_handleNR_10() const { return ___handleNR_10; }
	inline int32_t* get_address_of_handleNR_10() { return &___handleNR_10; }
	inline void set_handleNR_10(int32_t value)
	{
		___handleNR_10 = value;
	}

	inline static int32_t get_offset_of_handleRCount_11() { return static_cast<int32_t>(offsetof(PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4, ___handleRCount_11)); }
	inline int32_t get_handleRCount_11() const { return ___handleRCount_11; }
	inline int32_t* get_address_of_handleRCount_11() { return &___handleRCount_11; }
	inline void set_handleRCount_11(int32_t value)
	{
		___handleRCount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULLHANDLEREVERSE_TEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4_H
#ifndef RESETSUITCASE_T9B49849D0C79AE1D44622124F7BB483A4B02E3FE_H
#define RESETSUITCASE_T9B49849D0C79AE1D44622124F7BB483A4B02E3FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResetSuitcase
struct  ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject ResetSuitcase::front
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___front_4;
	// UnityEngine.GameObject ResetSuitcase::bottom
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___bottom_5;
	// UnityEngine.GameObject ResetSuitcase::mainPocket
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mainPocket_6;
	// System.Boolean ResetSuitcase::isTriggered
	bool ___isTriggered_7;

public:
	inline static int32_t get_offset_of_front_4() { return static_cast<int32_t>(offsetof(ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE, ___front_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_front_4() const { return ___front_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_front_4() { return &___front_4; }
	inline void set_front_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___front_4 = value;
		Il2CppCodeGenWriteBarrier((&___front_4), value);
	}

	inline static int32_t get_offset_of_bottom_5() { return static_cast<int32_t>(offsetof(ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE, ___bottom_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_bottom_5() const { return ___bottom_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_bottom_5() { return &___bottom_5; }
	inline void set_bottom_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___bottom_5 = value;
		Il2CppCodeGenWriteBarrier((&___bottom_5), value);
	}

	inline static int32_t get_offset_of_mainPocket_6() { return static_cast<int32_t>(offsetof(ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE, ___mainPocket_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mainPocket_6() const { return ___mainPocket_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mainPocket_6() { return &___mainPocket_6; }
	inline void set_mainPocket_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mainPocket_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainPocket_6), value);
	}

	inline static int32_t get_offset_of_isTriggered_7() { return static_cast<int32_t>(offsetof(ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE, ___isTriggered_7)); }
	inline bool get_isTriggered_7() const { return ___isTriggered_7; }
	inline bool* get_address_of_isTriggered_7() { return &___isTriggered_7; }
	inline void set_isTriggered_7(bool value)
	{
		___isTriggered_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESETSUITCASE_T9B49849D0C79AE1D44622124F7BB483A4B02E3FE_H
#ifndef ROTATEBACK_TBD721F8DFDD2B59FD333EA05EC6BEC437792A483_H
#define ROTATEBACK_TBD721F8DFDD2B59FD333EA05EC6BEC437792A483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateBack
struct  RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RotateBack::isTriggered
	bool ___isTriggered_4;
	// System.Boolean RotateBack::isTriggering
	bool ___isTriggering_5;
	// UnityEngine.Vector3 RotateBack::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_6;
	// UnityEngine.Vector3 RotateBack::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_7;
	// UnityEngine.Vector3 RotateBack::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_8;
	// System.Int32 RotateBack::thisNR
	int32_t ___thisNR_9;
	// System.Int32 RotateBack::thisRCount
	int32_t ___thisRCount_10;

public:
	inline static int32_t get_offset_of_isTriggered_4() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___isTriggered_4)); }
	inline bool get_isTriggered_4() const { return ___isTriggered_4; }
	inline bool* get_address_of_isTriggered_4() { return &___isTriggered_4; }
	inline void set_isTriggered_4(bool value)
	{
		___isTriggered_4 = value;
	}

	inline static int32_t get_offset_of_isTriggering_5() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___isTriggering_5)); }
	inline bool get_isTriggering_5() const { return ___isTriggering_5; }
	inline bool* get_address_of_isTriggering_5() { return &___isTriggering_5; }
	inline void set_isTriggering_5(bool value)
	{
		___isTriggering_5 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_6() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___thisInitV3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_6() const { return ___thisInitV3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_6() { return &___thisInitV3_6; }
	inline void set_thisInitV3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_6 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_7() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___thisTargetV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_7() const { return ___thisTargetV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_7() { return &___thisTargetV3_7; }
	inline void set_thisTargetV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_7 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_8() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___thisRotationV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_8() const { return ___thisRotationV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_8() { return &___thisRotationV3_8; }
	inline void set_thisRotationV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_8 = value;
	}

	inline static int32_t get_offset_of_thisNR_9() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___thisNR_9)); }
	inline int32_t get_thisNR_9() const { return ___thisNR_9; }
	inline int32_t* get_address_of_thisNR_9() { return &___thisNR_9; }
	inline void set_thisNR_9(int32_t value)
	{
		___thisNR_9 = value;
	}

	inline static int32_t get_offset_of_thisRCount_10() { return static_cast<int32_t>(offsetof(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483, ___thisRCount_10)); }
	inline int32_t get_thisRCount_10() const { return ___thisRCount_10; }
	inline int32_t* get_address_of_thisRCount_10() { return &___thisRCount_10; }
	inline void set_thisRCount_10(int32_t value)
	{
		___thisRCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEBACK_TBD721F8DFDD2B59FD333EA05EC6BEC437792A483_H
#ifndef ROTATEBOTTOM_T9DE19AB45DDDD715CA38B383EDE17845615FE0F1_H
#define ROTATEBOTTOM_T9DE19AB45DDDD715CA38B383EDE17845615FE0F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateBottom
struct  RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RotateBottom::isTriggered
	bool ___isTriggered_4;
	// System.Boolean RotateBottom::isTriggering
	bool ___isTriggering_5;
	// UnityEngine.Vector3 RotateBottom::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_6;
	// UnityEngine.Vector3 RotateBottom::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_7;
	// UnityEngine.Vector3 RotateBottom::thisTarget2V3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTarget2V3_8;
	// UnityEngine.Vector3 RotateBottom::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_9;
	// UnityEngine.Vector3 RotateBottom::thisRotation2V3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotation2V3_10;
	// System.Int32 RotateBottom::thisNR
	int32_t ___thisNR_11;
	// System.Int32 RotateBottom::thisNR2
	int32_t ___thisNR2_12;
	// System.Int32 RotateBottom::thisRCount
	int32_t ___thisRCount_13;
	// System.Int32 RotateBottom::thisRCount2
	int32_t ___thisRCount2_14;

public:
	inline static int32_t get_offset_of_isTriggered_4() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___isTriggered_4)); }
	inline bool get_isTriggered_4() const { return ___isTriggered_4; }
	inline bool* get_address_of_isTriggered_4() { return &___isTriggered_4; }
	inline void set_isTriggered_4(bool value)
	{
		___isTriggered_4 = value;
	}

	inline static int32_t get_offset_of_isTriggering_5() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___isTriggering_5)); }
	inline bool get_isTriggering_5() const { return ___isTriggering_5; }
	inline bool* get_address_of_isTriggering_5() { return &___isTriggering_5; }
	inline void set_isTriggering_5(bool value)
	{
		___isTriggering_5 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_6() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisInitV3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_6() const { return ___thisInitV3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_6() { return &___thisInitV3_6; }
	inline void set_thisInitV3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_6 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_7() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisTargetV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_7() const { return ___thisTargetV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_7() { return &___thisTargetV3_7; }
	inline void set_thisTargetV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_7 = value;
	}

	inline static int32_t get_offset_of_thisTarget2V3_8() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisTarget2V3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTarget2V3_8() const { return ___thisTarget2V3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTarget2V3_8() { return &___thisTarget2V3_8; }
	inline void set_thisTarget2V3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTarget2V3_8 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_9() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisRotationV3_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_9() const { return ___thisRotationV3_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_9() { return &___thisRotationV3_9; }
	inline void set_thisRotationV3_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_9 = value;
	}

	inline static int32_t get_offset_of_thisRotation2V3_10() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisRotation2V3_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotation2V3_10() const { return ___thisRotation2V3_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotation2V3_10() { return &___thisRotation2V3_10; }
	inline void set_thisRotation2V3_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotation2V3_10 = value;
	}

	inline static int32_t get_offset_of_thisNR_11() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisNR_11)); }
	inline int32_t get_thisNR_11() const { return ___thisNR_11; }
	inline int32_t* get_address_of_thisNR_11() { return &___thisNR_11; }
	inline void set_thisNR_11(int32_t value)
	{
		___thisNR_11 = value;
	}

	inline static int32_t get_offset_of_thisNR2_12() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisNR2_12)); }
	inline int32_t get_thisNR2_12() const { return ___thisNR2_12; }
	inline int32_t* get_address_of_thisNR2_12() { return &___thisNR2_12; }
	inline void set_thisNR2_12(int32_t value)
	{
		___thisNR2_12 = value;
	}

	inline static int32_t get_offset_of_thisRCount_13() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisRCount_13)); }
	inline int32_t get_thisRCount_13() const { return ___thisRCount_13; }
	inline int32_t* get_address_of_thisRCount_13() { return &___thisRCount_13; }
	inline void set_thisRCount_13(int32_t value)
	{
		___thisRCount_13 = value;
	}

	inline static int32_t get_offset_of_thisRCount2_14() { return static_cast<int32_t>(offsetof(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1, ___thisRCount2_14)); }
	inline int32_t get_thisRCount2_14() const { return ___thisRCount2_14; }
	inline int32_t* get_address_of_thisRCount2_14() { return &___thisRCount2_14; }
	inline void set_thisRCount2_14(int32_t value)
	{
		___thisRCount2_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEBOTTOM_T9DE19AB45DDDD715CA38B383EDE17845615FE0F1_H
#ifndef ROTATEFRONT_TA1D619428FF46A0E6482254AFCEB7F14C3A2CE03_H
#define ROTATEFRONT_TA1D619428FF46A0E6482254AFCEB7F14C3A2CE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateFront
struct  RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RotateFront::isTriggered
	bool ___isTriggered_4;
	// System.Boolean RotateFront::isTriggering
	bool ___isTriggering_5;
	// UnityEngine.Vector3 RotateFront::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_6;
	// UnityEngine.Vector3 RotateFront::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_7;
	// UnityEngine.Vector3 RotateFront::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_8;
	// System.Int32 RotateFront::thisNR
	int32_t ___thisNR_9;
	// System.Int32 RotateFront::thisRCount
	int32_t ___thisRCount_10;

public:
	inline static int32_t get_offset_of_isTriggered_4() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___isTriggered_4)); }
	inline bool get_isTriggered_4() const { return ___isTriggered_4; }
	inline bool* get_address_of_isTriggered_4() { return &___isTriggered_4; }
	inline void set_isTriggered_4(bool value)
	{
		___isTriggered_4 = value;
	}

	inline static int32_t get_offset_of_isTriggering_5() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___isTriggering_5)); }
	inline bool get_isTriggering_5() const { return ___isTriggering_5; }
	inline bool* get_address_of_isTriggering_5() { return &___isTriggering_5; }
	inline void set_isTriggering_5(bool value)
	{
		___isTriggering_5 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_6() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___thisInitV3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_6() const { return ___thisInitV3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_6() { return &___thisInitV3_6; }
	inline void set_thisInitV3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_6 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_7() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___thisTargetV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_7() const { return ___thisTargetV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_7() { return &___thisTargetV3_7; }
	inline void set_thisTargetV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_7 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_8() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___thisRotationV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_8() const { return ___thisRotationV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_8() { return &___thisRotationV3_8; }
	inline void set_thisRotationV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_8 = value;
	}

	inline static int32_t get_offset_of_thisNR_9() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___thisNR_9)); }
	inline int32_t get_thisNR_9() const { return ___thisNR_9; }
	inline int32_t* get_address_of_thisNR_9() { return &___thisNR_9; }
	inline void set_thisNR_9(int32_t value)
	{
		___thisNR_9 = value;
	}

	inline static int32_t get_offset_of_thisRCount_10() { return static_cast<int32_t>(offsetof(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03, ___thisRCount_10)); }
	inline int32_t get_thisRCount_10() const { return ___thisRCount_10; }
	inline int32_t* get_address_of_thisRCount_10() { return &___thisRCount_10; }
	inline void set_thisRCount_10(int32_t value)
	{
		___thisRCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEFRONT_TA1D619428FF46A0E6482254AFCEB7F14C3A2CE03_H
#ifndef ROTATEFRONT45_T2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13_H
#define ROTATEFRONT45_T2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateFront45
struct  RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RotateFront45::isTriggered
	bool ___isTriggered_4;
	// System.Boolean RotateFront45::isTriggering
	bool ___isTriggering_5;
	// UnityEngine.Vector3 RotateFront45::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_6;
	// UnityEngine.Vector3 RotateFront45::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_7;
	// UnityEngine.Vector3 RotateFront45::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_8;
	// System.Int32 RotateFront45::thisNR
	int32_t ___thisNR_9;
	// System.Int32 RotateFront45::thisRCount
	int32_t ___thisRCount_10;

public:
	inline static int32_t get_offset_of_isTriggered_4() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___isTriggered_4)); }
	inline bool get_isTriggered_4() const { return ___isTriggered_4; }
	inline bool* get_address_of_isTriggered_4() { return &___isTriggered_4; }
	inline void set_isTriggered_4(bool value)
	{
		___isTriggered_4 = value;
	}

	inline static int32_t get_offset_of_isTriggering_5() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___isTriggering_5)); }
	inline bool get_isTriggering_5() const { return ___isTriggering_5; }
	inline bool* get_address_of_isTriggering_5() { return &___isTriggering_5; }
	inline void set_isTriggering_5(bool value)
	{
		___isTriggering_5 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_6() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___thisInitV3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_6() const { return ___thisInitV3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_6() { return &___thisInitV3_6; }
	inline void set_thisInitV3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_6 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_7() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___thisTargetV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_7() const { return ___thisTargetV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_7() { return &___thisTargetV3_7; }
	inline void set_thisTargetV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_7 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_8() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___thisRotationV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_8() const { return ___thisRotationV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_8() { return &___thisRotationV3_8; }
	inline void set_thisRotationV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_8 = value;
	}

	inline static int32_t get_offset_of_thisNR_9() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___thisNR_9)); }
	inline int32_t get_thisNR_9() const { return ___thisNR_9; }
	inline int32_t* get_address_of_thisNR_9() { return &___thisNR_9; }
	inline void set_thisNR_9(int32_t value)
	{
		___thisNR_9 = value;
	}

	inline static int32_t get_offset_of_thisRCount_10() { return static_cast<int32_t>(offsetof(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13, ___thisRCount_10)); }
	inline int32_t get_thisRCount_10() const { return ___thisRCount_10; }
	inline int32_t* get_address_of_thisRCount_10() { return &___thisRCount_10; }
	inline void set_thisRCount_10(int32_t value)
	{
		___thisRCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEFRONT45_T2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13_H
#ifndef ROTATEFRONTREVERSE_TC98645E69053C367CF848E41A3E4977F51C3A705_H
#define ROTATEFRONTREVERSE_TC98645E69053C367CF848E41A3E4977F51C3A705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateFrontReverse
struct  RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RotateFrontReverse::isTriggered
	bool ___isTriggered_4;
	// System.Boolean RotateFrontReverse::isTriggering
	bool ___isTriggering_5;
	// UnityEngine.Vector3 RotateFrontReverse::thisInitV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisInitV3_6;
	// UnityEngine.Vector3 RotateFrontReverse::thisTargetV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisTargetV3_7;
	// UnityEngine.Vector3 RotateFrontReverse::thisRotationV3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___thisRotationV3_8;
	// System.Int32 RotateFrontReverse::thisNR
	int32_t ___thisNR_9;
	// System.Int32 RotateFrontReverse::thisRCount
	int32_t ___thisRCount_10;

public:
	inline static int32_t get_offset_of_isTriggered_4() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___isTriggered_4)); }
	inline bool get_isTriggered_4() const { return ___isTriggered_4; }
	inline bool* get_address_of_isTriggered_4() { return &___isTriggered_4; }
	inline void set_isTriggered_4(bool value)
	{
		___isTriggered_4 = value;
	}

	inline static int32_t get_offset_of_isTriggering_5() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___isTriggering_5)); }
	inline bool get_isTriggering_5() const { return ___isTriggering_5; }
	inline bool* get_address_of_isTriggering_5() { return &___isTriggering_5; }
	inline void set_isTriggering_5(bool value)
	{
		___isTriggering_5 = value;
	}

	inline static int32_t get_offset_of_thisInitV3_6() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___thisInitV3_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisInitV3_6() const { return ___thisInitV3_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisInitV3_6() { return &___thisInitV3_6; }
	inline void set_thisInitV3_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisInitV3_6 = value;
	}

	inline static int32_t get_offset_of_thisTargetV3_7() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___thisTargetV3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisTargetV3_7() const { return ___thisTargetV3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisTargetV3_7() { return &___thisTargetV3_7; }
	inline void set_thisTargetV3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisTargetV3_7 = value;
	}

	inline static int32_t get_offset_of_thisRotationV3_8() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___thisRotationV3_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_thisRotationV3_8() const { return ___thisRotationV3_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_thisRotationV3_8() { return &___thisRotationV3_8; }
	inline void set_thisRotationV3_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___thisRotationV3_8 = value;
	}

	inline static int32_t get_offset_of_thisNR_9() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___thisNR_9)); }
	inline int32_t get_thisNR_9() const { return ___thisNR_9; }
	inline int32_t* get_address_of_thisNR_9() { return &___thisNR_9; }
	inline void set_thisNR_9(int32_t value)
	{
		___thisNR_9 = value;
	}

	inline static int32_t get_offset_of_thisRCount_10() { return static_cast<int32_t>(offsetof(RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705, ___thisRCount_10)); }
	inline int32_t get_thisRCount_10() const { return ___thisRCount_10; }
	inline int32_t* get_address_of_thisRCount_10() { return &___thisRCount_10; }
	inline void set_thisRCount_10(int32_t value)
	{
		___thisRCount_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEFRONTREVERSE_TC98645E69053C367CF848E41A3E4977F51C3A705_H
#ifndef ROTATIONCONTROLLER_T41CB9629E51ECA40E7A0DC705A60605DE218C4EB_H
#define ROTATIONCONTROLLER_T41CB9629E51ECA40E7A0DC705A60605DE218C4EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotationController
struct  RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MakeAppearOnPlane RotationController::m_MakeAppearOnPlane
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 * ___m_MakeAppearOnPlane_4;
	// UnityEngine.UI.Slider RotationController::m_Slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_Slider_5;
	// UnityEngine.UI.Text RotationController::m_Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_Text_6;
	// System.Single RotationController::m_Min
	float ___m_Min_7;
	// System.Single RotationController::m_Max
	float ___m_Max_8;

public:
	inline static int32_t get_offset_of_m_MakeAppearOnPlane_4() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_MakeAppearOnPlane_4)); }
	inline MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 * get_m_MakeAppearOnPlane_4() const { return ___m_MakeAppearOnPlane_4; }
	inline MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 ** get_address_of_m_MakeAppearOnPlane_4() { return &___m_MakeAppearOnPlane_4; }
	inline void set_m_MakeAppearOnPlane_4(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3 * value)
	{
		___m_MakeAppearOnPlane_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MakeAppearOnPlane_4), value);
	}

	inline static int32_t get_offset_of_m_Slider_5() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Slider_5)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_Slider_5() const { return ___m_Slider_5; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_Slider_5() { return &___m_Slider_5; }
	inline void set_m_Slider_5(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_Slider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Slider_5), value);
	}

	inline static int32_t get_offset_of_m_Text_6() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Text_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_Text_6() const { return ___m_Text_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_Text_6() { return &___m_Text_6; }
	inline void set_m_Text_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_Text_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_6), value);
	}

	inline static int32_t get_offset_of_m_Min_7() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Min_7)); }
	inline float get_m_Min_7() const { return ___m_Min_7; }
	inline float* get_address_of_m_Min_7() { return &___m_Min_7; }
	inline void set_m_Min_7(float value)
	{
		___m_Min_7 = value;
	}

	inline static int32_t get_offset_of_m_Max_8() { return static_cast<int32_t>(offsetof(RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB, ___m_Max_8)); }
	inline float get_m_Max_8() const { return ___m_Max_8; }
	inline float* get_address_of_m_Max_8() { return &___m_Max_8; }
	inline void set_m_Max_8(float value)
	{
		___m_Max_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONCONTROLLER_T41CB9629E51ECA40E7A0DC705A60605DE218C4EB_H
#ifndef SCALECONTROLLER_T4CADE3F801EC93C79203B3407E4086AA4BC3FE1A_H
#define SCALECONTROLLER_T4CADE3F801EC93C79203B3407E4086AA4BC3FE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleController
struct  ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Slider ScaleController::m_Slider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ___m_Slider_4;
	// UnityEngine.UI.Text ScaleController::m_Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_Text_5;
	// System.Single ScaleController::m_Min
	float ___m_Min_6;
	// System.Single ScaleController::m_Max
	float ___m_Max_7;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin ScaleController::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_8;

public:
	inline static int32_t get_offset_of_m_Slider_4() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Slider_4)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get_m_Slider_4() const { return ___m_Slider_4; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of_m_Slider_4() { return &___m_Slider_4; }
	inline void set_m_Slider_4(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		___m_Slider_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Slider_4), value);
	}

	inline static int32_t get_offset_of_m_Text_5() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Text_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_Text_5() const { return ___m_Text_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_Text_5() { return &___m_Text_5; }
	inline void set_m_Text_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_Text_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_5), value);
	}

	inline static int32_t get_offset_of_m_Min_6() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Min_6)); }
	inline float get_m_Min_6() const { return ___m_Min_6; }
	inline float* get_address_of_m_Min_6() { return &___m_Min_6; }
	inline void set_m_Min_6(float value)
	{
		___m_Min_6 = value;
	}

	inline static int32_t get_offset_of_m_Max_7() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_Max_7)); }
	inline float get_m_Max_7() const { return ___m_Max_7; }
	inline float* get_address_of_m_Max_7() { return &___m_Max_7; }
	inline void set_m_Max_7(float value)
	{
		___m_Max_7 = value;
	}

	inline static int32_t get_offset_of_m_SessionOrigin_8() { return static_cast<int32_t>(offsetof(ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A, ___m_SessionOrigin_8)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_8() const { return ___m_SessionOrigin_8; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_8() { return &___m_SessionOrigin_8; }
	inline void set_m_SessionOrigin_8(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALECONTROLLER_T4CADE3F801EC93C79203B3407E4086AA4BC3FE1A_H
#ifndef SCENECONTROLLER_T5827DDFEB507363FD30290DC041EEE4EAE6251E4_H
#define SCENECONTROLLER_T5827DDFEB507363FD30290DC041EEE4EAE6251E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneController
struct  SceneController_t5827DDFEB507363FD30290DC041EEE4EAE6251E4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTROLLER_T5827DDFEB507363FD30290DC041EEE4EAE6251E4_H
#ifndef SCREENCAPTURE_T2374541D6035304D83868BE2B54D3C747DF22963_H
#define SCREENCAPTURE_T2374541D6035304D83868BE2B54D3C747DF22963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenCapture
struct  ScreenCapture_t2374541D6035304D83868BE2B54D3C747DF22963  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENCAPTURE_T2374541D6035304D83868BE2B54D3C747DF22963_H
#ifndef SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#define SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SetTargetFramerate
struct  SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 SetTargetFramerate::m_TargetFrameRate
	int32_t ___m_TargetFrameRate_4;

public:
	inline static int32_t get_offset_of_m_TargetFrameRate_4() { return static_cast<int32_t>(offsetof(SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0, ___m_TargetFrameRate_4)); }
	inline int32_t get_m_TargetFrameRate_4() const { return ___m_TargetFrameRate_4; }
	inline int32_t* get_address_of_m_TargetFrameRate_4() { return &___m_TargetFrameRate_4; }
	inline void set_m_TargetFrameRate_4(int32_t value)
	{
		___m_TargetFrameRate_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTARGETFRAMERATE_TCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0_H
#ifndef TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#define TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCameraImage
struct  TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.RawImage TestCameraImage::m_RawImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___m_RawImage_4;
	// UnityEngine.UI.Text TestCameraImage::m_ImageInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ImageInfo_5;
	// UnityEngine.Texture2D TestCameraImage::m_Texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_6;

public:
	inline static int32_t get_offset_of_m_RawImage_4() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_RawImage_4)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_m_RawImage_4() const { return ___m_RawImage_4; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_m_RawImage_4() { return &___m_RawImage_4; }
	inline void set_m_RawImage_4(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___m_RawImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RawImage_4), value);
	}

	inline static int32_t get_offset_of_m_ImageInfo_5() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_ImageInfo_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ImageInfo_5() const { return ___m_ImageInfo_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ImageInfo_5() { return &___m_ImageInfo_5; }
	inline void set_m_ImageInfo_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ImageInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageInfo_5), value);
	}

	inline static int32_t get_offset_of_m_Texture_6() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_Texture_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_Texture_6() const { return ___m_Texture_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_Texture_6() { return &___m_Texture_6; }
	inline void set_m_Texture_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_Texture_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifndef TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#define TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCamera
struct  TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCamera::oldTouchPositions
	Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* ___oldTouchPositions_4;
	// UnityEngine.Vector2 TouchCamera::oldTouchVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldTouchVector_5;
	// System.Single TouchCamera::oldTouchDistance
	float ___oldTouchDistance_6;

public:
	inline static int32_t get_offset_of_oldTouchPositions_4() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchPositions_4)); }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* get_oldTouchPositions_4() const { return ___oldTouchPositions_4; }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6** get_address_of_oldTouchPositions_4() { return &___oldTouchPositions_4; }
	inline void set_oldTouchPositions_4(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* value)
	{
		___oldTouchPositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_4), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_5() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldTouchVector_5() const { return ___oldTouchVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldTouchVector_5() { return &___oldTouchVector_5; }
	inline void set_oldTouchVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldTouchVector_5 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_6() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchDistance_6)); }
	inline float get_oldTouchDistance_6() const { return ___oldTouchDistance_6; }
	inline float* get_address_of_oldTouchDistance_6() { return &___oldTouchDistance_6; }
	inline void set_oldTouchDistance_6(float value)
	{
		___oldTouchDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#ifndef TOUCHCAMERAWITHROTATION_TABC91B3BC5774398A7E582A5BBF358FE03CD9166_H
#define TOUCHCAMERAWITHROTATION_TABC91B3BC5774398A7E582A5BBF358FE03CD9166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCameraWithRotation
struct  TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera TouchCameraWithRotation::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_4;
	// UnityEngine.GameObject TouchCameraWithRotation::rotationPlate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rotationPlate_5;
	// UnityEngine.GameObject TouchCameraWithRotation::rotationOffsetPlate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rotationOffsetPlate_6;
	// System.Single TouchCameraWithRotation::minFoV
	float ___minFoV_7;
	// System.Single TouchCameraWithRotation::maxFoV
	float ___maxFoV_8;
	// System.Boolean TouchCameraWithRotation::isEnabled
	bool ___isEnabled_9;
	// System.Boolean TouchCameraWithRotation::isOnlyZoomable
	bool ___isOnlyZoomable_10;
	// System.Boolean TouchCameraWithRotation::isOnlyRotate
	bool ___isOnlyRotate_11;
	// System.Boolean TouchCameraWithRotation::isLimitYAngle
	bool ___isLimitYAngle_12;
	// System.Single TouchCameraWithRotation::limitYAngleClockwise
	float ___limitYAngleClockwise_13;
	// System.Single TouchCameraWithRotation::limitYAngleAClockwise
	float ___limitYAngleAClockwise_14;
	// System.Boolean TouchCameraWithRotation::isRotationOffseted
	bool ___isRotationOffseted_15;
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCameraWithRotation::oldTouchPositions
	Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* ___oldTouchPositions_16;
	// UnityEngine.Vector2 TouchCameraWithRotation::oldTouchVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldTouchVector_17;
	// System.Single TouchCameraWithRotation::oldTouchDistance
	float ___oldTouchDistance_18;

public:
	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___mainCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_4), value);
	}

	inline static int32_t get_offset_of_rotationPlate_5() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___rotationPlate_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rotationPlate_5() const { return ___rotationPlate_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rotationPlate_5() { return &___rotationPlate_5; }
	inline void set_rotationPlate_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rotationPlate_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotationPlate_5), value);
	}

	inline static int32_t get_offset_of_rotationOffsetPlate_6() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___rotationOffsetPlate_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rotationOffsetPlate_6() const { return ___rotationOffsetPlate_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rotationOffsetPlate_6() { return &___rotationOffsetPlate_6; }
	inline void set_rotationOffsetPlate_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rotationOffsetPlate_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotationOffsetPlate_6), value);
	}

	inline static int32_t get_offset_of_minFoV_7() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___minFoV_7)); }
	inline float get_minFoV_7() const { return ___minFoV_7; }
	inline float* get_address_of_minFoV_7() { return &___minFoV_7; }
	inline void set_minFoV_7(float value)
	{
		___minFoV_7 = value;
	}

	inline static int32_t get_offset_of_maxFoV_8() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___maxFoV_8)); }
	inline float get_maxFoV_8() const { return ___maxFoV_8; }
	inline float* get_address_of_maxFoV_8() { return &___maxFoV_8; }
	inline void set_maxFoV_8(float value)
	{
		___maxFoV_8 = value;
	}

	inline static int32_t get_offset_of_isEnabled_9() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isEnabled_9)); }
	inline bool get_isEnabled_9() const { return ___isEnabled_9; }
	inline bool* get_address_of_isEnabled_9() { return &___isEnabled_9; }
	inline void set_isEnabled_9(bool value)
	{
		___isEnabled_9 = value;
	}

	inline static int32_t get_offset_of_isOnlyZoomable_10() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isOnlyZoomable_10)); }
	inline bool get_isOnlyZoomable_10() const { return ___isOnlyZoomable_10; }
	inline bool* get_address_of_isOnlyZoomable_10() { return &___isOnlyZoomable_10; }
	inline void set_isOnlyZoomable_10(bool value)
	{
		___isOnlyZoomable_10 = value;
	}

	inline static int32_t get_offset_of_isOnlyRotate_11() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isOnlyRotate_11)); }
	inline bool get_isOnlyRotate_11() const { return ___isOnlyRotate_11; }
	inline bool* get_address_of_isOnlyRotate_11() { return &___isOnlyRotate_11; }
	inline void set_isOnlyRotate_11(bool value)
	{
		___isOnlyRotate_11 = value;
	}

	inline static int32_t get_offset_of_isLimitYAngle_12() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isLimitYAngle_12)); }
	inline bool get_isLimitYAngle_12() const { return ___isLimitYAngle_12; }
	inline bool* get_address_of_isLimitYAngle_12() { return &___isLimitYAngle_12; }
	inline void set_isLimitYAngle_12(bool value)
	{
		___isLimitYAngle_12 = value;
	}

	inline static int32_t get_offset_of_limitYAngleClockwise_13() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___limitYAngleClockwise_13)); }
	inline float get_limitYAngleClockwise_13() const { return ___limitYAngleClockwise_13; }
	inline float* get_address_of_limitYAngleClockwise_13() { return &___limitYAngleClockwise_13; }
	inline void set_limitYAngleClockwise_13(float value)
	{
		___limitYAngleClockwise_13 = value;
	}

	inline static int32_t get_offset_of_limitYAngleAClockwise_14() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___limitYAngleAClockwise_14)); }
	inline float get_limitYAngleAClockwise_14() const { return ___limitYAngleAClockwise_14; }
	inline float* get_address_of_limitYAngleAClockwise_14() { return &___limitYAngleAClockwise_14; }
	inline void set_limitYAngleAClockwise_14(float value)
	{
		___limitYAngleAClockwise_14 = value;
	}

	inline static int32_t get_offset_of_isRotationOffseted_15() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isRotationOffseted_15)); }
	inline bool get_isRotationOffseted_15() const { return ___isRotationOffseted_15; }
	inline bool* get_address_of_isRotationOffseted_15() { return &___isRotationOffseted_15; }
	inline void set_isRotationOffseted_15(bool value)
	{
		___isRotationOffseted_15 = value;
	}

	inline static int32_t get_offset_of_oldTouchPositions_16() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___oldTouchPositions_16)); }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* get_oldTouchPositions_16() const { return ___oldTouchPositions_16; }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6** get_address_of_oldTouchPositions_16() { return &___oldTouchPositions_16; }
	inline void set_oldTouchPositions_16(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* value)
	{
		___oldTouchPositions_16 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_16), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_17() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___oldTouchVector_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldTouchVector_17() const { return ___oldTouchVector_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldTouchVector_17() { return &___oldTouchVector_17; }
	inline void set_oldTouchVector_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldTouchVector_17 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_18() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___oldTouchDistance_18)); }
	inline float get_oldTouchDistance_18() const { return ___oldTouchDistance_18; }
	inline float* get_address_of_oldTouchDistance_18() { return &___oldTouchDistance_18; }
	inline void set_oldTouchDistance_18(float value)
	{
		___oldTouchDistance_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERAWITHROTATION_TABC91B3BC5774398A7E582A5BBF358FE03CD9166_H
#ifndef UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#define UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::m_PlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_PlaneManager_6;
	// UnityEngine.Animator UIManager::m_MoveDeviceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_MoveDeviceAnimation_7;
	// UnityEngine.Animator UIManager::m_TapToPlaceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_TapToPlaceAnimation_8;
	// System.Boolean UIManager::m_ShowingTapToPlace
	bool ___m_ShowingTapToPlace_10;
	// System.Boolean UIManager::m_ShowingMoveDevice
	bool ___m_ShowingMoveDevice_11;

public:
	inline static int32_t get_offset_of_m_PlaneManager_6() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_PlaneManager_6)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_PlaneManager_6() const { return ___m_PlaneManager_6; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_PlaneManager_6() { return &___m_PlaneManager_6; }
	inline void set_m_PlaneManager_6(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_PlaneManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneManager_6), value);
	}

	inline static int32_t get_offset_of_m_MoveDeviceAnimation_7() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_MoveDeviceAnimation_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_MoveDeviceAnimation_7() const { return ___m_MoveDeviceAnimation_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_MoveDeviceAnimation_7() { return &___m_MoveDeviceAnimation_7; }
	inline void set_m_MoveDeviceAnimation_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_MoveDeviceAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoveDeviceAnimation_7), value);
	}

	inline static int32_t get_offset_of_m_TapToPlaceAnimation_8() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_TapToPlaceAnimation_8)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_TapToPlaceAnimation_8() const { return ___m_TapToPlaceAnimation_8; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_TapToPlaceAnimation_8() { return &___m_TapToPlaceAnimation_8; }
	inline void set_m_TapToPlaceAnimation_8(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_TapToPlaceAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapToPlaceAnimation_8), value);
	}

	inline static int32_t get_offset_of_m_ShowingTapToPlace_10() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingTapToPlace_10)); }
	inline bool get_m_ShowingTapToPlace_10() const { return ___m_ShowingTapToPlace_10; }
	inline bool* get_address_of_m_ShowingTapToPlace_10() { return &___m_ShowingTapToPlace_10; }
	inline void set_m_ShowingTapToPlace_10(bool value)
	{
		___m_ShowingTapToPlace_10 = value;
	}

	inline static int32_t get_offset_of_m_ShowingMoveDevice_11() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingMoveDevice_11)); }
	inline bool get_m_ShowingMoveDevice_11() const { return ___m_ShowingMoveDevice_11; }
	inline bool* get_address_of_m_ShowingMoveDevice_11() { return &___m_ShowingMoveDevice_11; }
	inline void set_m_ShowingMoveDevice_11(bool value)
	{
		___m_ShowingMoveDevice_11 = value;
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UIManager::s_Planes
	List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * ___s_Planes_9;

public:
	inline static int32_t get_offset_of_s_Planes_9() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___s_Planes_9)); }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * get_s_Planes_9() const { return ___s_Planes_9; }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B ** get_address_of_s_Planes_9() { return &___s_Planes_9; }
	inline void set_s_Planes_9(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * value)
	{
		___s_Planes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef ZOOM_T880AFB749C3A75A781108A6A230D45E6B07952F0_H
#define ZOOM_T880AFB749C3A75A781108A6A230D45E6B07952F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zoom
struct  Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera Zoom::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_4;
	// UnityEngine.GameObject Zoom::zoomInObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zoomInObject_5;
	// System.Boolean Zoom::isTriggered
	bool ___isTriggered_6;
	// System.Single Zoom::initFieldOfView
	float ___initFieldOfView_7;
	// System.Single Zoom::targetFieldOfView
	float ___targetFieldOfView_8;
	// System.Int32 Zoom::nStep
	int32_t ___nStep_9;
	// System.Single Zoom::waitForSeconds
	float ___waitForSeconds_10;
	// System.Single Zoom::waited
	float ___waited_11;
	// System.Boolean Zoom::isTriggering
	bool ___isTriggering_12;
	// System.String Zoom::trigger
	String_t* ___trigger_13;
	// System.Int32 Zoom::cameraNR
	int32_t ___cameraNR_14;
	// System.Int32 Zoom::cameraRCount
	int32_t ___cameraRCount_15;
	// System.Single Zoom::step
	float ___step_16;

public:
	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___mainCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_4), value);
	}

	inline static int32_t get_offset_of_zoomInObject_5() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___zoomInObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zoomInObject_5() const { return ___zoomInObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zoomInObject_5() { return &___zoomInObject_5; }
	inline void set_zoomInObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zoomInObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___zoomInObject_5), value);
	}

	inline static int32_t get_offset_of_isTriggered_6() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___isTriggered_6)); }
	inline bool get_isTriggered_6() const { return ___isTriggered_6; }
	inline bool* get_address_of_isTriggered_6() { return &___isTriggered_6; }
	inline void set_isTriggered_6(bool value)
	{
		___isTriggered_6 = value;
	}

	inline static int32_t get_offset_of_initFieldOfView_7() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___initFieldOfView_7)); }
	inline float get_initFieldOfView_7() const { return ___initFieldOfView_7; }
	inline float* get_address_of_initFieldOfView_7() { return &___initFieldOfView_7; }
	inline void set_initFieldOfView_7(float value)
	{
		___initFieldOfView_7 = value;
	}

	inline static int32_t get_offset_of_targetFieldOfView_8() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___targetFieldOfView_8)); }
	inline float get_targetFieldOfView_8() const { return ___targetFieldOfView_8; }
	inline float* get_address_of_targetFieldOfView_8() { return &___targetFieldOfView_8; }
	inline void set_targetFieldOfView_8(float value)
	{
		___targetFieldOfView_8 = value;
	}

	inline static int32_t get_offset_of_nStep_9() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___nStep_9)); }
	inline int32_t get_nStep_9() const { return ___nStep_9; }
	inline int32_t* get_address_of_nStep_9() { return &___nStep_9; }
	inline void set_nStep_9(int32_t value)
	{
		___nStep_9 = value;
	}

	inline static int32_t get_offset_of_waitForSeconds_10() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___waitForSeconds_10)); }
	inline float get_waitForSeconds_10() const { return ___waitForSeconds_10; }
	inline float* get_address_of_waitForSeconds_10() { return &___waitForSeconds_10; }
	inline void set_waitForSeconds_10(float value)
	{
		___waitForSeconds_10 = value;
	}

	inline static int32_t get_offset_of_waited_11() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___waited_11)); }
	inline float get_waited_11() const { return ___waited_11; }
	inline float* get_address_of_waited_11() { return &___waited_11; }
	inline void set_waited_11(float value)
	{
		___waited_11 = value;
	}

	inline static int32_t get_offset_of_isTriggering_12() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___isTriggering_12)); }
	inline bool get_isTriggering_12() const { return ___isTriggering_12; }
	inline bool* get_address_of_isTriggering_12() { return &___isTriggering_12; }
	inline void set_isTriggering_12(bool value)
	{
		___isTriggering_12 = value;
	}

	inline static int32_t get_offset_of_trigger_13() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___trigger_13)); }
	inline String_t* get_trigger_13() const { return ___trigger_13; }
	inline String_t** get_address_of_trigger_13() { return &___trigger_13; }
	inline void set_trigger_13(String_t* value)
	{
		___trigger_13 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_13), value);
	}

	inline static int32_t get_offset_of_cameraNR_14() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___cameraNR_14)); }
	inline int32_t get_cameraNR_14() const { return ___cameraNR_14; }
	inline int32_t* get_address_of_cameraNR_14() { return &___cameraNR_14; }
	inline void set_cameraNR_14(int32_t value)
	{
		___cameraNR_14 = value;
	}

	inline static int32_t get_offset_of_cameraRCount_15() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___cameraRCount_15)); }
	inline int32_t get_cameraRCount_15() const { return ___cameraRCount_15; }
	inline int32_t* get_address_of_cameraRCount_15() { return &___cameraRCount_15; }
	inline void set_cameraRCount_15(int32_t value)
	{
		___cameraRCount_15 = value;
	}

	inline static int32_t get_offset_of_step_16() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___step_16)); }
	inline float get_step_16() const { return ___step_16; }
	inline float* get_address_of_step_16() { return &___step_16; }
	inline void set_step_16(float value)
	{
		___step_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOM_T880AFB749C3A75A781108A6A230D45E6B07952F0_H
#ifndef FADEIN_T1AE46C152FC433A43DEA5A0EF1393D741794A425_H
#define FADEIN_T1AE46C152FC433A43DEA5A0EF1393D741794A425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// fadeIn
struct  fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single fadeIn::FadeRate
	float ___FadeRate_4;
	// UnityEngine.UI.RawImage fadeIn::image
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___image_5;
	// System.Single fadeIn::targetAlpha
	float ___targetAlpha_6;

public:
	inline static int32_t get_offset_of_FadeRate_4() { return static_cast<int32_t>(offsetof(fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425, ___FadeRate_4)); }
	inline float get_FadeRate_4() const { return ___FadeRate_4; }
	inline float* get_address_of_FadeRate_4() { return &___FadeRate_4; }
	inline void set_FadeRate_4(float value)
	{
		___FadeRate_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425, ___image_5)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_image_5() const { return ___image_5; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}

	inline static int32_t get_offset_of_targetAlpha_6() { return static_cast<int32_t>(offsetof(fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425, ___targetAlpha_6)); }
	inline float get_targetAlpha_6() const { return ___targetAlpha_6; }
	inline float* get_address_of_targetAlpha_6() { return &___targetAlpha_6; }
	inline void set_targetAlpha_6(float value)
	{
		___targetAlpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEIN_T1AE46C152FC433A43DEA5A0EF1393D741794A425_H
#ifndef BACKPACKCONTROL_TEDD5E4AB9CCA17F951BC9C004641504FCC16097E_H
#define BACKPACKCONTROL_TEDD5E4AB9CCA17F951BC9C004641504FCC16097E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackpackControl
struct  BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E  : public BaseControl_tA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171
{
public:
	// UnityEngine.GameObject BackpackControl::videoOverlay
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoOverlay_5;
	// UnityEngine.UI.RawImage BackpackControl::rainProofImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___rainProofImage_6;
	// UnityEngine.UI.RawImage BackpackControl::frontPocketImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___frontPocketImage_7;
	// UnityEngine.UI.RawImage BackpackControl::tracerImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___tracerImage_8;
	// UnityEngine.UI.RawImage BackpackControl::waterProofPocketImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___waterProofPocketImage_9;
	// UnityEngine.UI.RawImage BackpackControl::uzipImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___uzipImage_10;
	// UnityEngine.GameObject BackpackControl::backpackClose
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpackClose_11;
	// UnityEngine.GameObject BackpackControl::backpackHalfOpen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpackHalfOpen_12;
	// UnityEngine.GameObject BackpackControl::backpackMainOpen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpackMainOpen_13;
	// UnityEngine.GameObject BackpackControl::backpackOpen
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpackOpen_14;
	// UnityEngine.GameObject BackpackControl::backpackWithSuitcase
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___backpackWithSuitcase_15;
	// UnityEngine.Camera BackpackControl::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_16;
	// UnityEngine.GameObject[] BackpackControl::fList
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___fList_17;
	// UnityEngine.GameObject[] BackpackControl::cList
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___cList_18;
	// UnityEngine.GameObject[] BackpackControl::dList
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___dList_19;
	// UnityEngine.Vector3 BackpackControl::initCameraAngle
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initCameraAngle_20;
	// System.Single BackpackControl::initFoV
	float ___initFoV_21;
	// System.Int32 BackpackControl::fIndex
	int32_t ___fIndex_22;
	// Zoom BackpackControl::zoomScript
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * ___zoomScript_23;
	// Zoom BackpackControl::zoomOutScript
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * ___zoomOutScript_24;
	// TouchCameraWithRotation BackpackControl::cameraScript
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * ___cameraScript_25;
	// PullBackpackDownToSuitcase BackpackControl::pullBackpackDownToSuitcase
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268 * ___pullBackpackDownToSuitcase_26;
	// RotateFront BackpackControl::rotateFrontScript
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * ___rotateFrontScript_27;
	// RotateFront BackpackControl::closedRotateFrontScript
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * ___closedRotateFrontScript_28;
	// RotateFront45 BackpackControl::rotateFront45Script
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13 * ___rotateFront45Script_29;
	// RotateBack BackpackControl::rotateBackScript
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 * ___rotateBackScript_30;

public:
	inline static int32_t get_offset_of_videoOverlay_5() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___videoOverlay_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoOverlay_5() const { return ___videoOverlay_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoOverlay_5() { return &___videoOverlay_5; }
	inline void set_videoOverlay_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoOverlay_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoOverlay_5), value);
	}

	inline static int32_t get_offset_of_rainProofImage_6() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___rainProofImage_6)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_rainProofImage_6() const { return ___rainProofImage_6; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_rainProofImage_6() { return &___rainProofImage_6; }
	inline void set_rainProofImage_6(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___rainProofImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___rainProofImage_6), value);
	}

	inline static int32_t get_offset_of_frontPocketImage_7() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___frontPocketImage_7)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_frontPocketImage_7() const { return ___frontPocketImage_7; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_frontPocketImage_7() { return &___frontPocketImage_7; }
	inline void set_frontPocketImage_7(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___frontPocketImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___frontPocketImage_7), value);
	}

	inline static int32_t get_offset_of_tracerImage_8() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___tracerImage_8)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_tracerImage_8() const { return ___tracerImage_8; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_tracerImage_8() { return &___tracerImage_8; }
	inline void set_tracerImage_8(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___tracerImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___tracerImage_8), value);
	}

	inline static int32_t get_offset_of_waterProofPocketImage_9() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___waterProofPocketImage_9)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_waterProofPocketImage_9() const { return ___waterProofPocketImage_9; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_waterProofPocketImage_9() { return &___waterProofPocketImage_9; }
	inline void set_waterProofPocketImage_9(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___waterProofPocketImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___waterProofPocketImage_9), value);
	}

	inline static int32_t get_offset_of_uzipImage_10() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___uzipImage_10)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_uzipImage_10() const { return ___uzipImage_10; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_uzipImage_10() { return &___uzipImage_10; }
	inline void set_uzipImage_10(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___uzipImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___uzipImage_10), value);
	}

	inline static int32_t get_offset_of_backpackClose_11() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___backpackClose_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpackClose_11() const { return ___backpackClose_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpackClose_11() { return &___backpackClose_11; }
	inline void set_backpackClose_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpackClose_11 = value;
		Il2CppCodeGenWriteBarrier((&___backpackClose_11), value);
	}

	inline static int32_t get_offset_of_backpackHalfOpen_12() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___backpackHalfOpen_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpackHalfOpen_12() const { return ___backpackHalfOpen_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpackHalfOpen_12() { return &___backpackHalfOpen_12; }
	inline void set_backpackHalfOpen_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpackHalfOpen_12 = value;
		Il2CppCodeGenWriteBarrier((&___backpackHalfOpen_12), value);
	}

	inline static int32_t get_offset_of_backpackMainOpen_13() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___backpackMainOpen_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpackMainOpen_13() const { return ___backpackMainOpen_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpackMainOpen_13() { return &___backpackMainOpen_13; }
	inline void set_backpackMainOpen_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpackMainOpen_13 = value;
		Il2CppCodeGenWriteBarrier((&___backpackMainOpen_13), value);
	}

	inline static int32_t get_offset_of_backpackOpen_14() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___backpackOpen_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpackOpen_14() const { return ___backpackOpen_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpackOpen_14() { return &___backpackOpen_14; }
	inline void set_backpackOpen_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpackOpen_14 = value;
		Il2CppCodeGenWriteBarrier((&___backpackOpen_14), value);
	}

	inline static int32_t get_offset_of_backpackWithSuitcase_15() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___backpackWithSuitcase_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_backpackWithSuitcase_15() const { return ___backpackWithSuitcase_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_backpackWithSuitcase_15() { return &___backpackWithSuitcase_15; }
	inline void set_backpackWithSuitcase_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___backpackWithSuitcase_15 = value;
		Il2CppCodeGenWriteBarrier((&___backpackWithSuitcase_15), value);
	}

	inline static int32_t get_offset_of_mainCamera_16() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___mainCamera_16)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_16() const { return ___mainCamera_16; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_16() { return &___mainCamera_16; }
	inline void set_mainCamera_16(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_16 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_16), value);
	}

	inline static int32_t get_offset_of_fList_17() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___fList_17)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_fList_17() const { return ___fList_17; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_fList_17() { return &___fList_17; }
	inline void set_fList_17(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___fList_17 = value;
		Il2CppCodeGenWriteBarrier((&___fList_17), value);
	}

	inline static int32_t get_offset_of_cList_18() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___cList_18)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_cList_18() const { return ___cList_18; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_cList_18() { return &___cList_18; }
	inline void set_cList_18(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___cList_18 = value;
		Il2CppCodeGenWriteBarrier((&___cList_18), value);
	}

	inline static int32_t get_offset_of_dList_19() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___dList_19)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_dList_19() const { return ___dList_19; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_dList_19() { return &___dList_19; }
	inline void set_dList_19(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___dList_19 = value;
		Il2CppCodeGenWriteBarrier((&___dList_19), value);
	}

	inline static int32_t get_offset_of_initCameraAngle_20() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___initCameraAngle_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initCameraAngle_20() const { return ___initCameraAngle_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initCameraAngle_20() { return &___initCameraAngle_20; }
	inline void set_initCameraAngle_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initCameraAngle_20 = value;
	}

	inline static int32_t get_offset_of_initFoV_21() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___initFoV_21)); }
	inline float get_initFoV_21() const { return ___initFoV_21; }
	inline float* get_address_of_initFoV_21() { return &___initFoV_21; }
	inline void set_initFoV_21(float value)
	{
		___initFoV_21 = value;
	}

	inline static int32_t get_offset_of_fIndex_22() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___fIndex_22)); }
	inline int32_t get_fIndex_22() const { return ___fIndex_22; }
	inline int32_t* get_address_of_fIndex_22() { return &___fIndex_22; }
	inline void set_fIndex_22(int32_t value)
	{
		___fIndex_22 = value;
	}

	inline static int32_t get_offset_of_zoomScript_23() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___zoomScript_23)); }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * get_zoomScript_23() const { return ___zoomScript_23; }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 ** get_address_of_zoomScript_23() { return &___zoomScript_23; }
	inline void set_zoomScript_23(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * value)
	{
		___zoomScript_23 = value;
		Il2CppCodeGenWriteBarrier((&___zoomScript_23), value);
	}

	inline static int32_t get_offset_of_zoomOutScript_24() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___zoomOutScript_24)); }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * get_zoomOutScript_24() const { return ___zoomOutScript_24; }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 ** get_address_of_zoomOutScript_24() { return &___zoomOutScript_24; }
	inline void set_zoomOutScript_24(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * value)
	{
		___zoomOutScript_24 = value;
		Il2CppCodeGenWriteBarrier((&___zoomOutScript_24), value);
	}

	inline static int32_t get_offset_of_cameraScript_25() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___cameraScript_25)); }
	inline TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * get_cameraScript_25() const { return ___cameraScript_25; }
	inline TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 ** get_address_of_cameraScript_25() { return &___cameraScript_25; }
	inline void set_cameraScript_25(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * value)
	{
		___cameraScript_25 = value;
		Il2CppCodeGenWriteBarrier((&___cameraScript_25), value);
	}

	inline static int32_t get_offset_of_pullBackpackDownToSuitcase_26() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___pullBackpackDownToSuitcase_26)); }
	inline PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268 * get_pullBackpackDownToSuitcase_26() const { return ___pullBackpackDownToSuitcase_26; }
	inline PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268 ** get_address_of_pullBackpackDownToSuitcase_26() { return &___pullBackpackDownToSuitcase_26; }
	inline void set_pullBackpackDownToSuitcase_26(PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268 * value)
	{
		___pullBackpackDownToSuitcase_26 = value;
		Il2CppCodeGenWriteBarrier((&___pullBackpackDownToSuitcase_26), value);
	}

	inline static int32_t get_offset_of_rotateFrontScript_27() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___rotateFrontScript_27)); }
	inline RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * get_rotateFrontScript_27() const { return ___rotateFrontScript_27; }
	inline RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 ** get_address_of_rotateFrontScript_27() { return &___rotateFrontScript_27; }
	inline void set_rotateFrontScript_27(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * value)
	{
		___rotateFrontScript_27 = value;
		Il2CppCodeGenWriteBarrier((&___rotateFrontScript_27), value);
	}

	inline static int32_t get_offset_of_closedRotateFrontScript_28() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___closedRotateFrontScript_28)); }
	inline RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * get_closedRotateFrontScript_28() const { return ___closedRotateFrontScript_28; }
	inline RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 ** get_address_of_closedRotateFrontScript_28() { return &___closedRotateFrontScript_28; }
	inline void set_closedRotateFrontScript_28(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * value)
	{
		___closedRotateFrontScript_28 = value;
		Il2CppCodeGenWriteBarrier((&___closedRotateFrontScript_28), value);
	}

	inline static int32_t get_offset_of_rotateFront45Script_29() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___rotateFront45Script_29)); }
	inline RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13 * get_rotateFront45Script_29() const { return ___rotateFront45Script_29; }
	inline RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13 ** get_address_of_rotateFront45Script_29() { return &___rotateFront45Script_29; }
	inline void set_rotateFront45Script_29(RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13 * value)
	{
		___rotateFront45Script_29 = value;
		Il2CppCodeGenWriteBarrier((&___rotateFront45Script_29), value);
	}

	inline static int32_t get_offset_of_rotateBackScript_30() { return static_cast<int32_t>(offsetof(BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E, ___rotateBackScript_30)); }
	inline RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 * get_rotateBackScript_30() const { return ___rotateBackScript_30; }
	inline RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 ** get_address_of_rotateBackScript_30() { return &___rotateBackScript_30; }
	inline void set_rotateBackScript_30(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 * value)
	{
		___rotateBackScript_30 = value;
		Il2CppCodeGenWriteBarrier((&___rotateBackScript_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKPACKCONTROL_TEDD5E4AB9CCA17F951BC9C004641504FCC16097E_H
#ifndef SUITCASECONTROL_T1484B9A35CD0D02EF5179305540D4EC1CD7C2458_H
#define SUITCASECONTROL_T1484B9A35CD0D02EF5179305540D4EC1CD7C2458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SuitcaseControl
struct  SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458  : public BaseControl_tA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171
{
public:
	// UnityEngine.GameObject SuitcaseControl::videoOverlay
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoOverlay_5;
	// UnityEngine.UI.RawImage SuitcaseControl::usbPowerImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___usbPowerImage_6;
	// UnityEngine.UI.RawImage SuitcaseControl::zipperExpImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___zipperExpImage_7;
	// UnityEngine.UI.RawImage SuitcaseControl::frontPocketImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___frontPocketImage_8;
	// UnityEngine.UI.RawImage SuitcaseControl::tracerImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___tracerImage_9;
	// UnityEngine.UI.RawImage SuitcaseControl::hangerImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___hangerImage_10;
	// UnityEngine.UI.RawImage SuitcaseControl::wheelImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___wheelImage_11;
	// UnityEngine.GameObject SuitcaseControl::movingSuitcase
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___movingSuitcase_12;
	// UnityEngine.GameObject SuitcaseControl::layingSuitcase
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___layingSuitcase_13;
	// UnityEngine.GameObject SuitcaseControl::dualAccessSuitcase
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___dualAccessSuitcase_14;
	// UnityEngine.Camera SuitcaseControl::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_15;
	// UnityEngine.GameObject[] SuitcaseControl::fList
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___fList_16;
	// UnityEngine.GameObject[] SuitcaseControl::f7List
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___f7List_17;
	// UnityEngine.GameObject[] SuitcaseControl::dList
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___dList_18;
	// UnityEngine.Vector3 SuitcaseControl::initCameraAngle
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___initCameraAngle_19;
	// System.Single SuitcaseControl::initFoV
	float ___initFoV_20;
	// System.Int32 SuitcaseControl::fIndex
	int32_t ___fIndex_21;
	// Zoom SuitcaseControl::zoomScript
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * ___zoomScript_22;
	// Zoom SuitcaseControl::zoomOutScript
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * ___zoomOutScript_23;
	// TouchCameraWithRotation SuitcaseControl::cameraScript
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * ___cameraScript_24;
	// CloseSuitcase SuitcaseControl::closeSuitcaseScript
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5 * ___closeSuitcaseScript_25;
	// OpenSuitcase SuitcaseControl::openSuitcaseScript
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC * ___openSuitcaseScript_26;
	// RotateFront SuitcaseControl::rotateFrontScript
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * ___rotateFrontScript_27;
	// RotateBottom SuitcaseControl::rotateBottomScript
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1 * ___rotateBottomScript_28;
	// RotateBack SuitcaseControl::rotateBackScript
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 * ___rotateBackScript_29;
	// LayDownSuitcase SuitcaseControl::layDownSuitcase
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3 * ___layDownSuitcase_30;
	// LayDownSuitcaseOpenFullReverse SuitcaseControl::layDownSuitcaseClose
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2 * ___layDownSuitcaseClose_31;
	// PullHandle SuitcaseControl::pullHandle
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379 * ___pullHandle_32;
	// FlyHanger SuitcaseControl::flyHanger
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A * ___flyHanger_33;
	// System.Collections.IEnumerator SuitcaseControl::pendingRoutine
	RuntimeObject* ___pendingRoutine_34;

public:
	inline static int32_t get_offset_of_videoOverlay_5() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___videoOverlay_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoOverlay_5() const { return ___videoOverlay_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoOverlay_5() { return &___videoOverlay_5; }
	inline void set_videoOverlay_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoOverlay_5 = value;
		Il2CppCodeGenWriteBarrier((&___videoOverlay_5), value);
	}

	inline static int32_t get_offset_of_usbPowerImage_6() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___usbPowerImage_6)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_usbPowerImage_6() const { return ___usbPowerImage_6; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_usbPowerImage_6() { return &___usbPowerImage_6; }
	inline void set_usbPowerImage_6(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___usbPowerImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___usbPowerImage_6), value);
	}

	inline static int32_t get_offset_of_zipperExpImage_7() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___zipperExpImage_7)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_zipperExpImage_7() const { return ___zipperExpImage_7; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_zipperExpImage_7() { return &___zipperExpImage_7; }
	inline void set_zipperExpImage_7(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___zipperExpImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___zipperExpImage_7), value);
	}

	inline static int32_t get_offset_of_frontPocketImage_8() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___frontPocketImage_8)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_frontPocketImage_8() const { return ___frontPocketImage_8; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_frontPocketImage_8() { return &___frontPocketImage_8; }
	inline void set_frontPocketImage_8(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___frontPocketImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___frontPocketImage_8), value);
	}

	inline static int32_t get_offset_of_tracerImage_9() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___tracerImage_9)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_tracerImage_9() const { return ___tracerImage_9; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_tracerImage_9() { return &___tracerImage_9; }
	inline void set_tracerImage_9(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___tracerImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___tracerImage_9), value);
	}

	inline static int32_t get_offset_of_hangerImage_10() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___hangerImage_10)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_hangerImage_10() const { return ___hangerImage_10; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_hangerImage_10() { return &___hangerImage_10; }
	inline void set_hangerImage_10(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___hangerImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___hangerImage_10), value);
	}

	inline static int32_t get_offset_of_wheelImage_11() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___wheelImage_11)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_wheelImage_11() const { return ___wheelImage_11; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_wheelImage_11() { return &___wheelImage_11; }
	inline void set_wheelImage_11(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___wheelImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___wheelImage_11), value);
	}

	inline static int32_t get_offset_of_movingSuitcase_12() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___movingSuitcase_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_movingSuitcase_12() const { return ___movingSuitcase_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_movingSuitcase_12() { return &___movingSuitcase_12; }
	inline void set_movingSuitcase_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___movingSuitcase_12 = value;
		Il2CppCodeGenWriteBarrier((&___movingSuitcase_12), value);
	}

	inline static int32_t get_offset_of_layingSuitcase_13() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___layingSuitcase_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_layingSuitcase_13() const { return ___layingSuitcase_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_layingSuitcase_13() { return &___layingSuitcase_13; }
	inline void set_layingSuitcase_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___layingSuitcase_13 = value;
		Il2CppCodeGenWriteBarrier((&___layingSuitcase_13), value);
	}

	inline static int32_t get_offset_of_dualAccessSuitcase_14() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___dualAccessSuitcase_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_dualAccessSuitcase_14() const { return ___dualAccessSuitcase_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_dualAccessSuitcase_14() { return &___dualAccessSuitcase_14; }
	inline void set_dualAccessSuitcase_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___dualAccessSuitcase_14 = value;
		Il2CppCodeGenWriteBarrier((&___dualAccessSuitcase_14), value);
	}

	inline static int32_t get_offset_of_mainCamera_15() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___mainCamera_15)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_15() const { return ___mainCamera_15; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_15() { return &___mainCamera_15; }
	inline void set_mainCamera_15(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_15 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_15), value);
	}

	inline static int32_t get_offset_of_fList_16() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___fList_16)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_fList_16() const { return ___fList_16; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_fList_16() { return &___fList_16; }
	inline void set_fList_16(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___fList_16 = value;
		Il2CppCodeGenWriteBarrier((&___fList_16), value);
	}

	inline static int32_t get_offset_of_f7List_17() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___f7List_17)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_f7List_17() const { return ___f7List_17; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_f7List_17() { return &___f7List_17; }
	inline void set_f7List_17(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___f7List_17 = value;
		Il2CppCodeGenWriteBarrier((&___f7List_17), value);
	}

	inline static int32_t get_offset_of_dList_18() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___dList_18)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_dList_18() const { return ___dList_18; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_dList_18() { return &___dList_18; }
	inline void set_dList_18(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___dList_18 = value;
		Il2CppCodeGenWriteBarrier((&___dList_18), value);
	}

	inline static int32_t get_offset_of_initCameraAngle_19() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___initCameraAngle_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_initCameraAngle_19() const { return ___initCameraAngle_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_initCameraAngle_19() { return &___initCameraAngle_19; }
	inline void set_initCameraAngle_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___initCameraAngle_19 = value;
	}

	inline static int32_t get_offset_of_initFoV_20() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___initFoV_20)); }
	inline float get_initFoV_20() const { return ___initFoV_20; }
	inline float* get_address_of_initFoV_20() { return &___initFoV_20; }
	inline void set_initFoV_20(float value)
	{
		___initFoV_20 = value;
	}

	inline static int32_t get_offset_of_fIndex_21() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___fIndex_21)); }
	inline int32_t get_fIndex_21() const { return ___fIndex_21; }
	inline int32_t* get_address_of_fIndex_21() { return &___fIndex_21; }
	inline void set_fIndex_21(int32_t value)
	{
		___fIndex_21 = value;
	}

	inline static int32_t get_offset_of_zoomScript_22() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___zoomScript_22)); }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * get_zoomScript_22() const { return ___zoomScript_22; }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 ** get_address_of_zoomScript_22() { return &___zoomScript_22; }
	inline void set_zoomScript_22(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * value)
	{
		___zoomScript_22 = value;
		Il2CppCodeGenWriteBarrier((&___zoomScript_22), value);
	}

	inline static int32_t get_offset_of_zoomOutScript_23() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___zoomOutScript_23)); }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * get_zoomOutScript_23() const { return ___zoomOutScript_23; }
	inline Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 ** get_address_of_zoomOutScript_23() { return &___zoomOutScript_23; }
	inline void set_zoomOutScript_23(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * value)
	{
		___zoomOutScript_23 = value;
		Il2CppCodeGenWriteBarrier((&___zoomOutScript_23), value);
	}

	inline static int32_t get_offset_of_cameraScript_24() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___cameraScript_24)); }
	inline TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * get_cameraScript_24() const { return ___cameraScript_24; }
	inline TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 ** get_address_of_cameraScript_24() { return &___cameraScript_24; }
	inline void set_cameraScript_24(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * value)
	{
		___cameraScript_24 = value;
		Il2CppCodeGenWriteBarrier((&___cameraScript_24), value);
	}

	inline static int32_t get_offset_of_closeSuitcaseScript_25() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___closeSuitcaseScript_25)); }
	inline CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5 * get_closeSuitcaseScript_25() const { return ___closeSuitcaseScript_25; }
	inline CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5 ** get_address_of_closeSuitcaseScript_25() { return &___closeSuitcaseScript_25; }
	inline void set_closeSuitcaseScript_25(CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5 * value)
	{
		___closeSuitcaseScript_25 = value;
		Il2CppCodeGenWriteBarrier((&___closeSuitcaseScript_25), value);
	}

	inline static int32_t get_offset_of_openSuitcaseScript_26() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___openSuitcaseScript_26)); }
	inline OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC * get_openSuitcaseScript_26() const { return ___openSuitcaseScript_26; }
	inline OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC ** get_address_of_openSuitcaseScript_26() { return &___openSuitcaseScript_26; }
	inline void set_openSuitcaseScript_26(OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC * value)
	{
		___openSuitcaseScript_26 = value;
		Il2CppCodeGenWriteBarrier((&___openSuitcaseScript_26), value);
	}

	inline static int32_t get_offset_of_rotateFrontScript_27() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___rotateFrontScript_27)); }
	inline RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * get_rotateFrontScript_27() const { return ___rotateFrontScript_27; }
	inline RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 ** get_address_of_rotateFrontScript_27() { return &___rotateFrontScript_27; }
	inline void set_rotateFrontScript_27(RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03 * value)
	{
		___rotateFrontScript_27 = value;
		Il2CppCodeGenWriteBarrier((&___rotateFrontScript_27), value);
	}

	inline static int32_t get_offset_of_rotateBottomScript_28() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___rotateBottomScript_28)); }
	inline RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1 * get_rotateBottomScript_28() const { return ___rotateBottomScript_28; }
	inline RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1 ** get_address_of_rotateBottomScript_28() { return &___rotateBottomScript_28; }
	inline void set_rotateBottomScript_28(RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1 * value)
	{
		___rotateBottomScript_28 = value;
		Il2CppCodeGenWriteBarrier((&___rotateBottomScript_28), value);
	}

	inline static int32_t get_offset_of_rotateBackScript_29() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___rotateBackScript_29)); }
	inline RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 * get_rotateBackScript_29() const { return ___rotateBackScript_29; }
	inline RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 ** get_address_of_rotateBackScript_29() { return &___rotateBackScript_29; }
	inline void set_rotateBackScript_29(RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483 * value)
	{
		___rotateBackScript_29 = value;
		Il2CppCodeGenWriteBarrier((&___rotateBackScript_29), value);
	}

	inline static int32_t get_offset_of_layDownSuitcase_30() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___layDownSuitcase_30)); }
	inline LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3 * get_layDownSuitcase_30() const { return ___layDownSuitcase_30; }
	inline LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3 ** get_address_of_layDownSuitcase_30() { return &___layDownSuitcase_30; }
	inline void set_layDownSuitcase_30(LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3 * value)
	{
		___layDownSuitcase_30 = value;
		Il2CppCodeGenWriteBarrier((&___layDownSuitcase_30), value);
	}

	inline static int32_t get_offset_of_layDownSuitcaseClose_31() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___layDownSuitcaseClose_31)); }
	inline LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2 * get_layDownSuitcaseClose_31() const { return ___layDownSuitcaseClose_31; }
	inline LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2 ** get_address_of_layDownSuitcaseClose_31() { return &___layDownSuitcaseClose_31; }
	inline void set_layDownSuitcaseClose_31(LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2 * value)
	{
		___layDownSuitcaseClose_31 = value;
		Il2CppCodeGenWriteBarrier((&___layDownSuitcaseClose_31), value);
	}

	inline static int32_t get_offset_of_pullHandle_32() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___pullHandle_32)); }
	inline PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379 * get_pullHandle_32() const { return ___pullHandle_32; }
	inline PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379 ** get_address_of_pullHandle_32() { return &___pullHandle_32; }
	inline void set_pullHandle_32(PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379 * value)
	{
		___pullHandle_32 = value;
		Il2CppCodeGenWriteBarrier((&___pullHandle_32), value);
	}

	inline static int32_t get_offset_of_flyHanger_33() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___flyHanger_33)); }
	inline FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A * get_flyHanger_33() const { return ___flyHanger_33; }
	inline FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A ** get_address_of_flyHanger_33() { return &___flyHanger_33; }
	inline void set_flyHanger_33(FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A * value)
	{
		___flyHanger_33 = value;
		Il2CppCodeGenWriteBarrier((&___flyHanger_33), value);
	}

	inline static int32_t get_offset_of_pendingRoutine_34() { return static_cast<int32_t>(offsetof(SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458, ___pendingRoutine_34)); }
	inline RuntimeObject* get_pendingRoutine_34() const { return ___pendingRoutine_34; }
	inline RuntimeObject** get_address_of_pendingRoutine_34() { return &___pendingRoutine_34; }
	inline void set_pendingRoutine_34(RuntimeObject* value)
	{
		___pendingRoutine_34 = value;
		Il2CppCodeGenWriteBarrier((&___pendingRoutine_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUITCASECONTROL_T1484B9A35CD0D02EF5179305540D4EC1CD7C2458_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2704[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2711[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2720[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[3] = 
{
	NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90::get_offset_of_console_4(),
	NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90::get_offset_of_texture_5(),
	NativeToolkitExample_t4708B6A63253E5252BDCDF33B96A233837E02F90::get_offset_of_imagePath_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F), -1, sizeof(NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2724[10] = 
{
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnScreenshotTaken_4(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnScreenshotSaved_5(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnImageSaved_6(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnImagePicked_7(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnDialogComplete_8(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnRateComplete_9(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnCameraShotComplete_10(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_OnContactPicked_11(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_instance_12(),
	NativeToolkit_tE361B16825D9BEE76F18BBF40DA2D217B34BBD3F_StaticFields::get_offset_of_go_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[3] = 
{
	ImageType_t18E3FD0F43F4CE77C8D94F4627D48157BFBCD130::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2726[5] = 
{
	SaveStatus_t375ABFF625C7E35D370185AF24E13EE1D6B66BEC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[5] = 
{
	U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B::get_offset_of_U3CU3E1__state_0(),
	U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B::get_offset_of_U3CU3E2__current_1(),
	U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B::get_offset_of_screenArea_2(),
	U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B::get_offset_of_fileType_3(),
	U3CGrabScreenshotU3Ed__48_t60270DDAAE5D04C3B1C635C16F158B14BEAA509B::get_offset_of_fileName_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[7] = 
{
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_U3CU3E1__state_0(),
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_U3CU3E2__current_1(),
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_path_2(),
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_bytes_3(),
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_imageType_4(),
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_U3CcountU3E5__2_5(),
	U3CSaveU3Ed__50_tE38DB3144F095258C3548DD2EDFD79D5D1F269E7::get_offset_of_U3CsavedU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2729[4] = 
{
	U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E::get_offset_of_U3CU3E1__state_0(),
	U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E::get_offset_of_U3CU3E2__current_1(),
	U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E::get_offset_of_delay_2(),
	U3CWaitU3Ed__72_t0A5F586743F91C0D371ACDE454E07ED03D80400E::get_offset_of_U3CpauseTargetU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2730[26] = 
{
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_front_4(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_bottom_5(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_mainPocket_6(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontCover_7(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_isTriggered_8(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_isTriggering_9(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontInitV3_10(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_bottomInitV3_11(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_mainPocketInitV3_12(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontCoverInitV3_13(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontTargetV3_14(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_bottomTargetV3_15(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_mainPocketTargetV3_16(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontCoverTargetV3_17(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontRotationV3_18(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_bottomRotationV3_19(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_mainPocketRotationtV3_20(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontCoverRotationtV3_21(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontNR_22(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_bottomNR_23(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_mainPocketNR_24(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontCoverNR_25(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontRCount_26(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_bottomRCount_27(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_mainPocketRCount_28(),
	CloseSuitcase_tED1BE400BA10B9A96CED8ED24A9D106FB682C5A5::get_offset_of_frontCoverRCount_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2731[8] = 
{
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_handle_4(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_isTriggered_5(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_isTriggering_6(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_handleInitV3_7(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_handleTargetV3_8(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_handleTranslateV3_9(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_handleNR_10(),
	FlyHanger_tEE9BF9BA969EDF91FCB93BBE504BB185393B8A9A::get_offset_of_handleRCount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[8] = 
{
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_handle_4(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_isTriggered_5(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_isTriggering_6(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_handleInitV3_7(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_handleTargetV3_8(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_handleTranslateV3_9(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_handleNR_10(),
	FlyHangerReverse_t11FE92886A2C55E7C49CD80791DB01CBDEDBA6FA::get_offset_of_handleRCount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[25] = 
{
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_front_4(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_bottom_5(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_mainPocket_6(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_isTriggered_7(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_isTriggering_8(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_thisInitV3_9(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_frontInitV3_10(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_bottomInitV3_11(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_mainPocketInitV3_12(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_thisTargetV3_13(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_frontTargetV3_14(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_bottomTargetV3_15(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_mainPocketTargetV3_16(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_thisRotationV3_17(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_frontRotationV3_18(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_bottomRotationV3_19(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_mainPocketRotationtV3_20(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_thisNR_21(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_frontNR_22(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_bottomNR_23(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_mainPocketNR_24(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_thisRCount_25(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_frontRCount_26(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_bottomRCount_27(),
	LayDownSuitcase_t8BC596DA5382C87683030B463F10084C26325FC3::get_offset_of_mainPocketRCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[25] = 
{
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_front_4(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_bottom_5(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_mainPocket_6(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_isTriggered_7(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_isTriggering_8(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_thisInitV3_9(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_frontInitV3_10(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_bottomInitV3_11(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_mainPocketInitV3_12(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_thisTargetV3_13(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_frontTargetV3_14(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_bottomTargetV3_15(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_mainPocketTargetV3_16(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_thisRotationV3_17(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_frontRotationV3_18(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_bottomRotationV3_19(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_mainPocketRotationtV3_20(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_thisNR_21(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_frontNR_22(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_bottomNR_23(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_mainPocketNR_24(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_thisRCount_25(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_frontRCount_26(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_bottomRCount_27(),
	LayDownSuitcaseHalfToFull_tA28DED4A10A6A24B34806D101CFE80DA584AF482::get_offset_of_mainPocketRCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[25] = 
{
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_front_4(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_bottom_5(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_mainPocket_6(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_isTriggered_7(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_isTriggering_8(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_thisInitV3_9(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_frontInitV3_10(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_bottomInitV3_11(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_mainPocketInitV3_12(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_thisTargetV3_13(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_frontTargetV3_14(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_bottomTargetV3_15(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_mainPocketTargetV3_16(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_thisRotationV3_17(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_frontRotationV3_18(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_bottomRotationV3_19(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_mainPocketRotationtV3_20(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_thisNR_21(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_frontNR_22(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_bottomNR_23(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_mainPocketNR_24(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_thisRCount_25(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_frontRCount_26(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_bottomRCount_27(),
	LayDownSuitcaseHalfToFullReverse_t3F5F664DFE7E764B94E05C8DE5E9B30BCA7B4A09::get_offset_of_mainPocketRCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[25] = 
{
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_front_4(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_bottom_5(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_mainPocket_6(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_isTriggered_7(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_isTriggering_8(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_thisInitV3_9(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_frontInitV3_10(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_bottomInitV3_11(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_mainPocketInitV3_12(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_thisTargetV3_13(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_frontTargetV3_14(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_bottomTargetV3_15(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_mainPocketTargetV3_16(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_thisRotationV3_17(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_frontRotationV3_18(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_bottomRotationV3_19(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_mainPocketRotationtV3_20(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_thisNR_21(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_frontNR_22(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_bottomNR_23(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_mainPocketNR_24(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_thisRCount_25(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_frontRCount_26(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_bottomRCount_27(),
	LayDownSuitcaseOpenFull_t03B90B6F984897684EB0C39DEEAEAE83C061DA2D::get_offset_of_mainPocketRCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { sizeof (LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[25] = 
{
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_front_4(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_bottom_5(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_mainPocket_6(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_isTriggered_7(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_isTriggering_8(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_thisInitV3_9(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_frontInitV3_10(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_bottomInitV3_11(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_mainPocketInitV3_12(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_thisTargetV3_13(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_frontTargetV3_14(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_bottomTargetV3_15(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_mainPocketTargetV3_16(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_thisRotationV3_17(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_frontRotationV3_18(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_bottomRotationV3_19(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_mainPocketRotationtV3_20(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_thisNR_21(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_frontNR_22(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_bottomNR_23(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_mainPocketNR_24(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_thisRCount_25(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_frontRCount_26(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_bottomRCount_27(),
	LayDownSuitcaseOpenFullReverse_tAE474AB5DB6F2AC120F8D0863816B549DD4D30C2::get_offset_of_mainPocketRCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[25] = 
{
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_front_4(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_bottom_5(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_mainPocket_6(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_isTriggered_7(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_isTriggering_8(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_thisInitV3_9(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_frontInitV3_10(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_bottomInitV3_11(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_mainPocketInitV3_12(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_thisTargetV3_13(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_frontTargetV3_14(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_bottomTargetV3_15(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_mainPocketTargetV3_16(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_thisRotationV3_17(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_frontRotationV3_18(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_bottomRotationV3_19(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_mainPocketRotationtV3_20(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_thisNR_21(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_frontNR_22(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_bottomNR_23(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_mainPocketNR_24(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_thisRCount_25(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_frontRCount_26(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_bottomRCount_27(),
	LayDownSuitcaseReverse_t86CE0549E95B90BC9A7B70DB27C8BCAA9EA46E72::get_offset_of_mainPocketRCount_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[20] = 
{
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_front_4(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_bottom_5(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_mainPocket_6(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_isTriggered_7(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_isTriggering_8(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_frontInitV3_9(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_bottomInitV3_10(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_mainPocketInitV3_11(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_frontTargetV3_12(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_bottomTargetV3_13(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_mainPocketTargetV3_14(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_frontRotationV3_15(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_bottomRotationV3_16(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_mainPocketRotationtV3_17(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_frontNR_18(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_bottomNR_19(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_mainPocketNR_20(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_frontRCount_21(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_bottomRCount_22(),
	OpenSuitcase_tBA942EA8EE32F4F769CE5226982507C2D0CE09CC::get_offset_of_mainPocketRCount_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[8] = 
{
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_backpack_4(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_isTriggered_5(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_isTriggering_6(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_backpackInitV3_7(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_backpackTargetV3_8(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_backpackTranslateV3_9(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_backpackNR_10(),
	PullBackpackDownToSuitcase_tC4B6C719A7E08FBA5DF9B5E80D10C904524AD268::get_offset_of_backpackRCount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[8] = 
{
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_backpack_4(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_isTriggered_5(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_isTriggering_6(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_backpackInitV3_7(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_backpackTargetV3_8(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_backpackTranslateV3_9(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_backpackNR_10(),
	PullBakpackDownToSuitcaseReverse_t0EB8970BACE3576FC6A5FF857B14A835FB4E0E43::get_offset_of_backpackRCount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[9] = 
{
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_handle_4(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_isTriggered_5(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_isTriggering_6(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_handleInitV3_7(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_handleTargetV3_8(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_handleTranslateV3_9(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_handleNR_10(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_handleRCount_11(),
	PullHandle_tB8A75F932095398EE5B38BBDCDB47713B7965379::get_offset_of_waited_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[8] = 
{
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_handle_4(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_isTriggered_5(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_isTriggering_6(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_handleInitV3_7(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_handleTargetV3_8(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_handleTranslateV3_9(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_handleNR_10(),
	PullHandleReverse_tEEFFC78F7BDEF4AC7583E4C713CE7986089DCCC4::get_offset_of_handleRCount_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[4] = 
{
	ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE::get_offset_of_front_4(),
	ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE::get_offset_of_bottom_5(),
	ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE::get_offset_of_mainPocket_6(),
	ResetSuitcase_t9B49849D0C79AE1D44622124F7BB483A4B02E3FE::get_offset_of_isTriggered_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[7] = 
{
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_isTriggered_4(),
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_isTriggering_5(),
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_thisInitV3_6(),
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_thisTargetV3_7(),
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_thisRotationV3_8(),
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_thisNR_9(),
	RotateBack_tBD721F8DFDD2B59FD333EA05EC6BEC437792A483::get_offset_of_thisRCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[11] = 
{
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_isTriggered_4(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_isTriggering_5(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisInitV3_6(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisTargetV3_7(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisTarget2V3_8(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisRotationV3_9(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisRotation2V3_10(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisNR_11(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisNR2_12(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisRCount_13(),
	RotateBottom_t9DE19AB45DDDD715CA38B383EDE17845615FE0F1::get_offset_of_thisRCount2_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { sizeof (RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[7] = 
{
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_isTriggered_4(),
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_isTriggering_5(),
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_thisInitV3_6(),
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_thisTargetV3_7(),
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_thisRotationV3_8(),
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_thisNR_9(),
	RotateFront_tA1D619428FF46A0E6482254AFCEB7F14C3A2CE03::get_offset_of_thisRCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[7] = 
{
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_isTriggered_4(),
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_isTriggering_5(),
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_thisInitV3_6(),
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_thisTargetV3_7(),
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_thisRotationV3_8(),
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_thisNR_9(),
	RotateFront45_t2D69F0D1BF7188FF6C96AF39F209FB0FBE6DCE13::get_offset_of_thisRCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[7] = 
{
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_isTriggered_4(),
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_isTriggering_5(),
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_thisInitV3_6(),
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_thisTargetV3_7(),
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_thisRotationV3_8(),
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_thisNR_9(),
	RotateFrontReverse_tC98645E69053C367CF848E41A3E4977F51C3A705::get_offset_of_thisRCount_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[26] = 
{
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_videoOverlay_5(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_rainProofImage_6(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_frontPocketImage_7(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_tracerImage_8(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_waterProofPocketImage_9(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_uzipImage_10(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_backpackClose_11(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_backpackHalfOpen_12(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_backpackMainOpen_13(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_backpackOpen_14(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_backpackWithSuitcase_15(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_mainCamera_16(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_fList_17(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_cList_18(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_dList_19(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_initCameraAngle_20(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_initFoV_21(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_fIndex_22(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_zoomScript_23(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_zoomOutScript_24(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_cameraScript_25(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_pullBackpackDownToSuitcase_26(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_rotateFrontScript_27(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_closedRotateFrontScript_28(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_rotateFront45Script_29(),
	BackpackControl_tEDD5E4AB9CCA17F951BC9C004641504FCC16097E::get_offset_of_rotateBackScript_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (BaseControl_tA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[1] = 
{
	BaseControl_tA80FAE98EF4187F0C02A06E15CA0EBABDF1AA171::get_offset_of_freeViewIcon_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[4] = 
{
	U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098::get_offset_of_U3CU3E1__state_0(),
	U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098::get_offset_of_U3CU3E2__current_1(),
	U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098::get_offset_of_time_2(),
	U3CWaitAndDoU3Ed__3_tD3943973C2F0F450353630CBA3C912A07AB2D098::get_offset_of_action_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[5] = 
{
	U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427::get_offset_of_U3CU3E1__state_0(),
	U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427::get_offset_of_U3CU3E2__current_1(),
	U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427::get_offset_of_obj_2(),
	U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427::get_offset_of_U3CiU3E5__2_3(),
	U3CblinkU3Ed__4_t2B5BE89E6414DDCCD4FD554FC1F28EA984BEC427::get_offset_of_U3CrendererU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[30] = 
{
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_videoOverlay_5(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_usbPowerImage_6(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_zipperExpImage_7(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_frontPocketImage_8(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_tracerImage_9(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_hangerImage_10(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_wheelImage_11(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_movingSuitcase_12(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_layingSuitcase_13(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_dualAccessSuitcase_14(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_mainCamera_15(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_fList_16(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_f7List_17(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_dList_18(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_initCameraAngle_19(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_initFoV_20(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_fIndex_21(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_zoomScript_22(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_zoomOutScript_23(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_cameraScript_24(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_closeSuitcaseScript_25(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_openSuitcaseScript_26(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_rotateFrontScript_27(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_rotateBottomScript_28(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_rotateBackScript_29(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_layDownSuitcase_30(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_layDownSuitcaseClose_31(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_pullHandle_32(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_flyHanger_33(),
	SuitcaseControl_t1484B9A35CD0D02EF5179305540D4EC1CD7C2458::get_offset_of_pendingRoutine_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[15] = 
{
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_mainCamera_4(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_rotationPlate_5(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_rotationOffsetPlate_6(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_minFoV_7(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_maxFoV_8(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_isEnabled_9(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_isOnlyZoomable_10(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_isOnlyRotate_11(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_isLimitYAngle_12(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_limitYAngleClockwise_13(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_limitYAngleAClockwise_14(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_isRotationOffseted_15(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_oldTouchPositions_16(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_oldTouchVector_17(),
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166::get_offset_of_oldTouchDistance_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[13] = 
{
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_mainCamera_4(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_zoomInObject_5(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_isTriggered_6(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_initFieldOfView_7(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_targetFieldOfView_8(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_nStep_9(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_waitForSeconds_10(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_waited_11(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_isTriggering_12(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_trigger_13(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_cameraNR_14(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_cameraRCount_15(),
	Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0::get_offset_of_step_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67), -1, sizeof(ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2757[5] = 
{
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_FeatheringWidth_4(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields::get_offset_of_s_FeatheringUVs_5(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67_StaticFields::get_offset_of_s_Vertices_6(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_PlaneMeshVisualizer_7(),
	ARFeatheredPlaneMeshVisualizer_tE778AE37070D3D5969CFD0FEEE8826992D092A67::get_offset_of_m_FeatheredPlaneMaterial_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[7] = 
{
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_ARSession_4(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_ErrorText_5(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LogText_6(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_MappingStatusText_7(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_SaveButton_8(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LoadButton_9(),
	ARWorldMapController_tE07FBF1307D659E7AB45F66CFBA14CAC07C98737::get_offset_of_m_LogMessages_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[4] = 
{
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E1__state_0(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E2__current_1(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CU3E4__this_2(),
	U3CSaveU3Ed__27_tAAAF45083EB844EA39B7CB4CF64187F93C5A676B::get_offset_of_U3CrequestU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[8] = 
{
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CsessionSubsystemU3E5__2_3(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbytesPerFrameU3E5__3_4(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbytesRemainingU3E5__4_5(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CbinaryReaderU3E5__5_6(),
	U3CLoadU3Ed__28_t5A8E074BB5BE5EF0FAFAFD9EE65CED8B497CFA6D::get_offset_of_U3CallBytesU3E5__6_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[3] = 
{
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14::get_offset_of_oldTouchPositions_4(),
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14::get_offset_of_oldTouchVector_5(),
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14::get_offset_of_oldTouchDistance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[2] = 
{
	CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B::get_offset_of_m_ConfigurationNames_4(),
	CameraConfigController_t7D9D87FE02FA864F391F3BB2B9E9C7923F85279B::get_offset_of_m_Dropdown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[1] = 
{
	DisableVerticalPlanes_t40082019580280FBDED0DF62CC5DA4E1A8E32D19::get_offset_of_m_LogText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[3] = 
{
	fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425::get_offset_of_FadeRate_4(),
	fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425::get_offset_of_image_5(),
	fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425::get_offset_of_targetAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[4] = 
{
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CbrightnessU3Ek__BackingField_4(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CcolorTemperatureU3Ek__BackingField_5(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_U3CcolorCorrectionU3Ek__BackingField_6(),
	LightEstimation_tAC157B297F822653381F8F35A6DE3600920A4319::get_offset_of_m_Light_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { sizeof (LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[5] = 
{
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_BrightnessText_4(),
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_ColorTemperatureText_5(),
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_ColorCorrectionText_6(),
	0,
	LightEstimationUI_tABE99F2D6E580AB4AD14174CE323C527882D6DA4::get_offset_of_m_LightEstimation_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3), -1, sizeof(MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2767[4] = 
{
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3::get_offset_of_m_Content_4(),
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3::get_offset_of_m_Rotation_5(),
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3_StaticFields::get_offset_of_s_Hits_6(),
	MakeAppearOnPlane_tEFCA4242B61BEEA04B85419F892D7506BF9E7EB3::get_offset_of_m_SessionOrigin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC), -1, sizeof(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2768[5] = 
{
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_m_PlacedPrefab_4(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_U3CspawnedObjectU3Ek__BackingField_5(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields::get_offset_of_onPlacedObject_6(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC::get_offset_of_m_SessionOrigin_7(),
	PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_StaticFields::get_offset_of_s_Hits_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2), -1, sizeof(PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2769[4] = 
{
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_m_PlacedPrefab_4(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_U3CspawnedObjectU3Ek__BackingField_5(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2_StaticFields::get_offset_of_s_Hits_6(),
	PlaceOnPlane_tC5323EAA9F7603ADAB59DB462174996553CAF0C2::get_offset_of_m_SessionOrigin_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD), -1, sizeof(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2770[3] = 
{
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD::get_offset_of_m_TogglePlaneDetectionText_4(),
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD::get_offset_of_m_ARPlaneManager_5(),
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD_StaticFields::get_offset_of_s_Planes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[5] = 
{
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_MakeAppearOnPlane_4(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Slider_5(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Text_6(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Min_7(),
	RotationController_t41CB9629E51ECA40E7A0DC705A60605DE218C4EB::get_offset_of_m_Max_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[5] = 
{
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Slider_4(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Text_5(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Min_6(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_Max_7(),
	ScaleController_t4CADE3F801EC93C79203B3407E4086AA4BC3FE1A::get_offset_of_m_SessionOrigin_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (SceneController_t5827DDFEB507363FD30290DC041EEE4EAE6251E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (ScreenCapture_t2374541D6035304D83868BE2B54D3C747DF22963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[1] = 
{
	SetTargetFramerate_tCDEC437C7B06BF4E2E8DF4EE1D9912E5D02DB8A0::get_offset_of_m_TargetFrameRate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[3] = 
{
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_RawImage_4(),
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_ImageInfo_5(),
	TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6::get_offset_of_m_Texture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[7] = 
{
	0,
	0,
	0,
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_Animator_7(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_Plane_8(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_ShowTime_9(),
	FadePlaneOnBoundaryChange_t88629D7DC409498EABE1D1CCE054157CBE47CD7A::get_offset_of_m_UpdatingPlane_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C), -1, sizeof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2778[8] = 
{
	0,
	0,
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_PlaneManager_6(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_MoveDeviceAnimation_7(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_TapToPlaceAnimation_8(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields::get_offset_of_s_Planes_9(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_ShowingTapToPlace_10(),
	UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C::get_offset_of_m_ShowingMoveDevice_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[34] = 
{
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_Url_4(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_ShouldAutoPlay_5(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of__audioVolume_6(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_audioSourceOffset_7(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_flipHandedness_8(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of__clockScale_9(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_computeNormals_10(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_Settings_11(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_DefaultMaxVertexCount_12(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_DefaultMaxIndexCount_13(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_localSpaceBounds_14(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_meshes_15(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_meshFilter_16(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_meshRenderer_17(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_HVCollider_18(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_material_19(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_listener_20(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_pluginInterop_21(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_isInitialized_22(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_openInfo_23(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_fileInfo_24(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_lastFrameInfo_25(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnOpenNotify_26(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnUpdateFrameInfoNotify_27(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnRenderCallback_28(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnFatalError_29(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_OnEndOfStreamNotify_30(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_UnityBufferCoroutine_31(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_logger_32(),
	0,
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_wasPlaying_34(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_ShouldPauseAfterPlay_35(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_PauseFrameID_36(),
	HoloVideoObject_t9E0468A6578F63E3CA9A486E75BEDCAD24E6CFDB::get_offset_of_isInstanceDisposed_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (OnOpenEvent_t9A81B382CF60B0B61864AD817C74BD2D1B6F1DF1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (OnFrameInfoEvent_t254A414B4783E99BE1CDD836552B5BEE9BB14C3F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (OnRenderEvent_tEBC1B611E970CED1D3F6AE62BCC833366AAC60F6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (OnEndOfStream_t6ABE2224F14F21D78185646112D85DE309AA9091), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (OnFatalErrorEvent_tC7F367BBE1A9C66FCD14A7F2FFB0CDAC071FEE67), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[3] = 
{
	U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017::get_offset_of_U3CU3E1__state_0(),
	U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017::get_offset_of_U3CU3E2__current_1(),
	U3CFillUnityBuffersU3Ed__48_t43B9E283B7914A96EDF86C9C3B4E39161E387017::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[3] = 
{
	HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E::get_offset_of_ShouldLoopSequences_4(),
	HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E::get_offset_of_sequenceList_5(),
	HVConductor_t966F2F4E311B562E0F5CE66AEEC767CD63E9D59E::get_offset_of_currSequence_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[1] = 
{
	HVSequence_tA5A0E5C759DEDC1FE7B4B517AB3523A256C96BA1::get_offset_of_VideosToLoad_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F)+ sizeof (RuntimeObject), sizeof(HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F ), 0, 0 };
extern const int32_t g_FieldOffsetTable2788[2] = 
{
	HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F::get_offset_of_defaultMaxVertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HCapSettingsInterop_tD574A2802911FCB0A5F59921EE9264B708ECDF4F::get_offset_of_defaultMaxIndexCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9)+ sizeof (RuntimeObject), sizeof(SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2789[16] = 
{
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_AudioDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_UseHWTexture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_UseHWDecode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_UseKeyedMutex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_RenderViaClock_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_OutputNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_StartDownloadOnOpen_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_AutoLooping_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_lockHWTextures_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_forceSoftwareClock_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_PlaybackRate_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFMinGain_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFMaxGain_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFGainDistance_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_HRTFCutoffDistance_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFOpenInfo_tACB064007773FBC261C9EDBA22DA78C6D1C367F9::get_offset_of_AudioDeviceId_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366)+ sizeof (RuntimeObject), sizeof(SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[16] = 
{
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_hasAudio_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_duration100ns_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_frameCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxVertexCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxIndexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_bitrateMbps_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_fileSize_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_minX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_minY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_minZ_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxX_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxY_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_maxZ_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_fileWidth_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_fileHeight_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFileInfo_tD86F29AD52536786393A9657E2DB669F60A32366::get_offset_of_hasNormals_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { sizeof (SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2791[12] = 
{
	SVFReaderStateInterop_t7766360500DB4DD4843D984E227AB74F026ACEA7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2792[6] = 
{
	VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696)+ sizeof (RuntimeObject), sizeof(SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2793[6] = 
{
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_isLiveSVFSource_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_lastReadFrame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_unsuccessfulReadFrameCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_droppedFrameCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_errorHresult_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFStatusInterop_tABB537FA071DFAC368BC5DB4720FA1B92B181696::get_offset_of_lastKnownState_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E)+ sizeof (RuntimeObject), sizeof(SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2794[15] = 
{
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_frameTimestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_minX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_minY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_minZ_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_maxX_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_maxY_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_maxZ_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_frameId_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_vertexCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_indexCount_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_textureWidth_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_textureHeight_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_isEOS_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_isRepeatedFrame_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SVFFrameInfo_t53F94EC39C2B091B9624B7FD6B799BFBFD1DC26E::get_offset_of_isKeyFrame_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2)+ sizeof (RuntimeObject), sizeof(Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2795[16] = 
{
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m00_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m01_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m02_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m03_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m10_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m11_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m12_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m13_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m20_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m21_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m22_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m23_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m30_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m31_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m32_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix4x4PluginInterop_t66E8E2EC5E470D261DFBA691CBC8244F001820B2::get_offset_of_m33_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA)+ sizeof (RuntimeObject), sizeof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA ), 0, 0 };
extern const int32_t g_FieldOffsetTable2796[3] = 
{
	Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2797[8] = 
{
	SVFPlaybackState_t0D3DA5DF6EED7D29EE64D6C40C895D3155698B6F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2798[6] = 
{
	LogLevel_t614E5F716939DAB7D93BA30FEC1372E436EC62DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036)+ sizeof (RuntimeObject), sizeof(AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2799[2] = 
{
	AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AudioDeviceInfo_t105929EDE9CEF6D58A0CBF914F72C1C1B608D036::get_offset_of_Id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
