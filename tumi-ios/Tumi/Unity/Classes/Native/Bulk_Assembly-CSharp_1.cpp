﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<UnityEngine.Experimental.XR.FrameReceivedEventArgs>
struct Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC;
// System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>
struct Action_1_t386B526A237C5426A1502ADF2690D01DCC513454;
// System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs>
struct Action_1_t00C9993E42C217DC2FE1E88A5476EB13BCF14CCD;
// System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneRemovedEventArgs>
struct Action_1_tD8609389570174CF9943507CE0ABA181614FC950;
// System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneUpdatedEventArgs>
struct Action_1_tC5CF6AA49CB1A161A0AA9BFEE63D1C976B909722;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.XR.TrackableId,UnityEngine.XR.ARFoundation.ARPlane>
struct Dictionary_2_t994DCE812EA0005977731A1258D38DD3042B4069;
// System.Collections.Generic.HashSet`1<UnityEngine.Experimental.XR.TrackableId>
struct HashSet_1_t46CBD5E236B1CCC32C5AD60B818E2AE07AF6F05F;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Experimental.XR.BoundedPlane>
struct List_1_t54A25059B09A42125C1A8C6CC316E299E958548E;
// System.Collections.Generic.List`1<UnityEngine.Experimental.XR.TrackableId>
struct List_1_tB042082FE5AEFD0E6DD62F19F486F8CEC769D1CD;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>
struct List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TestCameraImage
struct TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6;
// TouchCamera
struct TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14;
// TouchCameraWithRotation
struct TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166;
// UIManager
struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.Experimental.XR.XRCameraSubsystem
struct XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.TextGenerator
struct TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.FontData
struct FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.XR.ARExtensions.ICameraImageApi
struct ICameraImageApi_tBAC003481E172628D623F3F9E7CFB8D2FD84EEE4;
// UnityEngine.XR.ARExtensions.XRCameraExtensions/OnImageRequestCompleteDelegate
struct OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2;
// UnityEngine.XR.ARFoundation.ARPlaneManager
struct ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A;
// UnityEngine.XR.ARFoundation.ARPlane[]
struct ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580;
// UnityEngine.XR.ARFoundation.ARSessionOrigin
struct ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF;
// Zoom
struct Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0;
// fadeIn
struct fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425;

extern RuntimeClass* ARSubsystemManager_t989EF64D895F30DF0C0E14F235B21675030E00EF_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t386B526A237C5426A1502ADF2690D01DCC513454_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
extern RuntimeClass* CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
extern RuntimeClass* Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
extern RuntimeClass* Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
extern RuntimeClass* PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
extern RuntimeClass* UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
extern RuntimeClass* XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral09FE346526A560596EE8648886F5C433EDED8DDB;
extern String_t* _stringLiteral395DF8F7C51F007019CB30201C49E884B46B92FA;
extern String_t* _stringLiteral737995F946D2EFF3A4EA8851D2971C4104D85693;
extern String_t* _stringLiteral89B1C6DD498B0F6FADC72C677B2AF05ED7CAA7B5;
extern String_t* _stringLiteral89F3B5900C827B56A0F0306DAE981AD2F046D285;
extern const RuntimeMethod* Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m642665A17D3104F99F2305FA4059BEE638357ED1_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m643F26A3137C3F5DB048DD827C3242DFCAAA1018_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m9C96EBE4BCAF28484D7D003DB4B84FE118B8A7DE_RuntimeMethod_var;
extern const RuntimeMethod* NativeArrayUnsafeUtility_GetUnsafePtr_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_m3191468493B96C1824159DCA7F015F91CAC23965_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_mE3869E2244689BB5F91E7ED43CCD898EEF385143_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m773C0C2E29CED4C69F2401D085CAC0BE162E7EDA_RuntimeMethod_var;
extern const RuntimeMethod* TestCameraImage_OnCameraFrameReceived_mF2A20651C82BFE41F637A82A191DF924A3DDF9E3_RuntimeMethod_var;
extern const RuntimeMethod* Texture2D_GetRawTextureData_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_mE453927FC6491AC23A63CD1174E492BC63409B2E_RuntimeMethod_var;
extern const RuntimeMethod* UIManager_FrameChanged_m89E1589B398FF8D7E9A225B41CC37B8C13D65CA2_RuntimeMethod_var;
extern const RuntimeMethod* UIManager_PlacedObject_m675DD8164B5BAB63DEDD49F28B34CCC2B208C9F9_RuntimeMethod_var;
extern const uint32_t TestCameraImage_OnCameraFrameReceived_mF2A20651C82BFE41F637A82A191DF924A3DDF9E3_MetadataUsageId;
extern const uint32_t TestCameraImage_OnDisable_m0971839455DC41D9B54FB3397EA22DB4DCCEF49A_MetadataUsageId;
extern const uint32_t TestCameraImage_OnEnable_mC86D4C2FC61F303D375510C7D9BA460B3C83D6A4_MetadataUsageId;
extern const uint32_t TouchCameraWithRotation_Update_m8B43FC15C287A6D9517B07613ED80FAD179F9BD5_MetadataUsageId;
extern const uint32_t TouchCameraWithRotation__ctor_mC06BB35183F833770B22D2E01CF377292E9292CD_MetadataUsageId;
extern const uint32_t TouchCameraWithRotation_reset_m3C8773298041A9F489D80A21CC1E46ED5A88DFFA_MetadataUsageId;
extern const uint32_t TouchCamera_Update_mBCAB9C5C1C2397C1BA76748838DA7AC417A5DC77_MetadataUsageId;
extern const uint32_t TouchCamera__ctor_m3AC5A6605BE56765CE796118F3E959418F204168_MetadataUsageId;
extern const uint32_t UIManager_FrameChanged_m89E1589B398FF8D7E9A225B41CC37B8C13D65CA2_MetadataUsageId;
extern const uint32_t UIManager_OnDisable_m0F04E11DEB17C6F3492D6C73445C23F15FC1221E_MetadataUsageId;
extern const uint32_t UIManager_OnEnable_m92BB1673E78D5547CEF8FCC6345C78EEA4C41851_MetadataUsageId;
extern const uint32_t UIManager_PlacedObject_m675DD8164B5BAB63DEDD49F28B34CCC2B208C9F9_MetadataUsageId;
extern const uint32_t UIManager_PlanesFound_m80FA266D580E616FC607D27AC8B464629E2D5130_MetadataUsageId;
extern const uint32_t UIManager__cctor_m3B18550BB979CE1BE0EDA898EE0486CCA6947226_MetadataUsageId;
extern const uint32_t Zoom__ctor_m48EC87C1D84179E33EFD99EA35B82C25F3574238_MetadataUsageId;
extern const uint32_t Zoom_action_mAFAECFBC243ACACC67D9DB2728A151600C44E736_MetadataUsageId;
extern const uint32_t fadeIn_Start_m56A19FCF0E6023205D065573956BC1DFCF0276AE_MetadataUsageId;
extern const uint32_t fadeIn_Update_m281E8EAF6D910F96634B5F9543A042ADF5BFDB13_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_H
#define LIST_1_T2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>
struct  List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B, ____items_1)); }
	inline ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580* get__items_1() const { return ____items_1; }
	inline ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_StaticFields, ____emptyArray_5)); }
	inline ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ARPlaneU5BU5D_tE73F0F514FE33A5596B37CDB7E33962711A85580* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#define DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T358B8F23BDC52A5DD700E727E204F9F7CDE12409_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#define NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T489DA527A77CE0DCFEBCD2BA969EEB82D410E998_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef RECTINT_T595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A_H
#define RECTINT_T595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectInt
struct  RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A 
{
public:
	// System.Int32 UnityEngine.RectInt::m_XMin
	int32_t ___m_XMin_0;
	// System.Int32 UnityEngine.RectInt::m_YMin
	int32_t ___m_YMin_1;
	// System.Int32 UnityEngine.RectInt::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.RectInt::m_Height
	int32_t ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_XMin_0)); }
	inline int32_t get_m_XMin_0() const { return ___m_XMin_0; }
	inline int32_t* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(int32_t value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_YMin_1)); }
	inline int32_t get_m_YMin_1() const { return ___m_YMin_1; }
	inline int32_t* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(int32_t value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_Width_2)); }
	inline int32_t get_m_Width_2() const { return ___m_Width_2; }
	inline int32_t* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(int32_t value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A, ___m_Height_3)); }
	inline int32_t get_m_Height_3() const { return ___m_Height_3; }
	inline int32_t* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(int32_t value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTINT_T595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#define VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_One_3)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#define VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3Interop
struct  Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA 
{
public:
	// System.Single Vector3Interop::x
	float ___x_0;
	// System.Single Vector3Interop::y
	float ___y_1;
	// System.Single Vector3Interop::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3Interop_t2EF806C1E83787D26A48D437D52765DB172117DA, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INTEROP_T2EF806C1E83787D26A48D437D52765DB172117DA_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#define NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77 
{
public:
	// T System.Nullable`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77, ___value_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_0() const { return ___value_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9DBDF4088AAEF0A6A6B9D1720769D79A41563E77_H
#ifndef NULLABLE_1_T2D31B61CB1F8452E868737CCA40082AA2D1F58DC_H
#define NULLABLE_1_T2D31B61CB1F8452E868737CCA40082AA2D1F58DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector2>
struct  Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC 
{
public:
	// T System.Nullable`1::value
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC, ___value_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_value_0() const { return ___value_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2D31B61CB1F8452E868737CCA40082AA2D1F58DC_H
#ifndef NULLABLE_1_TDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5_H
#define NULLABLE_1_TDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5_H
#ifndef ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#define ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.Allocator
struct  Allocator_t62A091275262E7067EAAD565B67764FA877D58D6 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t62A091275262E7067EAAD565B67764FA877D58D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOCATOR_T62A091275262E7067EAAD565B67764FA877D58D6_H
#ifndef INTEGRATEDSUBSYSTEM_TF67A736CD38F3A64A40687C90024FA7326AF7D86_H
#define INTEGRATEDSUBSYSTEM_TF67A736CD38F3A64A40687C90024FA7326AF7D86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem
struct  IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Experimental.IntegratedSubsystem::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.IntegratedSubsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_subsystemDescriptor_1() { return static_cast<int32_t>(offsetof(IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86, ___m_subsystemDescriptor_1)); }
	inline RuntimeObject* get_m_subsystemDescriptor_1() const { return ___m_subsystemDescriptor_1; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_1() { return &___m_subsystemDescriptor_1; }
	inline void set_m_subsystemDescriptor_1(RuntimeObject* value)
	{
		___m_subsystemDescriptor_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Experimental.IntegratedSubsystem
struct IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_subsystemDescriptor_1;
};
// Native definition for COM marshalling of UnityEngine.Experimental.IntegratedSubsystem
struct IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86_marshaled_com
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_subsystemDescriptor_1;
};
#endif // INTEGRATEDSUBSYSTEM_TF67A736CD38F3A64A40687C90024FA7326AF7D86_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#define TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T7C6B5101554065C47682E592D1E26079D4EC2DCE_H
#ifndef TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#define TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_tD902305F0B673116C42548A58E8BEED50177A33D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_TD902305F0B673116C42548A58E8BEED50177A33D_H
#ifndef TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#define TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t27DBEAB2242247A15EDE96D740F7EB73ACC938DB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T27DBEAB2242247A15EDE96D740F7EB73ACC938DB_H
#ifndef CAMERAIMAGEFORMAT_T3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_H
#define CAMERAIMAGEFORMAT_T3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImageFormat
struct  CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImageFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGEFORMAT_T3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_H
#ifndef CAMERAIMAGETRANSFORMATION_T9244C88533E9586E4237E49856EE71A34B60E48D_H
#define CAMERAIMAGETRANSFORMATION_T9244C88533E9586E4237E49856EE71A34B60E48D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImageTransformation
struct  CameraImageTransformation_t9244C88533E9586E4237E49856EE71A34B60E48D 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImageTransformation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraImageTransformation_t9244C88533E9586E4237E49856EE71A34B60E48D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGETRANSFORMATION_T9244C88533E9586E4237E49856EE71A34B60E48D_H
#ifndef PLANEDETECTIONFLAGS_T973AF579C70632C3A6345A61C688B125C5864215_H
#define PLANEDETECTIONFLAGS_T973AF579C70632C3A6345A61C688B125C5864215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.PlaneDetectionFlags
struct  PlaneDetectionFlags_t973AF579C70632C3A6345A61C688B125C5864215 
{
public:
	// System.Int32 UnityEngine.XR.ARExtensions.PlaneDetectionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneDetectionFlags_t973AF579C70632C3A6345A61C688B125C5864215, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEDETECTIONFLAGS_T973AF579C70632C3A6345A61C688B125C5864215_H
#ifndef VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#define VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRChannel
struct  VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496 
{
public:
	// System.Int32 VRChannel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VRChannel_t5144073EA223E4E52EB5A04144E13FFCB55C9496, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRCHANNEL_T5144073EA223E4E52EB5A04144E13FFCB55C9496_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NATIVEARRAY_1_TB6F81A87EBCD8958AF30E15FACF167CE524DEABE_H
#define NATIVEARRAY_1_TB6F81A87EBCD8958AF30E15FACF167CE524DEABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Collections.NativeArray`1<System.Byte>
struct  NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_pinvoke
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
// Native definition for COM marshalling of Unity.Collections.NativeArray`1
#ifndef NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
#define NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com_define
struct NativeArray_1_t350F3793D2FE9D9CD5A50725BE978ED846FE3098_marshaled_com
{
	void* ___m_Buffer_0;
	int32_t ___m_Length_1;
	int32_t ___m_AllocatorLabel_2;
};
#endif
#endif // NATIVEARRAY_1_TB6F81A87EBCD8958AF30E15FACF167CE524DEABE_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef INTEGRATEDSUBSYSTEM_1_T13F531EAA4D22C65C38F9A4EC080496DC444FC3D_H
#define INTEGRATEDSUBSYSTEM_1_T13F531EAA4D22C65C38F9A4EC080496DC444FC3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.IntegratedSubsystem`1<UnityEngine.Experimental.XR.XRCameraSubsystemDescriptor>
struct  IntegratedSubsystem_1_t13F531EAA4D22C65C38F9A4EC080496DC444FC3D  : public IntegratedSubsystem_tF67A736CD38F3A64A40687C90024FA7326AF7D86
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGRATEDSUBSYSTEM_1_T13F531EAA4D22C65C38F9A4EC080496DC444FC3D_H
#ifndef GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#define GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_TBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_H
#ifndef TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#define TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T387FE83BB848001FD06B14707AEA6D5A0F6A95F4_H
#ifndef TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#define TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t806752C775BA713A91B6588A07CA98417CABC003 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Position_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RawPosition_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_PositionDelta_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t806752C775BA713A91B6588A07CA98417CABC003, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T806752C775BA713A91B6588A07CA98417CABC003_H
#ifndef CAMERAIMAGE_TA347D573CD5319A54DF1A4D73E36B0975970723E_H
#define CAMERAIMAGE_TA347D573CD5319A54DF1A4D73E36B0975970723E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImage
struct  CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E 
{
public:
	// UnityEngine.Vector2Int UnityEngine.XR.ARExtensions.CameraImage::<dimensions>k__BackingField
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___U3CdimensionsU3Ek__BackingField_0;
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::<planeCount>k__BackingField
	int32_t ___U3CplaneCountU3Ek__BackingField_1;
	// UnityEngine.XR.ARExtensions.CameraImageFormat UnityEngine.XR.ARExtensions.CameraImage::<format>k__BackingField
	int32_t ___U3CformatU3Ek__BackingField_2;
	// System.Double UnityEngine.XR.ARExtensions.CameraImage::<timestamp>k__BackingField
	double ___U3CtimestampU3Ek__BackingField_3;
	// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::m_NativeHandle
	int32_t ___m_NativeHandle_4;
	// UnityEngine.XR.ARExtensions.ICameraImageApi UnityEngine.XR.ARExtensions.CameraImage::m_CameraImageApi
	RuntimeObject* ___m_CameraImageApi_5;

public:
	inline static int32_t get_offset_of_U3CdimensionsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CdimensionsU3Ek__BackingField_0)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_U3CdimensionsU3Ek__BackingField_0() const { return ___U3CdimensionsU3Ek__BackingField_0; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_U3CdimensionsU3Ek__BackingField_0() { return &___U3CdimensionsU3Ek__BackingField_0; }
	inline void set_U3CdimensionsU3Ek__BackingField_0(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___U3CdimensionsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CplaneCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CplaneCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CplaneCountU3Ek__BackingField_1() const { return ___U3CplaneCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CplaneCountU3Ek__BackingField_1() { return &___U3CplaneCountU3Ek__BackingField_1; }
	inline void set_U3CplaneCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CplaneCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CformatU3Ek__BackingField_2)); }
	inline int32_t get_U3CformatU3Ek__BackingField_2() const { return ___U3CformatU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CformatU3Ek__BackingField_2() { return &___U3CformatU3Ek__BackingField_2; }
	inline void set_U3CformatU3Ek__BackingField_2(int32_t value)
	{
		___U3CformatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtimestampU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___U3CtimestampU3Ek__BackingField_3)); }
	inline double get_U3CtimestampU3Ek__BackingField_3() const { return ___U3CtimestampU3Ek__BackingField_3; }
	inline double* get_address_of_U3CtimestampU3Ek__BackingField_3() { return &___U3CtimestampU3Ek__BackingField_3; }
	inline void set_U3CtimestampU3Ek__BackingField_3(double value)
	{
		___U3CtimestampU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_NativeHandle_4() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___m_NativeHandle_4)); }
	inline int32_t get_m_NativeHandle_4() const { return ___m_NativeHandle_4; }
	inline int32_t* get_address_of_m_NativeHandle_4() { return &___m_NativeHandle_4; }
	inline void set_m_NativeHandle_4(int32_t value)
	{
		___m_NativeHandle_4 = value;
	}

	inline static int32_t get_offset_of_m_CameraImageApi_5() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E, ___m_CameraImageApi_5)); }
	inline RuntimeObject* get_m_CameraImageApi_5() const { return ___m_CameraImageApi_5; }
	inline RuntimeObject** get_address_of_m_CameraImageApi_5() { return &___m_CameraImageApi_5; }
	inline void set_m_CameraImageApi_5(RuntimeObject* value)
	{
		___m_CameraImageApi_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraImageApi_5), value);
	}
};

struct CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_StaticFields
{
public:
	// UnityEngine.XR.ARExtensions.XRCameraExtensions_OnImageRequestCompleteDelegate UnityEngine.XR.ARExtensions.CameraImage::s_OnAsyncConversionComplete
	OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 * ___s_OnAsyncConversionComplete_6;

public:
	inline static int32_t get_offset_of_s_OnAsyncConversionComplete_6() { return static_cast<int32_t>(offsetof(CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_StaticFields, ___s_OnAsyncConversionComplete_6)); }
	inline OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 * get_s_OnAsyncConversionComplete_6() const { return ___s_OnAsyncConversionComplete_6; }
	inline OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 ** get_address_of_s_OnAsyncConversionComplete_6() { return &___s_OnAsyncConversionComplete_6; }
	inline void set_s_OnAsyncConversionComplete_6(OnImageRequestCompleteDelegate_tC602138547507C797A3BE8CC2D7022659FA457B2 * value)
	{
		___s_OnAsyncConversionComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_OnAsyncConversionComplete_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARExtensions.CameraImage
struct CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_marshaled_pinvoke
{
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___U3CdimensionsU3Ek__BackingField_0;
	int32_t ___U3CplaneCountU3Ek__BackingField_1;
	int32_t ___U3CformatU3Ek__BackingField_2;
	double ___U3CtimestampU3Ek__BackingField_3;
	int32_t ___m_NativeHandle_4;
	RuntimeObject* ___m_CameraImageApi_5;
};
// Native definition for COM marshalling of UnityEngine.XR.ARExtensions.CameraImage
struct CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E_marshaled_com
{
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___U3CdimensionsU3Ek__BackingField_0;
	int32_t ___U3CplaneCountU3Ek__BackingField_1;
	int32_t ___U3CformatU3Ek__BackingField_2;
	double ___U3CtimestampU3Ek__BackingField_3;
	int32_t ___m_NativeHandle_4;
	RuntimeObject* ___m_CameraImageApi_5;
};
#endif // CAMERAIMAGE_TA347D573CD5319A54DF1A4D73E36B0975970723E_H
#ifndef CAMERAIMAGECONVERSIONPARAMS_TEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1_H
#define CAMERAIMAGECONVERSIONPARAMS_TEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARExtensions.CameraImageConversionParams
struct  CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1 
{
public:
	// UnityEngine.RectInt UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_InputRect
	RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A  ___m_InputRect_0;
	// UnityEngine.Vector2Int UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_OutputDimensions
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___m_OutputDimensions_1;
	// UnityEngine.TextureFormat UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_Format
	int32_t ___m_Format_2;
	// UnityEngine.XR.ARExtensions.CameraImageTransformation UnityEngine.XR.ARExtensions.CameraImageConversionParams::m_Transformation
	int32_t ___m_Transformation_3;

public:
	inline static int32_t get_offset_of_m_InputRect_0() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_InputRect_0)); }
	inline RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A  get_m_InputRect_0() const { return ___m_InputRect_0; }
	inline RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A * get_address_of_m_InputRect_0() { return &___m_InputRect_0; }
	inline void set_m_InputRect_0(RectInt_t595A63F7EE2BC91A4D2DE5403C5FE94D3C3A6F7A  value)
	{
		___m_InputRect_0 = value;
	}

	inline static int32_t get_offset_of_m_OutputDimensions_1() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_OutputDimensions_1)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_m_OutputDimensions_1() const { return ___m_OutputDimensions_1; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_m_OutputDimensions_1() { return &___m_OutputDimensions_1; }
	inline void set_m_OutputDimensions_1(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___m_OutputDimensions_1 = value;
	}

	inline static int32_t get_offset_of_m_Format_2() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_Format_2)); }
	inline int32_t get_m_Format_2() const { return ___m_Format_2; }
	inline int32_t* get_address_of_m_Format_2() { return &___m_Format_2; }
	inline void set_m_Format_2(int32_t value)
	{
		___m_Format_2 = value;
	}

	inline static int32_t get_offset_of_m_Transformation_3() { return static_cast<int32_t>(offsetof(CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1, ___m_Transformation_3)); }
	inline int32_t get_m_Transformation_3() const { return ___m_Transformation_3; }
	inline int32_t* get_address_of_m_Transformation_3() { return &___m_Transformation_3; }
	inline void set_m_Transformation_3(int32_t value)
	{
		___m_Transformation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAIMAGECONVERSIONPARAMS_TEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1_H
#ifndef LIGHTESTIMATIONDATA_T15951D974E5311F3783622CEE5268A3899C4A2C1_H
#define LIGHTESTIMATIONDATA_T15951D974E5311F3783622CEE5268A3899C4A2C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.LightEstimationData
struct  LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1 
{
public:
	// System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.LightEstimationData::<averageBrightness>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CaverageBrightnessU3Ek__BackingField_0;
	// System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.LightEstimationData::<averageColorTemperature>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CaverageColorTemperatureU3Ek__BackingField_1;
	// System.Nullable`1<UnityEngine.Color> UnityEngine.XR.ARFoundation.LightEstimationData::<colorCorrection>k__BackingField
	Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  ___U3CcolorCorrectionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CaverageBrightnessU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1, ___U3CaverageBrightnessU3Ek__BackingField_0)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CaverageBrightnessU3Ek__BackingField_0() const { return ___U3CaverageBrightnessU3Ek__BackingField_0; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CaverageBrightnessU3Ek__BackingField_0() { return &___U3CaverageBrightnessU3Ek__BackingField_0; }
	inline void set_U3CaverageBrightnessU3Ek__BackingField_0(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CaverageBrightnessU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CaverageColorTemperatureU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1, ___U3CaverageColorTemperatureU3Ek__BackingField_1)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CaverageColorTemperatureU3Ek__BackingField_1() const { return ___U3CaverageColorTemperatureU3Ek__BackingField_1; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CaverageColorTemperatureU3Ek__BackingField_1() { return &___U3CaverageColorTemperatureU3Ek__BackingField_1; }
	inline void set_U3CaverageColorTemperatureU3Ek__BackingField_1(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CaverageColorTemperatureU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CcolorCorrectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1, ___U3CcolorCorrectionU3Ek__BackingField_2)); }
	inline Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  get_U3CcolorCorrectionU3Ek__BackingField_2() const { return ___U3CcolorCorrectionU3Ek__BackingField_2; }
	inline Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77 * get_address_of_U3CcolorCorrectionU3Ek__BackingField_2() { return &___U3CcolorCorrectionU3Ek__BackingField_2; }
	inline void set_U3CcolorCorrectionU3Ek__BackingField_2(Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  value)
	{
		___U3CcolorCorrectionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARFoundation.LightEstimationData
struct LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1_marshaled_pinvoke
{
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CaverageBrightnessU3Ek__BackingField_0;
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CaverageColorTemperatureU3Ek__BackingField_1;
	Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  ___U3CcolorCorrectionU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.XR.ARFoundation.LightEstimationData
struct LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1_marshaled_com
{
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CaverageBrightnessU3Ek__BackingField_0;
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CaverageColorTemperatureU3Ek__BackingField_1;
	Nullable_1_t9DBDF4088AAEF0A6A6B9D1720769D79A41563E77  ___U3CcolorCorrectionU3Ek__BackingField_2;
};
#endif // LIGHTESTIMATIONDATA_T15951D974E5311F3783622CEE5268A3899C4A2C1_H
#ifndef ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#define ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T591D2A86165F896B4B800BB5C25CE18672A55579_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef XRCAMERASUBSYSTEM_T9271DB5D8FEDD3431246FCB6D9257A940246E701_H
#define XRCAMERASUBSYSTEM_T9271DB5D8FEDD3431246FCB6D9257A940246E701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.XRCameraSubsystem
struct  XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701  : public IntegratedSubsystem_1_t13F531EAA4D22C65C38F9A4EC080496DC444FC3D
{
public:
	// System.Action`1<UnityEngine.Experimental.XR.FrameReceivedEventArgs> UnityEngine.Experimental.XR.XRCameraSubsystem::FrameReceived
	Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC * ___FrameReceived_2;

public:
	inline static int32_t get_offset_of_FrameReceived_2() { return static_cast<int32_t>(offsetof(XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701, ___FrameReceived_2)); }
	inline Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC * get_FrameReceived_2() const { return ___FrameReceived_2; }
	inline Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC ** get_address_of_FrameReceived_2() { return &___FrameReceived_2; }
	inline void set_FrameReceived_2(Action_1_t95D7D93D51DF3F6A6C456CD847A0A4150A398BBC * value)
	{
		___FrameReceived_2 = value;
		Il2CppCodeGenWriteBarrier((&___FrameReceived_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRCAMERASUBSYSTEM_T9271DB5D8FEDD3431246FCB6D9257A940246E701_H
#ifndef TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#define TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_TBBF96AC337723E2EF156DF17E09D4379FD05DE1C_H
#ifndef TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#define TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_TBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_H
#ifndef ARCAMERAFRAMEEVENTARGS_T25F661E15E542829DB3CAAD158FED01CC8874672_H
#define ARCAMERAFRAMEEVENTARGS_T25F661E15E542829DB3CAAD158FED01CC8874672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs
struct  ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672 
{
public:
	// UnityEngine.XR.ARFoundation.LightEstimationData UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::<lightEstimation>k__BackingField
	LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1  ___U3ClightEstimationU3Ek__BackingField_0;
	// System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs::<time>k__BackingField
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CtimeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3ClightEstimationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672, ___U3ClightEstimationU3Ek__BackingField_0)); }
	inline LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1  get_U3ClightEstimationU3Ek__BackingField_0() const { return ___U3ClightEstimationU3Ek__BackingField_0; }
	inline LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1 * get_address_of_U3ClightEstimationU3Ek__BackingField_0() { return &___U3ClightEstimationU3Ek__BackingField_0; }
	inline void set_U3ClightEstimationU3Ek__BackingField_0(LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1  value)
	{
		___U3ClightEstimationU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CtimeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672, ___U3CtimeU3Ek__BackingField_1)); }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  get_U3CtimeU3Ek__BackingField_1() const { return ___U3CtimeU3Ek__BackingField_1; }
	inline Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998 * get_address_of_U3CtimeU3Ek__BackingField_1() { return &___U3CtimeU3Ek__BackingField_1; }
	inline void set_U3CtimeU3Ek__BackingField_1(Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  value)
	{
		___U3CtimeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs
struct ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672_marshaled_pinvoke
{
	LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1_marshaled_pinvoke ___U3ClightEstimationU3Ek__BackingField_0;
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CtimeU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs
struct ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672_marshaled_com
{
	LightEstimationData_t15951D974E5311F3783622CEE5268A3899C4A2C1_marshaled_com ___U3ClightEstimationU3Ek__BackingField_0;
	Nullable_1_t489DA527A77CE0DCFEBCD2BA969EEB82D410E998  ___U3CtimeU3Ek__BackingField_1;
};
#endif // ARCAMERAFRAMEEVENTARGS_T25F661E15E542829DB3CAAD158FED01CC8874672_H
#ifndef ACTION_1_T386B526A237C5426A1502ADF2690D01DCC513454_H
#define ACTION_1_T386B526A237C5426A1502ADF2690D01DCC513454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>
struct  Action_1_t386B526A237C5426A1502ADF2690D01DCC513454  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T386B526A237C5426A1502ADF2690D01DCC513454_H
#ifndef ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#define ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_TF1A88E66B3B731DDA75A066DBAE9C55837660F5A_H
#ifndef CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#define CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_4), value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_5), value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#define TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestCameraImage
struct  TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.RawImage TestCameraImage::m_RawImage
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___m_RawImage_4;
	// UnityEngine.UI.Text TestCameraImage::m_ImageInfo
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_ImageInfo_5;
	// UnityEngine.Texture2D TestCameraImage::m_Texture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___m_Texture_6;

public:
	inline static int32_t get_offset_of_m_RawImage_4() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_RawImage_4)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_m_RawImage_4() const { return ___m_RawImage_4; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_m_RawImage_4() { return &___m_RawImage_4; }
	inline void set_m_RawImage_4(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___m_RawImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RawImage_4), value);
	}

	inline static int32_t get_offset_of_m_ImageInfo_5() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_ImageInfo_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_ImageInfo_5() const { return ___m_ImageInfo_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_ImageInfo_5() { return &___m_ImageInfo_5; }
	inline void set_m_ImageInfo_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_ImageInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ImageInfo_5), value);
	}

	inline static int32_t get_offset_of_m_Texture_6() { return static_cast<int32_t>(offsetof(TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6, ___m_Texture_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_m_Texture_6() const { return ___m_Texture_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_m_Texture_6() { return &___m_Texture_6; }
	inline void set_m_Texture_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___m_Texture_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTCAMERAIMAGE_TB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6_H
#ifndef TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#define TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCamera
struct  TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCamera::oldTouchPositions
	Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* ___oldTouchPositions_4;
	// UnityEngine.Vector2 TouchCamera::oldTouchVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldTouchVector_5;
	// System.Single TouchCamera::oldTouchDistance
	float ___oldTouchDistance_6;

public:
	inline static int32_t get_offset_of_oldTouchPositions_4() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchPositions_4)); }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* get_oldTouchPositions_4() const { return ___oldTouchPositions_4; }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6** get_address_of_oldTouchPositions_4() { return &___oldTouchPositions_4; }
	inline void set_oldTouchPositions_4(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* value)
	{
		___oldTouchPositions_4 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_4), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_5() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldTouchVector_5() const { return ___oldTouchVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldTouchVector_5() { return &___oldTouchVector_5; }
	inline void set_oldTouchVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldTouchVector_5 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_6() { return static_cast<int32_t>(offsetof(TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14, ___oldTouchDistance_6)); }
	inline float get_oldTouchDistance_6() const { return ___oldTouchDistance_6; }
	inline float* get_address_of_oldTouchDistance_6() { return &___oldTouchDistance_6; }
	inline void set_oldTouchDistance_6(float value)
	{
		___oldTouchDistance_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERA_T5804FE07848FB1046EA19DC751DF6A59C5F5FB14_H
#ifndef TOUCHCAMERAWITHROTATION_TABC91B3BC5774398A7E582A5BBF358FE03CD9166_H
#define TOUCHCAMERAWITHROTATION_TABC91B3BC5774398A7E582A5BBF358FE03CD9166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchCameraWithRotation
struct  TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera TouchCameraWithRotation::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_4;
	// UnityEngine.GameObject TouchCameraWithRotation::rotationPlate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rotationPlate_5;
	// UnityEngine.GameObject TouchCameraWithRotation::rotationOffsetPlate
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___rotationOffsetPlate_6;
	// System.Single TouchCameraWithRotation::minFoV
	float ___minFoV_7;
	// System.Single TouchCameraWithRotation::maxFoV
	float ___maxFoV_8;
	// System.Boolean TouchCameraWithRotation::isEnabled
	bool ___isEnabled_9;
	// System.Boolean TouchCameraWithRotation::isOnlyZoomable
	bool ___isOnlyZoomable_10;
	// System.Boolean TouchCameraWithRotation::isOnlyRotate
	bool ___isOnlyRotate_11;
	// System.Boolean TouchCameraWithRotation::isLimitYAngle
	bool ___isLimitYAngle_12;
	// System.Single TouchCameraWithRotation::limitYAngleClockwise
	float ___limitYAngleClockwise_13;
	// System.Single TouchCameraWithRotation::limitYAngleAClockwise
	float ___limitYAngleAClockwise_14;
	// System.Boolean TouchCameraWithRotation::isRotationOffseted
	bool ___isRotationOffseted_15;
	// System.Nullable`1<UnityEngine.Vector2>[] TouchCameraWithRotation::oldTouchPositions
	Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* ___oldTouchPositions_16;
	// UnityEngine.Vector2 TouchCameraWithRotation::oldTouchVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldTouchVector_17;
	// System.Single TouchCameraWithRotation::oldTouchDistance
	float ___oldTouchDistance_18;

public:
	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___mainCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_4), value);
	}

	inline static int32_t get_offset_of_rotationPlate_5() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___rotationPlate_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rotationPlate_5() const { return ___rotationPlate_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rotationPlate_5() { return &___rotationPlate_5; }
	inline void set_rotationPlate_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rotationPlate_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotationPlate_5), value);
	}

	inline static int32_t get_offset_of_rotationOffsetPlate_6() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___rotationOffsetPlate_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_rotationOffsetPlate_6() const { return ___rotationOffsetPlate_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_rotationOffsetPlate_6() { return &___rotationOffsetPlate_6; }
	inline void set_rotationOffsetPlate_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___rotationOffsetPlate_6 = value;
		Il2CppCodeGenWriteBarrier((&___rotationOffsetPlate_6), value);
	}

	inline static int32_t get_offset_of_minFoV_7() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___minFoV_7)); }
	inline float get_minFoV_7() const { return ___minFoV_7; }
	inline float* get_address_of_minFoV_7() { return &___minFoV_7; }
	inline void set_minFoV_7(float value)
	{
		___minFoV_7 = value;
	}

	inline static int32_t get_offset_of_maxFoV_8() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___maxFoV_8)); }
	inline float get_maxFoV_8() const { return ___maxFoV_8; }
	inline float* get_address_of_maxFoV_8() { return &___maxFoV_8; }
	inline void set_maxFoV_8(float value)
	{
		___maxFoV_8 = value;
	}

	inline static int32_t get_offset_of_isEnabled_9() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isEnabled_9)); }
	inline bool get_isEnabled_9() const { return ___isEnabled_9; }
	inline bool* get_address_of_isEnabled_9() { return &___isEnabled_9; }
	inline void set_isEnabled_9(bool value)
	{
		___isEnabled_9 = value;
	}

	inline static int32_t get_offset_of_isOnlyZoomable_10() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isOnlyZoomable_10)); }
	inline bool get_isOnlyZoomable_10() const { return ___isOnlyZoomable_10; }
	inline bool* get_address_of_isOnlyZoomable_10() { return &___isOnlyZoomable_10; }
	inline void set_isOnlyZoomable_10(bool value)
	{
		___isOnlyZoomable_10 = value;
	}

	inline static int32_t get_offset_of_isOnlyRotate_11() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isOnlyRotate_11)); }
	inline bool get_isOnlyRotate_11() const { return ___isOnlyRotate_11; }
	inline bool* get_address_of_isOnlyRotate_11() { return &___isOnlyRotate_11; }
	inline void set_isOnlyRotate_11(bool value)
	{
		___isOnlyRotate_11 = value;
	}

	inline static int32_t get_offset_of_isLimitYAngle_12() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isLimitYAngle_12)); }
	inline bool get_isLimitYAngle_12() const { return ___isLimitYAngle_12; }
	inline bool* get_address_of_isLimitYAngle_12() { return &___isLimitYAngle_12; }
	inline void set_isLimitYAngle_12(bool value)
	{
		___isLimitYAngle_12 = value;
	}

	inline static int32_t get_offset_of_limitYAngleClockwise_13() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___limitYAngleClockwise_13)); }
	inline float get_limitYAngleClockwise_13() const { return ___limitYAngleClockwise_13; }
	inline float* get_address_of_limitYAngleClockwise_13() { return &___limitYAngleClockwise_13; }
	inline void set_limitYAngleClockwise_13(float value)
	{
		___limitYAngleClockwise_13 = value;
	}

	inline static int32_t get_offset_of_limitYAngleAClockwise_14() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___limitYAngleAClockwise_14)); }
	inline float get_limitYAngleAClockwise_14() const { return ___limitYAngleAClockwise_14; }
	inline float* get_address_of_limitYAngleAClockwise_14() { return &___limitYAngleAClockwise_14; }
	inline void set_limitYAngleAClockwise_14(float value)
	{
		___limitYAngleAClockwise_14 = value;
	}

	inline static int32_t get_offset_of_isRotationOffseted_15() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___isRotationOffseted_15)); }
	inline bool get_isRotationOffseted_15() const { return ___isRotationOffseted_15; }
	inline bool* get_address_of_isRotationOffseted_15() { return &___isRotationOffseted_15; }
	inline void set_isRotationOffseted_15(bool value)
	{
		___isRotationOffseted_15 = value;
	}

	inline static int32_t get_offset_of_oldTouchPositions_16() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___oldTouchPositions_16)); }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* get_oldTouchPositions_16() const { return ___oldTouchPositions_16; }
	inline Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6** get_address_of_oldTouchPositions_16() { return &___oldTouchPositions_16; }
	inline void set_oldTouchPositions_16(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* value)
	{
		___oldTouchPositions_16 = value;
		Il2CppCodeGenWriteBarrier((&___oldTouchPositions_16), value);
	}

	inline static int32_t get_offset_of_oldTouchVector_17() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___oldTouchVector_17)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldTouchVector_17() const { return ___oldTouchVector_17; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldTouchVector_17() { return &___oldTouchVector_17; }
	inline void set_oldTouchVector_17(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldTouchVector_17 = value;
	}

	inline static int32_t get_offset_of_oldTouchDistance_18() { return static_cast<int32_t>(offsetof(TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166, ___oldTouchDistance_18)); }
	inline float get_oldTouchDistance_18() const { return ___oldTouchDistance_18; }
	inline float* get_address_of_oldTouchDistance_18() { return &___oldTouchDistance_18; }
	inline void set_oldTouchDistance_18(float value)
	{
		___oldTouchDistance_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHCAMERAWITHROTATION_TABC91B3BC5774398A7E582A5BBF358FE03CD9166_H
#ifndef UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#define UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManager
struct  UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::m_PlaneManager
	ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___m_PlaneManager_6;
	// UnityEngine.Animator UIManager::m_MoveDeviceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_MoveDeviceAnimation_7;
	// UnityEngine.Animator UIManager::m_TapToPlaceAnimation
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___m_TapToPlaceAnimation_8;
	// System.Boolean UIManager::m_ShowingTapToPlace
	bool ___m_ShowingTapToPlace_10;
	// System.Boolean UIManager::m_ShowingMoveDevice
	bool ___m_ShowingMoveDevice_11;

public:
	inline static int32_t get_offset_of_m_PlaneManager_6() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_PlaneManager_6)); }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * get_m_PlaneManager_6() const { return ___m_PlaneManager_6; }
	inline ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A ** get_address_of_m_PlaneManager_6() { return &___m_PlaneManager_6; }
	inline void set_m_PlaneManager_6(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * value)
	{
		___m_PlaneManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlaneManager_6), value);
	}

	inline static int32_t get_offset_of_m_MoveDeviceAnimation_7() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_MoveDeviceAnimation_7)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_MoveDeviceAnimation_7() const { return ___m_MoveDeviceAnimation_7; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_MoveDeviceAnimation_7() { return &___m_MoveDeviceAnimation_7; }
	inline void set_m_MoveDeviceAnimation_7(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_MoveDeviceAnimation_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MoveDeviceAnimation_7), value);
	}

	inline static int32_t get_offset_of_m_TapToPlaceAnimation_8() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_TapToPlaceAnimation_8)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_m_TapToPlaceAnimation_8() const { return ___m_TapToPlaceAnimation_8; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_m_TapToPlaceAnimation_8() { return &___m_TapToPlaceAnimation_8; }
	inline void set_m_TapToPlaceAnimation_8(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___m_TapToPlaceAnimation_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TapToPlaceAnimation_8), value);
	}

	inline static int32_t get_offset_of_m_ShowingTapToPlace_10() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingTapToPlace_10)); }
	inline bool get_m_ShowingTapToPlace_10() const { return ___m_ShowingTapToPlace_10; }
	inline bool* get_address_of_m_ShowingTapToPlace_10() { return &___m_ShowingTapToPlace_10; }
	inline void set_m_ShowingTapToPlace_10(bool value)
	{
		___m_ShowingTapToPlace_10 = value;
	}

	inline static int32_t get_offset_of_m_ShowingMoveDevice_11() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C, ___m_ShowingMoveDevice_11)); }
	inline bool get_m_ShowingMoveDevice_11() const { return ___m_ShowingMoveDevice_11; }
	inline bool* get_address_of_m_ShowingMoveDevice_11() { return &___m_ShowingMoveDevice_11; }
	inline void set_m_ShowingMoveDevice_11(bool value)
	{
		___m_ShowingMoveDevice_11 = value;
	}
};

struct UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UIManager::s_Planes
	List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * ___s_Planes_9;

public:
	inline static int32_t get_offset_of_s_Planes_9() { return static_cast<int32_t>(offsetof(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields, ___s_Planes_9)); }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * get_s_Planes_9() const { return ___s_Planes_9; }
	inline List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B ** get_address_of_s_Planes_9() { return &___s_Planes_9; }
	inline void set_s_Planes_9(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * value)
	{
		___s_Planes_9 = value;
		Il2CppCodeGenWriteBarrier((&___s_Planes_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIMANAGER_TA871F1BE896F9D7434A52E7413E3C9F11E6B323C_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef ARPLANEMANAGER_TC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_H
#define ARPLANEMANAGER_TC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARPlaneManager
struct  ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPlaneManager::m_PlanePrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_PlanePrefab_4;
	// UnityEngine.XR.ARExtensions.PlaneDetectionFlags UnityEngine.XR.ARFoundation.ARPlaneManager::m_DetectionFlags
	int32_t ___m_DetectionFlags_5;
	// System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs> UnityEngine.XR.ARFoundation.ARPlaneManager::planeAdded
	Action_1_t00C9993E42C217DC2FE1E88A5476EB13BCF14CCD * ___planeAdded_6;
	// System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneUpdatedEventArgs> UnityEngine.XR.ARFoundation.ARPlaneManager::planeUpdated
	Action_1_tC5CF6AA49CB1A161A0AA9BFEE63D1C976B909722 * ___planeUpdated_7;
	// System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneRemovedEventArgs> UnityEngine.XR.ARFoundation.ARPlaneManager::planeRemoved
	Action_1_tD8609389570174CF9943507CE0ABA181614FC950 * ___planeRemoved_8;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.XR.TrackableId,UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlaneManager::m_Planes
	Dictionary_2_t994DCE812EA0005977731A1258D38DD3042B4069 * ___m_Planes_9;
	// UnityEngine.XR.ARFoundation.ARSessionOrigin UnityEngine.XR.ARFoundation.ARPlaneManager::m_SessionOrigin
	ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * ___m_SessionOrigin_10;

public:
	inline static int32_t get_offset_of_m_PlanePrefab_4() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___m_PlanePrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_PlanePrefab_4() const { return ___m_PlanePrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_PlanePrefab_4() { return &___m_PlanePrefab_4; }
	inline void set_m_PlanePrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_PlanePrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlanePrefab_4), value);
	}

	inline static int32_t get_offset_of_m_DetectionFlags_5() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___m_DetectionFlags_5)); }
	inline int32_t get_m_DetectionFlags_5() const { return ___m_DetectionFlags_5; }
	inline int32_t* get_address_of_m_DetectionFlags_5() { return &___m_DetectionFlags_5; }
	inline void set_m_DetectionFlags_5(int32_t value)
	{
		___m_DetectionFlags_5 = value;
	}

	inline static int32_t get_offset_of_planeAdded_6() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___planeAdded_6)); }
	inline Action_1_t00C9993E42C217DC2FE1E88A5476EB13BCF14CCD * get_planeAdded_6() const { return ___planeAdded_6; }
	inline Action_1_t00C9993E42C217DC2FE1E88A5476EB13BCF14CCD ** get_address_of_planeAdded_6() { return &___planeAdded_6; }
	inline void set_planeAdded_6(Action_1_t00C9993E42C217DC2FE1E88A5476EB13BCF14CCD * value)
	{
		___planeAdded_6 = value;
		Il2CppCodeGenWriteBarrier((&___planeAdded_6), value);
	}

	inline static int32_t get_offset_of_planeUpdated_7() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___planeUpdated_7)); }
	inline Action_1_tC5CF6AA49CB1A161A0AA9BFEE63D1C976B909722 * get_planeUpdated_7() const { return ___planeUpdated_7; }
	inline Action_1_tC5CF6AA49CB1A161A0AA9BFEE63D1C976B909722 ** get_address_of_planeUpdated_7() { return &___planeUpdated_7; }
	inline void set_planeUpdated_7(Action_1_tC5CF6AA49CB1A161A0AA9BFEE63D1C976B909722 * value)
	{
		___planeUpdated_7 = value;
		Il2CppCodeGenWriteBarrier((&___planeUpdated_7), value);
	}

	inline static int32_t get_offset_of_planeRemoved_8() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___planeRemoved_8)); }
	inline Action_1_tD8609389570174CF9943507CE0ABA181614FC950 * get_planeRemoved_8() const { return ___planeRemoved_8; }
	inline Action_1_tD8609389570174CF9943507CE0ABA181614FC950 ** get_address_of_planeRemoved_8() { return &___planeRemoved_8; }
	inline void set_planeRemoved_8(Action_1_tD8609389570174CF9943507CE0ABA181614FC950 * value)
	{
		___planeRemoved_8 = value;
		Il2CppCodeGenWriteBarrier((&___planeRemoved_8), value);
	}

	inline static int32_t get_offset_of_m_Planes_9() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___m_Planes_9)); }
	inline Dictionary_2_t994DCE812EA0005977731A1258D38DD3042B4069 * get_m_Planes_9() const { return ___m_Planes_9; }
	inline Dictionary_2_t994DCE812EA0005977731A1258D38DD3042B4069 ** get_address_of_m_Planes_9() { return &___m_Planes_9; }
	inline void set_m_Planes_9(Dictionary_2_t994DCE812EA0005977731A1258D38DD3042B4069 * value)
	{
		___m_Planes_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Planes_9), value);
	}

	inline static int32_t get_offset_of_m_SessionOrigin_10() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A, ___m_SessionOrigin_10)); }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * get_m_SessionOrigin_10() const { return ___m_SessionOrigin_10; }
	inline ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF ** get_address_of_m_SessionOrigin_10() { return &___m_SessionOrigin_10; }
	inline void set_m_SessionOrigin_10(ARSessionOrigin_t61463C0A24AF925CF219B6C3F1720325C96720EF * value)
	{
		___m_SessionOrigin_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionOrigin_10), value);
	}
};

struct ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.XR.BoundedPlane> UnityEngine.XR.ARFoundation.ARPlaneManager::s_BoundedPlanes
	List_1_t54A25059B09A42125C1A8C6CC316E299E958548E * ___s_BoundedPlanes_11;
	// System.Collections.Generic.HashSet`1<UnityEngine.Experimental.XR.TrackableId> UnityEngine.XR.ARFoundation.ARPlaneManager::s_TrackableIds
	HashSet_1_t46CBD5E236B1CCC32C5AD60B818E2AE07AF6F05F * ___s_TrackableIds_12;
	// System.Collections.Generic.List`1<UnityEngine.Experimental.XR.TrackableId> UnityEngine.XR.ARFoundation.ARPlaneManager::s_PlanesToRemove
	List_1_tB042082FE5AEFD0E6DD62F19F486F8CEC769D1CD * ___s_PlanesToRemove_13;

public:
	inline static int32_t get_offset_of_s_BoundedPlanes_11() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_StaticFields, ___s_BoundedPlanes_11)); }
	inline List_1_t54A25059B09A42125C1A8C6CC316E299E958548E * get_s_BoundedPlanes_11() const { return ___s_BoundedPlanes_11; }
	inline List_1_t54A25059B09A42125C1A8C6CC316E299E958548E ** get_address_of_s_BoundedPlanes_11() { return &___s_BoundedPlanes_11; }
	inline void set_s_BoundedPlanes_11(List_1_t54A25059B09A42125C1A8C6CC316E299E958548E * value)
	{
		___s_BoundedPlanes_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_BoundedPlanes_11), value);
	}

	inline static int32_t get_offset_of_s_TrackableIds_12() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_StaticFields, ___s_TrackableIds_12)); }
	inline HashSet_1_t46CBD5E236B1CCC32C5AD60B818E2AE07AF6F05F * get_s_TrackableIds_12() const { return ___s_TrackableIds_12; }
	inline HashSet_1_t46CBD5E236B1CCC32C5AD60B818E2AE07AF6F05F ** get_address_of_s_TrackableIds_12() { return &___s_TrackableIds_12; }
	inline void set_s_TrackableIds_12(HashSet_1_t46CBD5E236B1CCC32C5AD60B818E2AE07AF6F05F * value)
	{
		___s_TrackableIds_12 = value;
		Il2CppCodeGenWriteBarrier((&___s_TrackableIds_12), value);
	}

	inline static int32_t get_offset_of_s_PlanesToRemove_13() { return static_cast<int32_t>(offsetof(ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_StaticFields, ___s_PlanesToRemove_13)); }
	inline List_1_tB042082FE5AEFD0E6DD62F19F486F8CEC769D1CD * get_s_PlanesToRemove_13() const { return ___s_PlanesToRemove_13; }
	inline List_1_tB042082FE5AEFD0E6DD62F19F486F8CEC769D1CD ** get_address_of_s_PlanesToRemove_13() { return &___s_PlanesToRemove_13; }
	inline void set_s_PlanesToRemove_13(List_1_tB042082FE5AEFD0E6DD62F19F486F8CEC769D1CD * value)
	{
		___s_PlanesToRemove_13 = value;
		Il2CppCodeGenWriteBarrier((&___s_PlanesToRemove_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEMANAGER_TC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A_H
#ifndef ZOOM_T880AFB749C3A75A781108A6A230D45E6B07952F0_H
#define ZOOM_T880AFB749C3A75A781108A6A230D45E6B07952F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zoom
struct  Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Camera Zoom::mainCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mainCamera_4;
	// UnityEngine.GameObject Zoom::zoomInObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___zoomInObject_5;
	// System.Boolean Zoom::isTriggered
	bool ___isTriggered_6;
	// System.Single Zoom::initFieldOfView
	float ___initFieldOfView_7;
	// System.Single Zoom::targetFieldOfView
	float ___targetFieldOfView_8;
	// System.Int32 Zoom::nStep
	int32_t ___nStep_9;
	// System.Single Zoom::waitForSeconds
	float ___waitForSeconds_10;
	// System.Single Zoom::waited
	float ___waited_11;
	// System.Boolean Zoom::isTriggering
	bool ___isTriggering_12;
	// System.String Zoom::trigger
	String_t* ___trigger_13;
	// System.Int32 Zoom::cameraNR
	int32_t ___cameraNR_14;
	// System.Int32 Zoom::cameraRCount
	int32_t ___cameraRCount_15;
	// System.Single Zoom::step
	float ___step_16;

public:
	inline static int32_t get_offset_of_mainCamera_4() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___mainCamera_4)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mainCamera_4() const { return ___mainCamera_4; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mainCamera_4() { return &___mainCamera_4; }
	inline void set_mainCamera_4(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mainCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_4), value);
	}

	inline static int32_t get_offset_of_zoomInObject_5() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___zoomInObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_zoomInObject_5() const { return ___zoomInObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_zoomInObject_5() { return &___zoomInObject_5; }
	inline void set_zoomInObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___zoomInObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___zoomInObject_5), value);
	}

	inline static int32_t get_offset_of_isTriggered_6() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___isTriggered_6)); }
	inline bool get_isTriggered_6() const { return ___isTriggered_6; }
	inline bool* get_address_of_isTriggered_6() { return &___isTriggered_6; }
	inline void set_isTriggered_6(bool value)
	{
		___isTriggered_6 = value;
	}

	inline static int32_t get_offset_of_initFieldOfView_7() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___initFieldOfView_7)); }
	inline float get_initFieldOfView_7() const { return ___initFieldOfView_7; }
	inline float* get_address_of_initFieldOfView_7() { return &___initFieldOfView_7; }
	inline void set_initFieldOfView_7(float value)
	{
		___initFieldOfView_7 = value;
	}

	inline static int32_t get_offset_of_targetFieldOfView_8() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___targetFieldOfView_8)); }
	inline float get_targetFieldOfView_8() const { return ___targetFieldOfView_8; }
	inline float* get_address_of_targetFieldOfView_8() { return &___targetFieldOfView_8; }
	inline void set_targetFieldOfView_8(float value)
	{
		___targetFieldOfView_8 = value;
	}

	inline static int32_t get_offset_of_nStep_9() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___nStep_9)); }
	inline int32_t get_nStep_9() const { return ___nStep_9; }
	inline int32_t* get_address_of_nStep_9() { return &___nStep_9; }
	inline void set_nStep_9(int32_t value)
	{
		___nStep_9 = value;
	}

	inline static int32_t get_offset_of_waitForSeconds_10() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___waitForSeconds_10)); }
	inline float get_waitForSeconds_10() const { return ___waitForSeconds_10; }
	inline float* get_address_of_waitForSeconds_10() { return &___waitForSeconds_10; }
	inline void set_waitForSeconds_10(float value)
	{
		___waitForSeconds_10 = value;
	}

	inline static int32_t get_offset_of_waited_11() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___waited_11)); }
	inline float get_waited_11() const { return ___waited_11; }
	inline float* get_address_of_waited_11() { return &___waited_11; }
	inline void set_waited_11(float value)
	{
		___waited_11 = value;
	}

	inline static int32_t get_offset_of_isTriggering_12() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___isTriggering_12)); }
	inline bool get_isTriggering_12() const { return ___isTriggering_12; }
	inline bool* get_address_of_isTriggering_12() { return &___isTriggering_12; }
	inline void set_isTriggering_12(bool value)
	{
		___isTriggering_12 = value;
	}

	inline static int32_t get_offset_of_trigger_13() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___trigger_13)); }
	inline String_t* get_trigger_13() const { return ___trigger_13; }
	inline String_t** get_address_of_trigger_13() { return &___trigger_13; }
	inline void set_trigger_13(String_t* value)
	{
		___trigger_13 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_13), value);
	}

	inline static int32_t get_offset_of_cameraNR_14() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___cameraNR_14)); }
	inline int32_t get_cameraNR_14() const { return ___cameraNR_14; }
	inline int32_t* get_address_of_cameraNR_14() { return &___cameraNR_14; }
	inline void set_cameraNR_14(int32_t value)
	{
		___cameraNR_14 = value;
	}

	inline static int32_t get_offset_of_cameraRCount_15() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___cameraRCount_15)); }
	inline int32_t get_cameraRCount_15() const { return ___cameraRCount_15; }
	inline int32_t* get_address_of_cameraRCount_15() { return &___cameraRCount_15; }
	inline void set_cameraRCount_15(int32_t value)
	{
		___cameraRCount_15 = value;
	}

	inline static int32_t get_offset_of_step_16() { return static_cast<int32_t>(offsetof(Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0, ___step_16)); }
	inline float get_step_16() const { return ___step_16; }
	inline float* get_address_of_step_16() { return &___step_16; }
	inline void set_step_16(float value)
	{
		___step_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOM_T880AFB749C3A75A781108A6A230D45E6B07952F0_H
#ifndef FADEIN_T1AE46C152FC433A43DEA5A0EF1393D741794A425_H
#define FADEIN_T1AE46C152FC433A43DEA5A0EF1393D741794A425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// fadeIn
struct  fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single fadeIn::FadeRate
	float ___FadeRate_4;
	// UnityEngine.UI.RawImage fadeIn::image
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___image_5;
	// System.Single fadeIn::targetAlpha
	float ___targetAlpha_6;

public:
	inline static int32_t get_offset_of_FadeRate_4() { return static_cast<int32_t>(offsetof(fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425, ___FadeRate_4)); }
	inline float get_FadeRate_4() const { return ___FadeRate_4; }
	inline float* get_address_of_FadeRate_4() { return &___FadeRate_4; }
	inline void set_FadeRate_4(float value)
	{
		___FadeRate_4 = value;
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425, ___image_5)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_image_5() const { return ___image_5; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}

	inline static int32_t get_offset_of_targetAlpha_6() { return static_cast<int32_t>(offsetof(fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425, ___targetAlpha_6)); }
	inline float get_targetAlpha_6() const { return ___targetAlpha_6; }
	inline float* get_address_of_targetAlpha_6() { return &___targetAlpha_6; }
	inline void set_targetAlpha_6(float value)
	{
		___targetAlpha_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADEIN_T1AE46C152FC433A43DEA5A0EF1393D741794A425_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#define RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___m_Texture_30;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___m_UVRect_31;

public:
	inline static int32_t get_offset_of_m_Texture_30() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_Texture_30)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_m_Texture_30() const { return ___m_Texture_30; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_m_Texture_30() { return &___m_Texture_30; }
	inline void set_m_Texture_30(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___m_Texture_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_30), value);
	}

	inline static int32_t get_offset_of_m_UVRect_31() { return static_cast<int32_t>(offsetof(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8, ___m_UVRect_31)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_m_UVRect_31() const { return ___m_UVRect_31; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_m_UVRect_31() { return &___m_UVRect_31; }
	inline void set_m_UVRect_31(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___m_UVRect_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T68991514DB8F48442D614E7904A298C936B3C7C8_H
#ifndef TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#define TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_FontData_30)); }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t29F4568F4FB8C463AAFE6DD21FA7A812B4FF1494 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCache_32)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_tD455BE18A64C7DDF854F6DB3CCEBF705121C58A8 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030_StaticFields, ___s_DefaultText_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_TE9317B57477F4B50AA4C16F460DE6F82DAD6D030_H
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  m_Items[1];

public:
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		m_Items[index] = value;
	}
};
// System.Nullable`1<UnityEngine.Vector2>[]
struct Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  m_Items[1];

public:
	inline Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_gshared (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!!0> UnityEngine.Texture2D::GetRawTextureData<System.Byte>()
extern "C" IL2CPP_METHOD_ATTR NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  Texture2D_GetRawTextureData_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_mE453927FC6491AC23A63CD1174E492BC63409B2E_gshared (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafePtr<System.Byte>(Unity.Collections.NativeArray`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR void* NativeArrayUnsafeUtility_GetUnsafePtr_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_m3191468493B96C1824159DCA7F015F91CAC23965_gshared (NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  p0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector2>::get_HasValue()
extern "C" IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_gshared (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Vector2>::.ctor(!0)
extern "C" IL2CPP_METHOD_ATTR void Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_gshared (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector2>::GetValueOrDefault()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_gshared (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector2>::get_Value()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_gshared (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C" IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_mE3869E2244689BB5F91E7ED43CCD898EEF385143_gshared (Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Nullable_1_get_Value_m773C0C2E29CED4C69F2401D085CAC0BE162E7EDA_gshared (Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);

// System.Void System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22 (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_gshared)(__this, p0, p1, method);
}
// System.Void UnityEngine.XR.ARFoundation.ARSubsystemManager::add_cameraFrameReceived(System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void ARSubsystemManager_add_cameraFrameReceived_mC5420AAC1E488E6C2B33C30C2936C05A07D6173B (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARFoundation.ARSubsystemManager::remove_cameraFrameReceived(System.Action`1<UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs>)
extern "C" IL2CPP_METHOD_ATTR void ARSubsystemManager_remove_cameraFrameReceived_m5407919D112A547D758CD53498740D0BB6390414 (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * p0, const RuntimeMethod* method);
// UnityEngine.Experimental.XR.XRCameraSubsystem UnityEngine.XR.ARFoundation.ARSubsystemManager::get_cameraSubsystem()
extern "C" IL2CPP_METHOD_ATTR XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * ARSubsystemManager_get_cameraSubsystem_m4D2E3C8CA66D41581D15BD876E7C2F318A995494 (const RuntimeMethod* method);
// System.Boolean UnityEngine.XR.ARExtensions.XRCameraExtensions::TryGetLatestImage(UnityEngine.Experimental.XR.XRCameraSubsystem,UnityEngine.XR.ARExtensions.CameraImage&)
extern "C" IL2CPP_METHOD_ATTR bool XRCameraExtensions_TryGetLatestImage_m5BB6DEE2E7A97A951F712D811C132BD6ECE28F7E (XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * p0, CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t CameraImage_get_width_m111259E3F3A6E974E2919FFC851D6B541D594112 (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t CameraImage_get_height_mC4274492B499A6F0076E1FC318EECBB89F1907B2 (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.XR.ARExtensions.CameraImage::get_planeCount()
extern "C" IL2CPP_METHOD_ATTR int32_t CameraImage_get_planeCount_mC1115B5A17DF2AF5A8D5B6BB16C83A87E6A00903 (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, const RuntimeMethod* method);
// System.Double UnityEngine.XR.ARExtensions.CameraImage::get_timestamp()
extern "C" IL2CPP_METHOD_ATTR double CameraImage_get_timestamp_m99A5F54F2FD195915DA87D3EF316A6D351501E6D (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, const RuntimeMethod* method);
// UnityEngine.XR.ARExtensions.CameraImageFormat UnityEngine.XR.ARExtensions.CameraImage::get_format()
extern "C" IL2CPP_METHOD_ATTR int32_t CameraImage_get_format_mC8941181520ADD672CD0AD46923FB9029BDB1A74 (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865 (String_t* p0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARExtensions.CameraImageConversionParams::.ctor(UnityEngine.XR.ARExtensions.CameraImage,UnityEngine.TextureFormat,UnityEngine.XR.ARExtensions.CameraImageTransformation)
extern "C" IL2CPP_METHOD_ATTR void CameraImageConversionParams__ctor_m4D98F12770AAF00164B7E9A1E54F9AD3633EB779 (CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1 * __this, CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E  p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!!0> UnityEngine.Texture2D::GetRawTextureData<System.Byte>()
inline NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  Texture2D_GetRawTextureData_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_mE453927FC6491AC23A63CD1174E492BC63409B2E (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  (*) (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *, const RuntimeMethod*))Texture2D_GetRawTextureData_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_mE453927FC6491AC23A63CD1174E492BC63409B2E_gshared)(__this, method);
}
// System.Void* Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::GetUnsafePtr<System.Byte>(Unity.Collections.NativeArray`1<!!0>)
inline void* NativeArrayUnsafeUtility_GetUnsafePtr_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_m3191468493B96C1824159DCA7F015F91CAC23965 (NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  p0, const RuntimeMethod* method)
{
	return ((  void* (*) (NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE , const RuntimeMethod*))NativeArrayUnsafeUtility_GetUnsafePtr_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_m3191468493B96C1824159DCA7F015F91CAC23965_gshared)(p0, method);
}
// System.Void System.IntPtr::.ctor(System.Void*)
extern "C" IL2CPP_METHOD_ATTR void IntPtr__ctor_m6360250F4B87C6AE2F0389DA0DEE1983EED73FB6 (intptr_t* __this, void* p0, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARExtensions.CameraImage::Convert(UnityEngine.XR.ARExtensions.CameraImageConversionParams,System.IntPtr,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void CameraImage_Convert_m2757C81D3F7ECFD8CD815D2947989341F398EA19 (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1  p0, intptr_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARExtensions.CameraImage::Dispose()
extern "C" IL2CPP_METHOD_ATTR void CameraImage_Dispose_m19F854832A4F22DCEF6C474ED836C1A37C6E897C (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
extern "C" IL2CPP_METHOD_ATTR void RawImage_set_texture_m897BC65663AFF15258A611CC6E3480B078F41D23 (RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * __this, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_m2A22A8E963E14F1221F768412663C8D11F806CD6 (const RuntimeMethod* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector2>::get_HasValue()
inline bool Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6 (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *, const RuntimeMethod*))Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_gshared)(__this, method);
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Touch_t806752C775BA713A91B6588A07CA98417CABC003  Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313 (int32_t p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9 (Touch_t806752C775BA713A91B6588A07CA98417CABC003 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Vector2>::.ctor(!0)
inline void Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446 (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D , const RuntimeMethod*))Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_gshared)(__this, p0, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector2>::GetValueOrDefault()
inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  (*) (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *, const RuntimeMethod*))Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.Single UnityEngine.Camera::get_orthographicSize()
extern "C" IL2CPP_METHOD_ATTR float Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, float p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C" IL2CPP_METHOD_ATTR int32_t Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, float p1, const RuntimeMethod* method);
// !0 System.Nullable`1<UnityEngine.Vector2>::get_Value()
inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30 (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  (*) (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *, const RuntimeMethod*))Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C" IL2CPP_METHOD_ATTR float Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C" IL2CPP_METHOD_ATTR int32_t Camera_get_pixelWidth_m67EC53853580E35527F32D6EA002FE21C234172E (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p0, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  p1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507 (float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m55C96FCD397CC69109261572710608D12A4CBD2B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790 (Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Camera_set_orthographicSize_mF15F37A294A7AA2ADD9519728A495DFA0A836428 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_localEulerAngles_mC5E92A989DD8B2E7B854F9D84B528A34AEAA1A17 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
inline bool Nullable_1_get_HasValue_mE3869E2244689BB5F91E7ED43CCD898EEF385143 (Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 *, const RuntimeMethod*))Nullable_1_get_HasValue_mE3869E2244689BB5F91E7ED43CCD898EEF385143_gshared)(__this, method);
}
// !0 System.Nullable`1<UnityEngine.Vector3>::get_Value()
inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Nullable_1_get_Value_m773C0C2E29CED4C69F2401D085CAC0BE162E7EDA (Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 * __this, const RuntimeMethod* method)
{
	return ((  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  (*) (Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 *, const RuntimeMethod*))Nullable_1_get_Value_m773C0C2E29CED4C69F2401D085CAC0BE162E7EDA_gshared)(__this, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C" IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Transform_RotateAround_m433D292B2A38A5A4DEC7DCAE0A8BEAC5C3B2D1DD (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  p1, float p2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C" IL2CPP_METHOD_ATTR float Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, float p0, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void PlaceMultipleObjectsOnPlane::add_onPlacedObject(System.Action)
extern "C" IL2CPP_METHOD_ATTR void PlaceMultipleObjectsOnPlane_add_onPlacedObject_mF5004B8D8CCAAD94807FA2FADFFDE2FBB0E5BDC6 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___value0, const RuntimeMethod* method);
// System.Void PlaceMultipleObjectsOnPlane::remove_onPlacedObject(System.Action)
extern "C" IL2CPP_METHOD_ATTR void PlaceMultipleObjectsOnPlane_remove_onPlacedObject_m8BBAEE55F38C8CA5149D79680E9517233C888F4E (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___value0, const RuntimeMethod* method);
// System.Boolean UIManager::PlanesFound()
extern "C" IL2CPP_METHOD_ATTR bool UIManager_PlanesFound_m80FA266D580E616FC607D27AC8B464629E2D5130 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// UnityEngine.Animator UIManager::get_moveDeviceAnimation()
extern "C" IL2CPP_METHOD_ATTR Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * UIManager_get_moveDeviceAnimation_m645816F7AAB91B52466F8C13A7233FA8A7D74CB7 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" IL2CPP_METHOD_ATTR void Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245 (Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.Animator UIManager::get_tapToPlaceAnimation()
extern "C" IL2CPP_METHOD_ATTR Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * UIManager_get_tapToPlaceAnimation_m3982F6E3229BB5F7260636EE62397CF2E8C5995B (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::get_planeManager()
extern "C" IL2CPP_METHOD_ATTR ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * UIManager_get_planeManager_mC0381B55B1D1A8BBA68B9DD9C5E7C4B08F935228 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::GetAllPlanes(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern "C" IL2CPP_METHOD_ATTR void ARPlaneManager_GetAllPlanes_mBB214CCB7992080E6880901861E93F57EEBAA7F0 (ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * __this, List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>::get_Count()
inline int32_t List_1_get_Count_m9C96EBE4BCAF28484D7D003DB4B84FE118B8A7DE (List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>::.ctor()
inline void List_1__ctor_m643F26A3137C3F5DB048DD827C3242DFCAAA1018 (List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Boolean UnityEngine.Input::GetKey(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKey_m9B7FD21BDC79FA7268AD0524BCA3E882E5FBEAFB (String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C" IL2CPP_METHOD_ATTR void Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * p0, const RuntimeMethod* method);
// System.Boolean Zoom::action()
extern "C" IL2CPP_METHOD_ATTR bool Zoom_action_mAFAECFBC243ACACC67D9DB2728A151600C44E736 (Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.RawImage>()
inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * Component_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m642665A17D3104F99F2305FA4059BEE638357ED1 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m3FED1FF44F93EF1C3A07526800331B638EF4105B_gshared)(__this, method);
}
// System.String UnityEngine.Object::get_name()
extern "C" IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364 (float p0, float p1, float p2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.UI.RawImage TestCameraImage::get_rawImage()
extern "C" IL2CPP_METHOD_ATTR RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * TestCameraImage_get_rawImage_mBE78AD9D9CFF8AE8F307EAD1381DE70E167E22BE (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, const RuntimeMethod* method)
{
	{
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_0 = __this->get_m_RawImage_4();
		return L_0;
	}
}
// System.Void TestCameraImage::set_rawImage(UnityEngine.UI.RawImage)
extern "C" IL2CPP_METHOD_ATTR void TestCameraImage_set_rawImage_m92E962650ABF41F48F2C7A45A152C7C8EB7E5C03 (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___value0, const RuntimeMethod* method)
{
	{
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_0 = ___value0;
		__this->set_m_RawImage_4(L_0);
		return;
	}
}
// UnityEngine.UI.Text TestCameraImage::get_imageInfo()
extern "C" IL2CPP_METHOD_ATTR Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * TestCameraImage_get_imageInfo_mC57ACB38C1196F43B4AA39355D0F0F164BCF0B25 (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, const RuntimeMethod* method)
{
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = __this->get_m_ImageInfo_5();
		return L_0;
	}
}
// System.Void TestCameraImage::set_imageInfo(UnityEngine.UI.Text)
extern "C" IL2CPP_METHOD_ATTR void TestCameraImage_set_imageInfo_mA1DB2774152FE0549F045FC4BA1CFDE78317075C (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___value0, const RuntimeMethod* method)
{
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_0 = ___value0;
		__this->set_m_ImageInfo_5(L_0);
		return;
	}
}
// System.Void TestCameraImage::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void TestCameraImage_OnEnable_mC86D4C2FC61F303D375510C7D9BA460B3C83D6A4 (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestCameraImage_OnEnable_mC86D4C2FC61F303D375510C7D9BA460B3C83D6A4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * L_0 = (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 *)il2cpp_codegen_object_new(Action_1_t386B526A237C5426A1502ADF2690D01DCC513454_il2cpp_TypeInfo_var);
		Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22(L_0, __this, (intptr_t)((intptr_t)TestCameraImage_OnCameraFrameReceived_mF2A20651C82BFE41F637A82A191DF924A3DDF9E3_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ARSubsystemManager_t989EF64D895F30DF0C0E14F235B21675030E00EF_il2cpp_TypeInfo_var);
		ARSubsystemManager_add_cameraFrameReceived_mC5420AAC1E488E6C2B33C30C2936C05A07D6173B(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestCameraImage::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void TestCameraImage_OnDisable_m0971839455DC41D9B54FB3397EA22DB4DCCEF49A (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestCameraImage_OnDisable_m0971839455DC41D9B54FB3397EA22DB4DCCEF49A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * L_0 = (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 *)il2cpp_codegen_object_new(Action_1_t386B526A237C5426A1502ADF2690D01DCC513454_il2cpp_TypeInfo_var);
		Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22(L_0, __this, (intptr_t)((intptr_t)TestCameraImage_OnCameraFrameReceived_mF2A20651C82BFE41F637A82A191DF924A3DDF9E3_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ARSubsystemManager_t989EF64D895F30DF0C0E14F235B21675030E00EF_il2cpp_TypeInfo_var);
		ARSubsystemManager_remove_cameraFrameReceived_m5407919D112A547D758CD53498740D0BB6390414(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestCameraImage::OnCameraFrameReceived(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern "C" IL2CPP_METHOD_ATTR void TestCameraImage_OnCameraFrameReceived_mF2A20651C82BFE41F637A82A191DF924A3DDF9E3 (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672  ___eventArgs0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TestCameraImage_OnCameraFrameReceived_mF2A20651C82BFE41F637A82A191DF924A3DDF9E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1  V_2;
	memset(&V_2, 0, sizeof(V_2));
	NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARSubsystemManager_t989EF64D895F30DF0C0E14F235B21675030E00EF_il2cpp_TypeInfo_var);
		XRCameraSubsystem_t9271DB5D8FEDD3431246FCB6D9257A940246E701 * L_0 = ARSubsystemManager_get_cameraSubsystem_m4D2E3C8CA66D41581D15BD876E7C2F318A995494(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(XRCameraExtensions_t9F3CD7B3BF04287369EE72FE0B6521D7A00C286D_il2cpp_TypeInfo_var);
		bool L_1 = XRCameraExtensions_TryGetLatestImage_m5BB6DEE2E7A97A951F712D811C132BD6ECE28F7E(L_0, (CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * L_2 = __this->get_m_ImageInfo_5();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		int32_t L_5 = CameraImage_get_width_m111259E3F3A6E974E2919FFC851D6B541D594112((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_7);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_8 = L_4;
		int32_t L_9 = CameraImage_get_height_mC4274492B499A6F0076E1FC318EECBB89F1907B2((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_11);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_12 = L_8;
		int32_t L_13 = CameraImage_get_planeCount_mC1115B5A17DF2AF5A8D5B6BB16C83A87E6A00903((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		RuntimeObject * L_15 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_12;
		double L_17 = CameraImage_get_timestamp_m99A5F54F2FD195915DA87D3EF316A6D351501E6D((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		double L_18 = L_17;
		RuntimeObject * L_19 = Box(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_19);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_20 = L_16;
		int32_t L_21 = CameraImage_get_format_mC8941181520ADD672CD0AD46923FB9029BDB1A74((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		int32_t L_22 = L_21;
		RuntimeObject * L_23 = Box(CameraImageFormat_t3B37B8C0CD1FDB32590DE83ABB369EEB65307EBD_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_23);
		String_t* L_24 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral89F3B5900C827B56A0F0306DAE981AD2F046D285, L_20, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_24);
		V_1 = 4;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_25 = __this->get_m_Texture_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_25, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_26)
		{
			goto IL_00ad;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_27 = __this->get_m_Texture_6();
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_27);
		int32_t L_29 = CameraImage_get_width_m111259E3F3A6E974E2919FFC851D6B541D594112((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_28) == ((uint32_t)L_29))))
		{
			goto IL_00ad;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_30 = __this->get_m_Texture_6();
		NullCheck(L_30);
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_30);
		int32_t L_32 = CameraImage_get_height_mC4274492B499A6F0076E1FC318EECBB89F1907B2((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		if ((((int32_t)L_31) == ((int32_t)L_32)))
		{
			goto IL_00c8;
		}
	}

IL_00ad:
	{
		int32_t L_33 = CameraImage_get_width_m111259E3F3A6E974E2919FFC851D6B541D594112((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		int32_t L_34 = CameraImage_get_height_mC4274492B499A6F0076E1FC318EECBB89F1907B2((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		int32_t L_35 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_36 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_36, L_33, L_34, L_35, (bool)0, /*hidden argument*/NULL);
		__this->set_m_Texture_6(L_36);
	}

IL_00c8:
	{
		CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E  L_37 = V_0;
		int32_t L_38 = V_1;
		CameraImageConversionParams__ctor_m4D98F12770AAF00164B7E9A1E54F9AD3633EB779((CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1 *)(&V_2), L_37, L_38, 2, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_39 = __this->get_m_Texture_6();
		NullCheck(L_39);
		NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  L_40 = Texture2D_GetRawTextureData_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_mE453927FC6491AC23A63CD1174E492BC63409B2E(L_39, /*hidden argument*/Texture2D_GetRawTextureData_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_mE453927FC6491AC23A63CD1174E492BC63409B2E_RuntimeMethod_var);
		V_3 = L_40;
	}

IL_00de:
	try
	{ // begin try (depth: 1)
		CameraImageConversionParams_tEFEA7C9CF777BD67E3221CBC7CB3D2FD1E99B4D1  L_41 = V_2;
		NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE  L_42 = V_3;
		void* L_43 = NativeArrayUnsafeUtility_GetUnsafePtr_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_m3191468493B96C1824159DCA7F015F91CAC23965(L_42, /*hidden argument*/NativeArrayUnsafeUtility_GetUnsafePtr_TisByte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_m3191468493B96C1824159DCA7F015F91CAC23965_RuntimeMethod_var);
		intptr_t L_44;
		memset(&L_44, 0, sizeof(L_44));
		IntPtr__ctor_m6360250F4B87C6AE2F0389DA0DEE1983EED73FB6((&L_44), (void*)(void*)L_43, /*hidden argument*/NULL);
		int32_t L_45 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tB6F81A87EBCD8958AF30E15FACF167CE524DEABE *)(&V_3))->___m_Length_1);
		CameraImage_Convert_m2757C81D3F7ECFD8CD815D2947989341F398EA19((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), L_41, (intptr_t)L_44, L_45, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x102, FINALLY_00fa);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00fa;
	}

FINALLY_00fa:
	{ // begin finally (depth: 1)
		CameraImage_Dispose_m19F854832A4F22DCEF6C474ED836C1A37C6E897C((CameraImage_tA347D573CD5319A54DF1A4D73E36B0975970723E *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RESET_LEAVE(0x102);
		IL2CPP_END_FINALLY(250)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(250)
	{
		IL2CPP_JUMP_TBL(0x102, IL_0102)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0102:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_46 = __this->get_m_Texture_6();
		NullCheck(L_46);
		Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA(L_46, /*hidden argument*/NULL);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_47 = __this->get_m_RawImage_4();
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_48 = __this->get_m_Texture_6();
		NullCheck(L_47);
		RawImage_set_texture_m897BC65663AFF15258A611CC6E3480B078F41D23(L_47, L_48, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestCameraImage::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TestCameraImage__ctor_mF3E7DACF75725B759BB920AFE1FE624CC9DCB69F (TestCameraImage_tB2E8016F260F8DEF38BA371E881CB56BBDB1B9C6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchCamera::Update()
extern "C" IL2CPP_METHOD_ATTR void TouchCamera_Update_mBCAB9C5C1C2397C1BA76748838DA7AC417A5DC77 (TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCamera_Update_mBCAB9C5C1C2397C1BA76748838DA7AC417A5DC77_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_10 = NULL;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float V_12 = 0.0f;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_14;
	memset(&V_14, 0, sizeof(V_14));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B8_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B8_1;
	memset(&G_B8_1, 0, sizeof(G_B8_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B8_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B7_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B7_1;
	memset(&G_B7_1, 0, sizeof(G_B7_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B7_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B9_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B9_2;
	memset(&G_B9_2, 0, sizeof(G_B9_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B9_3 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B11_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B11_1;
	memset(&G_B11_1, 0, sizeof(G_B11_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B11_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B10_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B10_1;
	memset(&G_B10_1, 0, sizeof(G_B10_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B10_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B12_0;
	memset(&G_B12_0, 0, sizeof(G_B12_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B12_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B12_2;
	memset(&G_B12_2, 0, sizeof(G_B12_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B12_3 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B14_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B14_1;
	memset(&G_B14_1, 0, sizeof(G_B14_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B14_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B13_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B13_1;
	memset(&G_B13_1, 0, sizeof(G_B13_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B13_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B15_0;
	memset(&G_B15_0, 0, sizeof(G_B15_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B15_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B15_2;
	memset(&G_B15_2, 0, sizeof(G_B15_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B15_3 = NULL;
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14 * G_B19_0 = NULL;
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14 * G_B18_0 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B20_0;
	memset(&G_B20_0, 0, sizeof(G_B20_0));
	TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14 * G_B20_1 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B23_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B23_1;
	memset(&G_B23_1, 0, sizeof(G_B23_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B23_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B22_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B22_1;
	memset(&G_B22_1, 0, sizeof(G_B22_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B22_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B24_0;
	memset(&G_B24_0, 0, sizeof(G_B24_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B24_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B24_2;
	memset(&G_B24_2, 0, sizeof(G_B24_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B24_3 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B26_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B26_1;
	memset(&G_B26_1, 0, sizeof(G_B26_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B26_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B25_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B25_1;
	memset(&G_B25_1, 0, sizeof(G_B25_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B25_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B27_0;
	memset(&G_B27_0, 0, sizeof(G_B27_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B27_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B27_2;
	memset(&G_B27_2, 0, sizeof(G_B27_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B27_3 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B29_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B29_1;
	memset(&G_B29_1, 0, sizeof(G_B29_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B29_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B28_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B28_1;
	memset(&G_B28_1, 0, sizeof(G_B28_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B28_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B30_0;
	memset(&G_B30_0, 0, sizeof(G_B30_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B30_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B30_2;
	memset(&G_B30_2, 0, sizeof(G_B30_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B30_3 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B32_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B32_1;
	memset(&G_B32_1, 0, sizeof(G_B32_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B32_2 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B31_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B31_1;
	memset(&G_B31_1, 0, sizeof(G_B31_1));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B31_2 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B33_0;
	memset(&G_B33_0, 0, sizeof(G_B33_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B33_1 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B33_2;
	memset(&G_B33_2, 0, sizeof(G_B33_2));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B33_3 = NULL;
	{
		int32_t L_0 = Input_get_touchCount_m2A22A8E963E14F1221F768412663C8D11F806CD6(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_1 = __this->get_oldTouchPositions_4();
		NullCheck(L_1);
		il2cpp_codegen_initobj(((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_2 = __this->get_oldTouchPositions_4();
		NullCheck(L_2);
		il2cpp_codegen_initobj(((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		return;
	}

IL_002c:
	{
		int32_t L_3 = Input_get_touchCount_m2A22A8E963E14F1221F768412663C8D11F806CD6(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_018f;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_4 = __this->get_oldTouchPositions_4();
		NullCheck(L_4);
		bool L_5 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (!L_5)
		{
			goto IL_005d;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_6 = __this->get_oldTouchPositions_4();
		NullCheck(L_6);
		bool L_7 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_008f;
		}
	}

IL_005d:
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_8 = __this->get_oldTouchPositions_4();
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_9 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_10 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_11), L_10, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_11);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_12 = __this->get_oldTouchPositions_4();
		NullCheck(L_12);
		il2cpp_codegen_initobj(((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		return;
	}

IL_008f:
	{
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_13 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_13;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_14 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_14;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = L_15;
		NullCheck(L_16);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_17 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_16, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_19 = __this->get_oldTouchPositions_4();
		NullCheck(L_19);
		int32_t L_20 = 0;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_21 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_6 = L_21;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_22 = V_1;
		V_7 = L_22;
		bool L_23 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_6), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B7_0 = L_18;
		G_B7_1 = L_17;
		G_B7_2 = L_16;
		if (L_23)
		{
			G_B8_0 = L_18;
			G_B8_1 = L_17;
			G_B8_2 = L_16;
			goto IL_00d6;
		}
	}
	{
		il2cpp_codegen_initobj((&V_8), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_24 = V_8;
		G_B9_0 = L_24;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_00e9;
	}

IL_00d6:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_6), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_26 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_27 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_25, L_26, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_28;
		memset(&L_28, 0, sizeof(L_28));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_28), L_27, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B9_0 = L_28;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_00e9:
	{
		V_4 = G_B9_0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_29 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		NullCheck(L_29);
		float L_30 = Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958(L_29, /*hidden argument*/NULL);
		V_5 = L_30;
		bool L_31 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B10_0 = G_B9_1;
		G_B10_1 = G_B9_2;
		G_B10_2 = G_B9_3;
		if (L_31)
		{
			G_B11_0 = G_B9_1;
			G_B11_1 = G_B9_2;
			G_B11_2 = G_B9_3;
			goto IL_010d;
		}
	}
	{
		il2cpp_codegen_initobj((&V_6), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_32 = V_6;
		G_B12_0 = L_32;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0120;
	}

IL_010d:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		float L_34 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_35 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_33, L_34, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_36;
		memset(&L_36, 0, sizeof(L_36));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_36), L_35, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B12_0 = L_36;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0120:
	{
		V_2 = G_B12_0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_37 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		NullCheck(L_37);
		int32_t L_38 = Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05(L_37, /*hidden argument*/NULL);
		V_3 = (((float)((float)L_38)));
		bool L_39 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_2), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B13_0 = G_B12_1;
		G_B13_1 = G_B12_2;
		G_B13_2 = G_B12_3;
		if (L_39)
		{
			G_B14_0 = G_B12_1;
			G_B14_1 = G_B12_2;
			G_B14_2 = G_B12_3;
			goto IL_0143;
		}
	}
	{
		il2cpp_codegen_initobj((&V_4), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_40 = V_4;
		G_B15_0 = L_40;
		G_B15_1 = G_B13_0;
		G_B15_2 = G_B13_1;
		G_B15_3 = G_B13_2;
		goto IL_015f;
	}

IL_0143:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_41 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_2), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		float L_42 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_41, L_42, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_44 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_43, (2.0f), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_45;
		memset(&L_45, 0, sizeof(L_45));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_45), L_44, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B15_0 = L_45;
		G_B15_1 = G_B14_0;
		G_B15_2 = G_B14_1;
		G_B15_3 = G_B14_2;
	}

IL_015f:
	{
		V_4 = G_B15_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_46 = Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_47 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_46, /*hidden argument*/NULL);
		NullCheck(G_B15_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_48 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(G_B15_1, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_49 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(G_B15_2, L_48, /*hidden argument*/NULL);
		NullCheck(G_B15_3);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(G_B15_3, L_49, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_50 = __this->get_oldTouchPositions_4();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_51 = V_1;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_52), L_51, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_52);
		return;
	}

IL_018f:
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_53 = __this->get_oldTouchPositions_4();
		NullCheck(L_53);
		bool L_54 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)((L_53)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (L_54)
		{
			goto IL_0254;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_55 = __this->get_oldTouchPositions_4();
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_56 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_56;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_57 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_58;
		memset(&L_58, 0, sizeof(L_58));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_58), L_57, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_58);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_59 = __this->get_oldTouchPositions_4();
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_60 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(1, /*hidden argument*/NULL);
		V_0 = L_60;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_61 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_62;
		memset(&L_62, 0, sizeof(L_62));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_62), L_61, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_59);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(1), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_62);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_63 = __this->get_oldTouchPositions_4();
		NullCheck(L_63);
		int32_t L_64 = 0;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_65 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_63)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		V_2 = L_65;
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_66 = __this->get_oldTouchPositions_4();
		NullCheck(L_66);
		int32_t L_67 = 1;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_68 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		V_4 = L_68;
		bool L_69 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_2), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		bool L_70 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B18_0 = __this;
		if (((int32_t)((int32_t)L_69&(int32_t)L_70)))
		{
			G_B19_0 = __this;
			goto IL_021c;
		}
	}
	{
		il2cpp_codegen_initobj((&V_6), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_71 = V_6;
		G_B20_0 = L_71;
		G_B20_1 = G_B18_0;
		goto IL_0234;
	}

IL_021c:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_72 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_2), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_73 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_74 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_72, L_73, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_75), L_74, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B20_0 = L_75;
		G_B20_1 = G_B19_0;
	}

IL_0234:
	{
		V_6 = G_B20_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_76 = Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_6), /*hidden argument*/Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_RuntimeMethod_var);
		NullCheck(G_B20_1);
		G_B20_1->set_oldTouchVector_5(L_76);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_77 = __this->get_address_of_oldTouchVector_5();
		float L_78 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_77, /*hidden argument*/NULL);
		__this->set_oldTouchDistance_6(L_78);
		return;
	}

IL_0254:
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_79 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		NullCheck(L_79);
		int32_t L_80 = Camera_get_pixelWidth_m67EC53853580E35527F32D6EA002FE21C234172E(L_79, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_81 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		NullCheck(L_81);
		int32_t L_82 = Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05(L_81, /*hidden argument*/NULL);
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_9), (((float)((float)L_80))), (((float)((float)L_82))), /*hidden argument*/NULL);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_83 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)2);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_84 = L_83;
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_85 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_85;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_86 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_84);
		(L_84)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_86);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_87 = L_84;
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_88 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(1, /*hidden argument*/NULL);
		V_0 = L_88;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_89 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_87);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_89);
		V_10 = L_87;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_90 = V_10;
		NullCheck(L_90);
		int32_t L_91 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_92 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_93 = V_10;
		NullCheck(L_93);
		int32_t L_94 = 1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_95 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_94));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_96 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_92, L_95, /*hidden argument*/NULL);
		V_11 = L_96;
		float L_97 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_11), /*hidden argument*/NULL);
		V_12 = L_97;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_98 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_99 = L_98;
		NullCheck(L_99);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_100 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_99, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_101 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_102 = __this->get_oldTouchPositions_4();
		NullCheck(L_102);
		int32_t L_103 = 0;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_104 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		V_8 = L_104;
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_105 = __this->get_oldTouchPositions_4();
		NullCheck(L_105);
		int32_t L_106 = 1;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_107 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		V_13 = L_107;
		bool L_108 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_8), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		bool L_109 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_13), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B22_0 = L_101;
		G_B22_1 = L_100;
		G_B22_2 = L_99;
		if (((int32_t)((int32_t)L_108&(int32_t)L_109)))
		{
			G_B23_0 = L_101;
			G_B23_1 = L_100;
			G_B23_2 = L_99;
			goto IL_0310;
		}
	}
	{
		il2cpp_codegen_initobj((&V_14), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_110 = V_14;
		G_B24_0 = L_110;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_0328;
	}

IL_0310:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_111 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_8), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_112 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_13), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_113 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_111, L_112, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_114;
		memset(&L_114, 0, sizeof(L_114));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_114), L_113, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B24_0 = L_114;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_0328:
	{
		V_6 = G_B24_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_115 = V_9;
		V_7 = L_115;
		bool L_116 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_6), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B25_0 = G_B24_1;
		G_B25_1 = G_B24_2;
		G_B25_2 = G_B24_3;
		if (L_116)
		{
			G_B26_0 = G_B24_1;
			G_B26_1 = G_B24_2;
			G_B26_2 = G_B24_3;
			goto IL_0343;
		}
	}
	{
		il2cpp_codegen_initobj((&V_13), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_117 = V_13;
		G_B27_0 = L_117;
		G_B27_1 = G_B25_0;
		G_B27_2 = G_B25_1;
		G_B27_3 = G_B25_2;
		goto IL_0356;
	}

IL_0343:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_118 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_6), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_119 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_120 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_118, L_119, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_121;
		memset(&L_121, 0, sizeof(L_121));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_121), L_120, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B27_0 = L_121;
		G_B27_1 = G_B26_0;
		G_B27_2 = G_B26_1;
		G_B27_3 = G_B26_2;
	}

IL_0356:
	{
		V_4 = G_B27_0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_122 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		NullCheck(L_122);
		float L_123 = Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958(L_122, /*hidden argument*/NULL);
		V_5 = L_123;
		bool L_124 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B28_0 = G_B27_1;
		G_B28_1 = G_B27_2;
		G_B28_2 = G_B27_3;
		if (L_124)
		{
			G_B29_0 = G_B27_1;
			G_B29_1 = G_B27_2;
			G_B29_2 = G_B27_3;
			goto IL_037a;
		}
	}
	{
		il2cpp_codegen_initobj((&V_6), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_125 = V_6;
		G_B30_0 = L_125;
		G_B30_1 = G_B28_0;
		G_B30_2 = G_B28_1;
		G_B30_3 = G_B28_2;
		goto IL_038d;
	}

IL_037a:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_126 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		float L_127 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_128 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_126, L_127, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_129;
		memset(&L_129, 0, sizeof(L_129));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_129), L_128, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B30_0 = L_129;
		G_B30_1 = G_B29_0;
		G_B30_2 = G_B29_1;
		G_B30_3 = G_B29_2;
	}

IL_038d:
	{
		V_2 = G_B30_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_130 = V_9;
		float L_131 = L_130.get_y_1();
		V_3 = L_131;
		bool L_132 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_2), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B31_0 = G_B30_1;
		G_B31_1 = G_B30_2;
		G_B31_2 = G_B30_3;
		if (L_132)
		{
			G_B32_0 = G_B30_1;
			G_B32_1 = G_B30_2;
			G_B32_2 = G_B30_3;
			goto IL_03ab;
		}
	}
	{
		il2cpp_codegen_initobj((&V_4), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_133 = V_4;
		G_B33_0 = L_133;
		G_B33_1 = G_B31_0;
		G_B33_2 = G_B31_1;
		G_B33_3 = G_B31_2;
		goto IL_03bd;
	}

IL_03ab:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_134 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_2), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		float L_135 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_136 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_134, L_135, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_137;
		memset(&L_137, 0, sizeof(L_137));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_137), L_136, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B33_0 = L_137;
		G_B33_1 = G_B32_0;
		G_B33_2 = G_B32_1;
		G_B33_3 = G_B32_2;
	}

IL_03bd:
	{
		V_4 = G_B33_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_138 = Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_4), /*hidden argument*/Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_139 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_138, /*hidden argument*/NULL);
		NullCheck(G_B33_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_140 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(G_B33_1, L_139, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_141 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(G_B33_2, L_140, /*hidden argument*/NULL);
		NullCheck(G_B33_3);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(G_B33_3, L_141, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_142 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_143 = L_142;
		NullCheck(L_143);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_144 = Transform_get_localRotation_mEDA319E1B42EF12A19A95AC0824345B6574863FE(L_143, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_145 = __this->get_address_of_oldTouchVector_5();
		float L_146 = L_145->get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_147 = V_11;
		float L_148 = L_147.get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_149 = __this->get_address_of_oldTouchVector_5();
		float L_150 = L_149->get_x_0();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_151 = V_11;
		float L_152 = L_151.get_y_1();
		float L_153 = __this->get_oldTouchDistance_6();
		float L_154 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_155 = Mathf_Clamp_m033DD894F89E6DCCDAFC580091053059C86A4507(((float)((float)((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_146, (float)L_148)), (float)((float)il2cpp_codegen_multiply((float)L_150, (float)L_152))))/(float)L_153))/(float)L_154)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_156 = asinf(L_155);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_157;
		memset(&L_157, 0, sizeof(L_157));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_157), (0.0f), (0.0f), ((float)((float)L_156/(float)(0.0174532924f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_158 = Quaternion_Euler_m55C96FCD397CC69109261572710608D12A4CBD2B(L_157, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_159 = Quaternion_op_Multiply_mDB9F738AA8160E3D85549F4FEDA23BC658B5A790(L_144, L_158, /*hidden argument*/NULL);
		NullCheck(L_143);
		Transform_set_localRotation_mE2BECB0954FFC1D93FB631600D9A9BEFF41D9C8A(L_143, L_159, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_160 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_161 = L_160;
		NullCheck(L_161);
		float L_162 = Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958(L_161, /*hidden argument*/NULL);
		float L_163 = __this->get_oldTouchDistance_6();
		float L_164 = V_12;
		NullCheck(L_161);
		Camera_set_orthographicSize_mF15F37A294A7AA2ADD9519728A495DFA0A836428(L_161, ((float)il2cpp_codegen_multiply((float)L_162, (float)((float)((float)L_163/(float)L_164)))), /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_165 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_166 = L_165;
		NullCheck(L_166);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_167 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_166, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_168 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_169 = V_10;
		NullCheck(L_169);
		int32_t L_170 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_171 = (L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_170));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_172 = V_10;
		NullCheck(L_172);
		int32_t L_173 = 1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_174 = (L_172)->GetAt(static_cast<il2cpp_array_size_t>(L_173));
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_175 = Vector2_op_Addition_m81A4D928B8E399DA3A4E3ACD8937EDFDCB014682(L_171, L_174, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_176 = V_9;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_177 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_175, L_176, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_178 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_m9560AE98225581598F654E6EEC19A77EBF186D2F_RuntimeMethod_var);
		NullCheck(L_178);
		float L_179 = Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958(L_178, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_180 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_177, L_179, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_181 = V_9;
		float L_182 = L_181.get_y_1();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_183 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_180, L_182, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_184 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_183, /*hidden argument*/NULL);
		NullCheck(L_168);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_185 = Transform_TransformDirection_m85FC1D7E1322E94F65DA59AEF3B1166850B183EF(L_168, L_184, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_186 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_167, L_185, /*hidden argument*/NULL);
		NullCheck(L_166);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_166, L_186, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_187 = __this->get_oldTouchPositions_4();
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_188 = V_10;
		NullCheck(L_188);
		int32_t L_189 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_190 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_189));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_191;
		memset(&L_191, 0, sizeof(L_191));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_191), L_190, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_187);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_191);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_192 = __this->get_oldTouchPositions_4();
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_193 = V_10;
		NullCheck(L_193);
		int32_t L_194 = 1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_195 = (L_193)->GetAt(static_cast<il2cpp_array_size_t>(L_194));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_196;
		memset(&L_196, 0, sizeof(L_196));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_196), L_195, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_192);
		(L_192)->SetAt(static_cast<il2cpp_array_size_t>(1), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_196);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_197 = V_11;
		__this->set_oldTouchVector_5(L_197);
		float L_198 = V_12;
		__this->set_oldTouchDistance_6(L_198);
		return;
	}
}
// System.Void TouchCamera::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TouchCamera__ctor_m3AC5A6605BE56765CE796118F3E959418F204168 (TouchCamera_t5804FE07848FB1046EA19DC751DF6A59C5F5FB14 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCamera__ctor_m3AC5A6605BE56765CE796118F3E959418F204168_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_0 = (Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6*)SZArrayNew(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6_il2cpp_TypeInfo_var, (uint32_t)2);
		__this->set_oldTouchPositions_4(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void TouchCameraWithRotation::reset(System.Nullable`1<UnityEngine.Vector3>)
extern "C" IL2CPP_METHOD_ATTR void TouchCameraWithRotation_reset_m3C8773298041A9F489D80A21CC1E46ED5A88DFFA (TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * __this, Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5  ___targetV30, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCameraWithRotation_reset_m3C8773298041A9F489D80A21CC1E46ED5A88DFFA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B2_0 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B1_0 = NULL;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * G_B3_1 = NULL;
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_rotationPlate_5();
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localEulerAngles_mC5E92A989DD8B2E7B854F9D84B528A34AEAA1A17(L_1, L_2, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = __this->get_rotationOffsetPlate_6();
		NullCheck(L_3);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_4 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_3, /*hidden argument*/NULL);
		bool L_5 = Nullable_1_get_HasValue_mE3869E2244689BB5F91E7ED43CCD898EEF385143((Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 *)(&___targetV30), /*hidden argument*/Nullable_1_get_HasValue_mE3869E2244689BB5F91E7ED43CCD898EEF385143_RuntimeMethod_var);
		G_B1_0 = L_4;
		if (L_5)
		{
			G_B2_0 = L_4;
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		goto IL_0037;
	}

IL_0030:
	{
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Nullable_1_get_Value_m773C0C2E29CED4C69F2401D085CAC0BE162E7EDA((Nullable_1_tDE125BFF5A28C3581BA48CED1BE5F03B0DB0F7C5 *)(&___targetV30), /*hidden argument*/Nullable_1_get_Value_m773C0C2E29CED4C69F2401D085CAC0BE162E7EDA_RuntimeMethod_var);
		G_B3_0 = L_7;
		G_B3_1 = G_B2_0;
	}

IL_0037:
	{
		NullCheck(G_B3_1);
		Transform_set_localEulerAngles_mC5E92A989DD8B2E7B854F9D84B528A34AEAA1A17(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchCameraWithRotation::Start()
extern "C" IL2CPP_METHOD_ATTR void TouchCameraWithRotation_Start_mA487330C1145D412E92E2B1F7D2A997C57B6DC36 (TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void TouchCameraWithRotation::Update()
extern "C" IL2CPP_METHOD_ATTR void TouchCameraWithRotation_Update_m8B43FC15C287A6D9517B07613ED80FAD179F9BD5 (TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCameraWithRotation_Update_m8B43FC15C287A6D9517B07613ED80FAD179F9BD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t806752C775BA713A91B6588A07CA98417CABC003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* V_11 = NULL;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B12_0;
	memset(&G_B12_0, 0, sizeof(G_B12_0));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B15_0;
	memset(&G_B15_0, 0, sizeof(G_B15_0));
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B18_0;
	memset(&G_B18_0, 0, sizeof(G_B18_0));
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * G_B28_0 = NULL;
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * G_B27_0 = NULL;
	Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  G_B29_0;
	memset(&G_B29_0, 0, sizeof(G_B29_0));
	TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * G_B29_1 = NULL;
	{
		bool L_0 = __this->get_isEnabled_9();
		if (!L_0)
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_1 = Input_get_touchCount_m2A22A8E963E14F1221F768412663C8D11F806CD6(/*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_007b;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_2 = __this->get_oldTouchPositions_16();
		NullCheck(L_2);
		il2cpp_codegen_initobj(((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_3 = __this->get_oldTouchPositions_16();
		NullCheck(L_3);
		il2cpp_codegen_initobj(((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		bool L_4 = __this->get_isRotationOffseted_15();
		if (L_4)
		{
			goto IL_0477;
		}
	}
	{
		__this->set_isRotationOffseted_15((bool)1);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_5 = __this->get_rotationOffsetPlate_6();
		NullCheck(L_5);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_5, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_7 = __this->get_rotationPlate_5();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localEulerAngles_mC5E92A989DD8B2E7B854F9D84B528A34AEAA1A17(L_6, L_9, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_10 = __this->get_rotationPlate_5();
		NullCheck(L_10);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_12 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localEulerAngles_mC5E92A989DD8B2E7B854F9D84B528A34AEAA1A17(L_11, L_12, /*hidden argument*/NULL);
		return;
	}

IL_007b:
	{
		bool L_13 = __this->get_isEnabled_9();
		if (!L_13)
		{
			goto IL_029d;
		}
	}
	{
		int32_t L_14 = Input_get_touchCount_m2A22A8E963E14F1221F768412663C8D11F806CD6(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_029d;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_15 = __this->get_oldTouchPositions_16();
		NullCheck(L_15);
		bool L_16 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (!L_16)
		{
			goto IL_00b7;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_17 = __this->get_oldTouchPositions_16();
		NullCheck(L_17);
		bool L_18 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (!L_18)
		{
			goto IL_00e9;
		}
	}

IL_00b7:
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_19 = __this->get_oldTouchPositions_16();
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_20 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_20;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_21 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_22), L_21, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_19);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_22);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_23 = __this->get_oldTouchPositions_16();
		NullCheck(L_23);
		il2cpp_codegen_initobj(((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		return;
	}

IL_00e9:
	{
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_24 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_24;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_25 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_25;
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_26 = __this->get_oldTouchPositions_16();
		NullCheck(L_26);
		int32_t L_27 = 0;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_28 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_7 = L_28;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_29 = V_1;
		V_8 = L_29;
		bool L_30 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_7), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (L_30)
		{
			goto IL_011e;
		}
	}
	{
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_31 = V_9;
		G_B12_0 = L_31;
		goto IL_0131;
	}

IL_011e:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_32 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_7), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_33 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_34 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_32, L_33, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_35), L_34, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B12_0 = L_35;
	}

IL_0131:
	{
		V_5 = G_B12_0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_36 = __this->get_mainCamera_4();
		NullCheck(L_36);
		float L_37 = Camera_get_orthographicSize_m700FCD8CF48BC59A0415A624328B4A627B88D958(L_36, /*hidden argument*/NULL);
		V_6 = L_37;
		bool L_38 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_5), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (L_38)
		{
			goto IL_0155;
		}
	}
	{
		il2cpp_codegen_initobj((&V_7), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_39 = V_7;
		G_B15_0 = L_39;
		goto IL_0168;
	}

IL_0155:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_40 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_5), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		float L_41 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_42 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_40, L_41, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_43;
		memset(&L_43, 0, sizeof(L_43));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_43), L_42, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B15_0 = L_43;
	}

IL_0168:
	{
		V_3 = G_B15_0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_44 = __this->get_mainCamera_4();
		NullCheck(L_44);
		int32_t L_45 = Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05(L_44, /*hidden argument*/NULL);
		V_4 = (((float)((float)L_45)));
		bool L_46 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_3), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (L_46)
		{
			goto IL_018c;
		}
	}
	{
		il2cpp_codegen_initobj((&V_5), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_47 = V_5;
		G_B18_0 = L_47;
		goto IL_01a9;
	}

IL_018c:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_48 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_3), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		float L_49 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_50 = Vector2_op_Division_m0961A935168EE6701E098E2B37013DFFF46A5077(L_48, L_49, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_51 = Vector2_op_Multiply_m8A843A37F2F3199EBE99DC7BDABC1DC2EE01AF56(L_50, (25.0f), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_52;
		memset(&L_52, 0, sizeof(L_52));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_52), L_51, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B18_0 = L_52;
	}

IL_01a9:
	{
		V_5 = G_B18_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_53 = Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_5), /*hidden argument*/Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_54 = Vector2_op_Implicit_mD152B6A34B4DB7FFECC2844D74718568FE867D6F(L_53, /*hidden argument*/NULL);
		V_2 = L_54;
		bool L_55 = __this->get_isLimitYAngle_12();
		if (!L_55)
		{
			goto IL_020a;
		}
	}
	{
		(&V_2)->set_y_3((0.0f));
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_56 = __this->get_rotationPlate_5();
		NullCheck(L_56);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_57 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_56, /*hidden argument*/NULL);
		NullCheck(L_57);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_58 = Transform_get_eulerAngles_mF2D798FA8B18F7A1A0C4A2198329ADBAF07E37CA(L_57, /*hidden argument*/NULL);
		float L_59 = L_58.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_60 = V_2;
		float L_61 = L_60.get_x_2();
		V_10 = ((float)il2cpp_codegen_add((float)L_59, (float)L_61));
		float L_62 = V_10;
		float L_63 = __this->get_limitYAngleAClockwise_14();
		if ((!(((float)L_62) > ((float)L_63))))
		{
			goto IL_020a;
		}
	}
	{
		float L_64 = V_10;
		float L_65 = __this->get_limitYAngleClockwise_13();
		if ((!(((float)L_64) < ((float)L_65))))
		{
			goto IL_020a;
		}
	}
	{
		(&V_2)->set_x_2((0.0f));
	}

IL_020a:
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_66 = __this->get_rotationPlate_5();
		NullCheck(L_66);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_67 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_66, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_68 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_69;
		memset(&L_69, 0, sizeof(L_69));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_69), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_70 = V_2;
		float L_71 = L_70.get_x_2();
		NullCheck(L_67);
		Transform_RotateAround_m433D292B2A38A5A4DEC7DCAE0A8BEAC5C3B2D1DD(L_67, L_68, L_69, L_71, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_72 = __this->get_rotationPlate_5();
		NullCheck(L_72);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_73 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_72, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_74 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_75;
		memset(&L_75, 0, sizeof(L_75));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_75), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_76 = V_2;
		float L_77 = L_76.get_y_3();
		NullCheck(L_73);
		Transform_RotateAround_m433D292B2A38A5A4DEC7DCAE0A8BEAC5C3B2D1DD(L_73, L_74, L_75, ((-L_77)), /*hidden argument*/NULL);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_78 = __this->get_oldTouchPositions_16();
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_79 = V_1;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_80;
		memset(&L_80, 0, sizeof(L_80));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_80), L_79, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_78);
		(L_78)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_80);
		__this->set_isRotationOffseted_15((bool)0);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_81 = __this->get_rotationPlate_5();
		NullCheck(L_81);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_82 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_81, /*hidden argument*/NULL);
		NullCheck(L_82);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_83 = Transform_get_rotation_m3AB90A67403249AECCA5E02BC70FCE8C90FE9FB9(L_82, /*hidden argument*/NULL);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_84 = L_83;
		RuntimeObject * L_85 = Box(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var, &L_84);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_85, /*hidden argument*/NULL);
		return;
	}

IL_029d:
	{
		bool L_86 = __this->get_isEnabled_9();
		if (L_86)
		{
			goto IL_02b0;
		}
	}
	{
		bool L_87 = __this->get_isOnlyZoomable_10();
		if (!L_87)
		{
			goto IL_0477;
		}
	}

IL_02b0:
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_88 = __this->get_oldTouchPositions_16();
		NullCheck(L_88);
		bool L_89 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)((L_88)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		if (L_89)
		{
			goto IL_0375;
		}
	}
	{
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_90 = __this->get_oldTouchPositions_16();
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_91 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_91;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_92 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_93;
		memset(&L_93, 0, sizeof(L_93));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_93), L_92, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_90);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_93);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_94 = __this->get_oldTouchPositions_16();
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_95 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(1, /*hidden argument*/NULL);
		V_0 = L_95;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_96 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_97;
		memset(&L_97, 0, sizeof(L_97));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_97), L_96, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_94);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(1), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_97);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_98 = __this->get_oldTouchPositions_16();
		NullCheck(L_98);
		int32_t L_99 = 0;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_100 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_99));
		V_3 = L_100;
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_101 = __this->get_oldTouchPositions_16();
		NullCheck(L_101);
		int32_t L_102 = 1;
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_103 = (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )(L_101)->GetAt(static_cast<il2cpp_array_size_t>(L_102));
		V_5 = L_103;
		bool L_104 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_3), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		bool L_105 = Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_5), /*hidden argument*/Nullable_1_get_HasValue_m86766498BBF2DC3AB0128F33AC3F200BF93BB1B6_RuntimeMethod_var);
		G_B27_0 = __this;
		if (((int32_t)((int32_t)L_104&(int32_t)L_105)))
		{
			G_B28_0 = __this;
			goto IL_033d;
		}
	}
	{
		il2cpp_codegen_initobj((&V_7), sizeof(Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC ));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_106 = V_7;
		G_B29_0 = L_106;
		G_B29_1 = G_B27_0;
		goto IL_0355;
	}

IL_033d:
	{
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_107 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_3), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_108 = Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_5), /*hidden argument*/Nullable_1_GetValueOrDefault_mEE564E3EE5C001E61D5537F31531B2B7C7A06D8C_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_109 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_107, L_108, /*hidden argument*/NULL);
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_110), L_109, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		G_B29_0 = L_110;
		G_B29_1 = G_B28_0;
	}

IL_0355:
	{
		V_7 = G_B29_0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_111 = Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30((Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC *)(&V_7), /*hidden argument*/Nullable_1_get_Value_m763182B16D2A96848EAFACFA31AC9985EFCC0F30_RuntimeMethod_var);
		NullCheck(G_B29_1);
		G_B29_1->set_oldTouchVector_17(L_111);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * L_112 = __this->get_address_of_oldTouchVector_17();
		float L_113 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)L_112, /*hidden argument*/NULL);
		__this->set_oldTouchDistance_18(L_113);
		return;
	}

IL_0375:
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_114 = __this->get_mainCamera_4();
		NullCheck(L_114);
		int32_t L_115 = Camera_get_pixelWidth_m67EC53853580E35527F32D6EA002FE21C234172E(L_114, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_116 = __this->get_mainCamera_4();
		NullCheck(L_116);
		int32_t L_117 = Camera_get_pixelHeight_m38879ACBA6B21C25E83AB07FA37A8E5EB7A51B05(L_116, /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_118;
		memset(&L_118, 0, sizeof(L_118));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_118), (((float)((float)L_115))), (((float)((float)L_117))), /*hidden argument*/NULL);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_119 = (Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6*)SZArrayNew(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6_il2cpp_TypeInfo_var, (uint32_t)2);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_120 = L_119;
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_121 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(0, /*hidden argument*/NULL);
		V_0 = L_121;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_122 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_120);
		(L_120)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_122);
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_123 = L_120;
		Touch_t806752C775BA713A91B6588A07CA98417CABC003  L_124 = Input_GetTouch_m1ABE5E9866FD4C5FDFC5DD8FF4E7DCEDE2DD9313(1, /*hidden argument*/NULL);
		V_0 = L_124;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_125 = Touch_get_position_mC0913727A83103C5E2B56A5D76AB8F911A79D1E9((Touch_t806752C775BA713A91B6588A07CA98417CABC003 *)(&V_0), /*hidden argument*/NULL);
		NullCheck(L_123);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D )L_125);
		V_11 = L_123;
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_126 = V_11;
		NullCheck(L_126);
		int32_t L_127 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_128 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_127));
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_129 = V_11;
		NullCheck(L_129);
		int32_t L_130 = 1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_131 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_130));
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_132 = Vector2_op_Subtraction_m2B347E4311EDBBBF27573E34899D2492E6B063C0(L_128, L_131, /*hidden argument*/NULL);
		V_12 = L_132;
		float L_133 = Vector2_get_magnitude_m66097AFDF9696BD3E88467D4398D4F82B8A4C7DF((Vector2_tA85D2DD88578276CA8A8796756458277E72D073D *)(&V_12), /*hidden argument*/NULL);
		V_13 = L_133;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_134 = __this->get_mainCamera_4();
		NullCheck(L_134);
		float L_135 = Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6(L_134, /*hidden argument*/NULL);
		float L_136 = V_13;
		float L_137 = __this->get_oldTouchDistance_18();
		V_14 = ((float)il2cpp_codegen_subtract((float)L_135, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_136, (float)L_137)), (float)(0.2f)))));
		float L_138 = V_14;
		float L_139 = __this->get_minFoV_7();
		if ((!(((float)L_138) < ((float)L_139))))
		{
			goto IL_0416;
		}
	}
	{
		float L_140 = __this->get_minFoV_7();
		V_14 = L_140;
		goto IL_0428;
	}

IL_0416:
	{
		float L_141 = V_14;
		float L_142 = __this->get_maxFoV_8();
		if ((!(((float)L_141) > ((float)L_142))))
		{
			goto IL_0428;
		}
	}
	{
		float L_143 = __this->get_maxFoV_8();
		V_14 = L_143;
	}

IL_0428:
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_144 = __this->get_mainCamera_4();
		float L_145 = V_14;
		NullCheck(L_144);
		Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5(L_144, L_145, /*hidden argument*/NULL);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_146 = __this->get_oldTouchPositions_16();
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_147 = V_11;
		NullCheck(L_147);
		int32_t L_148 = 0;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_149 = (L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_148));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_150;
		memset(&L_150, 0, sizeof(L_150));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_150), L_149, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_146);
		(L_146)->SetAt(static_cast<il2cpp_array_size_t>(0), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_150);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_151 = __this->get_oldTouchPositions_16();
		Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* L_152 = V_11;
		NullCheck(L_152);
		int32_t L_153 = 1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_154 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC  L_155;
		memset(&L_155, 0, sizeof(L_155));
		Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446((&L_155), L_154, /*hidden argument*/Nullable_1__ctor_mAD1BFA560688F8A5C246FB146A0333E7D8EF4446_RuntimeMethod_var);
		NullCheck(L_151);
		(L_151)->SetAt(static_cast<il2cpp_array_size_t>(1), (Nullable_1_t2D31B61CB1F8452E868737CCA40082AA2D1F58DC )L_155);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_156 = V_12;
		__this->set_oldTouchVector_17(L_156);
		float L_157 = V_13;
		__this->set_oldTouchDistance_18(L_157);
	}

IL_0477:
	{
		return;
	}
}
// System.Void TouchCameraWithRotation::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TouchCameraWithRotation__ctor_mC06BB35183F833770B22D2E01CF377292E9292CD (TouchCameraWithRotation_tABC91B3BC5774398A7E582A5BBF358FE03CD9166 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchCameraWithRotation__ctor_mC06BB35183F833770B22D2E01CF377292E9292CD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_minFoV_7((15.0f));
		__this->set_maxFoV_8((55.0f));
		__this->set_isEnabled_9((bool)1);
		__this->set_isRotationOffseted_15((bool)1);
		Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6* L_0 = (Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6*)SZArrayNew(Nullable_1U5BU5D_t9C784AFF279CDFCB9A08DEA5F4627A1E51B54AE6_il2cpp_TypeInfo_var, (uint32_t)2);
		__this->set_oldTouchPositions_16(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::get_planeManager()
extern "C" IL2CPP_METHOD_ATTR ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * UIManager_get_planeManager_mC0381B55B1D1A8BBA68B9DD9C5E7C4B08F935228 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * L_0 = __this->get_m_PlaneManager_6();
		return L_0;
	}
}
// System.Void UIManager::set_planeManager(UnityEngine.XR.ARFoundation.ARPlaneManager)
extern "C" IL2CPP_METHOD_ATTR void UIManager_set_planeManager_mFCA6E0BE924859A49679C4308B38175B11F6E445 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * ___value0, const RuntimeMethod* method)
{
	{
		ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * L_0 = ___value0;
		__this->set_m_PlaneManager_6(L_0);
		return;
	}
}
// UnityEngine.Animator UIManager::get_moveDeviceAnimation()
extern "C" IL2CPP_METHOD_ATTR Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * UIManager_get_moveDeviceAnimation_m645816F7AAB91B52466F8C13A7233FA8A7D74CB7 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get_m_MoveDeviceAnimation_7();
		return L_0;
	}
}
// System.Void UIManager::set_moveDeviceAnimation(UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void UIManager_set_moveDeviceAnimation_mEDDEC001C5239D180DF12312DB7C86F5BF16BC11 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___value0, const RuntimeMethod* method)
{
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = ___value0;
		__this->set_m_MoveDeviceAnimation_7(L_0);
		return;
	}
}
// UnityEngine.Animator UIManager::get_tapToPlaceAnimation()
extern "C" IL2CPP_METHOD_ATTR Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * UIManager_get_tapToPlaceAnimation_m3982F6E3229BB5F7260636EE62397CF2E8C5995B (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = __this->get_m_TapToPlaceAnimation_8();
		return L_0;
	}
}
// System.Void UIManager::set_tapToPlaceAnimation(UnityEngine.Animator)
extern "C" IL2CPP_METHOD_ATTR void UIManager_set_tapToPlaceAnimation_mE397658F309DD0BEE96A1F252B468216E505A930 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___value0, const RuntimeMethod* method)
{
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_0 = ___value0;
		__this->set_m_TapToPlaceAnimation_8(L_0);
		return;
	}
}
// System.Void UIManager::OnEnable()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnEnable_m92BB1673E78D5547CEF8FCC6345C78EEA4C41851 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_OnEnable_m92BB1673E78D5547CEF8FCC6345C78EEA4C41851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * L_0 = (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 *)il2cpp_codegen_object_new(Action_1_t386B526A237C5426A1502ADF2690D01DCC513454_il2cpp_TypeInfo_var);
		Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22(L_0, __this, (intptr_t)((intptr_t)UIManager_FrameChanged_m89E1589B398FF8D7E9A225B41CC37B8C13D65CA2_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ARSubsystemManager_t989EF64D895F30DF0C0E14F235B21675030E00EF_il2cpp_TypeInfo_var);
		ARSubsystemManager_add_cameraFrameReceived_mC5420AAC1E488E6C2B33C30C2936C05A07D6173B(L_0, /*hidden argument*/NULL);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_1, __this, (intptr_t)((intptr_t)UIManager_PlacedObject_m675DD8164B5BAB63DEDD49F28B34CCC2B208C9F9_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_il2cpp_TypeInfo_var);
		PlaceMultipleObjectsOnPlane_add_onPlacedObject_mF5004B8D8CCAAD94807FA2FADFFDE2FBB0E5BDC6(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::OnDisable()
extern "C" IL2CPP_METHOD_ATTR void UIManager_OnDisable_m0F04E11DEB17C6F3492D6C73445C23F15FC1221E (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_OnDisable_m0F04E11DEB17C6F3492D6C73445C23F15FC1221E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 * L_0 = (Action_1_t386B526A237C5426A1502ADF2690D01DCC513454 *)il2cpp_codegen_object_new(Action_1_t386B526A237C5426A1502ADF2690D01DCC513454_il2cpp_TypeInfo_var);
		Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22(L_0, __this, (intptr_t)((intptr_t)UIManager_FrameChanged_m89E1589B398FF8D7E9A225B41CC37B8C13D65CA2_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m739CC09A0C6288EBBA35AA1E22A850BEFE107A22_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(ARSubsystemManager_t989EF64D895F30DF0C0E14F235B21675030E00EF_il2cpp_TypeInfo_var);
		ARSubsystemManager_remove_cameraFrameReceived_m5407919D112A547D758CD53498740D0BB6390414(L_0, /*hidden argument*/NULL);
		Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_1 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
		Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_1, __this, (intptr_t)((intptr_t)UIManager_PlacedObject_m675DD8164B5BAB63DEDD49F28B34CCC2B208C9F9_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlaceMultipleObjectsOnPlane_t776325CC089E260D234CC3B495856446CEB362CC_il2cpp_TypeInfo_var);
		PlaceMultipleObjectsOnPlane_remove_onPlacedObject_m8BBAEE55F38C8CA5149D79680E9517233C888F4E(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::FrameChanged(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern "C" IL2CPP_METHOD_ATTR void UIManager_FrameChanged_m89E1589B398FF8D7E9A225B41CC37B8C13D65CA2 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, ARCameraFrameEventArgs_t25F661E15E542829DB3CAAD158FED01CC8874672  ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_FrameChanged_m89E1589B398FF8D7E9A225B41CC37B8C13D65CA2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = UIManager_PlanesFound_m80FA266D580E616FC607D27AC8B464629E2D5130(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0058;
		}
	}
	{
		bool L_1 = __this->get_m_ShowingMoveDevice_11();
		if (!L_1)
		{
			goto IL_0058;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_2 = UIManager_get_moveDeviceAnimation_m645816F7AAB91B52466F8C13A7233FA8A7D74CB7(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_4 = UIManager_get_moveDeviceAnimation_m645816F7AAB91B52466F8C13A7233FA8A7D74CB7(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_4, _stringLiteral737995F946D2EFF3A4EA8851D2971C4104D85693, /*hidden argument*/NULL);
	}

IL_002d:
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_5 = UIManager_get_tapToPlaceAnimation_m3982F6E3229BB5F7260636EE62397CF2E8C5995B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_7 = UIManager_get_tapToPlaceAnimation_m3982F6E3229BB5F7260636EE62397CF2E8C5995B(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_7, _stringLiteral09FE346526A560596EE8648886F5C433EDED8DDB, /*hidden argument*/NULL);
	}

IL_004a:
	{
		__this->set_m_ShowingTapToPlace_10((bool)1);
		__this->set_m_ShowingMoveDevice_11((bool)0);
	}

IL_0058:
	{
		return;
	}
}
// System.Boolean UIManager::PlanesFound()
extern "C" IL2CPP_METHOD_ATTR bool UIManager_PlanesFound_m80FA266D580E616FC607D27AC8B464629E2D5130 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_PlanesFound_m80FA266D580E616FC607D27AC8B464629E2D5130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * L_0 = UIManager_get_planeManager_mC0381B55B1D1A8BBA68B9DD9C5E7C4B08F935228(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		ARPlaneManager_tC2613B8C888EBC3F92BF68E60F0A3E4DCCFAD69A * L_2 = UIManager_get_planeManager_mC0381B55B1D1A8BBA68B9DD9C5E7C4B08F935228(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var);
		List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * L_3 = ((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->get_s_Planes_9();
		NullCheck(L_2);
		ARPlaneManager_GetAllPlanes_mBB214CCB7992080E6880901861E93F57EEBAA7F0(L_2, L_3, /*hidden argument*/NULL);
		List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * L_4 = ((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->get_s_Planes_9();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m9C96EBE4BCAF28484D7D003DB4B84FE118B8A7DE(L_4, /*hidden argument*/List_1_get_Count_m9C96EBE4BCAF28484D7D003DB4B84FE118B8A7DE_RuntimeMethod_var);
		return (bool)((((int32_t)L_5) > ((int32_t)0))? 1 : 0);
	}
}
// System.Void UIManager::PlacedObject()
extern "C" IL2CPP_METHOD_ATTR void UIManager_PlacedObject_m675DD8164B5BAB63DEDD49F28B34CCC2B208C9F9 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager_PlacedObject_m675DD8164B5BAB63DEDD49F28B34CCC2B208C9F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_ShowingTapToPlace_10();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_1 = UIManager_get_tapToPlaceAnimation_m3982F6E3229BB5F7260636EE62397CF2E8C5995B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m8B2A44B4B1406ED346D1AE6D962294FD58D0D534(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * L_3 = UIManager_get_tapToPlaceAnimation_m3982F6E3229BB5F7260636EE62397CF2E8C5995B(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Animator_SetTrigger_m68D29B7FA54C2F230F5AD780D462612B18E74245(L_3, _stringLiteral737995F946D2EFF3A4EA8851D2971C4104D85693, /*hidden argument*/NULL);
	}

IL_0025:
	{
		__this->set_m_ShowingTapToPlace_10((bool)0);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UIManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UIManager__ctor_m40CA6521CEDDF979D58B6050A6D294A32A1CEA69 (UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_ShowingMoveDevice_11((bool)1);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIManager::.cctor()
extern "C" IL2CPP_METHOD_ATTR void UIManager__cctor_m3B18550BB979CE1BE0EDA898EE0486CCA6947226 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIManager__cctor_m3B18550BB979CE1BE0EDA898EE0486CCA6947226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B * L_0 = (List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B *)il2cpp_codegen_object_new(List_1_t2517CE2CABA3F0EFDEC91E3FA83D4F0417EFF51B_il2cpp_TypeInfo_var);
		List_1__ctor_m643F26A3137C3F5DB048DD827C3242DFCAAA1018(L_0, /*hidden argument*/List_1__ctor_m643F26A3137C3F5DB048DD827C3242DFCAAA1018_RuntimeMethod_var);
		((UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_StaticFields*)il2cpp_codegen_static_fields_for(UIManager_tA871F1BE896F9D7434A52E7413E3C9F11E6B323C_il2cpp_TypeInfo_var))->set_s_Planes_9(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zoom::Start()
extern "C" IL2CPP_METHOD_ATTR void Zoom_Start_mA42CFFF6B9D92AFF162D6EE0EE7FDAF481F69203 (Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_nStep_9();
		__this->set_cameraNR_14(L_0);
		return;
	}
}
// System.Void Zoom::Update()
extern "C" IL2CPP_METHOD_ATTR void Zoom_Update_m15ADA6726F700FBDBFCAAFD90B966F0CDB3E05CE (Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_trigger_13();
		bool L_1 = Input_GetKey_m9B7FD21BDC79FA7268AD0524BCA3E882E5FBEAFB(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		__this->set_isTriggered_6((bool)1);
	}

IL_0014:
	{
		bool L_2 = __this->get_isTriggered_6();
		if (!L_2)
		{
			goto IL_00d4;
		}
	}
	{
		bool L_3 = __this->get_isTriggering_12();
		if (L_3)
		{
			goto IL_00a5;
		}
	}
	{
		__this->set_waited_11((0.0f));
		float L_4 = __this->get_initFieldOfView_7();
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_0050;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_5 = __this->get_mainCamera_4();
		NullCheck(L_5);
		float L_6 = Camera_get_fieldOfView_m065A50B70AC3661337ACA482DDEFA29CCBD249D6(L_5, /*hidden argument*/NULL);
		__this->set_initFieldOfView_7(L_6);
	}

IL_0050:
	{
		__this->set_isTriggering_12((bool)1);
		__this->set_cameraRCount_15(0);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_7 = __this->get_mainCamera_4();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_7, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_zoomInObject_5();
		NullCheck(L_9);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B(L_8, L_10, /*hidden argument*/NULL);
		float L_11 = __this->get_targetFieldOfView_8();
		float L_12 = __this->get_initFieldOfView_7();
		int32_t L_13 = __this->get_nStep_9();
		__this->set_step_16(((float)((float)((float)il2cpp_codegen_subtract((float)L_11, (float)L_12))/(float)(((float)((float)L_13))))));
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_14 = __this->get_mainCamera_4();
		float L_15 = __this->get_initFieldOfView_7();
		NullCheck(L_14);
		Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5(L_14, L_15, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		bool L_16 = Zoom_action_mAFAECFBC243ACACC67D9DB2728A151600C44E736(__this, /*hidden argument*/NULL);
		__this->set_isTriggered_6((bool)((((int32_t)L_16) == ((int32_t)0))? 1 : 0));
		bool L_17 = __this->get_isTriggered_6();
		if (L_17)
		{
			goto IL_00d4;
		}
	}
	{
		__this->set_isTriggering_12((bool)0);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_18 = __this->get_mainCamera_4();
		float L_19 = __this->get_targetFieldOfView_8();
		NullCheck(L_18);
		Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5(L_18, L_19, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		return;
	}
}
// System.Boolean Zoom::action()
extern "C" IL2CPP_METHOD_ATTR bool Zoom_action_mAFAECFBC243ACACC67D9DB2728A151600C44E736 (Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zoom_action_mAFAECFBC243ACACC67D9DB2728A151600C44E736_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		float L_0 = __this->get_waitForSeconds_10();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(L_2, /*hidden argument*/NULL);
		float L_3 = __this->get_waited_11();
		float L_4 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_waited_11(((float)il2cpp_codegen_add((float)L_3, (float)L_4)));
		float L_5 = __this->get_waited_11();
		float L_6 = __this->get_waitForSeconds_10();
		if ((!(((float)L_5) < ((float)L_6))))
		{
			goto IL_004d;
		}
	}
	{
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_7 = __this->get_mainCamera_4();
		NullCheck(L_7);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_7, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_9 = __this->get_zoomInObject_5();
		NullCheck(L_9);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_10 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B(L_8, L_10, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_004d:
	{
		V_0 = (bool)1;
		int32_t L_11 = __this->get_cameraRCount_15();
		int32_t L_12 = __this->get_cameraNR_14();
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_00a8;
		}
	}
	{
		V_0 = (bool)0;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_13 = __this->get_mainCamera_4();
		NullCheck(L_13);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_13, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = __this->get_zoomInObject_5();
		NullCheck(L_15);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_LookAt_mF2738B4AB464ABFB85D16BEB121808F6C73D669B(L_14, L_16, /*hidden argument*/NULL);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_17 = __this->get_mainCamera_4();
		float L_18 = __this->get_initFieldOfView_7();
		int32_t L_19 = __this->get_cameraRCount_15();
		float L_20 = __this->get_step_16();
		NullCheck(L_17);
		Camera_set_fieldOfView_m5006BA0D01A27619A053704D3BD6A8938F7DEDA5(L_17, ((float)il2cpp_codegen_add((float)L_18, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_19))), (float)L_20)))), /*hidden argument*/NULL);
		int32_t L_21 = __this->get_cameraRCount_15();
		__this->set_cameraRCount_15(((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1)));
	}

IL_00a8:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Void Zoom::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Zoom__ctor_m48EC87C1D84179E33EFD99EA35B82C25F3574238 (Zoom_t880AFB749C3A75A781108A6A230D45E6B07952F0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zoom__ctor_m48EC87C1D84179E33EFD99EA35B82C25F3574238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_initFieldOfView_7((55.0f));
		__this->set_targetFieldOfView_8((20.0f));
		__this->set_nStep_9(((int32_t)20));
		__this->set_waitForSeconds_10((1.5f));
		__this->set_trigger_13(_stringLiteral395DF8F7C51F007019CB30201C49E884B46B92FA);
		__this->set_cameraNR_14(((int32_t)20));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void fadeIn::Start()
extern "C" IL2CPP_METHOD_ATTR void fadeIn_Start_m56A19FCF0E6023205D065573956BC1DFCF0276AE (fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fadeIn_Start_m56A19FCF0E6023205D065573956BC1DFCF0276AE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_0 = Component_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m642665A17D3104F99F2305FA4059BEE638357ED1(__this, /*hidden argument*/Component_GetComponent_TisRawImage_t68991514DB8F48442D614E7904A298C936B3C7C8_m642665A17D3104F99F2305FA4059BEE638357ED1_RuntimeMethod_var);
		__this->set_image_5(L_0);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_1 = __this->get_image_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		String_t* L_3 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(__this, /*hidden argument*/NULL);
		String_t* L_4 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral89B1C6DD498B0F6FADC72C677B2AF05ED7CAA7B5, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_4, /*hidden argument*/NULL);
	}

IL_002f:
	{
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_5 = __this->get_image_5();
		NullCheck(L_5);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_6 = VirtFuncInvoker0< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_5);
		V_0 = L_6;
		(&V_0)->set_a_3((0.0f));
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_7 = __this->get_image_5();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_8 = V_0;
		NullCheck(L_7);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_8);
		return;
	}
}
// System.Void fadeIn::Update()
extern "C" IL2CPP_METHOD_ATTR void fadeIn_Update_m281E8EAF6D910F96634B5F9543A042ADF5BFDB13 (fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fadeIn_Update_m281E8EAF6D910F96634B5F9543A042ADF5BFDB13_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_0 = __this->get_image_5();
		NullCheck(L_0);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_1 = VirtFuncInvoker0< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		V_0 = L_1;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = V_0;
		float L_3 = L_2.get_a_3();
		float L_4 = __this->get_targetAlpha_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_5 = fabsf(((float)il2cpp_codegen_subtract((float)L_3, (float)L_4)));
		if ((!(((float)L_5) > ((float)(0.0001f)))))
		{
			goto IL_0055;
		}
	}
	{
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_6 = V_0;
		float L_7 = L_6.get_a_3();
		float L_8 = __this->get_targetAlpha_6();
		float L_9 = __this->get_FadeRate_4();
		float L_10 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Lerp_m9A74C5A0C37D0CDF45EE66E7774D12A9B93B1364(L_7, L_8, ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), /*hidden argument*/NULL);
		(&V_0)->set_a_3(L_11);
		RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * L_12 = __this->get_image_5();
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_13 = V_0;
		NullCheck(L_12);
		VirtActionInvoker1< Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_12, L_13);
	}

IL_0055:
	{
		return;
	}
}
// System.Void fadeIn::FadeOut()
extern "C" IL2CPP_METHOD_ATTR void fadeIn_FadeOut_m3857D92FC7663A4C4020A228D061250FBC5A0BCA (fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425 * __this, const RuntimeMethod* method)
{
	{
		__this->set_targetAlpha_6((0.0f));
		return;
	}
}
// System.Void fadeIn::FadeIn()
extern "C" IL2CPP_METHOD_ATTR void fadeIn_FadeIn_m95928EA49E8E5A289BE7ED340A91709D159F7553 (fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425 * __this, const RuntimeMethod* method)
{
	{
		__this->set_targetAlpha_6((1.0f));
		return;
	}
}
// System.Void fadeIn::.ctor()
extern "C" IL2CPP_METHOD_ATTR void fadeIn__ctor_mCB0A99CB8CF7354AAC860EEFBCD012803B0061F3 (fadeIn_t1AE46C152FC433A43DEA5A0EF1393D741794A425 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
