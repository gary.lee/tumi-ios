﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.IO.StringReader
struct StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.Logger
struct Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSON_TB4C7A53B66AB0AFB87D47818778AF9AB10586B7B_H
#define JSON_TB4C7A53B66AB0AFB87D47818778AF9AB10586B7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json
struct  Json_tB4C7A53B66AB0AFB87D47818778AF9AB10586B7B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_TB4C7A53B66AB0AFB87D47818778AF9AB10586B7B_H
#ifndef PARSER_T3B57A69142237AEB04F76D09DD2581735AE34413_H
#define PARSER_T3B57A69142237AEB04F76D09DD2581735AE34413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json_Parser
struct  Parser_t3B57A69142237AEB04F76D09DD2581735AE34413  : public RuntimeObject
{
public:
	// System.IO.StringReader MiniJSON.Json_Parser::json
	StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_t3B57A69142237AEB04F76D09DD2581735AE34413, ___json_1)); }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * get_json_1() const { return ___json_1; }
	inline StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3095DEB3D26F40D1A7F9B76835D80AFE70E47E12 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T3B57A69142237AEB04F76D09DD2581735AE34413_H
#ifndef SERIALIZER_TBBDA1DA8B8B2CC192031334142175F9F822049F4_H
#define SERIALIZER_TBBDA1DA8B8B2CC192031334142175F9F822049F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json_Serializer
struct  Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4  : public RuntimeObject
{
public:
	// System.Text.StringBuilder MiniJSON.Json_Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_TBBDA1DA8B8B2CC192031334142175F9F822049F4_H
#ifndef SVFPLAYBACKSTATEHELPER_T38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE_H
#define SVFPLAYBACKSTATEHELPER_T38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFPlaybackStateHelper
struct  SVFPlaybackStateHelper_t38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFPLAYBACKSTATEHELPER_T38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE_H
#ifndef SVFREADERSTATEHELPER_TF26BD4D553776D5DB620A2325BF3625E57D30684_H
#define SVFREADERSTATEHELPER_TF26BD4D553776D5DB620A2325BF3625E57D30684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFReaderStateHelper
struct  SVFReaderStateHelper_tF26BD4D553776D5DB620A2325BF3625E57D30684  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFREADERSTATEHELPER_TF26BD4D553776D5DB620A2325BF3625E57D30684_H
#ifndef SVFUNITYPLUGININTEROP_T0499F937D90ED0EB487AFB71739A0D17BA0717F8_H
#define SVFUNITYPLUGININTEROP_T0499F937D90ED0EB487AFB71739A0D17BA0717F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SVFUnityPluginInterop
struct  SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8  : public RuntimeObject
{
public:
	// UnityEngine.Logger SVFUnityPluginInterop::logger
	Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * ___logger_2;
	// System.Int32 SVFUnityPluginInterop::instanceId
	int32_t ___instanceId_4;
	// System.Boolean SVFUnityPluginInterop::isInstanceDisposed
	bool ___isInstanceDisposed_5;

public:
	inline static int32_t get_offset_of_logger_2() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8, ___logger_2)); }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * get_logger_2() const { return ___logger_2; }
	inline Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F ** get_address_of_logger_2() { return &___logger_2; }
	inline void set_logger_2(Logger_tEB67FD7450076B84B97F22DBE8B2911FC4FBC35F * value)
	{
		___logger_2 = value;
		Il2CppCodeGenWriteBarrier((&___logger_2), value);
	}

	inline static int32_t get_offset_of_instanceId_4() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8, ___instanceId_4)); }
	inline int32_t get_instanceId_4() const { return ___instanceId_4; }
	inline int32_t* get_address_of_instanceId_4() { return &___instanceId_4; }
	inline void set_instanceId_4(int32_t value)
	{
		___instanceId_4 = value;
	}

	inline static int32_t get_offset_of_isInstanceDisposed_5() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8, ___isInstanceDisposed_5)); }
	inline bool get_isInstanceDisposed_5() const { return ___isInstanceDisposed_5; }
	inline bool* get_address_of_isInstanceDisposed_5() { return &___isInstanceDisposed_5; }
	inline void set_isInstanceDisposed_5(bool value)
	{
		___isInstanceDisposed_5 = value;
	}
};

struct SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields
{
public:
	// System.Boolean SVFUnityPluginInterop::s_logstack
	bool ___s_logstack_1;

public:
	inline static int32_t get_offset_of_s_logstack_1() { return static_cast<int32_t>(offsetof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields, ___s_logstack_1)); }
	inline bool get_s_logstack_1() const { return ___s_logstack_1; }
	inline bool* get_address_of_s_logstack_1() { return &___s_logstack_1; }
	inline void set_s_logstack_1(bool value)
	{
		___s_logstack_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SVFUNITYPLUGININTEROP_T0499F937D90ED0EB487AFB71739A0D17BA0717F8_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef TOKEN_T53209D188AFD8042884B6D26CC7E3146600F5182_H
#define TOKEN_T53209D188AFD8042884B6D26CC7E3146600F5182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.Json_Parser_TOKEN
struct  TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182 
{
public:
	// System.Int32 MiniJSON.Json_Parser_TOKEN::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T53209D188AFD8042884B6D26CC7E3146600F5182_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (SVFPlaybackStateHelper_t38FB181A4A2C8E48930305CA86FC64E2DF8ED3EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (SVFReaderStateHelper_tF26BD4D553776D5DB620A2325BF3625E57D30684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8), -1, sizeof(SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2802[6] = 
{
	0,
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8_StaticFields::get_offset_of_s_logstack_1(),
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8::get_offset_of_logger_2(),
	0,
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8::get_offset_of_instanceId_4(),
	SVFUnityPluginInterop_t0499F937D90ED0EB487AFB71739A0D17BA0717F8::get_offset_of_isInstanceDisposed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (Json_tB4C7A53B66AB0AFB87D47818778AF9AB10586B7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (Parser_t3B57A69142237AEB04F76D09DD2581735AE34413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[2] = 
{
	0,
	Parser_t3B57A69142237AEB04F76D09DD2581735AE34413::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2805[13] = 
{
	TOKEN_t53209D188AFD8042884B6D26CC7E3146600F5182::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[1] = 
{
	Serializer_tBBDA1DA8B8B2CC192031334142175F9F822049F4::get_offset_of_builder_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
