/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

/// `HaircutManager` is used to make all operations with avatar haircuts.
public class HaircutManager {
    static var haircutModels = [String: HaircutModel]()
    static var loaderDict = [String: HaircutLoader]()
//
//    public class func with(preloadedDict: [String: BaseHaircutModel]) {
//        baseHaircuts = preloadedDict
//    }
//
//    public class func preloadHaircuts() {
//        let fileManager = FileManager.default
//        do {
//            let baseHaircutList = try fileManager.contentsOfDirectory(at: kHaircutsFolder(), includingPropertiesForKeys: nil, options: [])
//            for folderName in baseHaircutList {
//                var isDirectory: ObjCBool = false
//                fileManager.fileExists(atPath: folderName.path, isDirectory: &isDirectory)
//                if !isDirectory.boolValue {
//                    let baseHaircut = BaseHaircutModel(identity: folderName.lastPathComponent)
//                    if baseHaircut.isDownloaded() {
//                        baseHaircuts[baseHaircut.identity] = baseHaircut
//                    }
//                }
//            }
//        } catch {
//            print("TODO: initWithDisc error");
//        }
//    }

    /// Is used to download the haircut files from the server.
    /// At the first time the full set of files will be downloaded for each haircut type. Further only neсessary set of files will be downloaded.
    ///
    /// - parameter haircutModel: The haircut model for which files should be downloaded.
    ///
    /// - returns: Task handler object which can be used for subscription on progress and completion callbacks.
    public class func downloadHaircut(haircutModel:HaircutModel) -> TaskHandler<HaircutModel> {
        if let loader = loaderDict[haircutModel.identity] {
            return loader.tryDownloadModel()
        }

        let loader = HaircutLoader(haircutModel: haircutModel)
        loaderDict[haircutModel.identity] = loader
        return loader.tryDownloadModel()

    }
    
    /// Is used to retrieve the list of haircut models from the server.
    ///
    /// - parameter avatarModel: The avatar model for which the list of haircuts should be downloaded.
    /// - parameter completion: Closure to be executed once the request has finished.
    public class func getHaircuts(forAvatar avatarModel: AvatarModel, completion: @escaping CompletionHandler<[HaircutModel]>) {
        itSeez3DAvatarSDK.getHaircuts(forAvatarCode: avatarModel.responseAvatarModel.code, completion: { (result) in
            switch(result) {
            case .success(let value):
                let haircutModels = value.map({ (responseModel) -> HaircutModel in self.buildHaircut(fromResponse: responseModel)
                })
                completion(.success(haircutModels))

            case .failure(let error):
                completion(.failure(error))
            }
        })
    }

    /// Is used to build `HaircutModel` from the server response.
    ///
    /// - parameter response: Information about haircut received from server.
    ///
    /// - returns: The haircut model.
    public class func buildHaircut(fromResponse response:ResponseAvatarHaircutModel) -> HaircutModel {
        let haircutIdentity = HaircutModel.generateModelIdentity(haircut: response)
        if let haircutModel = haircutModels[haircutIdentity] {
            return haircutModel
        }
        let haircutModel = HaircutModel(haircut: response)
        haircutModels[haircutIdentity] = haircutModel
        return haircutModel
    }
}
