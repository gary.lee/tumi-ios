/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

let kZipFileName:String = "mesh.zip"
let kMeshFileName:String = "model.ply"
let kTextureFileName:String = "texture.jpg"
let kPreviewFileName:String = "preview.png"
let kPhotoFileName:String = "photo.jpg"
let requestFrequency = 5.0

import Alamofire

typealias Folder = String

public extension Folder {
    func generateURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL.appendingPathComponent(modelFolder).appendingPathComponent(self)

    }

    func generateURL(to: String) -> URL {
        return self.generateURL().appendingPathComponent(to)
    }
}

/// `AvatarModel` contains base information about the avatar model.
public class AvatarModel {
    var folder: Folder
    fileprivate var zipFileURL     : URL
    /// Can be used to present the current model on the screen.
    public var plyItem             : PlyItem
    /// File URL to preview image of this avatar.
    public var previewFileURL      : URL
    /// Information about avatar received from server.
    public var responseAvatarModel : ResponseAvatarModel

    internal init(responseAvatarModel: ResponseAvatarModel) {
        self.folder = responseAvatarModel.code
        self.responseAvatarModel = responseAvatarModel
        self.zipFileURL = self.folder.generateURL(to: kZipFileName)

        self.plyItem = PlyItem(meshFileURL: self.folder.generateURL(to: kMeshFileName), textureFileURL: self.folder.generateURL(to: kTextureFileName))

        self.previewFileURL = self.folder.generateURL(to: kPreviewFileName)
    }

    internal func download() -> DownloadManager.DownloadManagerResponseObject {
        let downloadRequestObject = DownloadManager.DownloadManagerRequestObject(identity: responseAvatarModel.code)

        if let zipURL = responseAvatarModel.meshURL {
            downloadRequestObject.downloadingList.append((link: zipURL, fileURL: zipFileURL))
        }
        if let textureURL = responseAvatarModel.textureURL {
            downloadRequestObject.downloadingList.append((link: textureURL, fileURL: plyItem.textureFileURL))
        }
//        if let previewURL = responseAvatarModel.previewURL {
//            downloadRequestObject.downloadingList.append((link: previewURL, fileURL: previewFileURL))
//        }

        return DownloadManager.startDownload(requestObject: downloadRequestObject)
    }
    
    /// Is used to determine whether ply item and preview were downloaded or not.
    public func isDownloaded() -> Bool {
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: self.plyItem.meshFileURL.path) && fileManager.fileExists(atPath: self.plyItem.textureFileURL.path)
    }
}

internal class AvatarSender: TaskHandler<AvatarModel> {

    internal func sendImage(image: UIImage, name: String, description: String) -> Self {
        itSeez3DAvatarSDK.sendImage(image: image, name: name, description: description, uploadingProgress: { (progress) in
            self.updateProgress(state: "Sending", percent: (Int)(progress * 100.0))
        }) { (result) in
            switch (result) {
            case .success(let value):
                self.taskComplete(withResult: Result.success(AvatarModel(responseAvatarModel: value)))
            case .failure(let error):
                self.taskComplete(withResult: Result.failure(IAError.sendingFailed(error: error)))
            }
        }

        return self
    }

}

internal class AvatarLoader: TaskHandler<AvatarModel> {

    var avatarModel : AvatarModel

    internal init(avatar: AvatarModel) {
        self.avatarModel = avatar
    }

    @objc private func retriveStatus() {
        let successHandler: (ResponseAvatarModel) -> Void = { [unowned self] model in
            self.avatarModel.responseAvatarModel = model
            self.updateProgress(state: model.status ?? "None", percent: model.progress ?? 0)

            if model.computed() {
                self.downloadModel()
            } else if model.failed() {
                self.taskComplete(withResult: Result.success(self.avatarModel))
            } else {
                Timer.scheduledTimer(timeInterval: requestFrequency, target: self, selector: #selector(self.retriveStatus), userInfo: nil, repeats: false)
            }
        }

        itSeez3DAvatarSDK.getAvatar(avatarCode: avatarModel.responseAvatarModel.code, completion: { [unowned self] (result) in
            switch (result) {
            case .success(let value):
                successHandler(value)
            case .failure(let error):
                self.taskComplete(withResult: Result.failure(IAError.downloadingModelFailed(reason: .retriveStatusFailed(error: error))))
            }
        })
    }

    @discardableResult
    func tryDownloadModel() -> Self {
        if !inProgress {
            inProgress = true
            retriveStatus()
        }
        return self
    }

    private func downloadModel() {
        if avatarModel.isDownloaded() {
            self.taskComplete(withResult: Result.success(avatarModel))
            return
        }

        self.avatarModel.download()
            .downloadingProgress(handler: { [unowned self] (progress) in
                self.updateProgress(state: "Downloading", percent: progress)
            })
            .completion { [unowned self] (result) in
                switch (result) {
                case .success:
                    self.taskComplete(withResult: Result.success(self.avatarModel))
                case .failure(let error):
                    self.taskComplete(withResult: Result.failure(IAError.downloadingModelFailed(reason: .downloadingFailed(error: error))))
                }
            }
    }
}
