/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#ifndef __SAVEMODEL3D_HPP__
#define __SAVEMODEL3D_HPP__

#include "types.hpp"
#include <vector>

namespace pointcloudMerge
{

    void saveModelToBinPLY(const std::string &filename,
                           const std::vector<Point3f> *vertices = 0,
                           const std::vector<Point3f> *verticesNormals = 0,
                           const std::vector<std::vector<size_t> > *faces = 0,
                           const std::vector<std::vector<Point2d> > *uvMapping = 0,
                           const std::string &textureFilename = "",
                           const std::vector<int> *facesFlags = 0,
                           const std::vector<std::string> *comments = 0,
                           const PhysicalUnits physicalUnits = M,
                           const bool needEncryption = false,
                           //                       /const std::vector<Vec4b> *verticesColors = 0,
                           const std::vector<float> *verticesWeights = 0);
}

#endif //__SAVEMODEL3D_HPP__
