/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#ifndef __LOADMODEL3D_HPP__
#define __LOADMODEL3D_HPP__

#include "types.hpp"
#include <vector>

namespace pointcloudMerge
{

    void loadModelFromBinPLY(const std::string &filename,
                             std::vector<Point3f> *vertices = 0,
                             std::vector<Point3f> *verticesNormals = 0,
                             std::vector<std::vector<size_t> > *faces = 0,
                             std::vector<std::vector<Point2d> > *uvMapping = 0,
                             std::string *textureFilename = 0,
                             PhysicalUnits *physicalUnits = 0,
                             std::vector<float> *verticesWeights = 0);

    void loadModelFromBinPLY(std::istream &inputMesh,
                             std::vector<Point3f> *vertices = 0,
                             std::vector<Point3f> *verticesNormals = 0,
                             std::vector<std::vector<size_t> > *faces = 0,
                             std::vector<std::vector<Point2d> > *uvMapping = 0,
                             std::string *textureBasename = 0,
                             PhysicalUnits *physicalUnits = 0,
                             std::vector<float> *verticesWeights = 0);
}

#endif //__LOADMODEL3D_HPP__
