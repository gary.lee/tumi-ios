/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

#ifndef TYPES_HPP_
#define TYPES_HPP_

#include <string>

enum PhysicalUnits
{
    PHYSICAL_UNITS_UNKNOWN,
    M,
    MM,
    CM,
    IN,

    PHYSICAL_UNITS_COUNT
};

const std::string PhysicalUnitsName [] = {"unknown",
                                          "meters",
                                          "millimeters",
                                          "centimetres",
                                          "inches"};
std::string physicalUnits2string(PhysicalUnits physicalUnits);
PhysicalUnits string2physicalUnits(const std::string &str);

class Point3f {
public:
    float x, y, z;
    Point3f(){};
    Point3f(float _x, float _y, float _z): x(_x), y(_y), z(_z){};
};

class Point2d {
public:
    double x, y;
    Point2d(){};
    Point2d(double _x, double _y): x(_x), y(_y){};
};



#endif /* _TYPES_HPP_ */
