/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import UIKit
import Zip
import Alamofire

internal class DownloadManager {

    public enum DownloadState {
        case failed(Error)
        case none
        case downloaded
    }

    internal typealias DownloadingProgressHandler = (Int) -> Void

    internal class DownloadManagerRequestObject {
        var identity: String
        var downloadingList = [(link: URL, fileURL: URL)]()

        init(identity: String) {
            self.identity = identity
        }
    }

    public class DownloadManagerResponseObject {
        public var requestObject       : DownloadManagerRequestObject
        var downloadDict               = [URL: (progress: Int, state: DownloadState)]()
        var downloadingProgressHandler :DownloadingProgressHandler?
        var completionHandler          :CompletionHandler<String>?

        internal init(requestObject: DownloadManagerRequestObject) {
            self.requestObject = requestObject
        }

        @discardableResult
        internal func downloadingProgress(handler: @escaping DownloadingProgressHandler) -> Self {
            downloadingProgressHandler = handler
            return self
        }

        @discardableResult
        internal func completion(handler: @escaping CompletionHandler<String>) -> Self {
            completionHandler = handler
            if isSuccessfullyFinished() {
                completionHandler?(.success(self.requestObject.identity))
            }

            return self
        }

        internal func updateProgress(progress: Int) {
            downloadingProgressHandler?(progress)
        }

        internal func updateDownloadingProgress(fileURL:URL, progress:Int) {
            if let downloadingStatus = downloadDict[fileURL] {
                self.downloadDict[fileURL] = (progress: progress, state:downloadingStatus.state)
            } else {
                self.downloadDict[fileURL] = (progress: progress, state:.none)
            }
            let dProgress = self.downloadDict.values.reduce(0, {$0 + $1.progress})

            self.updateProgress(progress: dProgress / downloadDict.values.count)
        }


        private func fileDownloaded(fileURL: URL) {

            if (fileURL.pathExtension == "zip") {
                do {
                    try Zip.unzipFile(fileURL, destination: fileURL.deletingLastPathComponent(), overwrite: true, password: nil, progress: { (progress) in
                    })
                    self.downloadDict[fileURL] = (progress: 100, state:.downloaded)
                    self.checkForFinished()
                }
                catch let error {
                    fileFailed(fileURL: fileURL, error: IAError.DownloadingError.unzipError(error: error))
                }
            } else {
                self.downloadDict[fileURL] = (progress: 100, state:.downloaded)
                self.checkForFinished()
            }
        }

        private func fileFailed(fileURL: URL, error: Error) {
            self.downloadDict[fileURL] = (progress: 100, state:.failed(error))

            self.checkForFinished()
        }

        func isFinished() -> (finished: Bool, error: Error?) {
            var count = 0
            var someError: Error?
            for downloadingObject in self.downloadDict.values
            {
                switch downloadingObject.state {
                case .downloaded:
                    count += 1
                case .failed(let error):
                    count += 1
                    someError = error
                default: break
                }
            }

            let isFinished = count == self.downloadDict.count
            return (finished: isFinished, error: someError)
        }

        internal func isSuccessfullyFinished() -> Bool {
            let task = isFinished()
            return task.finished && task.error == nil
        }

        internal func isUnsuccessfullyFinished() -> Bool {
            let task = isFinished()
            return task.finished && task.error != nil
        }

        private func checkForFinished() {
            let task = isFinished()
            if (task.finished) {
                if let error = task.error {
                    completionHandler?(Result.failure(error))
                } else {
                    completionHandler?(Result.success(self.requestObject.identity))
                }
            }
        }

        internal func startDownloading() {

            for (link, localFileURL) in requestObject.downloadingList {
                self.downloadDict[localFileURL] = (progress: 0, state:.none)
                itSeez3DAvatarSDK.downloadFile(link: link, to: localFileURL, downloadingProgress: { [unowned self] (progress, fileURL) in
                    self.updateDownloadingProgress(fileURL: fileURL, progress: (Int)(progress * 100.0))
                }) { [unowned self] (result) in
                    switch (result) {
                    case .success(let value):
                        self.fileDownloaded(fileURL: value)
                    case .failure(let error):
                        self.fileFailed(fileURL: localFileURL, error: IAError.DownloadingError.networkError(error: error))
                    }
                }
            }

        }

    }

    static var downloadDict = [String : DownloadManagerResponseObject]()

    internal class func startDownload(requestObject: DownloadManagerRequestObject) -> DownloadManagerResponseObject {
        if let responseObject = downloadDict[requestObject.identity] {

            guard responseObject.isUnsuccessfullyFinished() else {
                return responseObject
            }
        }

        let responseObject = DownloadManagerResponseObject(requestObject: requestObject)
        downloadDict[requestObject.identity] = responseObject
        responseObject.startDownloading()
        return responseObject
    }

}
