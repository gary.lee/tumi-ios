//
//  OAuthSwiftRequestAdapter.swift
//  OAuthSwift-Alamofire
//
//  Created by phimage on 05/10/16.
//  Copyright © 2016 phimage. All rights reserved.
//

import Foundation
import Alamofire
import OAuthSwift

// Add authentification headers from OAuthSwift to Alamofire request
internal class OAuthSwiftRequestAdapter: RequestAdapter {

    fileprivate let oauthSwift: OAuthSwift
    private var paramsLocation: OAuthSwiftHTTPRequest.ParamsLocation = .authorizationHeader
    private var dataEncoding: String.Encoding = .utf8

    public init(_ oauthSwift: OAuthSwift) {
        self.oauthSwift = oauthSwift
    }

    internal func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var config = OAuthSwiftHTTPRequest.Config(
            urlRequest: urlRequest,
            paramsLocation: paramsLocation,
            dataEncoding: dataEncoding
        )
        config.updateRequest(credential: oauthSwift.client.credential)
        config.urlRequest.setValue(Storage.user?.code, forHTTPHeaderField: "X-PlayerUID")

        return try OAuthSwiftHTTPRequest.makeRequest(config: config)
    }

}

internal class OAuthSwift2RequestAdapter: OAuthSwiftRequestAdapter, RequestRetrier {

    public init(_ oauthSwift: OAuth2Swift) {
        super.init(oauthSwift)
    }

    fileprivate var oauth2Swift: OAuth2Swift { return oauthSwift as! OAuth2Swift }

    fileprivate let lock = NSLock()
    fileprivate var isRefreshing = false
    fileprivate var requestsToRetry: [RequestRetryCompletion] = []

    internal func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }

        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)

            if !isRefreshing {
                refreshTokens { [weak self] succeeded in
                    guard let strongSelf = self else { return }

                    strongSelf.lock.lock() ; defer { strongSelf.lock.unlock() }

                    strongSelf.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    strongSelf.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }

    fileprivate typealias RefreshCompletion = (_ succeeded: Bool) -> Void

    fileprivate func refreshTokens(_ completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }

        isRefreshing = true

        oauth2Swift.authorize(deviceToken: "", grantType: kGrantType,
            success: { [weak self] (credential) in
                Storage.oauthCredential = credential
                guard let strongSelf = self else { return }
                completion(true)
                strongSelf.isRefreshing = false
            },
            failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                completion(false)
                strongSelf.isRefreshing = false
            }
        )
    }

}

