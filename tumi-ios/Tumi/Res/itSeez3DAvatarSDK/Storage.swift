/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
 * You may not use this file except in compliance with an authorized license
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
 * See the License for the specific language governing permissions and limitations under the License.
 * Written by Itseez3D, Inc. <support@itseez3D.com>, May 2017
 */

import UIKit
import OAuthSwift

let kCredentialUserDefaultsKey: String = "itseez3d_client_auth_credential"
let kUserDefaultsKey: String = "itseez3d_user_id"

internal class Storage {
    static var oauthCredential: OAuthSwiftCredential? {
        set {
            if let value = newValue {
                let encodedCredential = NSKeyedArchiver.archivedData(withRootObject: value)
                UserDefaults.standard.setValue(encodedCredential, forKey: kCredentialUserDefaultsKey)
            }
        } get {
            var credential: OAuthSwiftCredential? = nil
            let encodedCredential = UserDefaults.standard.value(forKey: kCredentialUserDefaultsKey) as? Data
            if let encodedCredential = encodedCredential {
               credential = NSKeyedUnarchiver.unarchiveObject(with: encodedCredential) as? OAuthSwiftCredential
            }
            return credential
        }
    }

    private static var _user: ResponseUserModel? = nil
    static var user: ResponseUserModel? {
        set {
            _user = newValue
            if let value = newValue {
                let encodedUser = NSKeyedArchiver.archivedData(withRootObject: value)
                UserDefaults.standard.setValue(encodedUser, forKey: kUserDefaultsKey)
            }
        } get {
            if let value = _user {
                return value
            }

            var user: ResponseUserModel? = nil
            let encodedUser = UserDefaults.standard.value(forKey: kUserDefaultsKey) as? Data
            if let encodedUser = encodedUser {
                _user = NSKeyedUnarchiver.unarchiveObject(with: encodedUser) as? ResponseUserModel
            }
            return _user
        }
    }
}
