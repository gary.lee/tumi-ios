//
//  AppDelegate.swift
//  Tumi
//
//  Created by Redspark on 03/04/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit
import FacebookCore
import UserNotifications

//, UNUserNotificationCenterDelegate

struct WeChat {
    static let appID = "wx6e113c7f8255ad0a"
    static let appSecret = "48ddd0a12a10f74f3da95aaa82d30f72"
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,WXApiDelegate {

    var window: UIWindow?
    
    var isupdateProfile = Bool()
    var isupdateNotification = Bool()
    
    var application: UIApplication?
    
    @objc var currentUnityController: UnityAppController!
    
    var isUnityRunning = false
    var unityEverStarted = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.application = application
        unity_init(CommandLine.argc, CommandLine.unsafeArgv)
        
        WXApi.registerApp(WeChat.appID)
        
        currentUnityController = UnityAppController()
        currentUnityController.application(application, didFinishLaunchingWithOptions: launchOptions)

        // Override point for customization after application launch.
        PushSetting()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        if UserDataHolder.sharedUser.isLoggedIn{
            
            let language = UserDataHolder.sharedUser.language
            
            if language == "en" || language == "English" || language == "english" {
                Bundle.setLanguage(lang: "en")
            }
            else if language == "raditional chinese"{
                Bundle.setLanguage(lang: "zh-Hans")
            }
            else if language == "chinese"{
                Bundle.setLanguage(lang: "zh-Hant")
            }
            
            
            Utility.sharedUtility.sideMenu()
        }
        
        // Pre-load Unity
        startUnity()
        stopUnity()
        return true
    }
    
    func PushSetting()  {
        
        UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
            if (granted)
            {
                DispatchQueue.main.async(execute: {() -> Void in
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        })
    }
    
    
    var orientationLock = UIInterfaceOrientationMask.portrait
    var myOrientation: UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("Got token data! (deviceToken)")
        
        let deviceTokenString : String = deviceToken.reduce("", {$0 + String(format: "%02X", $1)}) as String
        print(deviceTokenString)
        UserDataHolder.sharedUser.deviceToken = deviceTokenString
        
//        Constants.USERDEFAULTS.set(deviceTokenString, forKey:"deviceToken")
//        Constants.USERDEFAULTS.synchronize()
    }
        
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
        
    }
    private func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        print("Recived: \(userInfo)")
        //let dictNotification : NSDictionary = NSMutableDictionary (dictionary: userInfo)
    }
    
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//
//        print("User Info = ",notification.request.content.userInfo)
//        completionHandler([.alert, .badge, .sound])
//
//    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let dictNotification : NSDictionary = NSMutableDictionary (dictionary: response.notification.request.content.userInfo)
        print("User Info On Tap = ",dictNotification)
        completionHandler()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) {
            return true
        }
        
        return WXApi.handleOpen(url as URL, delegate: self)
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return WXApi.handleOpen(url, delegate: self)
    }
    
  

    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        if isUnityRunning {
            currentUnityController.applicationWillResignActive(application)
        }
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if isUnityRunning {
            currentUnityController.applicationDidEnterBackground(application)
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if isUnityRunning {
            currentUnityController.applicationWillEnterForeground(application)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if isUnityRunning {
            currentUnityController.applicationDidBecomeActive(application)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
//    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//        return self.orientationLock
//    }
    func startUnity() {
        if !isUnityRunning
        {
            NSLog("Start Unity...")
            isUnityRunning = true
            currentUnityController.applicationDidBecomeActive(application!)
        }
        
        if (unityEverStarted) {
            NSLog("Firing Unity Ready...")
            NotificationCenter.default.post(name: Notification.Name("UnityReady"), object:nil);
        }
        
        unityEverStarted = true
    }
    
    func stopUnity() {
        if isUnityRunning {
            NSLog("Stopping Unity...")
            currentUnityController.applicationWillResignActive(application!)
            isUnityRunning = false
        }
    }

}

