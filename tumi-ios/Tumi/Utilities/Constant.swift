//
//  Constant.swift
//  SwayamVar
//
//  Created by Dhruv Patel on 04/03/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import Foundation
import UIKit

let ProximaNova  = "ProximaNova-Regular"
let ProximaNovaSemibold = "ProximaNova-Semibold"
let ProximaNovaBold = "ProximaNova-Bold"
let ProximaNovaLight = "ProximaNova-Light"

var KuserDefault : UserDefaults =  UserDefaults.standard
let AppDelObj = UIApplication.shared.delegate as! AppDelegate

var Isloggin = "Isloggin"
var isLoggin : Bool =  false
var EmailID :String?
let storyBoard = UIStoryboard(name: "Main", bundle:Bundle.main)
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let device_type = "iOS"



//let baseURL = "http://redsparktech.a2hosted.com/tumi/api/web/tumi/users/"
let baseURL = "http://3.0.203.214/api/web/tumi/users/"

//let baseURL = "https://redsparktech.a2hosted.com/swayamvar/api/sync.php?action="

//API LIST

let LOGIN = "login"
let REGISTER = "signup"
let VERIFY_OTP = "verify-otp"
let SET_PASSWORD = "set-password"
let SET_PROFILE = "set-profile"
let GET_CATEGORIES = "get-categories"
let GET_VIDEOS = "get-videos"
let GET_IMAGES = "get-images"
let RESEND_OTP = "send-otp"
let GETPROFILE = "get-profile"
let GET_MEMBER_PROFILE = "get-member-details"
let RESET_PASSWORD = "reset-password"
let UPDATE_PROFILE = "update-profile"
let TERMS = "get-terms"
let FAQ = "faq"
let PRIVACY = "privacy"
let ONLINE_STORE = "online-stores"
let contactus = "contact"
let GET_MEMBERSHIP_DETAIL = "get-membership-details"
let SUB_CAT = "get-sub-categories"
let PRODUCT_LIST = "productlist"
let set_language = "set-language"
let get_language = "get-language"
let push_notification_list = "push-notification-list"
let get_online_store_id = "get-online-store-id"

let get_notification = "get-notification"
let set_notification = "set-notification"


let get_ARVideo = "get-video-image"

let SOCIAL_SIGNUP = "social-signup"
let SOCIAL_LOGIN = "social-login"

//let MATCHINGLIST = "matchinglist"
//let REQUESTFORMEETING = "requestformeeting"
//let MYREQUESTLIST = "myrequestlist"
//let SENDREQUEST = "MysendRequestList"
//let REQUESTAPPRRej = "requestApprRej"
//let MYSCHEDULE = "myschedule"
//let QRSCAN = "QrscanAttendance"
//let QRKITISSUE = "QrKitissue"
//let DELETEREQUEST = "deleteRequest"
//let LOGOUTAPI = "\(baseURL)logout"

//PARAMETER LIST




//Success code and other status code

let SUCCESS = 1
let statusRequested = "1"
let notstatusRequested = "0"




//Alert button title

let APPNAME = Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String
let OK = "Ok"
let LOGOUT = "Logout"
let CANCEL = "Cancel"
let CONTINUE = "Continue"
let BACK = "Back".localized()
let RESEND = "Re_send".localized()

let MEMBERSHIP_CONTENT = "membershipcontent"



//Common Messages

let INTERNETMSG = "There is no internet connection."
let BLANKFIELDMSG = "Please_fill_in".localized()
let INVALIDMSG = "Inavlid Mobile number or Password, Please try again."
let PASSWORDINCONSISTENTMSG = "Password_inconsisitent".localized()
let PASSWORDCHANGED = "Your password is changed."
let CONTACTUSTHANKYOU = "Thank you for your enquiry."
let CONTACTUSBLANKFIELD = "OOPS! Please fill in the required field."
let LOGOUTMSG = "Are you sure want to Logout?"
let BLANKMOBILENUMBER = "Please enter country code and mobile number"
let INVALIDCODE = "Invalid code."
let INVALIDEMAIL = "Email id Invalid"
