//
//  Utility.swift
//  SwayamVar
//
//  Created by Dhruv Patel on 25/02/19.
//  Copyright © 2019 Redspark. All rights reserved.
//

import Foundation
import UIKit
import LGSideMenuController
import ANLoader

class Utility{
    static let sharedUtility = Utility()
    //Set background image of view methods
    func backgroundImage(view: UIView,imageName:String) {
        
        let imageView = UIImageView(image: UIImage(named: imageName)!)
        imageView.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: UIScreen.main.bounds.size.width, height: view.frame.size.height)
        view.insertSubview(imageView, at: 0)
    }
    //Progress hud
    func ShowLoader(){
        ANLoader.showLoading("Loading", disableUI: true)
        ANLoader.activityColor = .darkGray
        ANLoader.activityBackgroundColor = .white
        ANLoader.activityTextColor = .black
    }
    
    func HideLoader(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
            ANLoader.hide()
        })
        
    }
    
    func showMessage(_ message: String){
        SKToast.show(withMessage: message)
        SKToast.backgroundStyle(.dark)
    }
    
    //Side menu
    func sideMenu(){
        
        let objHomeVc = storyBoard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
        let objSideMenuVc = storyBoard.instantiateViewController(withIdentifier: "SideMenuView") as? SideMenuView
        let navigationController = UINavigationController(rootViewController: objHomeVc!)
        let sideMenuController = LGSideMenuController(rootViewController: navigationController,
                                                      leftViewController: objSideMenuVc,
                                                      rightViewController: nil)
        sideMenuController.leftViewWidth = (objHomeVc?.view.frame.width)! - 100//250.0;
        sideMenuController.leftViewPresentationStyle = .slideAbove
        sideMenuController.isLeftViewSwipeGestureEnabled = false;
        
        if let window = appDelegate.window {
            window.rootViewController = sideMenuController
        }
        appDelegate.window?.makeKeyAndVisible()
        
        
    }
    func ShowLoginVC(){
         UserDataHolder.sharedUser.clearUserdefaults()
        let objLoginVc = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        let nav = UINavigationController(rootViewController: objLoginVc!)
        
        if let window = appDelegate.window {
            window.rootViewController = nav
        }
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func ShowLanguageVC(){
        UserDataHolder.sharedUser.clearUserdefaults()
        let objLoginVc = storyBoard.instantiateViewController(withIdentifier: "LanguageVC") as? LanguageVC
        let nav = UINavigationController(rootViewController: objLoginVc!)
        nav.navigationBar.isHidden = true
        if let window = appDelegate.window {
            window.rootViewController = nav
        }
        appDelegate.window?.makeKeyAndVisible()
    }
    

    
}
//Email validation extension

extension String {
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    var isValidPassword: Bool {
        
//        let passwordRegex = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"

       // let passwordRegex = "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*\\d)[A-Za-z\\d]{6,16}$"
        let passwordRegex = "^(?=.*[A-Za-z])(?=.*[0-9]).{5,16}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
}

// White space trim extension

extension String {
    var trim:String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}

//Alert view controller extension

extension  UIViewController {
    
    func showAlert(withMessage message:String) {
        
        let alert = UIAlertController(title: APPNAME, message: message.localized(), preferredStyle: .alert)
        let ok = UIAlertAction(title: OK.localized(), style: .default, handler: { action in
        })
        
        alert.addAction(ok)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}
extension UITextField {
    
    func setPadding(left: CGFloat? = nil, right: CGFloat? = nil){
        if let left = left {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: left, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
        
        if let right = right {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: right, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}
