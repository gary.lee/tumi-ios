//
//  PlaceMark.swift
//  OrdaChef
//
//  Created by Dhruv Patel on 12/07/18.
//  Copyright © 2018 Redspark. All rights reserved.
//

import UIKit
import MapKit

class PlaceMarkLocation: NSObject, MKAnnotation{
    var identifier = "placemark"
    var title: String?
    var subtitle: String?
    var index = Int()
    var coordinate: CLLocationCoordinate2D
    init(name:String, subName:String, lat:CLLocationDegrees,long:CLLocationDegrees,index:Int){
        title = name
        subtitle = subName
        self.index = index
        coordinate = CLLocationCoordinate2DMake(lat, long)
    }
}


class PlaceMark: NSObject {
    var current = [PlaceMarkLocation]()
    override init(){
    }
    
    func loadAndPrepareData(strName : String, strSubName : String, strLat : CLLocationDegrees, strLong : CLLocationDegrees, index : Int) -> Any{
        var places = [PlaceMarkLocation]()
        places += [PlaceMarkLocation(name:strName, subName:strSubName, lat:strLat, long:strLong, index: index)]
        return places
    }
}
